-------------------------------------Variables-------------------------------------------------
local sW,sH = guiGetScreenSize()
local browser = guiCreateBrowser(0.0075, 0.33, 0.28, 0.030, true, true, true)
local ChatUI = guiGetBrowser( browser )
local _openChat = false
local ChatBox = {}
local ChatHistory = {}
local ShowingChat = true;
local ChatLines = 15
local ChatFont = dxCreateFont(":Fonts/RMedium.ttf", 11)
local CurrentChatHistory
local CurrentChatHistoryText = nil

addEventHandler ("onClientResourceStart", getResourceRootElement(getThisResource()), 
    function()
        for i = 1, ChatLines do SendClientMessage("",255,0,255)end
		addEventHandler("onClientRender",root,DrawingChatBox)
    end)

addEventHandler( "onClientResourceStop", getResourceRootElement(getThisResource()),
    function ( stoppedRes )
        if stoppedRes == getThisResource() then
			destroyElement(browser)
			removeEventHandler("onClientRender",root, DrawingChatBox)
		end
    end
);
----------------------------------------------Панель ввода текста-----------------------------


addEventHandler( "onClientBrowserCreated", ChatUI, 
	function( )
		loadBrowserURL(ChatUI, "http://mta/local/ChatUI.html")
		guiSetVisible (browser, false)
		bindKey("F6", "down", playerOpenChat)
	end
)

function playerOpenChat()
	if _openChat then ShowChat(false)
	else ShowChat(true) end
end
--[[
Функция SendMessageToServer получает текст с EditBox чата и отправляет его на обработку серверной части
]]
function SendMessageToServer(text)
	ShowChat(false)
	text = string.gsub (text, "(%s)+","")
	if string.len(text) < 1 then return cancelEvent() end
	table.insert(ChatHistory, text)
	if #ChatHistory > ChatLines then
		table.remove(ChatHistory, 1)
	end
	if string.sub(text,1,1) == "/" then 
		triggerServerEvent("CommmandHandler", localPlayer, localPlayer, text)
		return cancelEvent()
	end
	triggerServerEvent("PlayerChat",localPlayer,localPlayer,text)
	--[[if not getPedAnimation(localPlayer) then
		setPedAnimation(localPlayer, "PED", "IDLE_CHAT", 0, false)
	end	]]
	CurrentChatHistory = 0
end
addEvent("SendMessageToServer", true)
addEventHandler("SendMessageToServer", resourceRoot, SendMessageToServer)

function ShowChat(bool)
	_openChat = bool
	if bool then
		guiSetVisible(browser, true);
		focusBrowser(ChatUI);
		showCursor(true);
		addEventHandler("onClientKey",root, ChatKeyPress)
	else
		guiSetVisible(browser, false);
		showCursor(false);
		removeEventHandler("onClientKey",root, ChatKeyPress)
	end
	CurrentChatHistory = 0
	CurrentChatHistoryText = nil
end

function ChatKeyPress(key,press)
	if press then
		if key == "F6" then
			ShowChat(false)
		elseif key == "arrow_u" then
			ChatHistoryF(true)
		elseif key == "arrow_d" then
			ChatHistoryF(false)
		end	
	end	
	cancelEvent()
end

---------------------------------------Бокс чата-----------------------------------------
--[[
Функция SendClientMessage получает текст с серверной части и отправляет его на отрисовку
]]
function SendClientMessage(text)
	table.insert(ChatBox, text);
	if #ChatBox > ChatLines then
		table.remove(ChatBox, 1);
	end
end
addEvent("SendClientMessage", true)
addEventHandler("SendClientMessage", localPlayer, SendClientMessage)

function DrawingChatBox()
	for i = 1, ChatLines do
		sY = i * 20;
		dxDrawBorderedText(ChatBox[i], sW/2-sW/2+10,20+sY, 150, 30, tocolor(255,255,255,255), 1, 1.05, ChatFont,"left","top",false,false,false,true)
	end	
end

function VievChat(bool)
	ShowingChat = bool;
end
addEvent("VievChat", true)
addEventHandler("VievChat", localPlayer, VievChat)

--[[
	Функция ChatHistoryF. Вставка сообщений из истории чата в бокс. 
	Аргументы:
	direction - При True - Вставляет текст из history+1
	при false - вставляет текст из history -1
]]



function ChatHistoryF(direction)
	executeBrowserJavascript(ChatUI, "document.getElementById('chat').selectionStart = document.getElementById('chat').selectionEnd = document.getElementById('chat').value.length;") 
	if CurrentChatHistory == 0 then CurrentChatHistory = #ChatHistory+1 end
	if direction then
		if CurrentChatHistory > 1 then
			CurrentChatHistory = CurrentChatHistory - 1
		end	
	else
		if CurrentChatHistory < #ChatHistory then 
			CurrentChatHistory = CurrentChatHistory + 1
		else
			CurrentChatHistory = 0
			return executeBrowserJavascript(ChatUI, "document.getElementById('chat').value = '';") 
		end	
	end
	if ChatHistory[CurrentChatHistory] == nil then return false end
	------------------------------
	--executeBrowserJavascript(ChatUI, "document.getElementById('chat').value = '"..ChatHistory[CurrentChatHistory].."';");
	executeBrowserJavascript(ChatUI, "SetChatText(\""..ChatHistory[CurrentChatHistory].."\");");
	------------------------------	
end

--[[

function ChatHistoryF(direction)
	executeBrowserJavascript(ChatUI, "document.getElementById('chat').selectionStart = document.getElementById('chat').selectionEnd = document.getElementById('chat').value.length;") 
	if direction then
		if CurrentChatHistory <= #ChatHistory then
			CurrentChatHistory = CurrentChatHistory + 1
		end	
	else
		if CurrentChatHistory <= 1 then 
			CurrentChatHistory = 0
			return executeBrowserJavascript(ChatUI, "document.getElementById('chat').value = '';") 
		else
			CurrentChatHistory = CurrentChatHistory - 1
		end	
	end
	if ChatHistory[CurrentChatHistory] == nil then return false end
	------------------------------
	executeBrowserJavascript(ChatUI, "document.getElementById('chat').value = '"..ChatHistory[CurrentChatHistory].."';");
	------------------------------	
end]]

-----------------------------------------------------------------------------------------------------------------------------------------------------
function dxDrawBorderedText(text, left, top, right, bottom, color, scale, outlinesize, font, alignX, alignY, clip, wordBreak, postGUI, colorCoded)
    local outlinesize = math.min(scale, outlinesize)
    if outlinesize > 0 then
        for offsetX=-outlinesize,outlinesize,outlinesize do
            for offsetY=-outlinesize,outlinesize,outlinesize do
                if not (offsetX == 0 and offsetY == 0) then
                    dxDrawText(text:gsub("#%x%x%x%x%x%x",""), left+offsetX, top+offsetY, right+offsetX, bottom+offsetY, tocolor(0, 0, 0, 255), scale, font, alignX, alignY, clip, wordBreak, postGUI)
                end
            end
        end
    end
    dxDrawText(text, left, top, right, bottom, color, scale, font, alignX, alignY, clip, wordBreak, postGUI, colorCoded, true)
end

function ClearChat()
    for i in ipairs(ChatBox) do
        ChatBox[i] = ""
    end
end
addEvent("ClearChat",true)
addEventHandler("ClearChat", localPlayer, ClearChat)