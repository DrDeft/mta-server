local ReplaceObj = {}
local ObjTable = {}

local ld = 0

function getObjectData(id)
	local objList = xmlLoadFile("object_list.xml")
	local data = xmlNodeGetAttributes(xmlFindChild(objList, "object", id-1 ) )
	xmlUnloadFile(objList)
	return data
end


function CreateObject(modelid, x, y, z, rx, ry, rz,isLowLOD)
	---Функции замены объекта--------------------------
	local obj
	if modelid > 20000 then
		local reObject = 5413+modelid-20000
		if ReplaceObj[modelid-20000] == nil then
			local ObjectInfo = getObjectData(modelid-20000)
			local dff = engineLoadDFF ("dff/"..ObjectInfo.dff..".dff") 
			local col = engineLoadCOL ("col/"..ObjectInfo.dff..".col")
			local txd = engineLoadTXD ("txd/"..ObjectInfo.txd..".txd")
			
			engineReplaceCOL(col, reObject)
			engineImportTXD(txd, reObject)
			if tonumber(ObjectInfo.alpha) == 1 then
				engineReplaceModel(dff, reObject, true)
			else
				engineReplaceModel(dff, reObject)
			end
			ReplaceObj[modelid-20000] = reObject
			
			table.insert(ObjTable, reObject)
		end
		obj = createObject(reObject, x, y, z, rx, ry, rz,isLowLOD)
	else
		obj = createObject(modelid, x, y, z, rx, ry, rz,isLowLOD)
	end
	if obj ~= false then ld = ld + 1 end
	return obj
	---------------------------------------------------
end

function UnloadCustomObjects()
	for i in ipairs(ObjTable) do
		engineRestoreCOL(ObjTable[i])
		engineRestoreModel(ObjTable[i]) 
		table.remove(ObjTable, i)
	end
	ReplaceObj = nil
	ReplaceObj = {}
end