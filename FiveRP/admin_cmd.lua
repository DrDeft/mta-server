--[[
	ToDO: Исправить баг с отображением причины наказания в командах(при слов >1)
]]



function cmd_gotoco(player, cmd, x,y,z)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 3 then return 1
	else

		if x == nil or y == nil or z == nil then return outputChatBox("Пиши: /gotoco [x] [y] [z]",player)
		else
			setElementPosition (player, x, y, z )
		end

	end

end
addCommandHandler("gotoco", cmd_gotoco) 


function cmd_spos(player, cmd, comment)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 3 then return  1 end
	local px,py,pz = getElementPosition(player)
	local _, _, pr = getElementRotation (player)    
	outputChatBox("Координаты отправлены в Log-файл", player)
	if not comment then
		outputDebugString(""..px.."|"..py.."|"..pz.."|"..pr.."")
	else
		outputDebugString(""..px.."|"..py.."|"..pz.."|"..pr.." //"..comment.."") end
end
addCommandHandler("spos", cmd_spos)

function cmd_myrot(player, cmd, comment)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 3 then return  1 end
	local px, py, pz = getElementRotation (player)    
	outputChatBox("Координаты отправлены в Log-файл", player)
	if not comment then
		outputDebugString(""..px.."|"..py.."|"..pz.."")
	else
		outputDebugString(""..px.."|"..py.."|"..pz.." //"..comment.."") end
end
addCommandHandler("srot", cmd_myrot)

function cmd_setint(player, cmd, id, int)
	local playerid = getPlayerID(player)
	id = tonumber(id)
	int = tonumber(int)
	if Players[playerid]["admin"] < 3 then return  1
	else
		if id == nil or int == nil then return outputChatBox("Пиши: /setint [playerid][int]", player)
		else
			if iSPlayer(id) then
				setPlayerInterior ( Players[id]["player"], int)
			else SendPlayerNotice(player, "Игрок с данным ID не найден!")	
			end
		end	
	end
end
addCommandHandler("setint", cmd_setint)

function cmd_ajail(player, cmd, id, jailtime, ...)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 2 then return 1 end
	if id == nil or jailtime == nil or ... == nil then return outputChatBox("#AAAAAAПиши: /ajail [playerid][время][причина]", player) end
	id = tonumber(id)
	jailtime = tonumber(jailtime)
	if not iSPlayer(id) then return SendPlayerNotice(player, "Игрок с данным ID не найден!") end	
	if getPedOccupiedVehicle (Players[id]["player"]) then removePedFromVehicle(Players[id]["player"]) end
	local reason = table.concat({...}, " ")
	if Players[playerid].jailtime > 0 then
		SendMessageToAll("#FF644BАдминистратор "..Players[playerid]["name"].." изменил срок заключения "..Players[id]["name"].." с "..math.ceil(Players[playerid].jailtime/60).." на "..jailtime.." минут. Причина: "..reason.."")
	else
		SendMessageToAll("#FF644BАдминистратор "..Players[playerid]["name"].." посадил игрока "..Players[id]["name"].." на "..jailtime.." минут. Причина: "..reason.."")
	end
	JailPlayer(Players[id]["player"], jailtime * 60)
end
addCommandHandler("ajail", cmd_ajail)
addCommandHandler("aarest", cmd_ajail)

function cmd_setvw(player, cmd, id, vw)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 3 then return  1
	else
		if id == nil or vw == nil then return outputChatBox("Пиши: /setvw [playerid][world]", player)
		else
			if iSPlayer(id) then
				setPlayerDimension ( player, vw)
			else SendPlayerNotice(player, "Игрок с данным ID не найден!")	
			end
		end	
	end
end
addCommandHandler("setvw", cmd_setvw)

function cmd_gohouseint(player, cmd, int)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 3 then return  1
	else
		if int == nil then return outputChatBox("Пиши: /gohouseint [int]", player)
		else
			int = tonumber(int)
			setPlayerInterior(player, houseint[int]["int"], houseint[int]["Pos_x"], houseint[int]["Pos_y"], houseint[int]["Pos_z"])
		end
	end
end
addCommandHandler("gohouseint", cmd_gohouseint)

function cmd_givemoney(player, cmd, id, money)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 11 then return  1
	else
		id = tonumber(id)
		if  id == nil or money == nil then return outputChatBox("Пиши: /givemoney [playerid][money]", player)
		else
			if iSPlayer(id) then
				GivePlayerMoneyAC(Players[id]["player"], tonumber(money))
			else SendPlayerNotice(player, "Игрок с данным ID не найден!")	
			end	
		end	
	end
end
addCommandHandler("givemoney", cmd_givemoney)

function cmd_adminchat(player, cmd, text)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 1 then return  1 end
	if text == nil then return outputChatBox("Пиши: /a [Текст]", player)
	else
		SendToAdmin(player, "#FF0000[A] "..Players[playerid]["name"]..": "..text.."")
	end
end
addCommandHandler("a", cmd_adminchat)

function cmd_setskin(player, cmd, pid, skin)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 2 then return 1 end
	pid = tonumber(pid)
	skin = tonumber(skin)
	if pid == nil or skin == nil or skin < 1 or skin > 312 then return outputChatBox("Пиши: /setskin [playerid][skin]", player) end
	if iSPlayer(pid) then
		SendToAdmin(player, "#A9A9A9[ADMCMD] "..Players[playerid]["name"].." установил игроку "..Players[pid]["name"].." скин № "..skin.."")
		AdminLog(player, "[ADMCMD][/setskin] "..Players[playerid]["name"].." установил игроку "..Players[pid]["name"].." скин № "..skin.."")
		Players[pid]["skin"] = skin
		setElementModel(Players[pid]["player"], skin)
		outputChatBox("Администратор "..Players[playerid]["name"].." установил вам  скин № "..skin.."", Players[pid][player])
	else return SendPlayerNotice(player, "Игрок с данным ID не найден!") end
end
addCommandHandler("setskin", cmd_setskin)

function cmd_tempskin(player, cmd, pid, skin)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 2 then return 1 end
	pid = tonumber(pid)
	skin = tonumber(skin)
	if pid == nil or skin == nil or skin < 1 or skin > 312 then return outputChatBox("Пиши: /tempskin [playerid][skin(1-312)]", player) end
	if iSPlayer(pid) then
		SendToAdmin(player, "#A9A9A9[ADMCMD] "..Players[playerid]["name"].." установил игроку "..Players[pid]["name"].." временный скин № "..skin.."")
		AdminLog(player, "[ADMCMD][/tempskin] "..Players[playerid]["name"].." установил игроку "..Players[pid]["name"].." временный скин № "..skin.."")
		setElementModel(Players[pid]["player"], skin)
		outputChatBox("Администратор "..Players[playerid]["name"].." установил вам временный скин № "..skin.."", Players[pid][player])
	else return SendPlayerNotice(player, "Игрок с данным ID не найден!") end
end
addCommandHandler("tempskin", cmd_tempskin)

function cmd_goto(player, cmd, pid)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 1 then return 1 end
	pid = tonumber(pid)
	if pid == playerid then return SendPlayerNotice(player, "Вы указали свой ID!") end
	if pid == nil then return outputChatBox("Пиши: /goto [playerid]", player) end
	if iSPlayer(pid) then
		local px, py, pz = getElementPosition(Players[pid]["player"])
		setElementPosition(player, px, py, pz)
		setPlayerInterior(Players[playerid]["player"], Players[pid]["int"])
		setPlayerDimension(Players[playerid]["player"], Players[pid]["vw"])
		outputChatBox("Вы телепортировались к "..Players[pid]["name"].."", player)
	else return SendPlayerNotice(player, "Игрок с данным ID не найден!") end
end	
addCommandHandler("goto", cmd_goto)

function cmd_gethere(player, cmd, pid)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 1 then return 1 end
	pid = tonumber(pid)
	if pid == playerid then return SendPlayerNotice(player, "Вы указали свой ID!") end
	if pid == nil then return outputChatBox("Пиши: /gethere [playerid]", player) end
	if iSPlayer(pid) then
		local px, py, pz = getElementPosition(player)
		setElementPosition(Players[pid]["player"], px, py, pz)
		setPlayerInterior(Players[pid]["player"], Players[playerid]["int"])
		setPlayerDimension(Players[pid]["player"], Players[playerid]["vw"])
		outputChatBox("Вы телепортировили к себе игрока "..Players[pid]["name"].."", player)
		outputChatBox("Вы были телепортированы администратором "..Players[playerid]["name"].."", Players[pid]["player"])
		SendToAdmin(player, "#A9A9A9[ADMCMD] "..Players[playerid]["name"].." телепортировал к себе игрока "..Players[pid]["name"].."")
	else return SendPlayerNotice(player, "Игрок с данным ID не найден!") end
end	
addCommandHandler("gethere", cmd_gethere)

function cmd_slap(player, cmd, pid, reason)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 1 then return 1 end
	pid = tonumber(pid)
	if pid == playerid then return SendPlayerNotice(player, "Вы указали свой ID!") end
	if pid == nil or reason == nil then return outputChatBox("Пиши: /slap [playerid][причина]", player) end
	if iSPlayer(pid) then
		AdminLog(player, "[ADMCMD][/slap]Администратор"..Players[playerid]["name"].."["..playerid.."] слапнул игрока "..Players[pid]["name"].."["..pid.."]. Причина: "..reason.."")
		outputChatBox("#FF644BАдминистратор"..Players[playerid]["name"].."["..playerid.."] слапнул игрока "..Players[pid]["name"].."["..pid.."]. Причина: "..reason.."", _, 231, 217, 176, true)
		local px, py, pz = getElementPosition(Players[pid]["player"])
		setElementPosition(Players[pid]["player"], px, py, pz+5)
	else return SendPlayerNotice(player, "Игрок с данным ID не авторизован!") end
end	
addCommandHandler("slap", cmd_slap)
		
function cmd_kick(player, cmd, pid, reason)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 2 then return 1 end
	pid = tonumber(pid)
	if pid == nil or reason == nil then return outputChatBox("Пиши: /kick [playerid][причина]", player) end
	if iSPlayer(pid) then
		outputChatBox("#FF644BАдминистратор"..Players[playerid]["name"].."["..playerid.."] кикнул игрока "..Players[pid]["name"].."["..pid.."]. Причина: "..reason.."", _, 231, 217, 176, true)
		AdminLog(player, "[ADMCMD][/kick]Администратор"..Players[playerid]["name"].."["..playerid.."] кикнул игрока "..Players[pid]["name"].."["..pid.."]. Причина: "..reason.."")
		kickPlayer ( Players[pid]["player"], Players[playerid]["name"], reason )
	else return SendPlayerNotice(player, "Игрок с данным ID не авторизован!") end
end	
addCommandHandler("kick", cmd_kick)	

function cmd_sethp(player, cmd, pid, hp)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 3 then return 1 end
	pid = tonumber(pid)
	hp = tonumber(hp)
	if pid == nil or hp == nil then return outputChatBox("Пиши: /sethp [playerid][HP]", player) end
	if hp < 0 or hp > 100 then return outputChatBox("Уровень HP должен быть не меньше 0 и не больше 100", player) end
	if iSPlayer(pid) then
		outputChatBox("Администратор "..Players[playerid]["name"].." установил вам уровень HP на "..hp.."", Players[pid]["player"])
		outputChatBox("Вы установили игроку "..Players[pid]["name"].." уровень HP на "..hp.."", player)
		AdminLog(player, "[ADMCMD][/sethp] Администратор "..Players[playerid]["name"].." Установил HP игроку "..Players[pid]["name"].." на "..hp.."")
		setElementHealthAC (Players[pid]["player"], hp)
	else return SendPlayerNotice(player, "Игрок с данным ID не авторизован!") end
end	
addCommandHandler("sethp", cmd_sethp)	

function cmd_mute(player, cmd, pid, time, reason)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 2 then return 1 end
	pid = tonumber(pid)
	time = tonumber(time)
	if pid == nil or time == nil or reason == nil then return outputChatBox("Пиши: /mute [playerid][время][причина]", player) end
	if time < 0 or time > 1500 then return outputChatBox("Время не может быть меньше 0 и больше 1500 минут!", player) end
	if iSPlayer(pid) then
		if Players[pid]["mute"] ~= nil then
			outputChatBox("#FF644BАдминистратор"..Players[playerid]["name"].."["..playerid.."] выдал бан чата игроку "..Players[pid]["name"].."["..pid.."] на "..time.." минут. Причина: "..reason.."", _, 231, 217, 176, true)
			AdminLog(player, "#FF644BАдминистратор"..Players[playerid]["name"].."["..playerid.."] выдал бан чата игроку "..Players[pid]["name"].."["..pid.."] на "..time.." минут. Причина: "..reason.."")
			Players[pid]["mute"] = time
		else
			outputChatBox("#FF644BАдминистратор"..Players[playerid]["name"].."["..playerid.."] изменил  бан чата игрока "..Players[pid]["name"].."["..pid.."] c "..Players[pid]["mute"].." на "..time.." минут. Причина: "..reason.."", _, 231, 217, 176, true)
			AdminLog(player, "#FF644BАдминистратор"..Players[playerid]["name"].."["..playerid.."] изменил бан чата игрока "..Players[pid]["name"].."["..pid.."] c "..Players[pid]["mute"].." на "..time.." минут. Причина: "..reason.."")
			Players[pid]["mute"] = time
		end	
	else return SendPlayerNotice(player, "Игрок с данным ID не авторизован!") end
end	
addCommandHandler("mute", cmd_mute)	
	
function cmd_givegun(player, cmd, pid, weapon, ammo)
	local playerid = getPlayerID(player)
	pid = tonumber(pid)
	weapon = tonumber(weapon)
	ammo = tonumber(ammo)
	if Players[playerid]["admin"] < 5 then return 1 end
	if pid == nil or weapon == nil or ammo == nil then return outputChatBox("Пиши: /givegun [playerid][ид оружия][кол-во патронов]", player) end
	if weapon < 1 or weapon > 46 then return outputChatBox("ID оружия должен быть от 1 до 46", player) end
	if ammo < 1 or ammo > 1000 then return outputChatBox("Кол-во патронов должно быть от 1 до 1000", player) end
	if iSPlayer(pid) then
		if getSlotFromWeapon(weapon) == 7 then return outputChatBox("Это оружие запрещено на сервере!", player) end
		outputChatBox("Администратор "..Players[playerid]["name"].." выдал вам оружие "..getWeaponNameFromID(weapon).."("..ammo..")", Players[pid]["player"])
		SendToAdmin(player, "#A9A9A9[ADMCMD] Администратор "..Players[playerid]["name"].." выдал игроку "..Players[pid]["name"].." оружие "..getWeaponNameFromID(weapon).."("..ammo..")")
		AdminLog(player, "[ADMCMD][/givegun]Администратор "..Players[playerid]["name"].." выдал игроку "..Players[pid]["name"].." оружие "..getWeaponNameFromID(weapon).."("..ammo..")")
		giveWeaponAC( Players[pid]["player"], weapon, ammo, true)
	else return SendPlayerNotice(player, "Игрок с данным ID не авторизован!") end
end	
addCommandHandler("givegun", cmd_givegun)	

function cmd_makeleader(player, cmd, pid, frac)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 11 then return 1 end 
	pid = tonumber(pid)
	frac = tonumber(frac)
	if pid == nil or frac == nil then return outputChatBox("Пиши: /makeleader [playerid][ид фрации]", player) end
	if iSPlayer(pid) then
		if frac > 0 and frac < #Fractions then
			if Players[pid].frac ~= 0 then unInvitePlayer(Players[pid].player) end
			outputChatBox("Администратор #0dc426"..Players[playerid]["name"].."#ffffff назначил вас лидером фракции #0dc426"..Fractions[frac]["name"].."", Players[pid]["player"], 255, 255, 255, true)
			SendToAdmin(player, "#A9A9A9[ADMCMD] Администратор "..Players[playerid]["name"].." выдал игроку "..Players[pid]["name"].." права лидера #0dc426"..Fractions[frac]["name"].."")
			AdminLog(player, "[ADMCMD][/makeleader]Администратор "..Players[playerid]["name"].." выдал игроку "..Players[pid]["name"].." права лидера #0dc426"..Fractions[frac]["name"].."")
			Players[pid]["frac"] = frac
			Players[pid]["rank"] = 20
			Players[pid]["post"] = 20
			Players[pid]["leader"] = true
		elseif frac == 0 then
			if Players[pid]["leader"] ~= true then return SendPlayerNotice(player, "У этого игрока нет прав лидера!") end
			outputChatBox("Администратор #0dc426"..Players[playerid]["name"].."#ffffff забрал у вас права лидера фракции #0dc426"..Fractions[Players[pid]["frac"]]["name"].."", Players[pid]["player"], 255, 255, 255, true)
			SendToAdmin(player, "#A9A9A9[ADMCMD] Администратор "..Players[playerid]["name"].." забрал у "..Players[pid]["name"].." права лидера #0dc426"..Fractions[Players[pid]["frac"]]["name"].."")
			AdminLog(player, "[ADMCMD][/makeleader]Администратор "..Players[playerid]["name"].." забрал у "..Players[pid]["name"].." права лидера #0dc426"..Fractions[Players[pid]["frac"]]["name"].."")
			if Players[pid].frac ~= 0 then unInvitePlayer(Players[pid].player) end
		end
	else return SendPlayerNotice(player, "Игрок с данным ID не авторизован!") end 	
end
addCommandHandler("makeleader", cmd_makeleader) 

function cmd_addfraccar(player, cmd, carmodel, fracid)
	local playerid = getPlayerID(player)
	local px,py,pz = getElementPosition(player)
	if Players[playerid]["admin"] < 11 then return 1 end 
	carmodel = tonumber(carmodel);
	fracid = tonumber(fracid);
	if carmodel == nil or fracid == nil then return outputChatBox("Пиши: /addfraccar [carid][fracid]", player) end
	local vid = ReturnFreeVehID()
	Vehicles[vid]["model"] = carmodel;
	Vehicles[vid]["type"] = 2;
	Vehicles[vid]["owner"] = fracid;
	Vehicles[vid]["vehicle"] = createVehicle ( Vehicles[vid]["model"], px, py, pz)
	Vehicles[vid]["fuel"] = 60
	Vehicles[vid]["lock"] = 0
	Vehicles[vid]["engine"] = 1
	Vehicles[vid]["light"] = 0
	Vehicles[vid]["price"] = 10
	Vehicles[vid]["pos"][1] = px
	Vehicles[vid]["pos"][2] = py
	Vehicles[vid]["pos"][3] = pz
	Vehicles[vid]["pos"][4] = 0
	Vehicles[vid]["paintjob"] = 0
	LoadedCars = LoadedCars+1
	setElementData(Vehicles[vid]["vehicle"], "vehicle.id", vid)
	dbExec(SQL, "INSERT INTO vehicle (model, type, owner) VALUES ("..Vehicles[vid]["model"]..",2,"..Vehicles[vid]["owner"]..")")
	local query = dbQuery(SQL, "SELECT MAX(id) id FROM `vehicle`")
	local result = dbPoll ( query, -1 )
	for _, row in ipairs ( result ) do
		Vehicles[vid]["mid"] = tonumber(row["id"])
	end
	warpPedIntoVehicle ( player, Vehicles[vid]["vehicle"])   
	dbFree ( query )
end	
addCommandHandler("addfraccar", cmd_addfraccar) 

function cmd_apark(player, cmd)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 11 then return 1 end 
	local vid = getElementData(getPedOccupiedVehicle(player), "vehicle.id")
	local vx, vy, vz = getElementPosition(getPedOccupiedVehicle(player))
	local _, _, rvz = getElementRotation(getPedOccupiedVehicle(player), "default")  
	Vehicles[vid]["pos"][1] = vx
	Vehicles[vid]["pos"][2] = vy
	Vehicles[vid]["pos"][3] = vz
	Vehicles[vid]["pos"][4] = rvz
	outputChatBox("Машина припаркована!", player)
end
addCommandHandler("apark", cmd_apark) 

function cmd_savepos(player, cmd)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 11 then return 1 end 
	local x,y,z = getElementPosition(player)
	outputDebugString(""..x.." "..y.." "..z.."")
end
addCommandHandler("savepos", cmd_savepos) 

function cmd_aeditbiz(player, cmd, bizid)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 11 then return 1 end 
	ShowSelectMenu(player, 50, "Редактирование бизнеса "..bizid.."", "Изменить тип\nИзменить интерьер\nПродать бизнес\nУстановить цену бизнеса\nСбросить статистику")
end
addCommandHandler("aeditbiz", cmd_aeditbiz) 

function cmd_setbizint(player, cmd, bizid, int)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 11 then return 1 end
	bizid = tonumber(bizid)
	int = tonumber(int)
	outputChatBox("Интерьер бизнеса "..bizid.." сменен с "..Bussines[bizid]["int"].." на "..int.."", player)
	Bussines[bizid]["int"] = int
end
addCommandHandler("setbizint", cmd_setbizint) 

function cmd_setbizintpos(player,cmd,int)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 11 then return 1 end
	int = tonumber(int)
	local intlist = xmlLoadFile ( "bussines/interior_list.xml" )
	local node = xmlFindChild ( intlist, "interior", int-1 )
	local x,y,z = getElementPosition(player)
	local rx,ry,rz = getElementRotation (player)     
	xmlNodeSetAttribute(node, "pos_x", x)
	xmlNodeSetAttribute(node, "pos_y", y)
	xmlNodeSetAttribute(node, "pos_z", z)
	xmlNodeSetAttribute(node, "pos_r", rz)
	xmlSaveFile(intlist)
	xmlUnloadFile(intlist) 
end
addCommandHandler("setbizintpos", cmd_setbizintpos) 

function cmd_targetfps(player, cmd, param)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 11 then return 1 end
	param = tonumber(param)
	setFPSLimit (param)         
end
addCommandHandler("targetfps", cmd_targetfps) 

function cmd_savebiz(player, cmd, bizid)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 11 then return 1 end
	bizid = tonumber(bizid)
	if bizid < 1 or bizid > #Bussines then return SendPlayerNotice(player, "Некорректный ID бизнеса") end
	SaveBussines(bizid)
	outputChatBox("Бизнес успешно сохранен!", player)
end
addCommandHandler("savebiz", cmd_savebiz)

function cmd_biztype(player, cmd, bizid)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 5 then return 1 end
	bizid = tonumber(bizid)
	if bizid < 1 or bizid > #Bussines then return SendPlayerNotice(player, "Некорректный ID бизнеса") end
	outputChatBox("Тип бизнеса "..Bussines[bizid]["name"].."["..bizid.."] - "..GetBussinesType(bizid).."", player)
end
addCommandHandler("biztype", cmd_biztype)

function cmd_fixcar(player, cmd, vehicle)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 4 then return 1 end
	if not vehicle then
		vehicle = getPedOccupiedVehicle(player)
		if vehicle == false then return SendPlayerNotice(player, "Вы должны быть в автомобиле или указать его ID!") end
	else
		vehicle = tonumber(vehicle)
		if vehicle < 1 or vehicle > #Vehicles then return SendPlayerNotice(player, "Вы должны быть в автомобиле или указать его ID!") end
		vehicle = Vehicles[vehicle]["vehicle"]
		if getElementType(vehicle) ~= "vehicle" then return SendPlayerNotice(player, "Вы указали некорректный ID авто!") end
	end
	fixCar(vehicle)
	SendToAdmin(player, "#A9A9A9[ADMCMD] Администратор "..Players[playerid]["name"].." починил авто ID:"..tostring(getElementData(vehicle, "vehicle.id")).."")
end
addCommandHandler("fixcar", cmd_fixcar)

function cmd_fuelcar(player, cmd, vehicle)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 4 then return 1 end
	if not vehicle then
		vehicle = getPedOccupiedVehicle(player)
		if vehicle == false then return SendPlayerNotice(player, "Вы должны быть в автомобиле или указать его ID!") end
		vehicle = getElementData(vehicle, "vehicle.id")
	else
		vehicle = tonumber(vehicle)
		if vehicle < 1 or vehicle > #Vehicles then return SendPlayerNotice(player, "Вы должны быть в автомобиле или указать его ID!") end
		if getElementType(Vehicles[vehicle]["vehicle"]) ~= "vehicle" then return SendPlayerNotice(player, "Вы указали некорректный ID авто!") end
	end
	Vehicles[vehicle]["fuel"] = 60
	SendToAdmin(player, "#A9A9A9[ADMCMD] Администратор "..Players[playerid]["name"].." заправил авто ID:"..vehicle.."")
end
addCommandHandler("fuelcar", cmd_fuelcar)
	
function cmd_tp(player, cmd)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 1 then return 1 end
	ShowSelectMenu(player, 14, "Телепортация", "Мэрия\nЛесопилка\nОружейный завод\nАвтошкола")
end
addCommandHandler("tp", cmd_tp)

function cmd_ban(player, cmd, pid, days, ...)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 4 then return 1 end
	if pid == nil or days == nil or ... == nil then return outputChatBox("Пиши: /ban [playerid][кол-во дней][причина]", player) end
	pid = tonumber(pid)
	days = tonumber(days)
	if not iSPlayer(pid) then return SendPlayerNotice(player, "Игрок не авторизован!") end
	if days < 1 or days > 30 then return SendPlayerNotice(player, "Кол-во дней должно быть от 1 до 30") end
	local reason = table.concat({...}, " ")
	if utf8.len(reason) < 3 then return SendPlayerNotice(player, "Причина должна быть более 3 символов!") end
	AdminLog(player, "[ADMCMD][/ban] "..Players[playerid]["name"].." забанил игрока "..Players[pid]["name"].." на "..days.." дней. Причина: "..reason.."")
	BanPlayer(Players[pid]["player"], player, days, reason)
	SendMessageToAll("#FF644BАдминистратор "..Players[playerid]["name"].." заблокировал игрока "..Players[pid]["name"].." на "..days.." дней. Причина: "..reason.."")
end
addCommandHandler("ban", cmd_ban)

function cmd_devmode(player, cmd)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 11 then return 1 end
	setDevelopmentMode (true)
	outputChatBox("Development Mode Enabled!", player)
end	
addCommandHandler("devmode", cmd_devmode)

function cmd_gzowner(player, cmd, owner)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 11 then return 1 end
	if not owner then return outputChatBox("Пиши: /gzowner [ID фракции владельца]", player) end
	owner = tonumber(owner)
	if owner < 1 or owner > MaxFractions then return SendPlayerNotice(player, "Некорректный ID фракции!") end
	local zoneid = getPlayerGangZoneID(player)
	if zoneid == nil then return SendPlayerNotice(player, "Вы должны быть в зоне захвата!")
	else 
		SendToAdmin(player, "#A9A9A9[ADMCMD] Администратор "..Players[playerid]["name"].." изменил владельца ганг-зоны №"..zoneid.." с "..Fractions[getGangZoneOwner(zoneid)]["name"].." на "..Fractions[owner]["name"]..".")
		setGangZoneOwner(zoneid, owner)
	end
end
addCommandHandler("gzowner", cmd_gzowner)

function cmd_gzid(player, cmd)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 11 then return 1 end
	local zoneid = getPlayerGangZoneID(player)
	outputChatBox("ZoneID: "..tostring(zoneid).."", player)
end
addCommandHandler("gzid", cmd_gzid)

function cmd_asetsalary(player, cmd, frac, rank, salary)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 11 then return 1 end
	if not frac or not rank or not salary then return outputChatBox("Пиши: /asetsalary [ID фракции][ранг][зарплата]", player) end
	frac = tonumber(frac)
	rank = tonumber(rank)
	salary = tonumber(salary)
	if not Fractions[frac] then return SendPlayerNotice(player, "Недействительный ID фракции!") end
	if rank < 1 or rank > 20 then return SendPlayerNotice(player, "Ранг должен быть от 1 до 20!") end
	if salary < 0 or salary > 100000 then return SendPlayerNotice(player, "Размер зарплаты должен быть от 0 до 100.000!") end
	outputChatBox("Вы изменили размер ЗП фракции #0dc426"..Fractions[frac].name.." #ffffffдля ранга #0dc426"..Fractions[frac].ranks[rank].."("..rank..") #ffffffc #0dc426$"..Fractions[frac].salary[rank].." #ffffffна #0dc426$"..salary.."", player)
	Fractions[frac].salary[rank] = salary
end
addCommandHandler("asetsalary", cmd_asetsalary)

function cmd_gotobizint(player,cmd,bizid)
	local playerid = getPlayerID(playerid)
	if Players[playerid]["admin"] < 11 then return 1 end
	bizid = tonumber(bizid)
	setPlayerInterior(player, BussinesInt[bizid]["int"]) -- ТП в интерьер
	setElementPosition(player, BussinesInt[bizid]["pos"]["x"],BussinesInt[bizid]["pos"]["y"], BussinesInt[bizid]["pos"]["z"])
end
addCommandHandler("gotobizint", cmd_gotobizint)

function cmd_setvehandling(player, cmd, property, value, vehicle)
local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 11 then return 1 end
	if not property or not value then return outputChatBox("Пиши: /setvehandling [параметр][значение][id авто]", player) end
	if not vehicle then
		vehicle = getPedOccupiedVehicle(player)
		if vehicle == false then return SendPlayerNotice(player, "Вы должны быть в автомобиле или указать его ID!") end
	else
		vehicle = tonumber(vehicle)
		if vehicle < 1 or vehicle > #Vehicles then return SendPlayerNotice(player, "Вы должны быть в автомобиле или указать его ID!") end
		vehicle = Vehicles[vehicle]["vehicle"]
		if getElementType(vehicle) ~= "vehicle" then return SendPlayerNotice(player, "Вы указали некорректный ID авто!") end
	end
	setVehicleHandling (vehicle, property, value) 
end
addCommandHandler("setvehandling", cmd_setvehandling)

function cmd_cc(player)
	local playerid = getPlayerID(player)
	if Players[playerid]["admin"] < 11 then return 1 end
	ClearChat()
end
addCommandHandler("cc", cmd_cc)