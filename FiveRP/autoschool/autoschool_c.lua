----------------------Сдача экзамена автошкола--------------------------
function cmd_testex(cmd)
	loadDrivingSchoolTestExam()
end
addCommandHandler("testex", cmd_testex) 

local DrivingSchoolColision = {}

local function drivingSchoolColHit()
	bindKey("lalt", "down", startDrivingSchoolTestExam)
end

local function drivingSchoolColLeave()
	unbindKey("lalt", "down", startDrivingSchoolTestExam)
end

function loadDrivingSchoolTestExam()
	DrivingSchoolColision[1] = createColCuboid(1531.5040039063, 6.8172312736511, 1076.2738769531, 1.35, 1.35, 1.9);
	DrivingSchoolColision[2] = createColCuboid(1531.5450195313, 8.6321977615356, 1076.2738769531, 1.35, 1.35, 1.9);
	DrivingSchoolColision[3] = createColCuboid(1531.7728759766, 10.812714958191, 1076.2738769531, 1.35, 1.35, 1.9);
	DrivingSchoolColision[4] = createColCuboid(1529.6575195312, 10.236426067352, 1076.2738769531, 1.2, 1.25, 1.9);
	DrivingSchoolColision[5] = createColCuboid(1527.9371582031, 10.244004440308, 1076.2738769531, 1, 1.45, 1.9);
	DrivingSchoolColision[6] = createColCuboid(1525.9592041016, 10.488066482544, 1076.2738769531, 1, 1.25, 1.95);
	for i in ipairs(DrivingSchoolColision) do
		addEventHandler("onClientColShapeHit", DrivingSchoolColision[i], drivingSchoolColHit)
		addEventHandler("onClientColShapeLeave", DrivingSchoolColision[i], drivingSchoolColLeave)
	end
end

function startDrivingSchoolTestExam()
	for i in ipairs(DrivingSchoolColision) do
		removeEventHandler("onClientColShapeHit", DrivingSchoolColision[i], drivingSchoolColHit)
		removeEventHandler("onClientColShapeLeave", DrivingSchoolColision[i], drivingSchoolColLeave)
		destroyElement(DrivingSchoolColision[i])
	end
	unbindKey("lalt", "down", startDrivingSchoolTestExam)
	triggerServerEvent("DrivingSchoolExam", localPlayer, localPlayer, 0)
	DrivingSchoolColision = nil
	DrivingSchoolColision = {}
	
	setElementPosition(localPlayer, 1532.285546875, 7.50390625, 1077.2238769531, false)
	setElementRotation(localPlayer, 0, 0, 272, "default",true)    
	setPedAnimation(localPlayer,  "PED","SEAT_down", -1, false, false, false)
end

function DrivingSchoolDriveExam(player)
	if player ~= localPlayer then return 1 end
	local markerTable = {}
	markerTable[1] = {-2045.8916015625, -84.5283203125, 34.988632202148}
	markerTable[2] = {-2073.447265625, -67.8603515625, 34.996173858643}
	markerTable[3] = {-2213.0087890625, -67.306640625, 34.996822357178}
	markerTable[4] = {-2402.560546875, -68.12109375, 34.98900604248}
	markerTable[5] = {-2499.03515625, -51.5693359375, 25.34783744812}
	markerTable[6] = {-2544.9052734375, 41.8291015625, 16.269538879395}
	markerTable[7] = {-2601.529296875, 71.763671875, 4.0041575431824}
	markerTable[8] = {-2582.3798828125, 155.1630859375, 4.002781867981}
	markerTable[9] = {-2571.71484375, 215.205078125, 8.4312725067139}
	markerTable[10] = {-2539.01171875, 289.2216796875, 17.472768783569}
	markerTable[11] = {-2482.919921875, 433.873046875, 28.857393264771}
	markerTable[12] = {-2403.0576171875, 416.365234375, 34.78577041626}
	markerTable[13] = {-2319.537109375, 406.4697265625, 34.847053527832}
	markerTable[14] = {-2255.1708984375, 335.287109375, 34.3994140625}
	markerTable[15] = {-2220.3154296875, 205.8798828125, 34.996326446533}
	markerTable[16] = {-2152.1640625, 137.6044921875, 34.996421813965}
	markerTable[17] = {-2076.0205078125, 107.5537109375, 31.854248046875}
	markerTable[18] = {-2009.6865234375, 67.9150390625, 28.643449783325}
	markerTable[19] = {-2009.7470703125, -52.6806640625, 34.990001678467}
	markerTable[20] = {-2049.4462890625, -84.4033203125, 34.98913192749}
	if source == DrivingSchoolMarker then
		destroyElement(source)
		destroyElement(DrivingSchoolBlip)
		DrivingSchoolExamQuest = DrivingSchoolExamQuest+1
		if DrivingSchoolExamQuest <= 20 then
			DrivingSchoolMarker = createMarker(markerTable[DrivingSchoolExamQuest][1], markerTable[DrivingSchoolExamQuest][2], markerTable[DrivingSchoolExamQuest][3], "checkpoint", 3.5, 255, 0, 0, 255)
			DrivingSchoolBlip = createBlipAttachedTo(DrivingSchoolMarker)
		else 
			triggerServerEvent("CompliteDrivingSchoolExam", localPlayer, true)
		end
	end
	cancelEvent()
end

function StartDrivingSchoolDriveExam()
	DrivingSchoolMarker = createMarker(-2045.8916015625, -84.5283203125, 34.988632202148, "checkpoint", 3.5, 255, 0, 0, 255)
	DrivingSchoolBlip = createBlipAttachedTo(DrivingSchoolMarker)
	DrivingSchoolExamQuest = 1
	addEventHandler("onClientMarkerHit", root, DrivingSchoolDriveExam, true, "high")
end
addEvent("StartDrivingSchoolDriveExam", true)
addEventHandler("StartDrivingSchoolDriveExam", root, StartDrivingSchoolDriveExam)

function StopDrivingSchoolDriveExam()
	destroyElement(DrivingSchoolMarker)
	destroyElement(DrivingSchoolBlip)
	DrivingSchoolExamQuest = nil
	DrivingSchoolBlip = nil
	DrivingSchoolMarker = nil
	triggerServerEvent("CompliteDrivingSchoolExam", localPlayer, false)
end
addEvent("StopDrivingSchoolDriveExam", true)
addEventHandler("StopDrivingSchoolDriveExam", root, StopDrivingSchoolDriveExam)