
Bussines = {}

local BussinesType = {"24/7", "Ammu Nation", "Кафе/Бар", "Магазин одежды"}
local BaseProdPrice = {}
BaseProdPrice[1] = {}
BaseProdPrice[2] = {}
BaseProdPrice[3] = {}
BaseProdPrice[4] = {}
local LoadedBussines = 0
local BussinesInt = {}
local function LoadBussinesInterior()
	local BusIntList = xmlLoadFile ( "bussines/interior_list.xml" )
	local i = 0
	while xmlFindChild ( BusIntList, "interior", i ) ~= false do
		local data = xmlNodeGetAttributes ( xmlFindChild ( BusIntList, "interior", i ) )
		BussinesInt[i+1] = {id = i+1, int = tonumber(data["int"]), pos = {x = data["pos_x"], y = data["pos_y"], z = data["pos_z"], r = data["pos_r"]}}
		i = i+1
	end
	xmlUnloadFile(BusIntList)  
end

function GetBussinesType(bizid)
	return BussinesType[Bussines[bizid]["type"]]
end

function GetProdPrice(type, id)
	return BaseProdPrice[type][i]
end

function LoadBussines()
	LoadBussinesInterior()
	local query = dbQuery(SQL, "SELECT * FROM `bussines`")
	local resultTable, num, err = dbPoll (query, -1) 
	if resultTable then 
		for i, row in ipairs(resultTable) do
			Bussines[i] = {id, mid, owner, price, name, money, product_cost, products, pos = {}, id_pickup, int, map_icon}
			Bussines[i]["id"] = i
			Bussines[i]["mid"] = row['id']
			Bussines[i]["owner"] = row['owner']
			Bussines[i]["price"] = row['price']
			Bussines[i]["type"] = row['type']
			Bussines[i]["name"] = row['name']
			local pos = explode("|", row['pos'])
			for x in ipairs(pos) do
				Bussines[i]["pos"][x] = tonumber(pos[x])
			end
			Bussines[i]["money"] = row['money']
			Bussines[i]["product_cost"] = row['product_cost']
			Bussines[i]["monthly_income"] = row['monthly_income']
			Bussines[i]["products"] = row['products']
			Bussines[i]["id_pickup"] = row['id_pickup']
			Bussines[i]["int"] = row['int']
			Bussines[i]["map_icon"] = row['map_icon']
			CreateBussines(i)
			LoadedBussines = LoadedBussines+1
		end
	end
	outputDebugString("Загруженно "..LoadedBussines.." бизнесов")
	LoadFuelStation()
end

function SaveBussines(id)
	if not id then
		--Сохраняем все
		for i in ipairs(Bussines) do
			dbExec(SQL, "UPDATE `bussines` SET \
				`owner` = '"..Bussines[i]["owner"].."', \
				`price` = '"..Bussines[i]["price"].."', \
				`name` = '"..Bussines[i]["name"].."', \
				`money` = '"..Bussines[i]["money"].."', \
				`pos` = '"..Bussines[i]["pos"][1].."|"..Bussines[i]["pos"][2].."|"..Bussines[i]["pos"][3].."|"..Bussines[i]["pos"][4].."|', \
				`id_pickup` = '"..Bussines[i]["id_pickup"].."', \
				`int` = '"..Bussines[i]["int"].."', \
				`map_icon` = '"..Bussines[i]["map_icon"].."' \
			WHERE `id` = '"..Bussines[i]["mid"].."' LIMIT 1")
		end
		outputDebugString("Бизнесы успешно сохранены!")
	else
		--Сохраняем конкретный бизнес
		dbExec(SQL, "UPDATE `bussines` SET \
			`owner` = '"..Bussines[id]["owner"].."', \
			`price` = '"..Bussines[id]["price"].."', \
			`name` = '"..Bussines[id]["name"].."', \
			`money` = '"..Bussines[id]["money"].."', \
			`pos` = '"..Bussines[id]["pos"][1].."|"..Bussines[id]["pos"][2].."|"..Bussines[id]["pos"][3].."|"..Bussines[id]["pos"][4].."|', \
			`id_pickup` = '"..Bussines[id]["id_pickup"].."', \
			`int` = '"..Bussines[id]["int"].."', \
			`map_icon` = '"..Bussines[id]["map_icon"].."' \
		WHERE `id` = '"..Bussines[id]["mid"].."' LIMIT 1")

		outputDebugString("Бизнес "..Bussines[id]["name"].."["..id.."] успешно сохранен!")
	end
end

local function CreateBussinesExit(bizid)
	local intid = Bussines[bizid]["int"]
	Bussines[bizid]["ePickup"] = createMarker( BussinesInt[intid]["pos"]["x"],BussinesInt[intid]["pos"]["y"], BussinesInt[intid]["pos"]["z"]+0.7, "arrow", 1)
	setElementData(Bussines[bizid]["ePickup"], "bussines.id", bizid)
	setElementInterior(Bussines[bizid]["ePickup"], BussinesInt[intid]["int"])
	setElementDimension(Bussines[bizid]["ePickup"], 5000+bizid)
end

function CreateBussines(id)
	Bussines[id]["icon"] = createBlip(Bussines[id]["pos"][1], Bussines[id]["pos"][2], Bussines[id]["pos"][3], Bussines[id]["map_icon"], _, _, _, _, _, _, 200)
	if Bussines[id]["owner"] == -1 then
		Bussines[id]["pickup"] = createPickup(Bussines[id]["pos"][1], Bussines[id]["pos"][2], Bussines[id]["pos"][3], 3, 1274, 0)
	else
		Bussines[id]["pickup"] = createPickup(Bussines[id]["pos"][1], Bussines[id]["pos"][2], Bussines[id]["pos"][3], 3, Bussines[id]["id_pickup"], 0)
	end	
	setElementData(Bussines[id]["pickup"], "bussines.id", id)
	CreateBussinesExit(id)
end

function EnterBussinesPickup(pickup, dimension)
	if getPedOccupiedVehicle(source) then return 1 end
	local playerid = getPlayerID(source)
	local i = getElementData(pickup, "bussines.id")
	if i == false then return 1 end
	if Bussines[i]["owner"] == -1 then
		ShowSelectMenu(source, 60, "Этот бизнес продается", "Войти\nКупить бизнес")
		Players[playerid]["tparam"]["bizpickup"] = i
	else
		if Bussines[i]["owner"] == Players[playerid]["mysqlid"] then
			Players[playerid]["tparam"]["bizpickup"] = i
			ShowSelectMenu(source, 70, "Управление бизнесом", "Войти\nСтатистика\nУправление бизнесом\nОткрыть/Закрыть бизнес\nПродать бизнес")
		else
			EnterBussines(source, i)
		end
	end	
end

addEventHandler("onPlayerPickupHit",getRootElement(),EnterBussinesPickup)

function EnterBussines(source, id)
	setPlayerInterior(source, BussinesInt[Bussines[id]["int"]]["int"]) -- ТП в интерьер
	setElementPosition(source, BussinesInt[Bussines[id]["int"]]["pos"]["x"],BussinesInt[Bussines[id]["int"]]["pos"]["y"], BussinesInt[Bussines[id]["int"]]["pos"]["z"])
	setPlayerDimension(source, 5000+id)
	setElementRotation(source, 0, 0, BussinesInt[Bussines[id]["int"]]["pos"]["r"])   
end


function ExitBussines(source, bizid)
	local playerid = getPlayerID(source)
	------------------------------------------------------------------------------------------------------------
	setPlayerInterior(source, 0)
	setElementPosition(source, Bussines[bizid]["pos"][1], Bussines[bizid]["pos"][2], Bussines[bizid]["pos"][3])
	setPlayerDimension(source, 0)
	setElementRotation(source, 0, 0, Bussines[bizid]["pos"][4])
	------------------------------------------------------------------------------------------------------------
	Players[playerid]["tparam"]["buildid"] = 0
end


local function ExitBussinesMarkerHit(markerHit, matchingDimension)
	if getPedOccupiedVehicle(source) then return 1 end
	local playerid = getPlayerID(source)
	if matchingDimension and markerHit then
		local bizid = getElementData(markerHit, "bussines.id")
		if not bizid then return 1 end
		Players[playerid]["tparam"]["buildid"] = bizid
		ExitBussines(source, bizid)
	end
end
addEventHandler("onPlayerMarkerHit",getRootElement(),ExitBussinesMarkerHit)

function BuyBussines(player, id)
	local playerid = getPlayerID(player)
	if Bussines[id]["owner"] ~= -1 then return SendPlayerNotice(player, "У этого бизнеса уже есть владелец!") end
	if Players[playerid]["money"] < Bussines[id]["price"] then return SendPlayerNotice(player, "У вас недостаточно средств!") end
	-----------------------------------------------------------------------------------------------------------------------------
	Bussines[id]["owner"] = Players[playerid]["mysqlid"]
	setPickupType (Bussines[id]["pickup"], 3, Bussines[id]["id_pickup"])
	GivePlayerMoneyAC(player, Bussines[id]["price"]*-1)
	triggerClientEvent(player, "PlayBuyBussinesSound", player)
	ShowMessage(player, 0, "Поздравляем вас с покупкой бизнеса", "Поздравляем вас с покупкой бизнеса!\nВы купили #0dc426"..Bussines[id]["name"].."["..id.."] #ffffffза #0dc426$"..Bussines[id]["price"].."\n#ffffffДля управления бизнесом встаньте на пикап \nили откройте интерактивное меню находясь внутри бизнеса!\nТак же не забывайте вовремя пополнять склад бизнеса и оплачивать счета!\n#FF0000В противном случае ваш бизнес будет конфискован.", "Понятно")
	-----------------------------------------------------------------------------------------------------------------------------
end

function BussinesBuyItem(playerid, bizid, itemid)
	local type = Bussines[bizid]["type"]
	local price = BaseProdPrice[type][itemid]/100*Bussines[bizid]["product_cost"]+BaseProdPrice[type][itemid]
	if Players[playerid]["money"] < price then return SendPlayerNotice(Players[playerid]["player"], "У вас недостаточно средств!") end
	-----------------------------------------------------------------------------------------------------------------------------------
	GivePlayerMoneyAC(Players[playerid]["player"], price*-1)
	Bussines[bizid]["money"] = Bussines[bizid]["money"]+price
	-----------------------------------------------------------------------------------------------------------------------------------
end

function SellBussines(player, bizid, type, price, tosell)
	local playerid = getPlayerID(player)
	if Bussines[bizid]["owner"] ~= Players[playerid].mysqlid then return SendPlayerNotice(player, "Вы не владелец этого бизнеса!") end
	if type == 0 then -- Продать в гос-во
		GivePlayerMoneyAC(Players[playerid]["player"], Bussines[bizid]["price"]/2)
		setPickupType(Bussines[bizid]["pickup"], 3, 1274)
		Bussines[bizid]["owner"] = -1
		outputChatBox("Вы упешно продали бизнес #0dc426\""..Bussines[bizid]["name"].."\" #ffffffгосударству за #0dc426$"..math.ceil(Bussines[bizid]["price"]/2).."")
	elseif type == 1 then --Продать в игроку
	
	end
end