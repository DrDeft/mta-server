local StationZone = {}

function LoadFuelStation()
	StationZone[1] = createColCuboid(1936.8691, -1779.2187, 12.7905, 8.7666, 12.7012, 4)
	StationZone[2] = createColSphere ( -90.9746, -1169.2373, 2.419, 8.5)
	StationZone[3] = createColCuboid(651.5849, -572.8789, 15.3359, 9.0, 18.0, 4)
	for i in ipairs(StationZone) do
		addEventHandler ( "onColShapeHit", StationZone[i], onEnterFuelStationZone)
	end
	outputDebugString("Загруженно заправочных станций: "..#StationZone.."")
end

function onEnterFuelStationZone(vehicle, vw)
	if getElementType (vehicle) ~= "vehicle" then return cancelEvent() end 
	local driver = getVehicleOccupant(vehicle)
	if driver then
		exports.Notice:vievDownNotice(driver, "Нажмите#0dc426 H #ffffffдля заправки авто")
		bindKey(driver, "h", "down", FuelVehicle, driver)
		triggerClientEvent(driver, "EnterZoneFuelStation", driver, source)
	end	
end

function onLeaveFuelStationZone(player)
	exports.Notice:vievDownNotice(player)
	unbindKey(player, "h", "down", FuelVehicle, player)
end
addEvent("onLeaveFuelStationZone", true)
addEventHandler("onLeaveFuelStationZone", root, onLeaveFuelStationZone)

function FuelVehicle(player)
	if not isPedInVehicle ( player ) then return 1 end
	ShowPlayerEditBox(player, 9, "Заправка транспорта", "Сколько литров бензина вы хотите заправить?\nЦена 1 литра:#0dc426 $50", "Отмена", "принять")
end