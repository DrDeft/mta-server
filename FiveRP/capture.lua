--[[
	ToDo:
	Проверить ф-цию подсчета игроков(выход из игры/смена ф-ции и тд)
	переделать triggerClientEvent для показа капта на таблицу игроков ф-ции
]]

local GangZones = {}
local CaptureInfo = {} --[[ Attack, Defend, ZoneID, AttackPoints, DefendPoints, Time, Timer, AttackCount, DefendCount, Sphere ]]

function giveGangZoneBonus()
	local _GangZone = {0,0,0,0}; -- Grove, ballas, vagos, aztecas
	for i in ipairs(GangZones) do
		_GangZone[GangZones[i].owner-9] = _GangZone[GangZones[i].owner-9]+1
	end
	Fractions[10].safe_money = Fractions[10].safe_money+350*_GangZone[1] -- Зачисляем деньги на счет банды. 350*кол-во терр
	Fractions[11].safe_money = Fractions[11].safe_money+350*_GangZone[2]
	Fractions[12].safe_money = Fractions[12].safe_money+350*_GangZone[3]
	Fractions[13].safe_money = Fractions[13].safe_money+350*_GangZone[4]
	--------------------Бонусы от территорий для банды----------------------
	
end
addCommandHandler("gzn", giveGangZoneBonus)

function loadGangZones(query)
	local resultTable, num, err = dbPoll (query, 0)
	local r,g,b,a
	for i, row in ipairs(resultTable) do
		GangZones[i] = {area, id, owner, pos = {}, size = {}}
		GangZones[i].id = row['id']
		GangZones[i].owner = row['owner']
		--------------------------------------------
		GangZones[i].pos[1] = row['pos_x']
		GangZones[i].pos[2] = row['pos_y']
		GangZones[i].size[1] = row['size_x']
		GangZones[i].size[2] = row['size_y']
		--------------------------------------------
		r,g,b,a = getColorFromString("#"..Fractions[GangZones[i].owner]["color"].."")
		GangZones[i].area = createRadarArea (GangZones[i].pos[1], GangZones[i].pos[2], GangZones[i].size[1], GangZones[i].size[2], r, g ,b, 170)  
		--------------------------------------------
	end
	outputDebugString("Загружено GangZones: "..#resultTable)
	dbFree(query)
end

function saveGangZone(id)
	dbExec(SQL, "UPDATE `gangzones` SET `owner` = '"..GangZones[id]["owner"].."' WHERE `id` = '"..GangZones[id]["id"].."' LIMIT 1")
	---------------------------------------------------
	--[[
	dbExec(SQL, "UPDATE `gangzones` SET \
	`owner` = '"..GangZones[i]["owner"].."', \
	`pos_x` = '"..GangZones[i]["pos"][1].."', \
	`pos_y` = '"..GangZones[i]["pos"][2].."', \
	`size_x` = '"..GangZones[i]["size"][1].."', \
	`size_y` = '"..GangZones[i]["size"][2].."' \
	WHERE `id` = '"..GangZones[i]["id"].."' LIMIT 1")
	---------------------------------------------------]]
end


--[[
	@checkStartCapture - Проверяет участвует ли фракция в капте с любой стороны или нет.
	Возвращает false - если не участвует \
	true - участвует
]]

function captureTimer(captureID)
	---------------------Прибавление очков---------------------------------
	--CaptureInfo[captureID].AttackPoints = CaptureInfo[captureID].AttackPoints+CaptureInfo[captureID].AttackCount
	--CaptureInfo[captureID].DefendPoints = CaptureInfo[captureID].DefendPoints+CaptureInfo[captureID].DefendCount
	------------------------------------------------------------------------
	------------------------------------------------------------------------
	CaptureInfo[captureID].Time = CaptureInfo[captureID].Time-1
	if CaptureInfo[captureID].AttackPoints > 399 or CaptureInfo[captureID].DefendPoints > 399 or CaptureInfo[captureID].Time < 1 then
		return finishCapture(captureID)
	end
	triggerClientEvent(GetFractionPlayers(CaptureInfo[captureID].Attack), "updateCaptureInfo", root, CaptureInfo[captureID].Time, CaptureInfo[captureID].AttackPoints, CaptureInfo[captureID].DefendPoints)
	triggerClientEvent(GetFractionPlayers(CaptureInfo[captureID].Defend), "updateCaptureInfo", root, CaptureInfo[captureID].Time, CaptureInfo[captureID].AttackPoints, CaptureInfo[captureID].DefendPoints)
end

function finishCapture(captureID)
	------------Выбор победителя-----------------
	if CaptureInfo[captureID].AttackPoints > CaptureInfo[captureID].DefendPoints then
		-- Победа атаки
		SendToFrac(_, CaptureInfo[captureID].Attack, "#00ffffВаша банда одержала победу в войне за территорию!")
		SendToFrac(_, CaptureInfo[captureID].Defend, "#00ffffВаша банда потерпела поражение в войне за территорию!")
		
		setGangZoneOwner(CaptureInfo[captureID].ZoneID, CaptureInfo[captureID].Attack)
		-------------------------------------------------------------------------------------
	elseif CaptureInfo[captureID].AttackPoints == CaptureInfo[captureID].DefendPoints then
		SendToFrac(_, CaptureInfo[captureID].Attack, "#00ffffВашей банде не удалось захватить новую территорию!")
		SendToFrac(_, CaptureInfo[captureID].Defend, "#00ffffВаша банда успешно отстояла свою территорию!")
		-- Ничья(Победа защиты)
	else 
		SendToFrac(_, CaptureInfo[captureID].Attack, "#00ffffВашей банде не удалось захватить новую территорию!")
		SendToFrac(_, CaptureInfo[captureID].Defend, "#00ffffВаша банда успешно отстояла свою территорию!")
		-- Победа защиты
	end
	setRadarAreaFlashing (GangZones[CaptureInfo[captureID].ZoneID].area, false)
	------------Очистка информации---------------
	if isTimer(CaptureInfo[captureID].Timer) then killTimer(CaptureInfo[captureID].Timer) end
	triggerClientEvent(GetFractionPlayers(CaptureInfo[captureID].Attack), "vievCaptureInfo", root, false)
	triggerClientEvent(GetFractionPlayers(CaptureInfo[captureID].Defend), "vievCaptureInfo", root, false)
	removeEventHandler("onColShapeHit", CaptureInfo[captureID].Sphere, onPlayerEnterGangZone)
	removeEventHandler("onColShapeLeave", CaptureInfo[captureID].Sphere, onPlayerLeaveGangZone)
	
	for i, _player in ipairs(GetFractionPlayers(CaptureInfo[captureID].Attack)) do -- Перебор игроков из комманды атаки
		removeEventHandler("onPlayerQuit", _player, playerLeaveFromGameOnCapture)
		removeEventHandler("unInvitePlayer", _player, playerLeaveFracOnCapture)
	end
	for i, _player in ipairs(GetFractionPlayers(CaptureInfo[captureID].Defend)) do -- Перебор игроков из комманды атаки
		removeEventHandler("onPlayerQuit", _player, playerLeaveFromGameOnCapture)
		removeEventHandler("unInvitePlayer", _player, playerLeaveFracOnCapture)
	end
	
	destroyElement(CaptureInfo[captureID].Sphere)
	
	Fractions[CaptureInfo[captureID].Attack].captureid = nil
	Fractions[CaptureInfo[captureID].Defend].captureid = nil
	------------Удаление табл--------------------
	table.remove(CaptureInfo, captureID)
	CaptureInfo[CaptureInfo] = nil
	
end

-------------------------------------------------------------------------------------
function onPlayerEnterGangZone(player, virtualWorld)
	local captureID = getElementData(source, "Capture.ID")
	if getElementType(player) == "player" then 
		local playerid = getPlayerID(player)
		if Players[playerid].frac == CaptureInfo[captureID].Attack then
			CaptureInfo[captureID].AttackCount = CaptureInfo[captureID].AttackCount+1
		elseif Players[playerid].frac == CaptureInfo[captureID].Defend then
			CaptureInfo[captureID].DefendCount = CaptureInfo[captureID].DefendCount+1
		end
	end
end

function onPlayerLeaveGangZone(player, virtualWorld)
	local captureID = getElementData(source, "Capture.ID")
	if getElementType(player) == "player" then 
		local playerid = getPlayerID(player)
		if Players[playerid].frac == CaptureInfo[captureID].Attack then
			CaptureInfo[captureID].AttackCount = CaptureInfo[captureID].AttackCount-1
		elseif Players[playerid].frac == CaptureInfo[captureID].Defend then
			CaptureInfo[captureID].DefendCount = CaptureInfo[captureID].DefendCount-1
		end
	end
end
-------------------------------------------------------------------------------------

function checkStartCapture(fracID)
	if Fractions[fracID].captureid ~= nil then
		return true
	else
		return false;
	end
end

--[[
	@getPlayersInGangZone - Возвращает таблицу с ID игроков внутри ганг-зоны.
	Возвращает таблицу игроков
]]

function getPlayersInGangZone(captureID)
	----------------------------------------------------------------
	local _sphere = createColRectangle (GangZones[zoneID].pos[1], GangZones[zoneID].pos[2], GangZones[zoneID].size[1], GangZones[zoneID].size[2])
	local _players = getElementsWithinColShape (_sphere, "player")
	destroyElement(_sphere)
	----------------------------------------------------------------
	local PlayerIDTable = {}
	for idx, player in ipairs(_players) do
		table.insert(PlayerIDTable, getPlayerID(player))
	end
	return PlayerIDTable
end

function startCapture(_zone, _attack, _defend)
	-------------Определяем ID капта------------------------
	local captureID = 1
	while CaptureInfo[captureID] ~= nil do
		captureID = captureID+1;
	end
	
	--------------------------------------------------------
	Fractions[_attack]["captureid"] = captureID
	Fractions[_defend]["captureid"] = captureID
	--------------------------------------------------------
	table.insert(CaptureInfo, {ZoneID = _zone, Attack = _attack, Defend = _defend, AttackPoints = 0, DefendPoints = 0, Time = 3, Sphere, AttackCount = 0, DefendCount = 0})
	setRadarAreaFlashing(GangZones[_zone].area, true)
	
	CaptureInfo[captureID].Sphere = createColRectangle(GangZones[_zone].pos[1], GangZones[_zone].pos[2], GangZones[_zone].size[1], GangZones[_zone].size[2])
	setElementData(CaptureInfo[captureID].Sphere, "Capture.ID", captureID)
	addEventHandler("onColShapeHit", CaptureInfo[captureID].Sphere, onPlayerEnterGangZone)
	addEventHandler("onColShapeLeave", CaptureInfo[captureID].Sphere, onPlayerLeaveGangZone)
	
	CaptureInfo[captureID].Timer = setTimer(captureTimer, 1000, CaptureInfo[captureID].Time, captureID)
	------------------------------Участники капта---------------------------
	local _AttackPlayers = GetFractionPlayers(_attack)
	local _DefendPlayers = GetFractionPlayers(_defend)
	for i in ipairs(_AttackPlayers) do
		addEventHandler("onPlayerQuit", _AttackPlayers[i], playerLeaveFromGameOnCapture)
		addEventHandler("unInvitePlayer", _AttackPlayers[i], playerLeaveFracOnCapture)
		triggerClientEvent(_AttackPlayers[i], "vievCaptureInfo", _AttackPlayers[i], true)
	end
	for i in ipairs(_DefendPlayers) do
		addEventHandler("onPlayerQuit", _DefendPlayers[i], playerLeaveFromGameOnCapture)
		addEventHandler("unInvitePlayer", _DefendPlayers[i], playerLeaveFracOnCapture)
		triggerClientEvent(_DefendPlayers[i], "vievCaptureInfo", _DefendPlayers[i], true)
	end
	------------------------------Подсчет игроков в зоне-----------------------
	local _players = getElementsWithinColShape (CaptureInfo[captureID].Sphere, "player")
	local playerid = nil
	for idx, player in ipairs(_players) do
		playerid = getPlayerID(player)
		if Players[playerid].frac == CaptureInfo[captureID].Attack then
			CaptureInfo[captureID].AttackCount = CaptureInfo[captureID].AttackCount+1
		elseif Players[playerid].frac == CaptureInfo[captureID].Defend then
			CaptureInfo[captureID].DefendCount = CaptureInfo[captureID].DefendCount+1
		end
	end
	local playerid = nil
	---------------------------------------------------------------------------
	SendToFrac(_, _defend, "#00ffffБанда #"..Fractions[_attack]["color"]..""..Fractions[_attack]["name"].."#00ffff объявила войну вашей банде за территорию #"..Fractions[_defend]["color"]..""..getZoneName(GangZones[_zone].pos[1], GangZones[_zone].pos[2], 0).."!")
	return captureID
end

function setGangZoneOwner(zoneID, owner)
	GangZones[zoneID].owner = owner
	setRadarAreaFlashing(GangZones[zoneID].area, false)
	local r,g,b,a = getColorFromString("#"..Fractions[GangZones[zoneID].owner]["color"].."")
	setRadarAreaColor(GangZones[zoneID].area, r, g, b, 170)
	saveGangZone(zoneID)	
end

function getGangZoneOwner(zoneID)
	return GangZones[zoneID].owner
end

function getPlayerGangZoneID(player)
	local px, py = getElementPosition(player)
	local zoneid = nil
	for i in ipairs(GangZones) do
		if isInsideRadarArea(GangZones[i].area, px, py) then
			zoneid = i
			break
		end
	end
	return zoneid;
end

function playerInvitedToFracOnCapture()
	local playerid = getPlayerID(source)
	if checkStartCapture(Players[playerid].frac) then
		playerJoinGameOnCapture(source)
		local CaptureID = Fractions[Players[playerid].frac].captureid
		local px, py = getElementPosition(source)
		if isInsideRadarArea(GangZones[CaptureInfo[CaptureID].ZoneID].area, px, py) then
			if Players[playerid].frac == CaptureInfo[CaptureID].Attack then
				CaptureInfo[CaptureID].AttackCount = CaptureInfo[CaptureID].AttackCount+1;
			elseif Players[playerid].frac == CaptureInfo[CaptureID].Defend then
				CaptureInfo[CaptureID].DefendCount = CaptureInfo[CaptureID].DefendCount+1;
			end
		end
	end
end
addEventHandler("InvitedPlayer", getRootElement(), playerInvitedToFracOnCapture)

--[[
	@playerLeaveFracOnCapture - 
	Ивент срабатывает при увольнении игрока. 
	Если игрок уволен во время капта и на территории GZ, то -1 к колличиству захватчиков или защитников
]]
function playerLeaveFracOnCapture(player)
	local playerid = getPlayerID(player)
	if checkStartCapture(Players[playerid].frac) then
		removeEventHandler("onPlayerQuit", player, playerLeaveFromGameOnCapture)
		removeEventHandler("unInvitePlayer", player, playerLeaveFracOnCapture)
		triggerClientEvent(player, "vievCaptureInfo", player, false)
		local CaptureID = Fractions[Players[playerid].frac].captureid
		local px, py = getElementPosition(player)
		if isInsideRadarArea(GangZones[CaptureInfo[CaptureID].ZoneID].area, px, py) then
			if Players[playerid].frac == CaptureInfo[CaptureID].Attack then
				CaptureInfo[CaptureID].AttackCount = CaptureInfo[CaptureID].AttackCount-1;
			elseif Players[playerid].frac == CaptureInfo[CaptureID].Defend then
				CaptureInfo[CaptureID].DefendCount = CaptureInfo[CaptureID].DefendCount-1;
			end
		end
	end
end

function playerLeaveFromGameOnCapture()
	local playerid = getPlayerID(source)
	if checkStartCapture(Players[playerid].frac) then
		local CaptureID = Fractions[Players[playerid].frac].captureid
		removeEventHandler("onPlayerQuit", source, playerLeaveFromGameOnCapture)
		removeEventHandler("unInvitePlayer", source, playerLeaveFracOnCapture)
		triggerClientEvent(source, "vievCaptureInfo", source, false)
		local px, py = getElementPosition(source)
		if isInsideRadarArea(GangZones[CaptureInfo[CaptureID].ZoneID].area, px, py) then
			if Players[playerid].frac == CaptureInfo[CaptureID].Attack then
				CaptureInfo[CaptureID].AttackCount = CaptureInfo[CaptureID].AttackCount-1;
			elseif Players[playerid].frac == CaptureInfo[CaptureID].Defend then
				CaptureInfo[CaptureID].DefendCount = CaptureInfo[CaptureID].DefendCount-1;
			end
		end
	end
end

function playerJoinGameOnCapture(player)
	local playerid = getPlayerID(player)
	local CaptureID = Fractions[Players[playerid].frac].captureid
	addEventHandler("onPlayerQuit", player, playerLeaveFromGameOnCapture)
	addEventHandler("unInvitePlayer", player, playerLeaveFracOnCapture)
	triggerClientEvent(player, "vievCaptureInfo", player, true)
end

function cmd_capture(player, cmd)
	local playerid = getPlayerID(player)
	if Players[playerid].rank < 8 then return SendPlayerNotice(player, "Вам недоступна данная функция!") end
	if Fractions[Players[playerid].frac]["type"] ~= 4 then return SendPlayerNotice(player, "Вам недоступна данная функция!") end
	if checkStartCapture(Players[playerid].frac) then return SendPlayerNotice(player, "Ваша банда уже участвует в войне за территорию!") end
	local zoneid = getPlayerGangZoneID(player)
	if zoneid == nil then return SendPlayerNotice(player, "Вы должны быть в зоне захвата!") end
	if GangZones[zoneid].owner == Players[playerid].frac then return SendPlayerNotice(player, "Эта территория уже принадлежит вашей банде!") end
	SendToFrac(player, Players[playerid].frac, "#00ffff "..Fractions[Players[playerid].frac].ranks[Players[playerid].rank].." "..Players[playerid]["name"].."["..playerid.."] объявил банде #"..Fractions[getGangZoneOwner(zoneid)]["color"]..""..Fractions[getGangZoneOwner(zoneid)]["name"].." #00ffffвойну за территорию #"..Fractions[getGangZoneOwner(zoneid)]["color"]..""..getZoneName(getElementPosition(player)).."!")
	startCapture(zoneid, Players[playerid].frac, getGangZoneOwner(zoneid))
	
	
end
addCommandHandler("capture", cmd_capture)

function onCapturePlayerWaster(ammo, killer, weapon, bodypart)
	local playerid = getPlayerID(source)
	if Players[playerid].frac ~= 0 and checkStartCapture(Players[playerid].frac) then
		if (getElementType(killer) == "player") then
			local CaptureID = Fractions[Players[playerid].frac].captureid
			local killerid = getPlayerID(killer)
			if Fractions[Players[playerid].frac].captureid == CaptureID then -- Если ID капта убийцы и убитого совпадает
				local pointamount = 0
				--[[
					За убийство противника в зоне находясь в зоне: 2 поинт
					За убийство противника вне зоны находясь в зоне: 1 поинт
					За убийство противника в зоне находясь вне зоны: 1 поинт
					За убийство противника вне зоны находясь вне зоны: 0 поинтов.
				]]
				if getPlayerGangZoneID(killer) == CaptureInfo[CaptureID].ZoneID then
					--Еслий убийца в зоне капта, то прибавляем +1 поинт за убийство из зоны
					pointamount = pointamount+1
				end
				if getPlayerGangZoneID(source) == CaptureInfo[CaptureID].ZoneID then
					--Еслий убитый в зоне капта, то прибавляем +1 поинт за убийство в зоне
					pointamount = pointamount+1
				end
				if Players[killerid].frac == CaptureInfo[CaptureID].Attack then
					CaptureInfo[CaptureID].AttackPoints = CaptureInfo[CaptureID].AttackPoints+pointamount;
				elseif Players[killerid].frac == CaptureInfo[CaptureID].Defend then
					CaptureInfo[CaptureID].DefendPoints = CaptureInfo[CaptureID].DefendPoints+pointamount;
				end
			end	
		end
	end	
end
addEventHandler("onPlayerWasted", getRootElement(), onCapturePlayerWaster)
