--[[
	ToDo:
	 function ShowChat(player, bool)

]]

function PlayerChat(Player,text)
    if pregFind(text, "([0-9]{1,6})[^[:digit:]^[:cntrl:]]{1,3}([0-9]{1,3})[^[:digit:]^[:cntrl:]]{1,3}([0-9]{1,3})[^[:digit:]^[:cntrl:]]{1,3}([0-9]{1,3})") then return end
	local x,y,z = getElementPosition(Player)
	local players = getElementsByType("player")
	local Sphere = createColSphere(x,y,z,15)
	local _Players = getElementsWithinColShape(Sphere, "player")
	local playerid = getPlayerID(Player)
	destroyElement(Sphere)
	local time = getRealTime()
	if time.second < 10 then time.second = "0"..time.second.."" end
	if time.minute < 10 then time.minute = "0"..time.minute.."" end
	if time.hour < 10 then time.hour = "0"..time.hour.."" end
	local message = "["..time.hour..":"..time.minute..":"..time.second.."] "..string.gsub((Players[playerid]["name"].." говорит: "..text), '#%x%x%x%x%x%x', '')
	for index,Player in ipairs( _Players ) do
		triggerClientEvent(Player,"SendClientMessage",Player, message)
	end	
    cancelEvent()
end

function SendMessageToAll(message)
	-- local time = getRealTime()
	-- if time.second < 10 then time.second = "0"..time.second.."" end
	-- message = "["..time.hour..":"..time.minute..":"..time.second.."]"..message..""
	triggerClientEvent("SendClientMessage",getRootElement(), message)
end

function OnPlayerRPChat(source, msg, type)
	local x,y,z = getElementPosition(source)
	local Sphere = createColSphere(x,y,z,15)
	local _Players = getElementsWithinColShape(Sphere, "player")
	destroyElement(Sphere)
	local time = getRealTime()
	if time.second < 10 then time.second = "0"..time.second.."" end
	local message
	if #msg < 3 then return 0 end
	if type == 0 then --/me
		message = "["..time.hour..":"..time.minute..":"..time.second.."] * "..Players[getPlayerID(source)]["name"].." "..msg..""
	elseif type == 1 then -- /do
		message = "["..time.hour..":"..time.minute..":"..time.second.."] * "..msg.." (( "..Players[getPlayerID(source)]["name"].." ))"
	elseif type == 2 then -- /try
		if math.random (0 ,1) == 0 then message = "["..time.hour..":"..time.minute..":"..time.second.."] * "..Players[getPlayerID(source)]["name"].." "..msg.." #00FF00 (Успешно)"
		else message = "["..time.hour..":"..time.minute..":"..time.second.."] * "..Players[getPlayerID(source)]["name"].." "..msg.." #FF0000 (Неудачно)"
		end
	elseif type == 3 then --/b чат
		message = "["..time.hour..":"..time.minute..":"..time.second.."] (("..string.gsub((Players[getPlayerID(source)]["name"].." говорит: "..msg.."))"), '#%x%x%x%x%x%x', '')
		
	end	
	for index,Player in ipairs( _Players ) do 
		if type ~= 3 then
			triggerClientEvent(Player,"SendClientMessage",Player, "#bea0d7"..message.."")
		else
			triggerClientEvent(Player,"SendClientMessage",Player, message)
		end	
	end
end	

---------------------------------------------------------------------------------
function SendToAdminChat(source,msg)
	local playerid = getPlayerID(source)
	for i in ipairs(Players) do
		if Players[i]["playerid"] ~= 0 and Players[i]["admin"] > 0 then
			SendClientMessage("#d3d3d3[A]"..Players[playerid]["name"]..": "..msg.."", Players[i]["player"])
		end	
	end
end

function SendToAdmin(source, msg)
	for i in ipairs(Players) do
		if Players[i]["playerid"] ~= 0 and Players[i]["admin"] > 0 then
			--outputChatBox(msg,Players[i]["player"], 211,211,211,true)
			SendClientMessage("#d3d3d3"..msg.."", Players[i]["player"])
		end	
	end
end

function SendToFrac(source, frac, msg)
	for i in ipairs(Players) do
		if Players[i]["playerid"] ~= 0 and Players[i]["frac"] == frac then
			SendClientMessage("#d3d3d3"..msg.."", Players[i]["player"])
		end	
	end
end

function SendToGosFrac(source, frac, msg)
	for i in ipairs(Players) do
		local fid = Players[i]["frac"]
		if Players[i]["playerid"] ~= 0 and Players[i]["frac"] > 0 and Fractions[fid]["type"] == 1 then
			SendClientMessage("#d3d3d3"..msg.."", Players[i]["player"])
		end	
	end
end

function SendToPoliceFrac(source, msg, r,g,b)
	for i in ipairs(Players) do
		local fid = Players[i]["frac"]
		if Players[i]["playerid"] ~= 0 and Players[i]["frac"] > 1 and Players[i]["frac"] < 3 then
			SendClientMessage("#d3d3d3"..msg.."", Players[i]["player"])
		end	
	end
end






---------------------------Обработка текста и команд----------------------------------

function SendClientMessage(text, Player, color)
	local time = getRealTime()
	if time.second < 10 then time.second = "0"..time.second.."" end
	if not color then
		text = "["..time.hour..":"..time.minute..":"..time.second.."]"..text..""
	else
		text = ""..color.."["..time.hour..":"..time.minute..":"..time.second.."]"..text..""
	end
	if Player ~= nil then
		triggerClientEvent(Player,"SendClientMessage",Player, text)
	else
		triggerClientEvent("SendClientMessage",getRootElement(), text)
	end	
    cancelEvent()
end

addEvent("PlayerChat",true)
addEventHandler("PlayerChat",getRootElement(),PlayerChat)


addEvent("CommmandHandler",true)
addEventHandler("CommmandHandler",getRootElement(), 
	function(Player, text)
		local indexArgs = string.find(text, " ")
		local command
		local argsWithSpaces
		if not indexArgs then  
			command = string.sub(text,2,string.len(text))	
			argsWithSpaces = ""			
		else	
			command = string.sub(text,2,indexArgs-1) 
			argsWithSpaces = string.sub(text,indexArgs+1,string.len(text))  
		end	
		if command == "me" then
			OnPlayerRPChat(Player, argsWithSpaces, 0)
			return 1;
		end
		executeCommandHandler(command, Player, argsWithSpaces)
	end
 )
 
 function ShowChat(player, bool)
	--triggerClientEvent(player,"ShowChat",player, bool)
 end

 function ClearChat(player)
	if not player then
		triggerClientEvent(getElementsByType("player"), "ClearChat", root)
	elseif isPlayerOK(player) then
		triggerClientEvent(player, "ClearChat", root)
	end

 end