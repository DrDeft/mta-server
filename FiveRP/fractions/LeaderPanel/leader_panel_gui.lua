GUILeaderPanel = {
    tab = {},
    tabpanel = {},
    label = {},
    gridlist = {},
    window = {}
}

local LocalFractionID = 1
local LocalFractions 
local font = "fonts/Roboto-Medium.ttf"
local screenW, screenH = guiGetScreenSize()
local members
local onlinemembers
local FVehicles
local LPanelCreated = false
local LPanelViev = false

if screenW < 1281  then 
	Medium = guiCreateFont(font, 8) 
else
	Medium = guiCreateFont(font, 10)
end	

function CreateLeaderPanel()
	if LPanelCreated then return VievLeaderPanel() end
	LPanelCreated = true
	GUILeaderPanel.window[1] = guiCreateWindow(0.24, 0.25, 0.52, 0.51, "Панель лидера", true)
	guiWindowSetSizable(GUILeaderPanel.window[1], false)

	GUILeaderPanel.tabpanel[1] = guiCreateTabPanel(0.01, 0.05, 0.98, 0.93, true, GUILeaderPanel.window[1])

	GUILeaderPanel.tab[1] = guiCreateTab("Информация", GUILeaderPanel.tabpanel[1])

	GUILeaderPanel.label[1] = guiCreateLabel(0.01, 0.06, 0.39, 0.05, "Статистика", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[1], Medium)
	guiLabelSetHorizontalAlign(GUILeaderPanel.label[1], "center", false)
	guiLabelSetVerticalAlign(GUILeaderPanel.label[1], "center")
	GUILeaderPanel.label[2] = guiCreateLabel(0.03, 0.13, 0.21, 0.04, "Членов фракции онлайн:", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[2], Medium)
	GUILeaderPanel.label[3] = guiCreateLabel(0.03, 0.17, 0.17, 0.04, "10", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[3], Medium)
	guiLabelSetColor(GUILeaderPanel.label[3], 80, 203, 51)
	GUILeaderPanel.label[4] = guiCreateLabel(0.03, 0.22, 0.21, 0.04, "Членов фракции:", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[4], Medium)
	GUILeaderPanel.label[5] = guiCreateLabel(0.03, 0.32, 0.21, 0.04, "Действия фракции:", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[5], Medium)
	GUILeaderPanel.label[6] = guiCreateLabel(0.03, 0.26, 0.17, 0.04, "100", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[6], Medium)
	guiLabelSetColor(GUILeaderPanel.label[6], 80, 203, 51)
	GUILeaderPanel.label[7] = guiCreateLabel(0.03, 0.36, 0.17, 0.04, "17", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[7], Medium)
	guiLabelSetColor(GUILeaderPanel.label[7], 80, 203, 51)
	GUILeaderPanel.label[8] = guiCreateLabel(0.02, 0.48, 0.39, 0.05, "Деньги фракции", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[8], Medium)
	guiLabelSetHorizontalAlign(GUILeaderPanel.label[8], "center", false)
	guiLabelSetVerticalAlign(GUILeaderPanel.label[8], "center")
	GUILeaderPanel.label[9] = guiCreateLabel(0.03, 0.54, 0.21, 0.04, "Баланс фракции:", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[9], Medium)
	GUILeaderPanel.label[10] = guiCreateLabel(0.03, 0.58, 0.17, 0.04, "$527512", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[10], Medium)
	guiLabelSetColor(GUILeaderPanel.label[10], 80, 203, 51)
	GUILeaderPanel.label[11] = guiCreateLabel(0.03, 0.64, 0.21, 0.04, "Доход фракции:", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[11], Medium)
	GUILeaderPanel.label[12] = guiCreateLabel(0.03, 0.68, 0.17, 0.04, "$28700", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[12], Medium)
	guiLabelSetColor(GUILeaderPanel.label[12], 80, 203, 51)
	GUILeaderPanel.label[13] = guiCreateLabel(0.03, 0.74, 0.21, 0.04, "Расходы", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[13], Medium)
	GUILeaderPanel.label[14] = guiCreateLabel(0.03, 0.78, 0.17, 0.04, "$15700", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[14], Medium)
	guiLabelSetColor(GUILeaderPanel.label[14], 80, 203, 51)
	GUILeaderPanel.label[15] = guiCreateLabel(0.03, 0.84, 0.21, 0.04, "Другое", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[15], Medium)
	GUILeaderPanel.label[16] = guiCreateLabel(0.03, 0.88, 0.17, 0.04, "$35", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[16], Medium)
	guiLabelSetColor(GUILeaderPanel.label[16], 80, 203, 51)
	GUILeaderPanel.label[17] = guiCreateLabel(0.53, 0.06, 0.39, 0.05, "О фракции", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[17], Medium)
	guiLabelSetHorizontalAlign(GUILeaderPanel.label[17], "center", false)
	guiLabelSetVerticalAlign(GUILeaderPanel.label[17], "center")
	GUILeaderPanel.label[18] = guiCreateLabel(0.54, 0.13, 0.21, 0.04, "Название фракции:", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[18], Medium)
	GUILeaderPanel.label[19] = guiCreateLabel(0.54, 0.17, 0.24, 0.04, "San Andreas Police Departament", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[19], Medium)
	guiLabelSetColor(GUILeaderPanel.label[19], 80, 203, 51)
	GUILeaderPanel.label[20] = guiCreateLabel(0.54, 0.22, 0.21, 0.04, "Выговоры лидера:", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[20], Medium)
	GUILeaderPanel.label[21] = guiCreateLabel(0.54, 0.26, 0.24, 0.04, "Wen Fox", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[21], Medium)
	guiLabelSetColor(GUILeaderPanel.label[21], 80, 203, 51)
	GUILeaderPanel.label[22] = guiCreateLabel(0.54, 0.32, 0.24, 0.04, "Лидер с:", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[22], Medium)
	GUILeaderPanel.label[23] = guiCreateLabel(0.54, 0.36, 0.17, 0.04, "20.12.2016", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[23], Medium)
	guiLabelSetColor(GUILeaderPanel.label[23], 80, 203, 51)
	GUILeaderPanel.label[24] = guiCreateLabel(0.78, 0.13, 0.17, 0.04, " ", true, GUILeaderPanel.tab[1]) -- пусто
	guiSetFont(GUILeaderPanel.label[24], Medium)
	GUILeaderPanel.label[25] = guiCreateLabel(0.78, 0.16, 0.17, 0.04, " ", true, GUILeaderPanel.tab[1]) -- пусто
	guiSetFont(GUILeaderPanel.label[25], Medium)
	guiLabelSetColor(GUILeaderPanel.label[25], 80, 203, 51)
	GUILeaderPanel.label[26] = guiCreateLabel(0.53, 0.48, 0.39, 0.05, "Состояние склада", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[26], Medium)
	guiLabelSetHorizontalAlign(GUILeaderPanel.label[26], "center", false)
	guiLabelSetVerticalAlign(GUILeaderPanel.label[26], "center")
	GUILeaderPanel.label[27] = guiCreateLabel(0.54, 0.55, 0.21, 0.04, "Параметр 1:", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[27], Medium)
	GUILeaderPanel.label[28] = guiCreateLabel(0.54, 0.59, 0.21, 0.04, "5292", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[28], Medium)
	guiLabelSetColor(GUILeaderPanel.label[28], 80, 203, 51)
	GUILeaderPanel.label[29] = guiCreateLabel(0.54, 0.65, 0.21, 0.04, "Параметр 2:", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[29], Medium)
	GUILeaderPanel.label[30] = guiCreateLabel(0.54, 0.69, 0.21, 0.04, "3123", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[30], Medium)
	guiLabelSetColor(GUILeaderPanel.label[30], 80, 203, 51)
	GUILeaderPanel.label[31] = guiCreateLabel(0.54, 0.75, 0.21, 0.04, "Параметр 3:", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[31], Medium)
	GUILeaderPanel.label[32] = guiCreateLabel(0.54, 0.79, 0.21, 0.04, "293", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[32], Medium)
	guiLabelSetColor(GUILeaderPanel.label[32], 80, 203, 51)
	GUILeaderPanel.label[33] = guiCreateLabel(0.54, 0.85, 0.21, 0.04, "Параметр 4:", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[33], Medium)
	GUILeaderPanel.label[34] = guiCreateLabel(0.54, 0.89, 0.21, 0.04, "375", true, GUILeaderPanel.tab[1])
	guiSetFont(GUILeaderPanel.label[34], Medium)
	guiLabelSetColor(GUILeaderPanel.label[34], 80, 203, 51)

	GUILeaderPanel.tab[2] = guiCreateTab("Состав", GUILeaderPanel.tabpanel[1])

	GUILeaderPanel.gridlist[1] = guiCreateGridList(0.00, 0.00, 1.00, 1.00, true, GUILeaderPanel.tab[2])
	guiGridListAddColumn(GUILeaderPanel.gridlist[1], "Ник", 0.2)
	guiGridListAddColumn(GUILeaderPanel.gridlist[1], "Последний визит", 0.2)
	guiGridListAddColumn(GUILeaderPanel.gridlist[1], "Ранг", 0.3)
	guiGridListAddColumn(GUILeaderPanel.gridlist[1], "Пост", 0.2)
	-- for i = 1, 2 do
		-- guiGridListAddRow(GUILeaderPanel.gridlist[1])
	-- end
	-- guiGridListSetItemText(GUILeaderPanel.gridlist[1], 0, 1, "Wen_Fox", false, false)
	-- guiGridListSetItemText(GUILeaderPanel.gridlist[1], 0, 2, "Онлайн", false, false)
	-- guiGridListSetItemColor(GUILeaderPanel.gridlist[1], 0, 2, 0, 254, 11, 255)
	-- guiGridListSetItemText(GUILeaderPanel.gridlist[1], 0, 3, "20 - Генерал", false, false)
	-- guiGridListSetItemText(GUILeaderPanel.gridlist[1], 0, 4, "20 - Лидер", false, false)
	-- guiGridListSetItemText(GUILeaderPanel.gridlist[1], 1, 1, "Alan_Konors", false, false)
	-- guiGridListSetItemText(GUILeaderPanel.gridlist[1], 1, 2, "20.12.2016", false, false)
	-- guiGridListSetItemText(GUILeaderPanel.gridlist[1], 1, 3, "1 - Рекрут", false, false)
	-- guiGridListSetItemText(GUILeaderPanel.gridlist[1], 1, 4, "1 - Пустой пост", false, false)

	GUILeaderPanel.tab[3] = guiCreateTab("Ранги и посты", GUILeaderPanel.tabpanel[1])
	GUILeaderPanel.tab[4] = guiCreateTab("Транспорт", GUILeaderPanel.tabpanel[1])   
	--------------------Список авто----------------------------------------------
	GUILeaderPanel.gridlist[2] = guiCreateGridList(0.00, 0.00, 1.00, 1.00, true, GUILeaderPanel.tab[4])
	guiGridListAddColumn(GUILeaderPanel.gridlist[2], "Марка", 0.4)
	guiGridListAddColumn(GUILeaderPanel.gridlist[2], "Номер", 0.3)
	LeaderPanelUpdateInfo()
	LPanelViev = true
end
addEvent("CreateLeaderPanel", true)
addEventHandler("CreateLeaderPanel", root, CreateLeaderPanel) 

function VievLeaderPanel()
	if not LPanelViev then 
		LPanelViev = true
		--Показ
	else
		LPanelViev = false
	-- закрытие
	end
	
	for i in ipairs(GUILeaderPanel.tab) do
		guiSetVisible(GUILeaderPanel.tab[i], LPanelViev)
	end	
	for i in ipairs(GUILeaderPanel.tabpanel) do
		guiSetVisible(GUILeaderPanel.tabpanel[i], LPanelViev)
	end
	for i in ipairs(GUILeaderPanel.label) do
		guiSetVisible(GUILeaderPanel.label[i], LPanelViev)
	end
	for i in ipairs(GUILeaderPanel.gridlist) do
		guiSetVisible(GUILeaderPanel.gridlist[i], LPanelViev)
	end
	for i in ipairs(GUILeaderPanel.window) do
		guiSetVisible(GUILeaderPanel.window[i], LPanelViev)
	end
end
function LeaderPanelUpdateInfo()
	triggerServerEvent("SendToLocalFractionData", resourceRoot, resourceRoot)
	setTimer (LeaderPanelUpdateInfos, 400, 1)
end

function LeaderPanelUpdateInfos()
	guiSetText ( GUILeaderPanel.label[3], ""..#onlinemembers.."" ) -- Онлайн членов фракции
	guiSetText ( GUILeaderPanel.label[6], ""..members.."" ) -- Членов фракции
	guiSetText ( GUILeaderPanel.label[7], ""..LocalFractions[LocalFractionID]["action"].."" ) -- Действия фракции(вместо авто)
	guiSetText ( GUILeaderPanel.label[10], "$"..LocalFractions[LocalFractionID]["safe_money"].."" ) -- Деньги фракции
	guiSetText ( GUILeaderPanel.label[12], "$"..LocalFractions[LocalFractionID]["income"].."" ) -- Доход
	guiSetText ( GUILeaderPanel.label[16], "$0" ) -- Поле "другое"
	guiSetText ( GUILeaderPanel.label[19], ""..LocalFractions[LocalFractionID]["name"].."" ) -- Название фракции
	guiSetText ( GUILeaderPanel.label[21],""..LocalFractions[LocalFractionID]["ownerwarn"].."/3" ) --  Варны лидера
	guiSetText ( GUILeaderPanel.label[23], ""..LocalFractions[LocalFractionID]["ownerdata"].."" ) -- Лидер с
	--guiSetText ( GUILeaderPanel.label[25], ""..LocalFractions[LocalFractionID]["ownerwarn"].."/3" ) -- Варны лидера
	
	--------------------------------Состав----------------------------------------------------------------------------
	for pid in ipairs(onlinemembers) do
		guiGridListAddRow(GUILeaderPanel.gridlist[1], onlinemembers[pid]["name"], "20.20.20", onlinemembers[pid]["rank"], onlinemembers[pid]["post"])
	end
	-------------------------------Авто фракци-------------------------------------------------------------------------
	for vid in ipairs(FVehicles) do
		guiGridListAddRow(GUILeaderPanel.gridlist[2], getVehicleName ( FVehicles[vid]["vehicle"] ), FVehicles[vid]["id"])
	end
end

function LocalSyncFractionData(data, onlinemember, member, fvehicle)
	LocalFractions = data
	onlinemembers = onlinemember
	members = member
	FVehicles = fvehicle
end
addEvent("LocalSyncFractionData", true)
addEventHandler("LocalSyncFractionData", root, LocalSyncFractionData) 