---------------------Команды лидеров--------------------------------------
function cmd_fmenu(player, cmd)
	local playerid = getPlayerID(player)
	if not Players[playerid]["leader"] then return SendPlayerNotice(player, "Вы должны быть лидером фракции!") end
end
addCommandHandler("fmenu", cmd_fmenu)

function cmd_setrank(player, cmd, pid, rank)
	local playerid = getPlayerID(player)
	if not rank then return outputChatBox("Пиши:/setrank [id] [Номер ранга]") end
	pid = tonumber(pid)
	rank = tonumber(rank)
	if not Players[playerid]["leader"] then return SendPlayerNotice(player, "Недостаточно прав!") end
	if rank < 1 or rank > 20 then return outputChatBox("Пиши:/setrank [id] [Номер ранга]") end
	if not iSPlayer(pid) then return SendPlayerNotice(player, "Игрок с данным ID не найден!") end	
	if Players[pid]["frac"] ~= Players[playerid]["frac"] then return SendPlayerNotice(player, "Этот игрок не в вашей фракции!") end
	SetRank(player, pid, rank)
end
addCommandHandler("setrank", cmd_setrank)

function cmd_setpost(player, cmd, pid, post)
	local playerid = getPlayerID(player)
	if not post then return outputChatBox("Пиши:/setrank [id] [Номер ранга]") end
	pid = tonumber(pid)
	post = tonumber(post)
	if post < 1 or post > 20 then return outputChatBox("Пиши:/setrank [id] [Номер ранга]") end
	if not iSPlayer(pid) then return SendPlayerNotice(player, "Игрок с данным ID не найден!") end	
	if not Players[playerid]["leader"] then return SendPlayerNotice(player, "Недостаточно прав!") end
	if Players[pid]["frac"] ~= Players[playerid]["frac"] then return SendPlayerNotice(player, "Этот игрок не в вашей фракции!") end
	SetPost(player, pid, post)
end
addCommandHandler("setpost", cmd_setpost)
---------------------------------------------------------------------------

function cmd_r(player, cmd, ...)
	local playerid = getPlayerID(player)
	local fid = Players[playerid]["frac"]
	if Players[playerid]["frac"] == nil or Players[playerid]["frac"] < 1 then return SendPlayerNotice(player, "У вас нет доступа к этой команде!") end
	local text = table.concat({...}, " ")
	if ... == nil then return outputChatBox("#AAAAAAПиши: /r [текст]", player) end
	text = string.gsub((""..text..""), '#%x%x%x%x%x%x', '')
	text = "**"..Fractions[fid]["ranks"][Players[playerid]["rank"]].." "..Players[playerid]["name"].."["..playerid.."] (рация): "..text.." **"
	SendToFrac(player, Players[playerid]["frac"], text, 255, 255, 255)
end
addCommandHandler("r", cmd_r)

function cmd_rb(player, cmd, ...)
	local playerid = getPlayerID(player)
	local fid = Players[playerid]["frac"]
	if Players[playerid]["frac"] == nil or Players[playerid]["frac"] < 1 then return SendPlayerNotice(player, "У вас нет доступа к этой команде!") end
	local text = table.concat({...}, " ")
	if ... == nil then return outputChatBox("#AAAAAAПиши: /rb [текст]", player) end
	text = string.gsub((""..text..""), '#%x%x%x%x%x%x', '')
	text = "(("..Fractions[fid]["ranks"][Players[playerid]["rank"]].." "..Players[playerid]["name"].."["..playerid.."] (рация): "..text.." ))"
	SendToFrac(player, Players[playerid]["frac"], text, 255, 255, 255)
end
addCommandHandler("rb", cmd_rb)

function cmd_d(player, cmd, ...)
	local playerid = getPlayerID(player)
	local fid = Players[playerid]["frac"]
	if Players[playerid]["frac"] == nil or Players[playerid]["frac"] < 1 and Fractions[fid]["type"] ~= 1 then return SendPlayerNotice(player, "У вас нет доступа к этой команде!") end
	if ... == nil then return outputChatBox("#AAAAAAПиши: /d [текст]", player) end
	local text = table.concat({...}, " ")
	text = string.gsub((""..text..""), '#%x%x%x%x%x%x', '')
	text = "#"..Fractions[fid]["color"]..""..Fractions[fid]["ranks"][Players[playerid]["rank"]].." "..Players[playerid]["name"].."["..playerid.."] (рация): "..text.." **"
	SendToGosFrac(player, Players[playerid]["frac"], text)
end
addCommandHandler("d", cmd_d)

function cmd_db(player, cmd, ...)
	local playerid = getPlayerID(player)
	local fid = Players[playerid]["frac"]
	if Players[playerid]["frac"] == nil or Players[playerid]["frac"] < 1 and Fractions[fid]["type"] ~= 1 then return SendPlayerNotice(player, "У вас нет доступа к этой команде!") end
	if ... == nil then return outputChatBox("#AAAAAAПиши: /db [текст]", player) end
	local text = table.concat({...}, " ")
	text = string.gsub((""..text..""), '#%x%x%x%x%x%x', '')
	text = "#"..Fractions[fid]["color"].."(("..Fractions[fid]["ranks"][Players[playerid]["rank"]].." "..Players[playerid]["name"].."["..playerid.."] (рация): "..text.." ))"
	SendToGosFrac(player, Players[playerid]["frac"], text)
end
addCommandHandler("db", cmd_db)

function cmd_gov(player, cmd, ...)
	local playerid = getPlayerID(player)
	local fid = Players[playerid]["frac"]
	if Players[playerid]["frac"] == nil or Players[playerid]["frac"] < 1 and Fractions[fid]["type"] ~= 1 then return SendPlayerNotice(player, "У вас нет доступа к этой команде!") end
	if ... == nil then return outputChatBox("#AAAAAAПиши: /db [текст]", player) end
	local text = table.concat({...}, " ")
	text = string.gsub((""..text..""), '#%x%x%x%x%x%x', '')
	text = "#"..Fractions[fid]["color"].."[Гос. Новости] "..Fractions[fid]["ranks"][Players[playerid]["rank"]].." "..Players[playerid]["name"].."["..playerid.."] "..text..""
	SendMessageToAll(text);
end
addCommandHandler("gov", cmd_gov)


function leaderpanel(player, cmd)
	--triggerClientEvent ( toplayer, "LocalSyncFractionData", player, Fractions)
	triggerClientEvent (player, "CreateLeaderPanel", player)
end

addCommandHandler("panel", leaderpanel)

function cmd_members(player, cmd)
	local playerid = getPlayerID(player)
	if Players[playerid]["frac"] == nil or Players[playerid]["frac"] == 0 then return SendPlayerNotice(player, "У вас нет доступа к этой команде!") end
	FracPlayers = GetFractionPlayers(Players[playerid]["frac"])
	outputChatBox ( "Члены фракции #0dc426"..Fractions[Players[playerid]["frac"]]["name"].."#ffffff онлайн:", player, 255, 255, 255)
	for i in ipairs(FracPlayers) do
		outputChatBox ( ""..getPlayerName (FracPlayers[i]).."", player, 255, 255, 255, true )
	end
	outputChatBox ( "Всего в сети #0dc426"..#FracPlayers.."#ffffff человек.", player, 255, 255, 255, true )
end

addCommandHandler("members", cmd_members)

function cmd_invite(player, cmd, pid)
	local playerid = getPlayerID(player)
	local pid = tonumber(pid)
	if Players[playerid]["frac"] == nil or Players[playerid]["frac"] == 0 then return SendPlayerNotice(player, "У вас нет доступа к этой команде!") end
	if not Players[playerid]["leader"] then return SendPlayerNotice(player, "Вы должны быть лидером фракции!") end
	if pid == nil then return outputChatBox("#AAAAAAПиши: /invite [id]") end
	if not iSPlayerOK(pid) then return SendPlayerNotice(player, "Игрок не авторизован!") end
	if not checkPlayerTradeStatus(playerid) then return SendPlayerNotice(player, "Игрок уже рассматривает другое предложение!") end
	if pid == playerid then return SendPlayerNotice(player, "Вы указали свой ID") end
	if Players[playerid].frac ~= 0 then return SendPlayerNotice(player, "Игрок уже состоит во фракции!") end
	sendTradeOffer("#0dc426"..Players[playerid].name.." #ffffffпредлагает вам вступить во фракцию #0dc426"..Fractions[Players[playerid]["frac"]]["name"].."#ffffff\nВы согласны?", 1, playerid, pid, Players[playerid]["frac"], 0)
	--TParams[playerid].YouTradeOffer = pid
	--TParams[pid].TradeOffer = playerid
	--TParams[pid].ObjectTradeID = Players[playerid]["frac"]
	--Players[pid]["orderboxid"] = exports.OrderBox:ShowPlayerOrderBox(Players[pid]["player"], 1, "#0dc426"..Players[playerid].name.." #ffffffпредлагает вам вступить во фракцию #0dc426"..Fractions[Players[playerid]["frac"]]["name"].."#ffffff\nВы согласны?");
end
addCommandHandler("invite", cmd_invite)

function cmd_uninvite(player, cmd, pid)
	local playerid = getPlayerID(player)
	local pid = tonumber(pid)
	if Players[playerid]["frac"] == nil or Players[playerid]["frac"] == 0 then return SendPlayerNotice(player, "У вас нет доступа к этой команде!") end
	if not Players[playerid]["leader"] then return SendPlayerNotice(player, "Вы должны быть лидером фракции!") end
	if pid == nil then return outputChatBox("#AAAAAAПиши: /uninvite [id]") end
	if not iSPlayerOK(pid) then return SendPlayerNotice(player, "Игрок не авторизован!") end
	if (Players[pid].frac ~= Players[playerid]["frac"]) or Players[pid].leader then return SendPlayerNotice(player, "Игрок не в вашей фракции или является лидером!") end
	unInvitePlayer(Players[pid].player)
	outputChatBox("Вы были исключены из фракции лидером "..Players[playerid].name.."!", Players[pid].player)
end
addCommandHandler("uninvite", cmd_uninvite)