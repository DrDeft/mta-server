GUIInvite = {
    label = {},
    staticimage = {}
}
local screenW, screenH = guiGetScreenSize()
local viev = false
local created = false
local frac = 0;
local font = "fonts/Roboto-Medium.ttf"
if screenW < 1600 then
	Medium = guiCreateFont(font, 10)
	dxMedium = dxCreateFont(font, 9)
else 
	Medium = guiCreateFont(font, 11) 
	dxMedium = dxCreateFont(font, 11)
	end	

function CreateGUIInvite()
	GUIInvite.staticimage[1] = guiCreateStaticImage(0.36, 0.42, 0.28, 0.16, ":FiveRP/fractions/images/window.png", true)
		
	GUIInvite.label[2] = guiCreateLabel(0.00, 0.00, 1.00, 0.16, "Приглашение во фракцию", true, GUIInvite.staticimage[1])
	guiSetFont(GUIInvite.label[2], Medium)
	guiLabelSetHorizontalAlign(GUIInvite.label[2], "center", false)
	guiLabelSetVerticalAlign(GUIInvite.label[2], "center")
	GUIInvite.staticimage[2] = guiCreateStaticImage(0.68, 0.75, 0.30, 0.20, ":FiveRP/house/images/bottom.png", true, GUIInvite.staticimage[1])
	guiSetProperty(GUIInvite.staticimage[2], "ImageColours", "tl:FF000000 tr:FF000000 bl:FF000000 br:FF000000")

	GUIInvite.label[3] = guiCreateLabel(0.03, 0.12, 0.91, 0.76, "Отмена", true, GUIInvite.staticimage[2])
	guiSetFont(GUIInvite.label[3], Medium)
	guiLabelSetHorizontalAlign(GUIInvite.label[3], "center", false)
	guiLabelSetVerticalAlign(GUIInvite.label[3], "center")

	GUIInvite.staticimage[3] = guiCreateStaticImage(0.36, 0.75, 0.30, 0.20, ":FiveRP/house/images/bottom.png", true, GUIInvite.staticimage[1])
	guiSetProperty(GUIInvite.staticimage[3], "ImageColours", "tl:FF000000 tr:FF000000 bl:FF000000 br:FF000000")

	GUIInvite.label[4] = guiCreateLabel(0.03, 0.12, 0.91, 0.76, "Принять", true, GUIInvite.staticimage[3])
	guiSetFont(GUIInvite.label[4], Medium)
	guiLabelSetHorizontalAlign(GUIInvite.label[4], "center", false)
	guiLabelSetVerticalAlign(GUIInvite.label[4], "center") 
	guiSetEnabled(GUIInvite.label[3], false)
	guiSetEnabled(GUIInvite.label[4], false)
	created = true
end

addEventHandler("onClientRender", root,
    function()
		if viev then
			if screenW < 1600 then
				dxDrawText("#0dc426Wen_Fox #ffffffпредлагает вам вступить во фракцию:\n#0b4fedSan Andreas Police Departament\n#ffffffВы хотите принять это предложение?", screenW * 0.3677, screenH * 0.4546, screenW * 0.6323, screenH * 0.5398, tocolor(255, 255, 255, 255), 1.00, dxMedium, "center", "top", false, true, true, true, false)
			else
				dxDrawText("Лидер #0dc426Wen_Fox #ffffffпредлагает вам вступить во фракцию:\n#0b4fedSan Andreas Police Departament\n#ffffffВы хотите принять это предложение?", screenW * 0.3677, screenH * 0.4546, screenW * 0.6323, screenH * 0.5398, tocolor(255, 255, 255, 255), 1.00, dxMedium, "center", "top", false, true, true, true, false)
			end
		end
	end
)

function VievInviteGUI(bool, fracs)
	if not created then CreateGUIInvite()
	else
		for i in ipairs(GUIInvite.staticimage) do
			guiSetVisible(GUIInvite.staticimage[i], bool)
		end
		for i in ipairs(GUIInvite.label) do
			guiSetVisible(GUIInvite.label[i], bool)
		end
	end
	
	if bool then
		viev = true 
		frac = fracs
	else 
		viev = false
		if fracs == nil then fracs = 0 end
	end
end
addEvent("VievInviteGUI", true)
addEventHandler("VievInviteGUI", root, VievInviteGUI) 

addEventHandler("onClientMouseEnter", root, function() --Событие при наведении на GUI-элемент

	if not viev then return 0
	else
		if source == GUIInvite.staticimage[2] or source == GUIInvite.staticimage[3] then
			guiSetProperty(source, "ImageColours", "tl:FF000115 tr:FF000115 bl:FF000115 br:FF000115")
		end
	end
end)

addEventHandler("onClientMouseLeave", root, function() --Событие при наведении на GUI-элемент

	if not viev then return 0
	else
		if source == GUIInvite.staticimage[2] or source == GUIInvite.staticimage[3] then
			guiSetProperty(source, "ImageColours", "tl:FF000000 tr:FF000000 bl:FF000000 br:FF000000")
		end
	end
end)

addEventHandler("onClientGUIClick", root, function() --Событие при нажатие на GUI-элемент
	if not viev then return 0
	else
		if source == GUIInvite.staticimage[2] then
			triggerServerEvent ( "AcceptInvite", getLocalPlayer(), getLocalPlayer(), frac, false )
			guiSetProperty(source, "ImageColours", "tl:FF000000 tr:FF000000 bl:FF000000 br:FF000000")
		elseif source == GUIInvite.staticimage[3] then
			triggerServerEvent ( "AcceptInvite", getLocalPlayer(), getLocalPlayer(), frac, true )
			guiSetProperty(source, "ImageColours", "tl:FF000000 tr:FF000000 bl:FF000000 br:FF000000")
		end
	end

end)

