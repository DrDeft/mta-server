MaxFractions = 0

---------------------------------------------------------
function LoadFractions()
	local query = dbQuery(SQL, "SELECT * FROM `fractions`")
	local resultTable, num, err = dbPoll (query, -1 ) 
	local tmale_skin, tfemale_skin, tposts, tskins, tsalary
	if resultTable then 
	for i, row in ipairs(resultTable) do 
		--Данные считаны с БД
		Fractions[i] = {ID, MySQLID, name, ownername, safe_money, tax, income = 0, action = 0, purchase, type, spawn_int, spawn_vw, color, spawn_pos = {}, male_skins = {}, female_skins ={}, posts = {}, ranks = {}, salary, ownerdata = 0, ownerwarn = 0}
		Fractions[i]["ID"] = i
		Fractions[i]["MySQLID"] = tonumber(row['id'])
		Fractions[i]["name"] = tostring(row['name'])
		Fractions[i]["safe_money"] = tonumber(row['safe_money'])
		Fractions[i]["tax"] = tonumber(row['tax'])
		Fractions[i]["type"] = tonumber(row['type'])
		Fractions[i]["spawn_int"] = tonumber(row['spawn_int'])
		Fractions[i]["spawn_vw"] = tonumber(row['spawn_vw'])
		Fractions[i]["color"] = row['color']
		Fractions[i]["ownerdata"] = row['ownerdata']
		Fractions[i]["ownerwarn"] = row['ownerwarn']
		---------------------------------------------
		tmale_skin = tostring(row['male_skins'])
		tfemale_skin = tostring(row['female_skins'])
		tposts = tostring(row['posts'])
		tskins = tostring(row['ranks'])
		tsalary = row['salary']
		---------------------------------------------
		Fractions[i]["salary"] = explode("|", tsalary)
		local male_skin = explode("|", tmale_skin)
		local female_skins = explode("|", tfemale_skin)
		local posts = explode("|", tposts)
		local ranks = explode("|", tskins)
		for x in ipairs(posts) do
			Fractions[i]["male_skins"][x] = male_skin[x]
			Fractions[i]["female_skins"][x] = female_skins[x]
			Fractions[i]["posts"][x] = posts[x]
			Fractions[i]["ranks"][x] = ranks[x]
		end
		MaxFractions = MaxFractions+1
	end 
	loadGangCommon()
	outputDebugString("Фракции успешно загружены! Загружено "..MaxFractions.." фракций!")
	
	end

end	
function SaveFractions()
	for i in ipairs(Fractions) do
		--local i = 1
		local ms = Fractions[i]["male_skins"]
		local fs = Fractions[i]["female_skins"]
		local p = Fractions[i]["posts"]
		local r = Fractions[i]["ranks"]
		dbExec(SQL, "UPDATE `fractions` SET \
		`name` = '"..Fractions[i]["name"].."', \
		`safe_money` = '"..Fractions[i]["safe_money"].."', \
		`tax` = '"..Fractions[i]["tax"].."', \
		`color` = '"..Fractions[i]["color"].."', \
		`type` = '"..Fractions[i]["type"].."', \
		`spawn_int` = '"..Fractions[i]["spawn_int"].."', \
		`spawn_vw` = '"..Fractions[i]["spawn_vw"].."', \
		`male_skins` = '"..ms[1].."|"..ms[2].."|"..ms[3].."|"..ms[4].."|"..ms[5].."|"..ms[6].."|"..ms[7].."|"..ms[8].."|"..ms[9].."|"..ms[10].."|"..ms[11].."|"..ms[12].."|"..ms[13].."|"..ms[14].."|"..ms[15].."|"..ms[16].."|"..ms[17].."|"..ms[18].."|"..ms[19].."|"..ms[20].."', \
		`female_skins` = '"..fs[1].."|"..fs[2].."|"..fs[3].."|"..fs[4].."|"..fs[5].."|"..fs[6].."|"..fs[7].."|"..fs[8].."|"..fs[9].."|"..fs[10].."|"..fs[11].."|"..fs[12].."|"..fs[13].."|"..fs[14].."|"..fs[15].."|"..fs[16].."|"..fs[17].."|"..fs[18].."|"..fs[19].."|"..fs[20].."', \
		`posts` = '"..p[1].."|"..p[2].."|"..p[3].."|"..p[4].."|"..p[5].."|"..p[6].."|"..p[7].."|"..p[8].."|"..p[9].."|"..p[10].."|"..p[11].."|"..p[12].."|"..p[13].."|"..p[14].."|"..p[15].."|"..p[16].."|"..p[17].."|"..p[18].."|"..p[19].."|"..p[20].."', \
		`ranks` = '"..r[1].."|"..r[2].."|"..r[3].."|"..r[4].."|"..r[5].."|"..r[6].."|"..r[7].."|"..r[8].."|"..r[9].."|"..r[10].."|"..r[11].."|"..r[12].."|"..r[13].."|"..r[14].."|"..r[15].."|"..r[16].."|"..r[17].."|"..r[18].."|"..r[19].."|"..r[20].."' \
		WHERE `id` = '"..Fractions[i]["MySQLID"].."' LIMIT 1")
		ms = nil
		fs = nil
		p = nil
		r = nil
		sa = nil
	end
	SaveFracSalary()
	outputDebugString("Фракции успешно сохранены!")
end
function SaveFracSalary()
	for i in ipairs(Fractions) do
		local sa = Fractions[i]["salary"]
		dbExec(SQL, "UPDATE `fractions` SET \
		`salary` = '"..sa[1].."|"..sa[2].."|"..sa[3].."|"..sa[4].."|"..sa[5].."|"..sa[6].."|"..sa[7].."|"..sa[8].."|"..sa[9].."|"..sa[10].."|"..sa[11].."|"..sa[12].."|"..sa[13].."|"..sa[14].."|"..sa[15].."|"..sa[16].."|"..sa[17].."|"..sa[18].."|"..sa[19].."|"..sa[20].."' \
		WHERE `id` = '"..Fractions[i]["MySQLID"].."' LIMIT 1")
	end
end


function SetRank(source, pid, rank)
	local playerid = getPlayerID(source)
	if not Players[playerid]["leader"] then return SendPlayerNotice(player, "Недостаточно прав!") end
	if Players[pid]["frac"] ~= Players[playerid]["frac"] then return SendPlayerNotice(player, "Этот игрок не в вашей фракции!") end
	local frac = Players[pid]["frac"]
	outputChatBox("Вы изменили ранг игроку #0dc426"..Players[pid]["name"].."#ffffff c #0dc426"..Fractions[frac]["ranks"][Players[pid]["rank"]].."#ffffff на #0dc426"..Fractions[frac]["ranks"][rank].."", source, 255, 255, 255, true)
	outputChatBox("#0dc426"..Players[playerid]["name"].."#ffffff сменил ваш ранг c #0dc426"..Fractions[frac]["ranks"][Players[pid]["rank"]].."#ffffff на #0dc426"..Fractions[frac]["ranks"][rank].."", Players[pid]["player"], 255, 255, 255, true)
	Players[pid]["rank"] = rank
end

function SetPost(source, pid, post)
	local playerid = getPlayerID(source)
	if not Players[playerid]["leader"] then return SendPlayerNotice(player, "Недостаточно прав!") end
	if Players[pid]["frac"] ~= Players[playerid]["frac"] then return SendPlayerNotice(player, "Этот игрок не в вашей фракции!") end
	local frac = Players[pid]["frac"]
	outputChatBox("Вы изменили пост игроку #0dc426"..Players[pid]["name"].."#ffffff c #0dc426"..Fractions[frac]["posts"][Players[pid]["post"]].."#ffffff на #0dc426"..Fractions[frac]["posts"][post].."", source, 255, 255, 255, true)
	
	outputChatBox("#0dc426"..Players[playerid]["name"].."#ffffff сменил ваш пост c #0dc426"..Fractions[frac]["posts"][Players[pid]["post"]].."#ffffff на #0dc426"..Fractions[frac]["posts"][post].."", Players[pid]["player"], 255, 255, 255, true)
	Players[pid]["post"] = post
end

function AcceptInvite(source, frac)
	local playerid = getPlayerID(source)
	Players[playerid]["frac"] = frac
	Players[playerid]["rank"] = 1
	Players[playerid]["post"] = 1
	outputChatBox("Вы были приняты во фракцию: #0dc426"..Fractions[frac]["name"].."", source, 255, 255, 255, true)
	triggerEvent("InvitedPlayer", source)
end
addEvent("AcceptInvite", true)
addEventHandler("AcceptInvite", root, AcceptInvite) 

function unInvitePlayer(player)
	triggerEvent("unInvitePlayer", player, player) 
	local playerid = getPlayerID(player)
	Players[playerid]["frac"] = 0
	Players[playerid]["rank"] = 0
	Players[playerid]["post"] = 0
	Players[playerid]["leader"] = false
end

function StartJob(source)
end

function SendToLocalFractionData(toplayer)
	local playerid = getPlayerID(toplayer)
	local FOnline = GetFractionPlayers(Players[1]["frac"])
	local FVehicle = GetFractionVehicle(Players[1]["frac"])
	triggerClientEvent ("LocalSyncFractionData", toplayer, Fractions, FOnline, 25, FVehicle)
end
addEvent("SendToLocalFractionData", true)
addEventHandler("SendToLocalFractionData", root, SendToLocalFractionData) 

function AcceptHeal(player, bool)
	local playerid = getPlayerID(player)
	if bool then
		if Players[playerid]["money"] < TParams[playerid].TradeMoney then return SendPlayerNotice(player, "У вас недостаточно средств!") end
		setElementHealthAC(player, 100.0)
		GivePlayerMoneyAC(source, TParams[playerid].TradeMoney*-1)
		OnPlayerRPChat(player, "принимает лечение от доктора "..Players[TParams[playerid].TradeOffer]["name"]..".", 0)
		local tax = TParams[playerid].TradeMoney / 100 * Data["ftax"]
		Fractions[1]["safe_money"] = Fractions[1]["safe_money"] + tax
		Fractions[5]["safe_money"] = Fractions[5]["safe_money"] + (TParams[playerid].TradeMoney - tax)
	end
	TParams[playerid].TradeMoney = nil
	TParams[playerid].TradeOffer = nil
end

function AcceptFine(player, bool) --Оплата штрафа
	local playerid = getPlayerID(player)
	if bool then
		if Players[playerid]["money"] < Players[playerid]["tparam"]["trademoney"] then OnPlayerRPChat(player, "отказывается от оплаты штрафа.#ffffff((Недостаточно средств))", 0) return SendPlayerNotice(player, "У вас недостаточно средств!") end
		GivePlayerMoneyAC(source, Players[playerid]["tparam"]["trademoney"]*-1)
		OnPlayerRPChat(player, "оплачивает штраф.", 0)
		local tax = Players[playerid]["tparam"]["trademoney"] / 100 * Data["ftax"]
		Fractions[1]["safe_money"] = Fractions[1]["safe_money"] + tax
		Fractions[2]["safe_money"] = Fractions[2]["safe_money"] + (Players[playerid]["tparam"]["trademoney"] - tax)
	else
		OnPlayerRPChat(player, "отказывается от оплаты штрафа.", 0)
	end
	Players[playerid]["tparam"]["trademoney"] = nil
	Players[playerid]["tparam"]["tradeid"] = nil
end

function StartLive(player, bool)
	local playerid = getPlayerID(player)
	if bool then
		if Data["newslive"] ~= nil then
			SendPlayerNotice(Players[Players[playerid]["tparam"]["tradeid"]]["player"], "Предложение недоступно! Прямая трансляция уже ведется!") 
			return SendPlayerNotice(player, "Предложение недоступно! Прямая трансляция уже ведется!") 
		end
		Data["newslive"] = Players[playerid]["tparam"]["tradeid"]
		Players[playerid]["newslive"] = Players[playerid]["tparam"]["tradeid"]
		Players[Players[playerid]["tparam"]["tradeid"]]["newslive"] = playerid
		outputChatBox(""..Players[playerid]["name"].." принял предложение провести трансляцию! ((/stoplive что бы остановить трансляцию))",Players[Players[playerid]["tparam"]["tradeid"]]["player"])
		outputChatBox("Вы приняли предложение поучаствовать в прямой трансляции! ((/stoplive что бы остановить трансляцию))", player)
	else
		outputChatBox(""..Players[playerid]["name"].." отказался от  предложение провести трансляцию!",Players[Players[playerid]["tparam"]["tradeid"]]["player"])
		outputChatBox("Вы отказались от предложения поучаствовать в прямой трансляции!", player)
	end
end

function AcceptLicense(player, bool)
	local playerid = getPlayerID(player)
	if bool then
		if Players[playerid]["money"] < Players[playerid]["tparam"]["trademoney"] then OnPlayerRPChat(player, "отказывается от получения лицензии.#ffffff((Недостаточно средств))", 0) return SendPlayerNotice(player, "У вас недостаточно средств!") end
		GivePlayerMoneyAC(source, Players[playerid]["tparam"]["trademoney"]*-1)
		OnPlayerRPChat(player, "подписывает договор на прохождение обучения.", 0)
		local tax = Players[playerid]["tparam"]["trademoney"] / 100 * Data["ftax"]
		Fractions[1]["safe_money"] = Fractions[1]["safe_money"] + tax
		Fractions[8]["safe_money"] = Fractions[8]["safe_money"] + (Players[playerid]["tparam"]["trademoney"] - tax)
		outputChatBox("Вы оплатили обучение в автошколе на лицензию категории #0dc426"..Players[playerid]["tparam"]["givliccategory"].."#ffffff у #0dc426"..Players[Players[playerid]["tparam"]["tradeid"]]["name"].."#ffffff за #0dc426$"..Players[playerid]["tparam"]["trademoney"].."", player)	
		outputChatBox("#0dc426"..Players[playerid]["name"].." #ffffffоплатил обучение на лицензию категории #0dc426"..Players[playerid]["tparam"]["givliccategory"].."#ffffff за #0dc426$"..Players[playerid]["tparam"]["trademoney"].."", Players[Players[playerid]["tparam"]["tradeid"]]["player"])
	else
		OnPlayerRPChat(player, "отказывается от обучения в автошколе.", 0)
	end	
	Players[playerid]["tparam"]["tradeid"] = nil
end

function SuspectPlayer(policeid, suspectid, reason)
	if policeid == suspectid then return SendPlayerNotice(Players[policeid]["player"], "Вы указали свой ID!") end
	if iSPlayerOK(suspectid) ~= true then return SendPlayerNotice(Players[policeid]["player"], "Игрок не авторизован!") end
	if Players[suspectid]["wanted"] >= 6 then return SendPlayerNotice(Players[policeid]["player"], "У игрока максимальный уровень розыска!") end
	Players[suspectid]["wanted"] = Players[suspectid]["wanted"] + 1
	setPlayerWantedLevel ( Players[suspectid]["player"], Players[suspectid]["wanted"]) 
	SendToPoliceFrac(Players[policeid]["player"], ""..Players[suspectid]["name"].." совершил(а) преступление: "..reason..". Свидетель: "..Players[policeid]["name"].."", 27,18,150)
	outputChatBox("Вы совершили преступление: "..reason.." . Свидетель: "..Players[policeid]["name"].."", Players[suspectid]["player"], 51, 38, 230)
end

function TicketPlayer(playerid, id, price, reason)
	local player = Players[playerid]["player"]
	if not iSPlayerOK(id) then return SendPlayerNotice(player, "Игрок не авторизован!") end
	if id == playerid then return SendPlayerNotice(player, "Вы указали свой ID!") end
	local px,py,pz = getElementPosition(player)
	local tx,ty,tz = getElementPosition(Players[id]["player"])
	if getDistanceBetweenPoints3D(px,py,pz,tx,ty,tz) > 2.0 then return SendPlayerNotice(player, "Вы далеко от игрока!") end
	if Players[id]["tradeid"] ~= nil then return SendPlayerNotice(player, "Игрок уже рассматривает другое предложение!") end
	OnPlayerRPChat(player, "выписывает штраф для "..Players[id]["name"].."", 0)
	ShowMessage(Players[id]["player"], 3, "Оплата штрафа", "#283CFF"..Fractions[Players[playerid]["frac"]]["posts"][Players[playerid]["post"]].." "..Players[playerid]["name"].." #ffffffвыписал вам штраф в размере #0dc426$"..price.."\n#ffffffПричина: #0dc426"..reason.."\n#ffffffВы хотите оплатить его сейчас?", "Нет", "Да")
	Players[id]["trademoney"] = price
	Players[id]["tradeid"] = playerid
end

function GetFractionPlayers(fid, bool)
	if bool == nil then bool = false end
	if fid == 0 or fid == nil then return false end
	local fracplayertable = {}
	for pid in ipairs(Players) do
		if Players[pid]["frac"] == fid then
			table.insert(fracplayertable, Players[pid]["player"])
		end	
	end
	if not bool then
		return fracplayertable
	else 
	-- Добавить вывод игроков из БД
	end
end
addEvent("GetFractionPlayers", true)
addEventHandler("GetFractionPlayers", root, GetFractionPlayers)

function GetFractionVehicle(fid)
	if fid == 0 or fid == nil then return false end
	local fracvehicletable = {}
	for vid in ipairs(Vehicles) do
		if Vehicles[vid]["type"] == 2 and Vehicles[vid]["owner"] == fid then
			table.insert(fracvehicletable, Vehicles[vid])
		end	
	end
	return fracvehicletable
end
addEvent("GetFractionVehicle", true)
addEventHandler("GetFractionVehicle", root, GetFractionVehicle)
