function cmd_givelic(player, cmd, id, price, license)
	local playerid = getPlayerID(player)
	if Players[playerid]["frac"] ~= 8 then return SendPlayerNotice(player, "У вас нет доступа к данной функции!") end
	if Players[playerid].int ~= 20001 then return SendPlayerNotice(player, "Вы должны быть в здании автошколы!") end
	local id = tonumber(id)
	local price = tonumber(price)
	if id == nil or price == nil or license == nil then 
		outputChatBox("#AAAAAAПиши: /givelic [id][цена][лицензия]")
		return outputChatBox("#AAAAAAДоступные варианты: A,B,C,H(Полеты), W(Катера)")
	end
	--if id == playerid then return SendPlayerNotice(player, "Вы указали свой ID") end
	local license = string.upper(license)
	if license ~= "A" and license ~= "B" and license ~= "C" and license ~= "H" and license ~= "W" then return  outputChatBox("#AAAAAAДоступные варианты лицензии: A,B,C,H(Полеты), W(Катера)") end
	if not iSPlayerOK(id) then return SendPlayerNotice(player, "Игрок не авторизован!") end
	if isPedInVehicle(Players[playerid]["player"]) then return SendPlayerNotice(player, "Вы не должны быть в автомобиле!") end
	if isPedInVehicle(Players[id]["player"]) then return SendPlayerNotice(player, "Игрок не должны быть в автомобиле!") end
	if(Players[playerid]["cuffed"] ~= 0) then return SendPlayerNotice(player, "Вы не должны быть в наручниках!") end
	local px,py,pz = getElementPosition(player)
	local tx,ty,tz = getElementPosition(Players[id]["player"])
	if getDistanceBetweenPoints3D(px,py,pz,tx,ty,tz) > 2.0 then return SendPlayerNotice(player, "Вы далеко от игрока!") end
	if Players[id]["tparam"]["tradeid"] ~= nil then return SendPlayerNotice(player, "Игрок уже рассматривает другое предложение!") end
	ShowMessage(Players[id]["player"], 5, "Получение лицензии", "#0dc426"..Fractions[Players[playerid]["frac"]]["posts"][Players[playerid]["post"]].." "..Fractions[Players[playerid]["frac"]]["ranks"][Players[playerid]["rank"]].." "..Players[playerid]["name"].." #ffffffпрелагает вам\nпройти обучение на лицензию категории #0dc426"..license.."#ffffff за #0dc426$"..price.."\n#ffffffВы хотите пройти обучение?", "Нет", "Да")
	Players[id]["tparam"]["trademoney"] = price
	Players[id]["tparam"]["tradeid"] = playerid
	Players[id]["tparam"]["givliccategory"] = license
end
addCommandHandler("givelic", cmd_givelic)