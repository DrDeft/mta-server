function cmd_news(player, cmd, ...)
	local playerid = getPlayerID(player)
	if Players[playerid]["frac"] ~= 7 then return SendPlayerNotice(player, "У вас нет доступа к данной функции!") end
	if Players[playerid]["jailtime"] > 0 then return SendPlayerNotice(player, "Нельзя писать новости находясь в тюрьме или больнице!") end
	if ... == nil or #... < 3 then return outputChatBox("#AAAAAAПиши: /news [текст]", player) end
	local text = table.concat({...}, " ")
	SendMessageToAll("#FFFF00[Новости] "..Fractions[Players[playerid]["frac"]]["posts"][Players[playerid]["post"]].." "..Fractions[Players[playerid]["frac"]]["ranks"][Players[playerid]["rank"]].." "..Players[playerid]["name"]..": "..text.."")
end
addCommandHandler("news", cmd_news)

function cmd_live(player, cmd, id)
	local playerid = getPlayerID(player)
	if Players[playerid]["frac"] ~= 7 then return SendPlayerNotice(player, "У вас нет доступа к данной функции!") end
	if Data["newslive"] ~= nil then return SendPlayerNotice(player, "Прямая трансляция уже ведется!") end
	if Players[playerid]["jailtime"] > 0 then return SendPlayerNotice(player, "Нельзя вести трансляцию находясь в тюрьме или больнице!") end
	if id == nil then return outputChatBox("#AAAAAAПиши: /live [id]", player) end
	local id = tonumber(id)
	if not iSPlayerOK(id) then return SendPlayerNotice(player, "Игрок не авторизован!") end
	if not checkPlayerTradeStatus(id) then return SendPlayerNotice(player, "Игрок уже рассматривает другое предложение!") end
	if isPedInVehicle ( player ) then
		local vid = getElementData(getPedOccupiedVehicle(player), "vehicle.id")
		if Vehicles[vid]["type"] ~= 2 then return SendPlayerNotice(player, "Вы должны быть в спец.транспорте или в студии!") end
		if Vehicles[vid]["owner"] ~= 7 then return SendPlayerNotice(player, "Вы должны быть в спец.транспорте или в студии!") end
		--Players[playerid]["tradeid"] = playerid
		sendTradeOffer("#283CFF"..Fractions[Players[playerid]["frac"]]["posts"][Players[playerid]["post"]].." "..Players[playerid]["name"].." #ffffff приглашает вас на прямую трансляцию\nВы хотите принять участие?", 3, playerid, id)
		--ShowMessage(Players[id]["player"], 4, "Прямая трансляция", "#283CFF"..Fractions[Players[playerid]["frac"]]["posts"][Players[playerid]["post"]].." "..Players[playerid]["name"].." #ffffff приглашает вас на прямую трансляцию\nВы хотите принять участие?", "Нет", "Да")
	end	
end
addCommandHandler("live", cmd_live)

function cmd_stoplive(player, cmd)
	local playerid = getPlayerID(player)
	if Players[playerid]["newslive"] == nil then return SendPlayerNotice(player, "У вас нет доступа к данной функции!") end
	if Data["newslive"] == nil then return SendPlayerNotice(player, "Прямая трансляция не ведется!") end
	SendToFrac(player, 7, "FFFF00"..Players[playerid]["name"].." завершил прямую трансляцию!")
	outputChatBox("#AAAAAAВы завершили прямую трансляцию!", player)
	Data["newslive"] = nil
	Players[Players[playerid]["tradeid"]]["newslive"] = nil
	Players[playerid]["newslive"] = nil
end
addCommandHandler("stoplive", cmd_stoplive)