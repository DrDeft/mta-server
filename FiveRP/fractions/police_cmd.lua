---------------------Сделать проверку рангов!--------------------------------

function cmd_su(player, cmd, id, ...)
	local playerid = getPlayerID(player)
	if Players[playerid]["frac"] == nil or Players[playerid]["frac"] < 2 or Players[playerid]["frac"] > 3 then 
		return SendPlayerNotice(player, "У вас нет доступа к данной функции!") 
	end
	local id = tonumber(id)
	if id == nil or id < 1 or #... < 1 then return outputChatBox("#AAAAAAПиши: /su [id][причина]", player) end
	SuspectPlayer(playerid, id, table.concat({...}, " "))
end
addCommandHandler("su", cmd_su)

function cmd_clear(player, cmd, id)
	local playerid = getPlayerID(player)
	if Players[playerid]["frac"] == nil or Players[playerid]["frac"] < 2 or Players[playerid]["frac"] > 3 or Players[playerid]["rank"] < 6 then 
		return SendPlayerNotice(player, "У вас нет доступа к данной функции!") 
	end
	local id = tonumber(id)
	if id == nil or id < 1 then return outputChatBox("#AAAAAAПиши: /clear [id]", player) end
	if id == playerid then return SendPlayerNotice(player, "Вы указали свой ID!") end
	if iSPlayerOK(id) ~= true then return SendPlayerNotice(player, "Игрок не авторизован!") end
	if getPedOccupiedVehicleSeat(player) ~= 0 and getPedOccupiedVehicleSeat(player) ~= 1 then return SendPlayerNotice(player, "Вы должны быть на переднем сидении полицейского автомобиля!") end
	local vehicleid = getElementData(getPedOccupiedVehicle(player), "vehicle.id")
	if Vehicles[vehicleid]["type"] ~= 2 or Vehicles[vehicleid]["owner"] ~= 2 then return SendPlayerNotice(player, "Вы должны быть на переднем сидении полицейского автомобиля!") end
	Players[id]["wanted"] = 0
	setPlayerWantedLevel ( Players[id]["player"], Players[id]["wanted"]) 
	SendToPoliceFrac(player, ""..Players[playerid]["name"].." удалил "..Players[id]["name"].." из базы полиции!", 27,18,150)
	outputChatBox(""..Players[playerid]["name"].." удалил вас из базы полиции!", Players[id]["player"], 27,18,150)
end
addCommandHandler("clear", cmd_clear)

function cmd_tazer(player, cmd)
	local playerid = getPlayerID(player)
	if Players[playerid]["frac"] == nil or Players[playerid]["frac"] < 2 or Players[playerid]["frac"] > 4 then 
		return SendPlayerNotice(player, "У вас нет доступа к данной функции!") 
	end
	if getPedWeapon(player) ~= 23 then return SendPlayerNotice(player, "У вас в руках должен быть пистолет с глушителем!") end
	if Players[playerid]["tazer"] == nil or Players[playerid]["tazer"] == false then
		--Включаем тазер
		Players[playerid]["tazer"] = true
		OnPlayerRPChat(player, "переводит пистолет в режим \"Tazer\".", 0)
		--Tazered(player, player, 5)
	else
		Players[playerid]["tazer"] = nil
		OnPlayerRPChat(player, "переводит пистолет в боевой режим.", 0)
		--Отключаем тазер
	end
end
addCommandHandler("tazer", cmd_tazer)

function cmd_cuff(player, cmd, id)
	local playerid = getPlayerID(player)
	if Players[playerid]["frac"] == nil or Players[playerid]["frac"] < 2 or Players[playerid]["frac"] > 3 or Players[playerid]["rank"] < 2 then 
		return SendPlayerNotice(player, "У вас нет доступа к данной функции!") 
	end
	local id = tonumber(id)
	if not id or id < 1 then return outputChatBox("#AAAAAAПиши: /cuff [id]", player) end
	if not iSPlayerOK(id) then return SendPlayerNotice(player, "Игрок не авторизован!") end
	if id == playerid then return SendPlayerNotice(player, "Вы указали свой ID!") end
	if isPedInVehicle(player) then return SendPlayerNotice(player, "Нельзя использовать в автомобиле!") end
	if isPedInVehicle(Players[id]["player"]) then return SendPlayerNotice(player, "Игрок в автомобиле!") end
	OnPlayerRPChat(player, "надевает наручники на "..Players[id]["name"]..".", 0)
	Players[id]["cuffed"] = 1	
	CuffPlayer(Players[id]["player"], true)
end
addCommandHandler("cuff", cmd_cuff)

function cmd_uncuff(player, cmd, id)
	local playerid = getPlayerID(player)
	if Players[playerid]["frac"] == nil or Players[playerid]["frac"] < 2 or Players[playerid]["frac"] > 3 or Players[playerid]["rank"] < 2 then 
		return SendPlayerNotice(player, "У вас нет доступа к данной функции!") 
	end
	local id = tonumber(id)
	if not id or id < 1 then return outputChatBox("#AAAAAAПиши: /uncuff [id]", player) end
	if not iSPlayerOK(id) then return SendPlayerNotice(player, "Игрок не авторизован!") end
	if id == playerid then return SendPlayerNotice(player, "Вы указали свой ID!") end
	if isPedInVehicle(player) then return SendPlayerNotice(player, "Нельзя использовать в автомобиле!") end
	if isPedInVehicle(Players[id]["player"]) then return SendPlayerNotice(player, "Игрок в автомобиле!") end
	OnPlayerRPChat(player, "снимает наручники на "..Players[id]["name"]..".", 0)
	Players[id]["cuffed"] = 0
	CuffPlayer(Players[id]["player"], false)
end
addCommandHandler("uncuff", cmd_uncuff)

function cmd_ceject(player, cmd, id)
	local playerid = getPlayerID(player)
	if Players[playerid]["frac"] == nil or Players[playerid]["frac"] < 2 or Players[playerid]["frac"] > 3 or Players[playerid]["rank"] < 6 then 
		return SendPlayerNotice(player, "У вас нет доступа к данной функции!") 
	end
	local id = tonumber(id)
	if not id or id < 1 then return outputChatBox("#AAAAAAПиши: /ceject [id]", player) end
	if not iSPlayerOK(id) then return SendPlayerNotice(player, "Игрок не авторизован!") end
	if id == playerid then return SendPlayerNotice(player, "Вы указали свой ID!") end
	local px,py,pz = getElementPosition(player)
	local tx,ty,tz = getElementPosition(Players[id]["player"])
	if not isPedInVehicle(Players[id]["player"]) then return SendPlayerNotice(player, "Игрок не в автомобиле!") end
	if getDistanceBetweenPoints3D(px,py,pz,tx,ty,tz) > 3.0 then return SendPlayerNotice(player, "Вы далеко от игрока!") end
	OnPlayerRPChat(player, "вытаскивает "..Players[id]["name"].." из автомобиля.", 0)
	removePedFromVehicle(Players[id]["player"])
end
addCommandHandler("ceject", cmd_ceject)

function cmd_drag(player, cmd, id)
	local playerid = getPlayerID(player)
	if Players[playerid]["frac"] == nil or Players[playerid]["frac"] < 2 or Players[playerid]["frac"] > 3 or Players[playerid]["rank"] < 2 then 
		return SendPlayerNotice(player, "У вас нет доступа к данной функции!") end
	local id = tonumber(id)
	if not id or id < 1 then return outputChatBox("#AAAAAAПиши: /drag [id]", player) end
	if not iSPlayerOK(id) then return SendPlayerNotice(player, "Игрок не авторизован!") end
	if id == playerid then return SendPlayerNotice(player, "Вы указали свой ID!") end
	local px,py,pz = getElementPosition(player)
	local tx,ty,tz = getElementPosition(Players[id]["player"])
	if isPedInVehicle(Players[id]["player"]) then return SendPlayerNotice(player, "Игрок в автомобиле!") end
	if getDistanceBetweenPoints3D(px,py,pz,tx,ty,tz) > 2.0 then return SendPlayerNotice(player, "Вы далеко от игрока!") end
	if isPedInVehicle(Players[playerid]["player"]) then return SendPlayerNotice(player, "Вы в автомобиле!") end
	if(Players[id]["cuffed"] == 0) then return SendPlayerNotice(player, "Игрок должен быть в наручниках!") end
	if(Players[playerid]["cuffed"] ~= 0) then return SendPlayerNotice(player, "Нельзя тащить игрока находясь в наручниках") end
	if Players[playerid]["drag"] ~= nil then
		UnDragPlayer(playerid)
	end 
	if Players[id]["dragid"] ~= nil then return SendPlayerNotice(player, "Недоступно в данный момент!") end
	OnPlayerRPChat(player, "пристегивает "..Players[id]["name"].." к своим наручникам.", 0)
	Players[playerid]["dragid"] = id
	Players[id]["dragid"] = playerid
	Players[playerid]["drag"] = setTimer(DragPlayerTimer, 1000, 0, player, Players[id]["player"])
	outputChatBox("Что бы отстегнуть игрока от себя пиши: /drag [id] еще раз!", player)
end
addCommandHandler("drag", cmd_drag)

function cmd_ticket(player, cmd, id, price, ...)
	local playerid = getPlayerID(player)
	if Players[playerid]["frac"] == nil or Players[playerid]["frac"] ~= 2 then return SendPlayerNotice(player, "У вас нет доступа к данной функции!") end
	local id = tonumber(id)
	local price = tonumber(price)
	local reason = table.concat({...}, " ")
	if not id or not price or #... == nil or #... < 2 then return outputChatBox("#AAAAAAПиши: /ticket [id][сумма][причина]", player) end -- ToDo: Fix?
	if not iSPlayerOK(id) then return SendPlayerNotice(player, "Игрок не авторизован!") end
	TicketPlayer(playerid, id, price, reason)
end
addCommandHandler("ticket", cmd_ticket)

function cmd_putveh(player, cmd, id)
	local vehicle = getPedOccupiedVehicle(player)
	local playerid = getPlayerID(player)
	if not vehicle then return SendPlayerNotice(player, "Вы должны быть в автомобиле!") end
	if Players[playerid]["frac"] == nil or Players[playerid]["frac"] < 2 or Players[playerid]["frac"] > 3 or Players[playerid]["rank"] < 2 then 
		return SendPlayerNotice(player, "У вас нет доступа к данной функции!") end
	if not id then return outputChatBox("#AAAAAAПиши: /putveh [id]", player) end
	local id = tonumber(id)
	if not iSPlayerOK(id) then return SendPlayerNotice(player, "Игрок не авторизован!") end
	if id == playerid then return SendPlayerNotice(player, "Вы указали свой ID!") end
	local px,py,pz = getElementPosition(player)
	local tx,ty,tz = getElementPosition(Players[id]["player"])
	if(Players[id]["cuffed"] == 0) then return SendPlayerNotice(player, "Игрок должен быть в наручниках!") end
	if getDistanceBetweenPoints3D(px,py,pz,tx,ty,tz) > 2.0 then return SendPlayerNotice(player, "Вы далеко от игрока!") end
	if getVehicleOccupant(vehicle,3) == false then   
		warpPedIntoVehicle(Players[id]["player"], vehicle, 3)   
	elseif getVehicleOccupant(vehicle,4) == false then
		warpPedIntoVehicle(Players[id]["player"], vehicle, 4) 
	else
		SendPlayerNotice(player, "В автомобиле нет свободных мест!")
	end
end
addCommandHandler("putveh", cmd_putveh)