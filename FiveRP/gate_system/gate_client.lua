local GATEGUI = {
    button = {},
    window = {},
    label = {}
}

local GUIGateMenu = {
	window = {},
	edit = {},
	button = {},
	label,
	combobox
	
}


local gcreated = false
local mcreated = false
local screenW, screenH = guiGetScreenSize()

local GInfooID = nil

local LocalGateInfo

function ActiveGateSound(gate, time)
	local sound = playSFX3D ( "genrl", 44, 0, 0, 0, 0, true)
	attachElements ( sound, gate)
	setSoundMaxDistance ( sound, 70 )
	setSoundVolume (sound, 1.0 )
	setTimer ( function()
		stopSound ( sound )
	end, time, 1 )
end
addEvent("ActiveGateSound", true)
addEventHandler("ActiveGateSound", localPlayer, ActiveGateSound)

local function CreateGateEditMenu()
	if mcreated then return 1 end
	GUIGateMenu.window[1] = guiCreateWindow(0.39, 0.7, 0.22, 0.16, "Изменить объект", true)
	guiWindowSetSizable(GUIGateMenu.window[1], false)

	GUIGateMenu.label = guiCreateLabel(0.01, 0.14, 0.97, 0.24, "Текущий ID объекта : 991\nВведите новый ID объекта и нажмите принять", true, GUIGateMenu.window[1])
	guiSetFont(GUIGateMenu.label,fmedium)
	guiLabelSetHorizontalAlign(GUIGateMenu.label, "center", false)
	GUIGateMenu.edit[1] = guiCreateEdit((429 - 202) / 2, (169 - 30) / 2, 202, 30, "", false, GUIGateMenu.window[1])
	GUIGateMenu.button[1] = guiCreateButton(0.11, 0.67, 0.32, 0.18, "Принять", true, GUIGateMenu.window[1])
	GUIGateMenu.button[2] = guiCreateButton(0.58, 0.67, 0.32, 0.18, "Отмена", true, GUIGateMenu.window[1])
	
	GUIGateMenu.combobox = guiCreateComboBox(0.31, 0.38, 0.38, 0.53, "Домашние", true, GUIGateMenu.window[1])
	guiComboBoxAddItem(GUIGateMenu.combobox, "Фракционные")
	guiComboBoxAddItem(GUIGateMenu.combobox, "Рабочие")
	guiComboBoxAddItem(GUIGateMenu.combobox, "Домашние")
	guiSetVisible(GUIGateMenu.combobox, false)
	
	mcreated = true
end

local function VievGateEditMenu(bool, type)
	if mcreated == false then CreateGateEditMenu() end
	if type == nil then type = 0 end
	guiSetVisible(GUIGateMenu.window[1], bool)
	guiSetVisible(GUIGateMenu.label, bool)
	guiSetVisible(GUIGateMenu.button[1], bool)
	guiSetVisible(GUIGateMenu.button[2], bool)
	if type == 0 then
		guiSetVisible(GUIGateMenu.edit[1], bool)
		guiSetVisible(GUIGateMenu.combobox, false)
	else
		guiComboBoxSetSelected(GUIGateMenu.combobox, 0)
		guiSetVisible(GUIGateMenu.combobox, bool)
		guiSetVisible(GUIGateMenu.edit[1], false)
	end	
	if not bool then
		guiSetVisible(GUIGateMenu.edit[1], false)
		guiSetVisible(GUIGateMenu.combobox, false)
	end
end

local function CreateGateInformationBox()
	if gcreated then return 1 end
	GATEGUI.window[1] = guiCreateWindow(0.40, 0.22, 0.21, 0.33, "Управление воротами", true)
    guiWindowSetSizable(GATEGUI.window[1], false)
	
	GATEGUI.label[1] = guiCreateLabel(0.00, 0.21, 0.98, 0.07, "ID ворот/дверей: 1", true, GATEGUI.window[1])
	guiSetFont(GATEGUI.label[1], fmedium)
	guiLabelSetHorizontalAlign(GATEGUI.label[1], "center", false)
	guiLabelSetVerticalAlign(GATEGUI.label[1], "center")
	GATEGUI.label[2] = guiCreateLabel(0.00, 0.28, 0.98, 0.07, "ID объекта: 988", true, GATEGUI.window[1])
	guiSetFont(GATEGUI.label[2], fmedium)
	guiLabelSetHorizontalAlign(GATEGUI.label[2], "center", false)
	guiLabelSetVerticalAlign(GATEGUI.label[2], "center")
	GATEGUI.label[3] = guiCreateLabel(0.00, 0.35, 0.98, 0.07, "Тип: Ворота", true, GATEGUI.window[1])
	guiSetFont(GATEGUI.label[3], fmedium)
	guiLabelSetHorizontalAlign(GATEGUI.label[3], "center", false)
	guiLabelSetVerticalAlign(GATEGUI.label[3], "center")
	GATEGUI.label[4] = guiCreateLabel(0.00, 0.42, 0.98, 0.07, "Тип владельца: Фракционные", true, GATEGUI.window[1])
	guiSetFont(GATEGUI.label[4], fmedium)
	guiLabelSetHorizontalAlign(GATEGUI.label[4], "center", false)
	guiLabelSetVerticalAlign(GATEGUI.label[4], "center")
	GATEGUI.label[5] = guiCreateLabel(0.00, 0.49, 0.98, 0.07, "ID владельца: 1", true, GATEGUI.window[1])
	guiSetFont(GATEGUI.label[5], fmedium)
	guiLabelSetHorizontalAlign(GATEGUI.label[5], "center", false)
	guiLabelSetVerticalAlign(GATEGUI.label[5], "center")
	GATEGUI.label[6] = guiCreateLabel(0.00, 0.56, 0.98, 0.07, "Доступны с ранга: 3", true, GATEGUI.window[1])
	guiSetFont(GATEGUI.label[6], fmedium)
	guiLabelSetHorizontalAlign(GATEGUI.label[6], "center", false)
	guiLabelSetVerticalAlign(GATEGUI.label[6], "center")
	GATEGUI.label[7] = guiCreateLabel(0.00, 0.63, 0.98, 0.07, "Интерьер: 0", true, GATEGUI.window[1])
	guiSetFont(GATEGUI.label[7], fmedium)
	guiLabelSetHorizontalAlign(GATEGUI.label[7], "center", false)
	guiLabelSetVerticalAlign(GATEGUI.label[7], "center")
	GATEGUI.label[8] = guiCreateLabel(0.00, 0.70, 0.98, 0.07, "Вирт.Мир: 2", true, GATEGUI.window[1])
	guiSetFont(GATEGUI.label[8], fmedium)
	guiLabelSetHorizontalAlign(GATEGUI.label[8], "center", false)
	guiLabelSetVerticalAlign(GATEGUI.label[8], "center")
	GATEGUI.label[9] = guiCreateLabel(0.00, 0.79, 0.98, 0.07, "Координаты открытых ворот", true, GATEGUI.window[1])
	guiSetFont(GATEGUI.label[9], fmedium)
	guiLabelSetHorizontalAlign(GATEGUI.label[9], "center", false)
	guiLabelSetVerticalAlign(GATEGUI.label[9], "center")
	GATEGUI.label[10] = guiCreateLabel(0.00, 0.86, 0.98, 0.07, "Координаты закрытых ворот", true, GATEGUI.window[1])
	guiSetFont(GATEGUI.label[10], fmedium)
	guiLabelSetHorizontalAlign(GATEGUI.label[10], "center", false)
	guiLabelSetVerticalAlign(GATEGUI.label[10], "center")
	GATEGUI.label[11] = guiCreateLabel(0, 30, 403, 26, "Для редактирования информации нажмите на строку", false, GATEGUI.window[1])
	guiSetFont(GATEGUI.label[11], fmedium)
	guiLabelSetColor(GATEGUI.label[11], 254, 0, 0)
	guiLabelSetHorizontalAlign(GATEGUI.label[11], "center", false)
	GATEGUI.label[12] = guiCreateLabel(0.78, 0.93, 0.17, 0.05, "Закрыть", true, GATEGUI.window[1])
	guiSetFont(GATEGUI.label[12], fmedium)
	guiLabelSetHorizontalAlign(GATEGUI.label[12], "center", false)    
	
	gcreated = true
end

local function ReloadGateInfoGUI()
	guiSetText(GATEGUI.label[1], "ID ворот/дверей: LID: "..LocalGateInfo["id"].." MID: "..LocalGateInfo["mysqlid"].."")
	guiSetText(GATEGUI.label[2], "ID объекта: "..LocalGateInfo["object"].."")
	if LocalGateInfo["type"] == 1 then
		guiSetText(GATEGUI.label[3], "Тип: Ворота")
	elseif LocalGateInfo["type"] == 2 then
		guiSetText(GATEGUI.label[3], "Тип: Двери")
	else
		guiSetText(GATEGUI.label[3], "Тип: Шлагбаум")
	end
	
	if LocalGateInfo["otype"] == 1 then
		guiSetText(GATEGUI.label[4], "Тип владельца: Фракционные")
	elseif LocalGateInfo["otype"] == 2 then
		guiSetText(GATEGUI.label[4], "Тип владельца: Рабочие")
	else
		guiSetText(GATEGUI.label[4], "Тип владельца: Домашние")
	end
	guiSetText(GATEGUI.label[5], "ID владельца: "..LocalGateInfo["owner"].."")
	guiSetText(GATEGUI.label[6], "Доступны с ранга:: "..LocalGateInfo["orank"].."")
	guiSetText(GATEGUI.label[7], "Интерьер: "..LocalGateInfo["int"].."")
	guiSetText(GATEGUI.label[8], "Вирт.Мир: "..LocalGateInfo["world"].."")
end

local function UpdateGateInfo()
	local ParamId
	if GInfooID == 2 or GInfooID == 3 then
		ParamId = 1
	else
		if guiGetText (GUIGateMenu.edit[1]) == nil then return 1 end
		ParamId = tonumber(guiGetText(GUIGateMenu.edit[1]))
	end
	if GInfooID == 1 then
		LocalGateInfo["object"] = ParamId
		setElementModel (LocalGateInfo["gate"], ParamId)
	
	elseif GInfooID == 3 then 
		if guiComboBoxGetSelected(GUIGateMenu.combobox) == -1 then ParamId = 1
		else ParamId = guiComboBoxGetSelected(GUIGateMenu.combobox)+1 end
		LocalGateInfo["otype"] = ParamId
	elseif GInfooID == 4 then LocalGateInfo["owner"] = ParamId
	elseif GInfooID == 5 then LocalGateInfo["orank"] = ParamId
	elseif GInfooID == 6 then LocalGateInfo["int"] = ParamId
	elseif GInfooID == 7 then LocalGateInfo["world"] = ParamId
	end
	VievGateEditMenu(false)
	ReloadGateInfoGUI()
end


local function VievGateMenu(bool)
	if bool then 
		addEventHandler("onClientGUIClick", root, OnClientGateEditClick)
		addEventHandler("onClientMouseEnter", root, OnClientGateMouseEnter)
		addEventHandler("onClientMouseLeave", root, OnClientGateMouseLeave)
	else
		removeEventHandler("onClientGUIClick", root, OnClientGateEditClick)
		removeEventHandler("onClientMouseEnter", root, OnClientGateMouseEnter)
		removeEventHandler("onClientMouseLeave", root, OnClientGateMouseLeave)
	end
	if not gcreated then CreateGateInformationBox() end
	guiSetVisible(GATEGUI.window[1], bool)
	for i in ipairs(GATEGUI.label) do
		guiSetVisible(GATEGUI.label[i], bool)
	end
end

local function UpdateGateGUIInfo(Gate)
	LocalGateInfo = Gate
	if not gcreated then CreateGateInformationBox() end
	guiSetText(GATEGUI.label[1], "ID ворот/дверей: LID: "..Gate["id"].." MID: "..Gate["mysqlid"].."")
	guiSetText(GATEGUI.label[2], "ID объекта: "..Gate["object"].."")
	if Gate["type"] == 1 then
		guiSetText(GATEGUI.label[3], "Тип: Ворота")
	elseif Gate["type"] == 2 then
		guiSetText(GATEGUI.label[3], "Тип: Двери")
	else
		guiSetText(GATEGUI.label[3], "Тип: Шлагбаум")
	end
	
	if Gate["otype"] == 1 then
		guiSetText(GATEGUI.label[4], "Тип владельца: Фракционные")
	elseif Gate["otype"] == 2 then
		guiSetText(GATEGUI.label[4], "Тип владельца: Рабочие")
	else
		guiSetText(GATEGUI.label[4], "Тип владельца: Домашние")
	end
	guiSetText(GATEGUI.label[5], "ID владельца: "..Gate["owner"].."")
	guiSetText(GATEGUI.label[6], "Доступны с ранга:: "..Gate["orank"].."")
	guiSetText(GATEGUI.label[7], "Интерьер: "..Gate["int"].."")
	guiSetText(GATEGUI.label[8], "Вирт.Мир: "..Gate["world"].."")
	VievGateMenu(true)
	
end
addEvent("UpdateGateGUIInfo", true)
addEventHandler("UpdateGateGUIInfo", localPlayer, UpdateGateGUIInfo)

local function UpdateGateEditMenu()
	
end

function OnClientGateEditClick()
	if source == GATEGUI.label[2] then -- ИД Объекта
		VievGateEditMenu(true)
		guiSetText ( GUIGateMenu.label, "Текущий ID объекта : "..LocalGateInfo["object"].."\nВведите новый ID объекта и нажмите принять" )
		GInfooID = 1
	elseif source == GATEGUI.label[3] then -- Тип
	elseif source == GATEGUI.label[4] then -- Тип владельца
		GInfooID = 3
		VievGateEditMenu(true, 1)
	elseif source == GATEGUI.label[5] then -- Ид владельца
		VievGateEditMenu(true)
		guiSetText ( GUIGateMenu.label, "Текущий ID владельца : "..LocalGateInfo["owner"].."\nВведите новый ID(MID) владельца и нажмите принять" )
		GInfooID = 4
	elseif source == GATEGUI.label[6] then -- Ранг доступа
		VievGateEditMenu(true)
		guiSetText ( GUIGateMenu.label, "Объект доступен с "..LocalGateInfo["orank"].." ранга\nВведите ранг у которого есть доступ и нажмите принять" )
		GInfooID = 5
	elseif source == GATEGUI.label[7] then -- Интерьер
		VievGateEditMenu(true)
		guiSetText ( GUIGateMenu.label, "Объект находиться в "..LocalGateInfo["int"].." интерьере\nВведите новый ID интерьера и нажмите принять" )	
		GInfooID = 6
	elseif source == GATEGUI.label[8] then -- Вирт мир
		VievGateEditMenu(true)
		guiSetText ( GUIGateMenu.label, "Объект находиться в "..LocalGateInfo["world"].." Вирт.Мире\nВведите новый ID интерьера и нажмите принять" )	
		GInfooID = 7
	elseif source == GATEGUI.label[9] then -- коорд открытых
	elseif source == GATEGUI.label[10] then -- коорд закрытых
	elseif source == GATEGUI.label[12] then -- закрыть окно
		VievGateMenu(false)
		VievGateEditMenu(false)
		triggerServerEvent ( "UpdateGateGUIInfo", resourceRoot, LocalGateInfo["id"], LocalGateInfo)
	elseif source == GUIGateMenu.button[1] then
		UpdateGateInfo()
	elseif source == GUIGateMenu.button[2] then
		VievGateEditMenu(false)
	end
end

function OnClientGateMouseEnter()
	if source == GATEGUI.label[2] or source == GATEGUI.label[3] or source == GATEGUI.label[4] or source == GATEGUI.label[5] or source == GATEGUI.label[6] or source == GATEGUI.label[7] or source == GATEGUI.label[8] or source == GATEGUI.label[9] or source == GATEGUI.label[10] or source == GATEGUI.label[12] then
		guiLabelSetColor (source, 80, 80, 80)
	end	
end

function OnClientGateMouseLeave()
	if source == GATEGUI.label[2] or source == GATEGUI.label[3] or source == GATEGUI.label[4] or source == GATEGUI.label[5] or source == GATEGUI.label[6] or source == GATEGUI.label[7] or source == GATEGUI.label[8] or source == GATEGUI.label[9] or source == GATEGUI.label[10] or source == GATEGUI.label[12] then
		guiLabelSetColor (source, 255, 255, 255)
	end
end
