local LoadedGates = 0

function cmd_testmove(player, cmd)
	MoveGate(1, true)
	VievGatePanel(player, 1)
end
addCommandHandler("testmove", cmd_testmove)  

function LoadGates()
	local query = dbQuery(SQL, "SELECT * FROM `gates`")
	local resultTable, num, err = dbPoll (query, -1 ) 
	if resultTable then 
		for i, row in ipairs(resultTable) do 
			local tspos, tsrpos, tepos, terpos
			local spos, srpos, epos, erpos
			Gates[i] = {id = i, gate, mysqlid, object, type, owner, otype, orank, int, world, spos = {}, srpos = {}, epos = {}, erpos = {}}
			Gates[i]["mysqlid"] = tonumber(row["id"])
			Gates[i]["object"] = tonumber(row["object"])
			Gates[i]["type"] = tonumber(row["type"])
			Gates[i]["owner"] = tonumber(row["owner"])
			Gates[i]["otype"] = tonumber(row["otype"])
			Gates[i]["orank"] = tonumber(row["orank"])
			Gates[i]["int"] = tonumber(row["int"])
			Gates[i]["world"] = tonumber(row["world"])
			tspos = row["spos"]
			tsrpos = row["srpos"]
			tepos = row["epos"]
			terpos = row["erpos"]
			-------------------------------------------
			spos = explode("|", tspos)
			srpos = explode("|", tsrpos)
			epos = explode("|", tepos)
			erpos = explode("|", terpos)
			
			for x in ipairs(spos) do
				Gates[i]["spos"][x] = spos[x]
				Gates[i]["srpos"][x] = srpos[x]
				Gates[i]["epos"][x] = epos[x]
				Gates[i]["erpos"][x] = erpos[x]
			end
			Gates[i]["gate"] = createObject ( Gates[i]["object"], Gates[i]["spos"][1], Gates[i]["spos"][2], Gates[i]["spos"][3],  Gates[i]["srpos"][1], Gates[i]["srpos"][2], Gates[i]["srpos"][3] )
			setElementDimension (Gates[i]["gate"], Gates[i]["world"])
			setElementInterior (Gates[i]["gate"], Gates[i]["int"])
			LoadedGates = LoadedGates+1
			
		end
	end	
end

function SaveGate()
	for i in ipairs(Gates) do
		if Gates[i]["gate"] ~= nil then
			dbExec(SQL, "UPDATE `gates` SET \
				`object` = '"..Gates[i]["object"].."', \
				`type` = '"..Gates[i]["type"].."', \
				`owner` = '"..Gates[i]["owner"].."', \
				`otype` = '"..Gates[i]["otype"].."', \
				`orank` = '"..Gates[i]["orank"].."', \
				`int` = '"..Gates[i]["int"].."', \
				`world` = '"..Gates[i]["world"].."', \
				`spos` = '"..Gates[i]["spos"][1].."|"..Gates[i]["spos"][1].."|"..Gates[i]["spos"][1].."', \
				`srpos` = '"..Gates[i]["srpos"][1].."|"..Gates[i]["srpos"][1].."|"..Gates[i]["srpos"][1].."', \
				`epos` = '"..Gates[i]["epos"][1].."|"..Gates[i]["epos"][1].."|"..Gates[i]["epos"][1].."', \
				`erpos` = '"..Gates[i]["erpos"][1].."|"..Gates[i]["erpos"][1].."|"..Gates[i]["erpos"][1].."' \
			WHERE `id` = '"..Gates[i]["mysqlid"].."' LIMIT 1")
		end		
	end
end

function ReturnFreeGateID()
	local id = nil
	for i in ipairs(Gates) do 
		if Gates[i]["gate"] == nil then id = i end
	end
	if id == nil then
		id = #Gates+1
		return id
	else
		return id
	end	
end

function MoveGate(gid, bool)
	if Gates[gid]["Opening"] ~= nil then return 1 end -- Ворота уже открываются
	local opentime = getDistanceBetweenPoints3D ( Gates[gid]["epos"][1], Gates[gid]["epos"][2], Gates[gid]["epos"][3], Gates[gid]["erpos"][1], Gates[gid]["erpos"][2], Gates[gid]["erpos"][3] ) * 2.0
	local Sphere = createColSphere(Gates[gid]["spos"][1], Gates[gid]["spos"][2], Gates[gid]["spos"][3],50)
	local _Players = getElementsWithinColShape(Sphere, "player")
	destroyElement(Sphere)
	triggerClientEvent(_Players, "ActiveGateSound", root, Gates[gid]["gate"], opentime*1000)
	_Players = nil
	if bool then
		--Открываем ворота
		moveObject (Gates[gid]["gate"], opentime*1000, Gates[gid]["epos"][1], Gates[gid]["epos"][2], Gates[gid]["epos"][3], Gates[gid]["erpos"][1], Gates[gid]["erpos"][2], Gates[gid]["erpos"][3] )
		setTimer ( TimerMoveGate, 15*1000+(opentime*1000), 1, gid, true )
	else
		--Закрываем ворота
		moveObject (Gates[gid]["gate"], 10*1000, Gates[gid]["spos"][1], Gates[gid]["spos"][2], Gates[gid]["spos"][3], Gates[gid]["srpos"][1], Gates[gid]["srpos"][2], Gates[gid]["srpos"][3] )
	end
end

function TimerMoveGate(gid, bool)
	if bool then MoveGate(gid, false)
	else MoveGate(gid, true)
	end	
end

function VievGatePanel(player, gid)
	triggerClientEvent(player, "UpdateGateGUIInfo", player, Gates[gid])
end

function UpdateGateGUIInfo(gid, info)
	Gates[gid] = info
end
addEvent("UpdateGateGUIInfo", true)
addEventHandler("UpdateGateGUIInfo", root, UpdateGateGUIInfo)

