local screenW, screenH = guiGetScreenSize()
local Rvertices = {}
local Lvertices = {}
---------------------
local PRvertices = {}
local PLvertices = {}

local CaptureGUILabel = {}

local VPos = {RightX = screenW*0.53697, UpY = screenH*0.0342, DownY = screenH*0.07592, LeftX = screenW*0.3890, shift147 = screenW*0.07656, shift20 = screenW*0.01041, multiProc = (screenW*0.07656)/100} -- ToDo: 
		   --{RightX = 1031, UpY = 37, DownY = 82, LeftX = 747, shift147 = 147, shift20 = 20, multiProc = 1.45}

function createCaptureUI()
	-----------Правый----------
	local vertice = {VPos.RightX+VPos.shift147, VPos.UpY, tocolor(255, 255,255, 150)}
	table.insert(Rvertices, vertice)
	vertice = {VPos.RightX, VPos.UpY, tocolor(255, 255,255, 150)}
	table.insert(Rvertices, vertice)
	vertice = {VPos.RightX-VPos.shift20, VPos.DownY, tocolor(255, 50,50, 255)}
	table.insert(Rvertices, vertice)
	vertice = {VPos.RightX+VPos.shift147-VPos.shift20, VPos.DownY, tocolor(255, 50,50, 255)}
	table.insert(Rvertices, vertice)
	-------------------Левый-----------
	vertice = {VPos.LeftX+VPos.shift147, VPos.UpY, tocolor(255, 255,255, 150)}
	table.insert(Lvertices, vertice)
	vertice = {VPos.LeftX, VPos.UpY, tocolor(255, 255,255, 150)}
	table.insert(Lvertices, vertice)
	vertice = {VPos.LeftX+VPos.shift20, VPos.DownY, tocolor(50, 50,255, 255)}
	table.insert(Lvertices, vertice)
	vertice = {VPos.LeftX+VPos.shift147+VPos.shift20, VPos.DownY, tocolor(50, 50,255, 255)}
	table.insert(Lvertices, vertice)
	-------------------Заполнение левый-----------
	local LProcZapoln = VPos.shift147-(0*1.45) -- 20 - Процент заполнения

	vertice = {VPos.LeftX+VPos.shift147, VPos.UpY, tocolor(0, 0,255, 255)} -- Старт верх
	table.insert(PLvertices, vertice)
	vertice = {(VPos.LeftX)+LProcZapoln, VPos.UpY, tocolor(0, 0,255, 255)}
	table.insert(PLvertices, vertice)
	vertice = {(VPos.LeftX+VPos.shift20)+LProcZapoln, VPos.DownY, tocolor(0, 0,255, 255)}
	table.insert(PLvertices, vertice)
	vertice = {VPos.LeftX+VPos.shift147+VPos.shift20, VPos.DownY, tocolor(0, 0,255, 255)} -- Старт низ
	table.insert(PLvertices, vertice)
	-------------------Заполнение правый-----------
	local RProcZapoln = VPos.shift147-(0*1.45) -- 20 - Процент заполнения
	vertice = {VPos.RightX, VPos.UpY, tocolor(255, 50,50, 255)} -- start
	table.insert(PRvertices, vertice)
	local vertice = {(VPos.RightX+VPos.shift147)-RProcZapoln, VPos.UpY, tocolor(255, 50,50, 255)} -- pr
	table.insert(PRvertices, vertice)
	vertice = {(VPos.RightX+VPos.shift147-VPos.shift20)-RProcZapoln, VPos.DownY, tocolor(255, 50,50, 255)} -- 3 pr
	table.insert(PRvertices, vertice)
	vertice = {VPos.RightX-VPos.shift20, VPos.DownY, tocolor(255, 50,50, 255)} -- 4 s
	table.insert(PRvertices, vertice)
	------------------------------------------------
	--ToDo: изменить font0_RMedium
	CaptureGUILabel[1] = guiCreateLabel(screenW*0.46614, VPos.UpY, screenW*0.07031, screenH*0.040740, "10:00", false)
	guiSetFont(CaptureGUILabel[1], Five26)
	guiLabelSetHorizontalAlign(CaptureGUILabel[1], "center", false)
	guiLabelSetVerticalAlign(CaptureGUILabel[1], "center")
	-------------------------------------------------
	CaptureGUILabel[2] = guiCreateLabel(screenW*0.395833, VPos.UpY, screenW*0.07031, screenH*0.040740, "0", false)
	guiSetFont(CaptureGUILabel[2], Five26)
	guiLabelSetHorizontalAlign(CaptureGUILabel[2], "right", false)
	guiLabelSetVerticalAlign(CaptureGUILabel[2], "center")
	-------------------------------------------------
	CaptureGUILabel[3] = guiCreateLabel(VPos.RightX, VPos.UpY, screenW*0.07031, screenH*0.040740, "0", false)
	guiSetFont(CaptureGUILabel[3], Five26)
	guiLabelSetVerticalAlign(CaptureGUILabel[3], "center")
	-------------------------------------------------
	CaptureGUILabel[4] = guiCreateLabel(screenW*0.46614, screenH*0.01203, screenW*0.07031, screenH*0.02222, "TIME REMAINING", false)
	guiSetFont(CaptureGUILabel[4], fmedium)
	guiLabelSetHorizontalAlign(CaptureGUILabel[4], "center", false)
	-------------------------------------------------
	CaptureGUILabel[5] = guiCreateLabel(screenW*0.46614, screenH*0.075, screenW*0.07031, screenH*0.02222, "400 to Win", false)
	guiSetFont(CaptureGUILabel[5], fmedium)
	guiLabelSetHorizontalAlign(CaptureGUILabel[5], "center", false) 
	addEventHandler("onClientPreRender", root, preRenderCaptureUI)
end

function updateCaptureInfo(LostTime, AttackPoint, DefendPoint)
	for i in ipairs(PRvertices) do
		table.remove(PRvertices)
		table.remove(PLvertices)
	end
	guiSetText(CaptureGUILabel[2], AttackPoint)
	guiSetText(CaptureGUILabel[3], DefendPoint)
	----------------------------------------------
	local minute =  math.floor(LostTime/60)
	local seconds = LostTime%60
	if seconds < 10 then seconds = "0"..seconds.."" end
	guiSetText(CaptureGUILabel[1], ""..minute..":"..seconds.."")
	-------------------Заполнение левый-----------
	local LProcZapoln = VPos.shift147-((AttackPoint/4)*VPos.multiProc)

	vertice = {VPos.LeftX+VPos.shift147, VPos.UpY, tocolor(0, 0,255, 255)}
	table.insert(PLvertices, vertice)
	vertice = {(VPos.LeftX)+LProcZapoln, VPos.UpY, tocolor(0, 0,255, 255)}
	table.insert(PLvertices, vertice)
	vertice = {(VPos.LeftX+VPos.shift20)+LProcZapoln, VPos.DownY, tocolor(0, 0,255, 255)}
	table.insert(PLvertices, vertice)
	vertice = {VPos.LeftX+VPos.shift147+VPos.shift20, VPos.DownY, tocolor(0, 0,255, 255)}
	table.insert(PLvertices, vertice)
	-------------------Заполнение правый-----------
	local RProcZapoln = VPos.shift147-((DefendPoint/4)*VPos.multiProc)
	vertice = {VPos.RightX, VPos.UpY, tocolor(255, 50,50, 255)}
	table.insert(PRvertices, vertice)
	local vertice = {(VPos.RightX+VPos.shift147)-RProcZapoln, VPos.UpY, tocolor(255, 50,50, 255)}
	table.insert(PRvertices, vertice)
	vertice = {(VPos.RightX+VPos.shift147-VPos.shift20)-RProcZapoln, VPos.DownY, tocolor(255, 50,50, 255)}
	table.insert(PRvertices, vertice)
	vertice = {VPos.RightX-VPos.shift20, VPos.DownY, tocolor(255, 50,50, 255)}
	table.insert(PRvertices, vertice)
	------------------------------------------------
end
addEvent("updateCaptureInfo", true)
addEventHandler("updateCaptureInfo", localPlayer, updateCaptureInfo)

function preRenderCaptureUI()
	dxDrawPrimitive("trianglefan", false, unpack(Rvertices))
	dxDrawPrimitive("trianglefan", false, unpack(Lvertices))
	dxDrawPrimitive("trianglefan", false, unpack(PLvertices))
	dxDrawPrimitive("trianglefan", false, unpack(PRvertices))
	---------------------------------------------------------
end



function vievCaptureInfo(bool)
	if bool then
		if #CaptureGUILabel < 1 then
			createCaptureUI()
		end
	else
		for i in ipairs(PRvertices) do
			table.remove(PRvertices)
			table.remove(PLvertices)
			-----------------------
			table.remove(Lvertices)
			table.remove(PRvertices)
			-----------------------
		end
		for i in ipairs(CaptureGUILabel) do
			destroyElement(CaptureGUILabel[i])
			CaptureGUILabel[i] = nil
		end
		removeEventHandler("onClientPreRender", root, preRenderCaptureUI)
	end
end
addEvent("vievCaptureInfo", true)
addEventHandler("vievCaptureInfo", localPlayer, vievCaptureInfo)
