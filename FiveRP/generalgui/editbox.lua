local screenW, screenH = guiGetScreenSize()
local EditBox = {
    keys= {},
    label,
    edit
}

local BoxSizeY = 215 -- 0.21
local dxPosY = screenH*0.4231+215*0.1488+6 -- Позиция бокса + Размер титла + 6 пикселей

local ecreated = false
local eventid = nil
local EditBoxText = {}
local function CreateEditBox()
	if ecreated then return 1 end
	
	EditBox.Box = guiCreateStaticImage(screenW * 0.3906, screenH*0.4231, screenW*0.2192, screenH*0.1990, ":FiveRP/loginpanel/images/top.png", false) -- 421, 215
	guiSetProperty(EditBox.Box, "ImageColours", "tl:C9000000 tr:C9000000 bl:C9000000 br:C9000000")

	EditBox.edit = guiCreateEdit(8, 117, 403, 34, "", false, EditBox.Box)
	guiSetFont(EditBox.edit, fmedium)
	guiSetProperty(EditBox.edit, "NormalTextColour", "FF727272")
	guiSetProperty(EditBox.edit, "ActiveSelectionColour", "FF259925")
	---------------------------------------------------
	EditBox.keys[1] = guiCreateStaticImage(274, 161, 137, 38, ":images/button.png", false, EditBox.Box)
	guiSetProperty(EditBox.keys[1], "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
	---------------------------------------------------
	EditBox.keys[2] = guiCreateStaticImage(127, 161, 137, 38, ":images/button.png", false, EditBox.Box)
	guiSetProperty(EditBox.keys[2], "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
	--------------------------------------------------
	EditBox.keys[3] = guiCreateLabel(0.00, 0.00, 1.00, 1.00, "-", true, EditBox.keys[1])
	EditBox.keys[4] = guiCreateLabel(0.00, 0.00, 1.00, 1.00, "-", true, EditBox.keys[2])
	guiSetEnabled(EditBox.keys[3], false)
	guiSetEnabled(EditBox.keys[4], false)
	
	guiSetFont(EditBox.keys[3], fmedium)
	guiSetFont(EditBox.keys[4], fmedium)
	
	guiLabelSetHorizontalAlign(EditBox.keys[3], "center", false)
	guiLabelSetVerticalAlign(EditBox.keys[3], "center")
	
	guiLabelSetHorizontalAlign(EditBox.keys[4], "center", false)
	guiLabelSetVerticalAlign(EditBox.keys[4], "center")
	
	guiLabelSetColor(EditBox.keys[3], 22, 26, 31)
	guiLabelSetColor(EditBox.keys[4], 22, 26, 31)
	--------------------------------------------------
	EditBox.TitleBox = guiCreateStaticImage(0, 0, 424, 215*0.1488, ":FiveRP/loginpanel/images/pane.png", false, EditBox.Box)
	guiSetProperty(EditBox.TitleBox, "ImageColours", GUIConf.TitleColor)
	
	EditBox.label = guiCreateLabel(0.00, 0.10, 1.00, 1.00, "-", true, EditBox.TitleBox)
	guiSetFont(EditBox.label, Five26)
	guiLabelSetHorizontalAlign(EditBox.label, "center", false)
	guiLabelSetVerticalAlign(EditBox.label, "center")
	---------------------------------------------------
	created = true
	VievEditBox(false)
end
addEventHandler("onResourceStart", root, CreateEditBox)


local function RenderEditBoxText()
	dxDrawText(EditBoxText[2], PosX * screenW, dxPosY, screenW * 0.6349, screenH * 0.5056, tocolor(255, 255, 255, 255), 1.00, dxMedium, "left", "top", false, false, true, true, false)
end

local function MouseKeyEnter()
	if source == EditBox.keys[1] or source == EditBox.keys[2] then
		guiSetProperty(source, "ImageColours", "tl:FF006400  tr:FF006400 bl:FF006400 br:FF006400")
	end	
end

local function MouseKeyLeave()
	if source == EditBox.keys[1] or source == EditBox.keys[2] then
		guiSetProperty(source, "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
	end	
end

local function MouseKeyClick()
	if source == EditBox.keys[1] then
		triggerServerEvent("EditBoxHandler", localPlayer, localPlayer, eventid, guiGetText(EditBox.edit), 0)
		VievEditBox(false)
	elseif source == EditBox.keys[2] then
		triggerServerEvent("EditBoxHandler", localPlayer, localPlayer, eventid, guiGetText(EditBox.edit), 1)
		VievEditBox(false)
	end
end

function VievEditBox(bool)
	if not created then CreateEditBox() end
	guiSetVisible(EditBox.Box, bool)
	if bool then
		addEventHandler("onClientRender", root, RenderEditBoxText)
		guiSetText(EditBox.label, EditBoxText[1])
		guiSetText(EditBox.edit, "")
		
		addEventHandler("onClientMouseEnter", EditBox.keys[1], MouseKeyEnter)
		addEventHandler("onClientMouseLeave", EditBox.keys[1], MouseKeyLeave)
		addEventHandler("onClientGUIClick", EditBox.keys[1], MouseKeyClick)
		
		addEventHandler("onClientMouseEnter", EditBox.keys[2], MouseKeyEnter)
		addEventHandler("onClientMouseLeave", EditBox.keys[2], MouseKeyLeave)
		addEventHandler("onClientGUIClick", EditBox.keys[2], MouseKeyClick)
	else
		removeEventHandler("onClientRender", root, RenderEditBoxText)
		
		removeEventHandler("onClientMouseEnter", EditBox.keys[1], MouseKeyEnter)
		removeEventHandler("onClientMouseLeave", EditBox.keys[1], MouseKeyLeave)
		removeEventHandler("onClientGUIClick", EditBox.keys[1], MouseKeyClick)
		
		removeEventHandler("onClientMouseEnter", EditBox.keys[2], MouseKeyEnter)
		removeEventHandler("onClientMouseLeave", EditBox.keys[2], MouseKeyLeave)
		removeEventHandler("onClientGUIClick", EditBox.keys[2], MouseKeyClick)
	end
end

local function ShowEditBox(event, title, text, key1, key2)
	if not created then CreateEditBox() end
	eventid = event
	EditBoxText[1] = title
	text = string.gsub(text, "\t", "          ")
	EditBoxText[2] = text
	EditBoxText[3] = key1
	EditBoxText[4] = key2
	guiSetText(EditBox.keys[3], key1)
	guiSetText(EditBox.keys[4], key2)
	local line = explode("\n",text)
	local zline = #line-1
	--------------------------Символы в строке---------------
	local MaxLenLine = 0
	local z = 0
	local LineWSize
	for i in ipairs(line) do
		z = utf8.len(line[i])
		if MaxLenLine < z then  MaxLenLine = z LineWSize = dxGetTextWidth(line[i], 1, dxMedium, true) end
		
	end	
	--------------------Размер Y------------------------------
	BoxSizeY = 215+(FontSizeY*(zline-4))+5
	if zline < 5 then BoxSizeY = 215
	else end
	--KeyPosY = BoxPosY+BoxSizeY-0.05*screenH
	
	guiSetPosition(EditBox.edit, 8, BoxSizeY-38-16-44, false)
	
	--------------------Размер X------------------------------
	
	local SizeX = 10+LineWSize+10
	if SizeX < 424 then
		SizeX = 424
	end
	
	guiSetSize(EditBox.Box, SizeX, BoxSizeY, false)
	guiSetSize(EditBox.TitleBox, SizeX, 32, false)
	
	PosX = 0.5-(SizeX/screenW)/2
	
	guiSetSize(EditBox.edit, SizeX-16, 34, false)
	
	guiSetPosition(EditBox.keys[1], SizeX-147, BoxSizeY-38-16, false)
	guiSetPosition(EditBox.keys[2], SizeX-147-10-137, BoxSizeY-38-16, false)
	-----------------Позиция бокса---------------------------
	guiSetPosition(EditBox.Box, PosX, 0.5-(BoxSizeY/screenH)/2, true)
	---------------------------------------------------------
	PosX = PosX+0.0041
	dxPosY = (0.5-(BoxSizeY/screenH)/2) * screenH  + 215*0.1488+6
	VievEditBox(true)
end
addEvent("ShowEditBox", true)
addEventHandler("ShowEditBox", root, ShowEditBox)
