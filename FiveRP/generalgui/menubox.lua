local MSelect = {
	label = {},
	staticimage = {},
	line = {},
	lineimage = {}
}
local mlines = 7
local screenW, screenH = guiGetScreenSize()
local CreatedMSelectGUI = false
local MSelectMenyY = 0.44*screenH --475
local mline
local mselectid = 1
local mselectbox = 1
local mselectvieved = false
local eventid

local enter_exit_timer

local function CreateMSelectGUI()
	
	MSelect.TitleBox = guiCreateStaticImage(screenW*0.77, MSelectMenyY-65, screenW*0.18, 65, ":FiveRP/loginpanel/images/pane.png", false)
	guiSetProperty(MSelect.TitleBox, "ImageColours", GUIConf.TitleColor)
	MSelect.label[1] = guiCreateLabel(0.01, 0.08, 0.98, 0.83, "Текст Меню", true, MSelect.TitleBox)
	guiSetFont(MSelect.label[1], Five26)
	guiLabelSetHorizontalAlign(MSelect.label[1], "center", false)
	guiLabelSetVerticalAlign(MSelect.label[1], "center")

	MSelect.lineimage[1] = guiCreateStaticImage(screenW*0.77, MSelectMenyY, screenW*0.18, 32, ":FiveRP/loginpanel/images/pane.png", false)
	guiSetProperty(MSelect.lineimage[1], "ImageColours", "tl:D5010000 tr:D5010000 bl:D5010000 br:D5010000")


	MSelect.lineimage[2] = guiCreateStaticImage(screenW*0.77, MSelectMenyY+(32*1), screenW*0.18, 32, ":FiveRP/loginpanel/images/pane.png", false)
	guiSetProperty(MSelect.lineimage[2], "ImageColours", "tl:D5010000 tr:D5010000 bl:D5010000 br:D5010000")


	MSelect.lineimage[3] = guiCreateStaticImage(screenW*0.77, MSelectMenyY+(32*2), screenW*0.18, 32, ":FiveRP/loginpanel/images/pane.png", false)
	guiSetProperty(MSelect.lineimage[3], "ImageColours", "tl:D5010000 tr:D5010000 bl:D5010000 br:D5010000")


	MSelect.lineimage[4] = guiCreateStaticImage(screenW*0.77, MSelectMenyY+(32*3), screenW*0.18, 32, ":FiveRP/loginpanel/images/pane.png", false)
	guiSetProperty(MSelect.lineimage[4], "ImageColours", "tl:D5010000 tr:D5010000 bl:D5010000 br:D5010000")


	MSelect.lineimage[5] = guiCreateStaticImage(screenW*0.77, MSelectMenyY+(32*4), screenW*0.18, 32, ":FiveRP/loginpanel/images/pane.png", false)
	guiSetProperty(MSelect.lineimage[5], "ImageColours", "tl:D5010000 tr:D5010000 bl:D5010000 br:D5010000")


	MSelect.lineimage[6] = guiCreateStaticImage(screenW*0.77, MSelectMenyY+(32*5), screenW*0.18, 32, ":FiveRP/loginpanel/images/pane.png", false)
	guiSetProperty(MSelect.lineimage[6], "ImageColours", "tl:D5010000 tr:D5010000 bl:D5010000 br:D5010000")


	MSelect.lineimage[7] = guiCreateStaticImage(screenW*0.77, MSelectMenyY+(32*6), screenW*0.18, 32, ":FiveRP/loginpanel/images/pane.png", false)
	guiSetProperty(MSelect.lineimage[7], "ImageColours", "tl:D5010000 tr:D5010000 bl:D5010000 br:D5010000")


	MSelect.EndLine = guiCreateStaticImage(screenW*0.77, MSelectMenyY+(32*7), screenW*0.18, 32, ":FiveRP/loginpanel/images/pane.png", false)
	guiSetProperty(MSelect.EndLine, "ImageColours", "tl:FE010000 tr:FE010000 bl:FE010000 br:FE010000")
	
	MSelect.label[2] = guiCreateLabel(0.00, 0.09, 0.98, 0.80, "1/7", true, MSelect.EndLine)
	guiSetFont(MSelect.label[2], fmedium)
	guiLabelSetHorizontalAlign(MSelect.label[2], "center", false)
	guiLabelSetVerticalAlign(MSelect.label[2], "center")
	
	for i in ipairs(MSelect.lineimage) do
		guiSetProperty(MSelect.lineimage[i], "ImageColours", "tl:D5010000 tr:D5010000 bl:D5010000 br:D5010000")
		MSelect.line[i] = guiCreateLabel(0.02, 0.09, 0.98, 0.80, "-", true, MSelect.lineimage[i])
		guiSetFont(MSelect.line[i], fmedium)
		guiLabelSetHorizontalAlign(MSelect.line[i], "left", false)
		guiLabelSetVerticalAlign(MSelect.line[i], "center")
	end

	--Установка фона-----------------------------------------
	CreatedMSelectGUI = true
	
end

local function VievMselectGUI(bool)
	if CreatedMSelectGUI == false then CreateMSelectGUI() end
	if bool and mlines < 1 then return 1 end
	if bool then
		if isTimer(enter_exit_timer) then
			killTimer(enter_exit_timer)
		end	
		toggleControl("enter_exit", false) 
		--------------Биндим клавиши------------------------
		bindKey( "arrow_d", "down", MSelectPressKey) 
		bindKey( "arrow_u", "down", MSelectPressKey) 
		bindKey( "enter", "down", MSelectEnter) 
		------------Скрываем элементы-----------------------
		for i in ipairs(MSelect.line) do
			guiSetVisible(MSelect.line[i], false)
			guiSetVisible(MSelect.lineimage[i], false)
			
			guiLabelSetColor(MSelect.line[i], 255, 255, 255)
			guiSetProperty(MSelect.lineimage[i], "ImageColours", "tl:BF000000 tr:BF000000 bl:BF000000 br:BF000000")
		end
		---------------Показываем каркас меню----------------
		guiSetVisible(MSelect.TitleBox, true)
		guiSetVisible(MSelect.label[2], true)
		guiSetVisible(MSelect.label[1], true)
		---------------Показываем строки меню----------------
		for i = 1, mlines do
			if i > 7 then break end
			guiSetVisible(MSelect.lineimage[i], bool)
			guiSetVisible(MSelect.line[i], bool)
		end
		----------------Устанавливаем последнюю строку-------
		if mlines < 7 then
			guiSetPosition (MSelect.EndLine, screenW*0.77, MSelectMenyY+(32*(mlines)), false)
		else
			guiSetPosition ( MSelect.EndLine, screenW*0.77, MSelectMenyY+(32*7), false)
		end
		guiSetText(MSelect.label[2], "1/"..mlines.."")
		guiSetVisible(MSelect.EndLine, true)
		guiSetVisible(MSelect.label[2], true)
		mselectvieved = true
		------------------Раскрашиваем элементы-----------------------
		guiLabelSetColor(MSelect.line[1], 0, 0, 0)
		guiSetProperty(MSelect.lineimage[1], "ImageColours", "tl:FFFFFFFF tr:FFFFFFFF bl:FFFFFFFF br:FFFFFFFF")
		
	else
		------------------Скрываем элементы меню-------------------
		mselectvieved = false
		
		
		guiSetVisible(MSelect.EndLine, false)
		guiSetVisible(MSelect.label[2], false)
		guiSetVisible(MSelect.TitleBox, false)
		guiSetVisible(MSelect.label[1], false)
		
		for i = 1, 7 do
			guiSetVisible(MSelect.lineimage[i], false)
			guiSetVisible(MSelect.line[i], false)
			if i > 7 then break end
		end

		unbindKey( "arrow_d", "down", MSelectPressKey) 
		unbindKey( "arrow_u", "down", MSelectPressKey)
		unbindKey( "enter", "down", MSelectEnter) 
		end
end

function ShowSelectMenu(event, title, text)
	mselectid = nil
	mselectbox = 1
	eventid = event
	VievMselectGUI(false)
	if not CreatedMSelectGUI then CreateMSelectGUI() end
	guiSetText(MSelect.label[1], title)
	text = ""..text.."\nЗакрыть"
	mselectid = 1
	mline = explode("\n", text)
	mlines = #mline
	if mlines > 7 then
		for i = 1, 7 do
			guiSetText(MSelect.line[i], mline[i])
		end
	else
		for i in ipairs(mline) do
			guiSetText(MSelect.line[i], mline[i])
		end	
	end
	VievMselectGUI(true)
end
addEvent("ShowSelectMenu", true)
addEventHandler("ShowSelectMenu", root, ShowSelectMenu)

local function MSelectPaint(paintid)
	playSFX("genrl", 53, 6, false)
	
	for i = 1, 7 do
		guiLabelSetColor(MSelect.line[i], 255, 255, 255)
		guiSetProperty(MSelect.lineimage[i], "ImageColours", "tl:BF000000 tr:BF000000 bl:BF000000 br:BF000000")
	end
	if not paintid then return 1 end
	guiLabelSetColor(MSelect.line[paintid], 0, 0, 0)
	guiSetProperty(MSelect.lineimage[paintid], "ImageColours", "tl:FFFFFFFF tr:FFFFFFFF bl:FFFFFFFF br:FFFFFFFF")

	guiSetText(MSelect.label[2], ""..mselectid.."/"..mlines.."")
end


local function MoveSelectLine(param)
	if param == 1 then -- Возвращаемся в начало списка  //  Проверку на кол-во элементов в списке
		for i = 1, 7 do 
			guiSetText(MSelect.line[i], mline[i])
		end
	elseif param == 2 then -- Листание списка вверх
		for i = 1, 7 do
			guiSetText(MSelect.line[i], mline[(mselectid-7) + i])
		end
	elseif param == 3 then -- Возвращаемся в конец списка
		guiSetText(MSelect.line[1], mline[(mselectid-6)])
		guiSetText(MSelect.line[2], mline[(mselectid-5)])
		guiSetText(MSelect.line[3], mline[(mselectid-4)])
		guiSetText(MSelect.line[4], mline[(mselectid-3)])
		guiSetText(MSelect.line[5], mline[(mselectid-2)])
		guiSetText(MSelect.line[6], mline[mselectid-1])
		guiSetText(MSelect.line[7], mline[mselectid])
	elseif param == 4 then
		guiSetText(MSelect.line[1], mline[mselectid])
		guiSetText(MSelect.line[2], mline[mselectid+1])
		guiSetText(MSelect.line[3], mline[mselectid+2])
		guiSetText(MSelect.line[4], mline[mselectid+3])
		guiSetText(MSelect.line[5], mline[mselectid+4])
		guiSetText(MSelect.line[6], mline[mselectid+5])
		guiSetText(MSelect.line[7], mline[mselectid+6])
	
	end	
end

function MSelectPressKey(key, state)
	if key == "arrow_d" then
		mselectid = mselectid+1
		
		mselectbox = mselectbox+1
		if mselectbox > 7 then
			mselectbox = 7
		end	
		if mselectid > mlines then
			mselectid = 1
			mselectbox = 1
			if mlines > 7 then
				MoveSelectLine(1) -- Сброс меню
			end	
		end
		if mselectid > 7 and mselectbox == 7 then 
			MoveSelectLine(2) -- Листание меню вниз
		end
		MSelectPaint(mselectbox)
	elseif key == "arrow_u" then 
		
		mselectid = mselectid-1
		mselectbox = mselectbox-1
		if mselectbox < 1 and mselectid < 1 then
			mselectid = mlines
			mselectbox = mlines
			if mlines > 7 then
				mselectbox = 7
				MoveSelectLine(3)
			end	
		elseif mselectbox < 1 and mselectid > 1 then
			MoveSelectLine(4)
			mselectbox = 1
		elseif mselectbox < 1 and mselectid == 1 then
			mselectbox = 1
			mselectid = 1
			MoveSelectLine(1)
		end
		MSelectPaint(mselectbox)
	end
	local statusevent = triggerEvent("MSelectPressKey", localPlayer, mselectid) 
end

function MSelectEnter()	
	if mselectid == mlines then triggerServerEvent ( "SelectMenuHandler", localPlayer, localPlayer,  eventid, 0) --Закрываем меню
	elseif mselectid ~= mlines then  triggerServerEvent ( "SelectMenuHandler", localPlayer, localPlayer,  eventid, mselectid, mline) end
	mselectid = nil
	mselectbox = 1
	VievMselectGUI(false)
	mline = nil
	enter_exit_timer = setTimer ( function()
	toggleControl("enter_exit", true) 
	end, 100, 1 )
end