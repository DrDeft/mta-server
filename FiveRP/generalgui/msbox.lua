
GUIMSBox = {
	keys = {},
	keystext = {},
    key2 = {},
    key1 = {},
    key3 = {}
}
local TitleText = ""
local MSGText = ""
local KeyText = {nil, nil, nil}
local EventID
local screenW, screenH = guiGetScreenSize()
local BoxPosY = screenH* 0.34
local BoxSizeY = 166 -- 1,3 строки = 0.13
local KeyPosY = BoxPosY+BoxSizeY-0.05
local PosX
local SizeX

local font = "fonts/Roboto-Medium.ttf"
if screenW < 1281  then 
	dxMedium = dxCreateFont(font, 8) 
	FontSizeY = 15
else
	dxMedium = dxCreateFont(font, 10)
	FontSizeY = 18
end

local function CreateMSBoxGUI()
	GUIMSBox.Box = guiCreateStaticImage((screenW - 451) / 2, (screenH - 166) / 2, 451, 166, ":FiveRP/loginpanel/images/pane.png", false)
	guiSetProperty(GUIMSBox.Box, "ImageColours", "tl:C9000000 tr:C9000000 bl:C9000000 br:C9000000")

	GUIMSBox.TitleBox = guiCreateStaticImage(0, 0, 451, 32, ":FiveRP/loginpanel/images/pane.png", false, GUIMSBox.Box)
	guiSetProperty(GUIMSBox.TitleBox, "ImageColours", GUIConf.TitleColor)
	
	GUIMSBox.keys[1] = guiCreateStaticImage(304, 121, 137, 38, ":images/button.png", false, GUIMSBox.Box)
	GUIMSBox.keys[2] = guiCreateStaticImage(157, 121, 137, 38, ":images/button.png", false, GUIMSBox.Box)
	GUIMSBox.keys[3] = guiCreateStaticImage(10, 121, 137, 38, ":images/button.png", false, GUIMSBox.Box)  

	GUIMSBox.Title = guiCreateLabel(0.00, 0.10, 1.00, 0.78, "-", true, GUIMSBox.TitleBox)
	guiSetFont(GUIMSBox.Title, Five26)
	guiLabelSetHorizontalAlign(GUIMSBox.Title, "center", false)
	guiLabelSetVerticalAlign(GUIMSBox.Title, "center")
	
	for i in ipairs(GUIMSBox.keys) do
		GUIMSBox.keystext[i] = guiCreateLabel(0.00, 0.00, 1.00, 1.00, "-", true, GUIMSBox.keys[i])
		guiLabelSetHorizontalAlign(GUIMSBox.keystext[i], "center", true)
		guiLabelSetVerticalAlign(GUIMSBox.keystext[i], "center")
		guiSetEnabled (GUIMSBox.keystext[i], false)
		guiSetProperty(GUIMSBox.keys[i], "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
		guiSetFont(GUIMSBox.keystext[i], fmedium)
		guiLabelSetColor(GUIMSBox.keystext[i], 22, 26, 31)
	end

	
end

local function UpdateGUIPos()
	guiSetPosition(GUIMSBox.keys[1], SizeX - (10+137), BoxSizeY-(7+38), false) -- 26 с конца SizeX - 26
	guiSetPosition(GUIMSBox.keys[2], SizeX - ((137+20)+137), BoxSizeY-(7+38), false)
	guiSetPosition(GUIMSBox.keys[3], SizeX - ((137+30)+137*2), BoxSizeY-(7+38), false)
end

local function ShowMessageBox(eventid, title, text, key1, key2, key3)
	if key1 == nil then return 1 end
	EventID = eventid
	KeyText[1] = key1
	KeyText[2] = key2
	KeyText[3] = key3
	TitleText = title
	MSGText = "\n"..text.."\n"
	local line = explode("\n",text)
	zline = #line-1
	BoxSizeY = 141+(FontSizeY*(zline-3))+10
	VievMSBox(true)
	if zline < 4 then BoxSizeY = 141
	else end
	KeyPosY = BoxPosY+BoxSizeY-0.05*screenH
	--------------------------Символы в строке---------------
	local MaxLenLine = 0
	local z = 0
	local LineWSize
	for i in ipairs(line) do
		z = utf8.len(line[i])
		if MaxLenLine < z then  MaxLenLine = z LineWSize = dxGetTextWidth(line[i], 1, dxMedium, true) end
	end	
	---------------------------------------------------------
	SizeX = 5+LineWSize+15
	
	if SizeX < 451 then
		SizeX = 451
	end
	
	guiSetSize(GUIMSBox.Box, SizeX, BoxSizeY, false)
	guiSetSize(GUIMSBox.TitleBox, SizeX, 32, false)
	PosX = 0.5-(SizeX/screenW)/2
	BoxPosY = 0.5-(BoxSizeY/screenH)/2
	guiSetPosition(GUIMSBox.Box, PosX*screenW, BoxPosY*screenH, false)
	UpdateGUIPos()
end
addEvent("ShowMessageBox", true)
addEventHandler("ShowMessageBox", root, ShowMessageBox)

addEvent("HideMessageBox", true)
addEventHandler("HideMessageBox", root, function() VievMSBox(false) end)

local function RenderMSGText()
	dxDrawText(MSGText, PosX*screenW+10, screenH*BoxPosY+20, screenW*29.5, screenH*(BoxPosY+BoxSizeY-0.05), tocolor(255, 255, 255, 255), 1.00, dxMedium, "left", "top", false, false, true, true, false)
end

local function MouseKeyEnter()
	if source == GUIMSBox.keys[1] or source == GUIMSBox.keys[2] or source == GUIMSBox.keys[3] then
		guiSetProperty(source,"ImageColours",  "tl:FF006400  tr:FF006400 bl:FF006400 br:FF006400")
	end
end

local function MouseKeyLeave()
	if source == GUIMSBox.keys[1] or source == GUIMSBox.keys[2] or source == GUIMSBox.keys[3] then
		guiSetProperty(source,"ImageColours",  "tl:FF008000  tr:FF008000 bl:FF008000 br:FF008000")
	end
end

local function MouseKeyClick()
	if source == GUIMSBox.keys[1] then
		--Кнопка1
		triggerServerEvent("MessageBoxHandler", localPlayer, localPlayer, 1, EventID)
		VievMSBox(false)
	elseif source == GUIMSBox.keys[2] then
		--Кнопка2
		triggerServerEvent("MessageBoxHandler", localPlayer, localPlayer, 2, EventID)
		VievMSBox(false)
	elseif source == GUIMSBox.keys[3] then
		-- Кнопка3
		triggerServerEvent("MessageBoxHandler", localPlayer, localPlayer, 3, EventID)
		VievMSBox(false)
	end
end

function VievMSBox(bool)
	if GUIMSBox.Box == nil then CreateMSBoxGUI() end
	guiSetVisible(GUIMSBox.Box, bool)
	guiSetText(GUIMSBox.Title, TitleText)
	if bool then 
		addEventHandler("onClientRender", root, RenderMSGText)
		addEventHandler("onClientMouseEnter", root, MouseKeyEnter)
		addEventHandler("onClientMouseLeave", root, MouseKeyLeave)
		addEventHandler("onClientGUIClick", root, MouseKeyClick)
		
		guiSetText(GUIMSBox.keystext[1], KeyText[1])
		if KeyText[2] ~= nil then
			guiSetText(GUIMSBox.keystext[2], KeyText[2])
			guiSetVisible(GUIMSBox.keys[2], true)
		else
			guiSetVisible(GUIMSBox.keys[2], false)
		end	
		if KeyText[3] ~= nil then
			guiSetText(GUIMSBox.keystext[3], KeyText[3])
			guiSetVisible(GUIMSBox.keys[3], true)
		else 
			guiSetVisible(GUIMSBox.keys[3], false)
		end	
	else 
		removeEventHandler("onClientRender", root, RenderMSGText) 
		removeEventHandler("onClientMouseEnter", root, MouseKeyEnter)
		removeEventHandler("onClientMouseLeave", root, MouseKeyLeave)
		removeEventHandler("onClientGUIClick", root, MouseKeyClick)
	end
	
end
