GUINotice = {
    label = {},
    staticimage = {}
}
local NoticeTimer
local NoticeSound
local ActNotice = false
local DownNoticeText = ""


local font = "fonts/Roboto-Medium.ttf"
local screenW, screenH = guiGetScreenSize()
if screenW < 1281  then 
	Medium = guiCreateFont(font, 8) 
else
	Medium = guiCreateFont(font, 10) end

addEventHandler("onClientResourceStart", resourceRoot,
    function()
        GUINotice.staticimage[1] = guiCreateStaticImage(0.78, 0.00, 0.16, 0.05, ":FiveRP/loginpanel/images/pane.png", true)
        guiSetProperty(GUINotice.staticimage[1], "ImageColours", "tl:ED010000 tr:ED010000 bl:ED010000 br:ED010000")

        GUINotice.label[1] = guiCreateLabel(0.01, 0.09, 0.98, 0.84, "У вас недостаточно средств и нет прав на эту команду", true, GUINotice.staticimage[1])
        guiSetFont(GUINotice.label[1], Medium)
        guiLabelSetHorizontalAlign(GUINotice.label[1], "left", true) 
		
		guiSetVisible(GUINotice.label[1], false)
		guiSetVisible(GUINotice.staticimage[1], false)
    end
)

function VievGUINotice(bool)
	guiSetVisible(GUINotice.label[1], bool)
	guiSetVisible(GUINotice.staticimage[1], bool)
end

function HideNootice()
	VievGUINotice(false)
	ActNotice = false
end

function GetTextNotice(text, sound)
	local number = string.len(text)
	if number < 35*2 then
		guiSetSize ( GUINotice.staticimage[1], 0.16, 0.03, true)
		guiSetSize ( GUINotice.label[1], 0.98, 0.84, true)
	else
		guiSetSize ( GUINotice.staticimage[1], 0.16, 0.05, true)
		guiSetSize ( GUINotice.label[1], 0.98, 0.84, true)
	end
	guiSetText(GUINotice.label[1], text)
	VievGUINotice(true)
	if ActNotice == true then
		if isTimer(NoticeTimer) then killTimer ( NoticeTimer ) end
	end
	
	NoticeTimer = setTimer ( HideNootice, 5000, 1)
	NoticeSound = playSFX("genrl", 53, 6, false)
	setSoundVolume(NoticeSound, 0.5)
	ActNotice = true
end
addEvent("GetTextNotice", true)
addEventHandler("GetTextNotice", localPlayer, GetTextNotice)
