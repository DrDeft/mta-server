local HospitalBed = {}
HospitalBed[1] = {playerid = nil, pos = {311.540, 1126.380, 3093.607, 270}}
HospitalBed[2] = {playerid = nil, pos = {308.540, 1126.380, 3093.607, 270}}
HospitalBed[3] = {playerid = nil, pos = {305.540, 1126.380, 3093.607, 270}}

local PreDeathPlayers = {}

local function findFreeHospitalBed()
	local x = nil;
	for i in ipairs(HospitalBed) do
		if HospitalBed[i].playerid == nil then
			x = i;
			break
		end
	end
	return x;
end

local function freeHospitalBed(bed)
	HospitalBed[bed].playerid = nil;
end

function playerToHospital(player, hospitaltime)
	local playerid = getPlayerID(player);
	if hospitaltime == nil then hospitaltime = 5 end -- 5 min
	Players[playerid]["hospital"] = hospitaltime;
	local Bed = findFreeHospitalBed();
	if not Bed then --[[ ToDO: Добавить действие при отсутствии места в больнице ]] return 1 end
	HospitalBed[Bed].playerid = playerid;
	setElementHealthAC(player, 10)
	--------------------------------------------------------------
	setPlayerInterior(player, 20002)-- ИНТ больницы
	setPlayerDimension (player, 1)
	--------------------------------------------------------------
	setElementPosition(player, HospitalBed[Bed].pos[1], HospitalBed[Bed].pos[2], HospitalBed[Bed].pos[3]);
	setElementRotation(player, 0, 0, HospitalBed[Bed].pos[4], "default", true)  ;
	------------------------Анимация-----------------------------
	setPedAnimation (player, "CRACK", "crckdeth2", -1, false);
	-------------------------------------------------------------
	TParams[playerid].HospitalBed = Bed
end

function playerLeaveHospital(player)
	local playerid = getPlayerID(player);
	if iSPlayerOK(playerid) and TParams[playerid].HospitalBed then
		freeHospitalBed(TParams[playerid].HospitalBed)
		TParams[playerid].HospitalBed = nil
		setPedAnimation(player, false)
		--[[
			ToDo: Убрать игрока с таймера больницы
		]]
	end
end

function hospitalTimer()
	for i in ipairs(HospitalBed) do
		if HospitalBed[i].playerid then
			if iSPlayerOK(HospitalBed[i].playerid) then
				Players[HospitalBed[i].playerid]["hospital"] = Players[HospitalBed[i].playerid]["hospital"] -1;
				if Players[HospitalBed[i].playerid]["hospital"] < 1 then
					setElementHealthAC(Players[HospitalBed[i].playerid]["player"], 100)
					playerLeaveHospital(Players[HospitalBed[i].playerid]["player"])
				end
			else
				freeHospitalBed(i)
			end
		end
	end
	-- for i in ipairs(HospitalPlayerTimer) do
		-- if iSPlayerOK(HospitalPlayerTimer[i]) then
		
			-- Players[HospitalPlayerTimer[i]]["hospital"] = Players[HospitalPlayerTimer[i]]["hospital"] -1;
			
			-- if Players[HospitalPlayerTimer[i]]["hospital"] < 1 then
				-- playerLeaveHospital(Players[HospitalPlayerTimer[i]]["player"])
				-- setElementHealthAC(Players[HospitalPlayerTimer[i]]["player"], 100)
				-- table.remove(HospitalPlayerTimer, i)
			-- end
			-- -- if Players[HospitalPlayerTimer[i]]["health"] < 100 then
				-- -- setElementHealthAC(Players[HospitalPlayerTimer[i]]["player"], Players[HospitalPlayerTimer[i]]["health"]+1)
			-- -- else
				-- -- playerLeaveHospital(Players[HospitalPlayerTimer[i]]["player"])
				-- -- table.remove(HospitalPlayerTimer, i)
				-- -- -- Выписываем игрока из больницы
			-- -- end
		-- else
			-- table.remove(HospitalPlayerTimer, i)
		-- end	
	-- end
end
setTimer(hospitalTimer, 1000, 0);

function preDeathTimer()
	for i in ipairs(PreDeathPlayers) do
		if TParams[PreDeathPlayers[i]].PreDeathTime then
			if TParams[PreDeathPlayers[i]].PreDeathTime > 0 then
				TParams[PreDeathPlayers[i]].PreDeathTime = TParams[PreDeathPlayers[i]].PreDeathTime-1
			else
				onPlayerDeath(Players[PreDeathPlayers[i]].player)
				TParams[PreDeathPlayers[i]].PreDeathTime = nil
			end
		else
			table.remove(PreDeathPlayers, i) -- Игрок вышел
		end
	end
end
setTimer(preDeathTimer, 1000, 0);

----------------------------------------Игрок умер------------------------------
--[[
	@setPlayerPreDeathState - устанавливает предсмертное состояние игроку. 
	Принимает параметры: - Игрок
	Статус состояния: - Установить/Отключить
]]

function onPlayerDeath(player)
	local playerid = getPlayerID(player)
	spawnPlayer(player, 0, 0, 0)
	setElementModel(player, Players[playerid]["skin"])
	takeWeaponAC(player, "all")
	setElementHealthAC(player, 10)
	playerToHospital(player)
	setPlayerPreDeathState(player, false)
end

function setPlayerPreDeathState(player, bool)
	local playerid = getPlayerID(player)
	if bool then
		local x,y,z = getElementPosition(player)
		spawnPlayer(player, x, y, z, 0, Players[playerid]["skin"], 0, Players[playerid]["vw"])
		setPlayerInterior(player, Players[playerid]["int"])
		setElementHealthAC(player, 10)
		setPedAnimation(player, "CRACK", "crckdeth2", -1, true, false, false, true)
		TParams[playerid].PreDeathState = true;
		TParams[playerid].PreDeathTime = 600; -- Время которое игрок находится в состоянии при смерти(10мин)
		table.insert(PreDeathPlayers, playerid)
	else
		TParams[playerid].PreDeathState = nil;
		TParams[playerid].PreDeathTime = nil;
	end
end

--[[
	@backPlayerToHospital - возвращает игрока в больницу если он не закончил лечение.
]]

function backPlayerToHospital(player)
	local playerid = getPlayerID(player)
	local Bed = TParams[playerid].HospitalBed
	spawnPlayer(player, HospitalBed[Bed].pos[1], HospitalBed[Bed].pos[2], HospitalBed[Bed].pos[3], HospitalBed[Bed].pos[4], Players[playerid]["skin"])
	setElementHealthAC(player, 10)
	--------------------------------------------------------------
	setPlayerInterior(player, 20002)-- ИНТ больницы
	setPlayerDimension(player, 1)
	--------------------------------------------------------------
	setElementPosition(player, HospitalBed[Bed].pos[1], HospitalBed[Bed].pos[2], HospitalBed[Bed].pos[3]);
	setElementRotation(player, 0, 0, HospitalBed[Bed].pos[4], "default", true);
	------------------------Анимация------------------------------
	setPedAnimation(player, "CRACK", "crckdeth2", -1, false);
end
