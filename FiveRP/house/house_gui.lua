local HouseGUI = {}
local screenW, screenH = guiGetScreenSize()
local HouseInfo

local PosX = screenW/2-192.5
local PosY = screenH/2-192.5

local function CreateHouseInfo()
	HouseGUI.TitleBox = guiCreateStaticImage(PosX, PosY, 385, 32, ":FiveRP/loginpanel/images/pane.png", false)
	guiSetProperty(HouseGUI.TitleBox, "ImageColours", GUIConf.TitleColor)
	HouseGUI.Title = guiCreateLabel(0, 0, 1, 1, "О доме", true, HouseGUI.TitleBox)
	guiSetFont(HouseGUI.Title, Five26)
	guiLabelSetHorizontalAlign(HouseGUI.Title, "center", false)
	guiLabelSetVerticalAlign (HouseGUI.Title, "center")

	HouseGUI.Box = guiCreateStaticImage(PosX, PosY+32, 385, 172, ":FiveRP/loginpanel/images/pane.png", false)
	guiSetProperty(HouseGUI.Box, "ImageColours", "tl:C9000000 tr:C9000000 bl:C9000000 br:C9000000")

	HouseGUI.HouseIcon = guiCreateStaticImage(10, 10, 128, 128, ":images/home.png", false, HouseGUI.Box)
	HouseGUI.HouseNumber = guiCreateLabel(10, 139, 128, 23, "Дом №342", false, HouseGUI.Box)
	guiSetFont(HouseGUI.HouseNumber, fmedium)
	guiLabelSetHorizontalAlign(HouseGUI.HouseNumber, "center", false)
	
	HouseGUI.Button = guiCreateStaticImage(238, 124, 137, 38, ":images/button.png", false, HouseGUI.Box) 
	HouseGUI.ButtonText = guiCreateLabel(0, 0, 1, 1, "Назад", true, HouseGUI.Button)
	guiSetProperty(HouseGUI.Button, "ImageColours", GUIConf.TitleColor)
	
	
	guiLabelSetHorizontalAlign(HouseGUI.ButtonText, "center", false)
	guiLabelSetVerticalAlign (HouseGUI.ButtonText, "center")
	guiSetFont(HouseGUI.ButtonText, fmedium)
	guiSetEnabled(HouseGUI.ButtonText, false)
	guiLabelSetColor(HouseGUI.ButtonText, 22, 26, 31)
end

local function DrawingHouseText()
	dxDrawText("Владелец: #008000"..HouseInfo["OwnerName"].."#ffffff\nДвери: "..HouseInfo["Doors"].."\nЦена: #008000$"..HouseInfo["Price"].."#ffffff\nКомнат:#008000 5#ffffff\nРайон: #008000"..HouseInfo["district"].."#ffffff\nАренда: "..HouseInfo["RentPrice"].."", PosX+158, PosY+42, 1184, 578, tocolor(255, 255, 255, 255), 1.00, dxMedium, "left", "top", false, false, true, true, false)
end

local function MouseKeyEnter()
	if source == HouseGUI.Button then
		guiSetProperty(source, "ImageColours", "tl:FF006400  tr:FF006400 bl:FF006400 br:FF006400")
	end
end

local function MouseKeyLeave()
	if source == HouseGUI.Button then
		guiSetProperty(source, "ImageColours", GUIConf.TitleColor)
	end	
end

local function MouseKeyClick()
	if source == HouseGUI.Button then
		if getElementDimension(localPlayer) == HouseInfo.ID+10000 and getElementInterior(localPlayer) == HouseInfo["Interior"] then
			triggerServerEvent("ShowSelectMenu", localPlayer, localPlayer, 16, "Дом №"..HouseInfo.ID.."", "Выйти\nОткрыть/Закрыть замок\nИнформация\nУправление домом\nОткрыть сейф")
		else
			if HouseInfo["Owner"] == -1 then
				triggerServerEvent("ShowSelectMenu", localPlayer, localPlayer, 15, "Дом №"..HouseInfo.ID.."", "Войти\nОткрыть/Закрыть замок\nИнформация\nКупить дом")
			else
				triggerServerEvent("ShowSelectMenu", localPlayer, localPlayer, 15, "Дом №"..HouseInfo.ID.."", "Войти\nОткрыть/Закрыть замок\nИнформация\nАренда")
			end
		end	
		VievHouseInfo(false)
	end	
end

function VievHouseInfo(bool, hInfo)
	if HouseGUI.TitleBox == nil then CreateHouseInfo() end
	guiSetVisible(HouseGUI.TitleBox, bool)
	guiSetVisible(HouseGUI.Box, bool)
	if bool then
		if HouseInfo then return 1 end
		HouseInfo = hInfo
		guiSetText(HouseGUI.HouseNumber, "Дом №"..HouseInfo.ID)
		
		if HouseInfo["Owner"] == -1 then
			HouseInfo["OwnerName"] = "#ff0000Дом продаётся#ffffff"
		end
		
		if HouseInfo["Doors"] == 1 then
			HouseInfo["Doors"] = "#ff0000Закрыты#ffffff"
		else
			HouseInfo["Doors"] = "#008000Открыты#ffffff"
		end	
		
		if HouseInfo["RentPrice"] == 0 then
			HouseInfo["RentPrice"] = "#ff0000Не сдается#ffffff"
		else
			HouseInfo["RentPrice"] = "#008000$"..HouseInfo["RentPrice"].."#ffffff"
		end
		
		HouseInfo["district"] = getZoneName(HouseInfo["Pos_x"], HouseInfo["Pos_x"], HouseInfo["Pos_x"])
		
		addEventHandler("onClientRender", root, DrawingHouseText)
		
		addEventHandler("onClientMouseEnter", HouseGUI.Button, MouseKeyEnter)
		addEventHandler("onClientMouseLeave", HouseGUI.Button, MouseKeyLeave)
		addEventHandler("onClientGUIClick", HouseGUI.Button, MouseKeyClick)
	else
		HouseInfo = nil
		removeEventHandler("onClientRender", root, DrawingHouseText)
		
		removeEventHandler("onClientMouseEnter", HouseGUI.Button, MouseKeyEnter)
		removeEventHandler("onClientMouseLeave", HouseGUI.Button, MouseKeyLeave)
		removeEventHandler("onClientGUIClick", HouseGUI.Button, MouseKeyClick)
	end
end
addEvent("VievHouseInfo", true)
addEventHandler("VievHouseInfo", localPlayer, VievHouseInfo)


