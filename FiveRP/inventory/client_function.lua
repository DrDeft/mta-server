local selectcell = 0
local aselectcell = 0
local iteminfotext = ""
local acellmode = nil 
--[[
	ID: 1 - Магазин
	ID: 2 - Багажник авто
	ID: 3 - Сейф Дома
	ID: 4 - Склад фракции
]]
local aparam = nil

local RareName = { "Обычный", "#42aaffРедкий#ffffff", "#ffff00Очень редкий#ffffff", "#ffa500Легендарный#ffffff"}

ItemData = {}
local PlayerItems
altItems = {}

function OpenAltInventory(type, items, param)
	altItems = items
	acellmode = type
	aparam = param -- Наценка магазина/ID Авто
	VievaInventory()
end
addEvent("OpenAltInventory", true)
addEventHandler("OpenAltInventory", localPlayer, OpenAltInventory)


local function LoadItemsData(data)
	ItemData = data
end
addEvent("LoadItemsData", true)
addEventHandler("LoadItemsData", localPlayer, LoadItemsData)

local function CallSyncItems(res)
	if res ~= getThisResource() then return 1 end
	triggerServerEvent("SyncLocalItemData", resourceRoot, localPlayer)
	CreateInventoryGUI()
end
addEventHandler("onClientResourceStart", resourceRoot, CallSyncItems)


function SelectSlotEnter()
	if getElementParent(source) == GUIInventory.background then -- Определяем родителя GUI окна(Инвентарь)
		for i in ipairs(GUIInventory.cell) do
			if source == GUIInventory["cell"][i] then
				guiSetProperty(GUIInventory.cell[i], "ImageColours", "tl:FF696464 tr:FF696464 bl:FF696464 br:FF696464")
				if PlayerItems[i]["item"] ~= 0 then VievItemInfo(i, 1) end
				break
			end
		end
	elseif getElementParent(source) == GUIInventory.abackground then
		for i in ipairs(GUIInventory.acell) do
			if source == GUIInventory["acell"][i] then
				guiSetProperty(GUIInventory.acell[i], "ImageColours", "tl:FF696464 tr:FF696464 bl:FF696464 br:FF696464")
				if altItems[i] ~= 0 and acellmode == 1 then VievItemInfo(i, 2) end
				break
			end
		end
	end	
end
addEventHandler( "onClientMouseEnter",  getRootElement(), SelectSlotEnter) 

local function SelectSlotLeave()
	if getElementParent(source) == GUIInventory.background then -- Определяем родителя GUI окна(Инвентарь)
		for i in ipairs(GUIInventory.cell) do
			if source == GUIInventory["cell"][i] then
				guiSetProperty(GUIInventory.cell[i], "ImageColours", "tl:FF837f7f tr:FF837f7f bl:FF837f7f br:FF837f7f")
				if PlayerItems[i]["item"] ~= 0 then VievGUIItemInfo(false) end
				break
			end
		end
	elseif getElementParent(source) == GUIInventory.abackground then
		for i in ipairs(GUIInventory.acell) do
			if source == GUIInventory["acell"][i] then
				guiSetProperty(GUIInventory.acell[i], "ImageColours", "tl:FF837f7f tr:FF837f7f bl:FF837f7f br:FF837f7f")
				if altItems[i] ~= 0 then VievGUIItemInfo(false) end
				break
			end
		end
	end	
end
addEventHandler( "onClientMouseLeave",  getRootElement(), SelectSlotLeave) 

local function SelectSlotClick(button)
	VievGUIItemInfo(false)
	if getElementParent(source) == GUIInventory.background then
		if source == GUIInventory.help then
			return VievInventoryHelp()
		end
		for i in ipairs(GUIInventory.cell) do
			if source == GUIInventory["cell"][i] then
				if selectcell ~= 0 and PlayerItems[selectcell]["item"] ~= 0 and PlayerItems[i]["item"] == 0 then -- Перемещаем предмет в пустой слот
					return MoveItemToSlot(selectcell, i)
				end
				if getKeyState( "lctrl" ) then
					if PlayerItems[i]["item"] ~= 0 then
						return triggerServerEvent("dropItem", resourceRoot, localPlayer, i)
					end
				end
				selectcell = i
				break
			end
		end
	elseif getElementParent(source) == GUIInventory.abackground then
		for i in ipairs(GUIInventory.acell) do
			if source == GUIInventory["acell"][i] then
				if acellmode == 2 then -- Багажник авто
					if altItems[aselectcell] ~= 0 and altItems[i] == 0 then -- Перемещаем предмет в пустой слот
						if acellmode == 2 or acellmode == 3 then
							return altMoveitem(aselectcell, i)
						end	
					end
					aselectcell = i
					break
				end
			end
		end
	end
end
addEventHandler("onClientGUIClick", root, SelectSlotClick)

local function SelectSlotDoubleClick()
	if getElementParent(source) == GUIInventory.abackground then
		for i in ipairs(GUIInventory.acell) do
			if source == GUIInventory["acell"][i] then
				if altItems[i] ~= 0 then
					if acellmode == 1 then -- Покупка элемента
						BuyItem(altItems[i])
					elseif acellmode == 2 then -- Багажник авто
						triggerServerEvent("addItem", resourceRoot, localPlayer, altItems[i])
						triggerServerEvent("removeAltItem", resourceRoot, aparam, aselectcell, acellmode, localPlayer)
					end
				end
				break
			end
		end
	elseif getElementParent(source) == GUIInventory.background then
		for i in ipairs(GUIInventory.cell) do
			if source == GUIInventory["cell"][i] then
				if getKeyState( "lctrl" ) then
					if acellmode == 2 then
						if PlayerItems[i]["item"]  ~= 0 then
							triggerServerEvent("removeItem", resourceRoot, localPlayer, i)
							triggerServerEvent("addAltItem", resourceRoot, aparam, PlayerItems[i]["item"] , acellmode, localPlayer)
						end	
					end
				else
					triggerServerEvent("usingItem", resourceRoot, localPlayer, i)
				end	
			end
		end
	end
end
addEventHandler("onClientGUIDoubleClick", root, SelectSlotDoubleClick)


function MoveItemToSlot(slot, toslot)
	if slot < 1 or slot > 36 then return false end
	triggerServerEvent("moveItem", resourceRoot, localPlayer, slot, toslot, acellmode)
	selectcell = 0
	aselectcell = 0
end

function altMoveitem(slot, toslot)
	if slot < 1 or slot > 36 then return false end
	triggerServerEvent("altMoveItem", resourceRoot, localPlayer, aparam, slot, toslot, acellmode)
	selectcell = 0
	aselectcell = 0
end

function DeleteItem(item)

end

function AddItem(item)

end

function UpdateItemTabeleList(itemtable)

end

function BuyItem(item)
	triggerServerEvent("buyItem", resourceRoot, localPlayer, item)
end

function UpdateInvenoryGUI(bool)
	ViveInvIcons(bool)
end

function SyncAltItemData(data, type, param)
	-------------------
	selectcell = 0
	aselectcell = 0
	--------------------
	altItems = data
	acellmode = type
	aparam = param -- Наценка магазина/ID Авто
	UpdateInvenoryGUI(true)
end
addEvent("SyncAltItemData", true)
addEventHandler("SyncAltItemData", localPlayer, SyncAltItemData)

function SyncPlayerItemData(data)
	PlayerItems = data
	-------
	selectcell = 0
	aselectcell = 0
	-------
	UpdateInvenoryGUI()
end
addEvent("SyncPlayerItemData", true)
addEventHandler("SyncPlayerItemData", localPlayer, SyncPlayerItemData)

local posItemInfo = {0,0}

function VievItemInfo(slot, type)
	VievGUIItemInfo(true)
	-----------------------Обновление информации-------------------------------
	if type == 1 then -- В инвентаре игрока
		guiSetText(GUIItemInfo.label[2], ItemData[PlayerItems[slot]["item"]]["name"])
		iteminfotext = "Информация о предмете:\n\nРедкость: "..RareName[PlayerItems[slot]["rare"]].."\nВес: "..ItemData[PlayerItems[slot]["item"]]["weight"]*PlayerItems[slot]["amount"].." кг.\nКол-Во: "..PlayerItems[slot]["amount"].." шт.\nОписание: "..ItemData[PlayerItems[slot]["item"]]["desc"]..""
	elseif type == 2 then -- В магазине
		guiSetText(GUIItemInfo.label[2], ItemData[altItems[slot]]["name"])
		iteminfotext = "Информация о предмете:\n\nЦена: $"..ItemData[PlayerItems[slot]["item"]]["price"]*aparam.."\nРедкость: Обычная\nВес: "..ItemData[PlayerItems[slot]["item"]]["weight"].." кг.\nКол-Во: 1 шт.\nОписание: "..ItemData[PlayerItems[slot]["item"]]["desc"].."\n\n((Дважды кликните по предмету для покупки))"
	end	
	---------------------------------------------------------------------------
	if type == 1 then
		local px,py = guiGetPosition (GUIInventory.background, false)
		posItemInfo[1], posItemInfo[2] = guiGetPosition (GUIInventory.cell[slot], false)
		posItemInfo[1] = posItemInfo[1]+px
		posItemInfo[2] = posItemInfo[2]+py
	else
		local px,py = guiGetPosition (GUIInventory.abackground, false)
		posItemInfo[1], posItemInfo[2] = guiGetPosition (GUIInventory.acell[slot], false)
		posItemInfo[1] = posItemInfo[1]+px
		posItemInfo[2] = posItemInfo[2]+py
	end
	guiSetPosition(GUIItemInfo.staticimage[1], posItemInfo[1]+65,posItemInfo[2], false)
	guiBringToFront(GUIItemInfo.staticimage[1])
end

function DrawItemInfoText(text, x,y,sx,sy)
	dxDrawText(iteminfotext, posItemInfo[1]+65+3, posItemInfo[2]+35, 261, 171, tocolor(255, 255, 255, 255), 1.00, dxMedium, "left", "top", false, false, true, true, false)
	
end

function VievInventoryHelp()

end

function ViveInvIcons(bool)
	if bool then
		for i in ipairs(altItems) do
			if altItems[i] ~= 0 then
				guiStaticImageLoadImage(GUIInventory.aitems[i], ":inventory/icons/"..altItems[i]..".png")
				guiSetAlpha(GUIInventory.aitems[i], 0.8)
			else
				guiSetAlpha(GUIInventory.aitems[i], 0)
			end	
		end
	else 
		for i in ipairs(PlayerItems) do
			if PlayerItems[i]["item"] ~= 0 then
				guiStaticImageLoadImage(GUIInventory.items[i], ":inventory/icons/"..PlayerItems[i]["item"]..".png")
				guiSetAlpha(GUIInventory.items[i], 0.8)
			else
				guiSetAlpha(GUIInventory.items[i], 0)
			end
		end
	end
end