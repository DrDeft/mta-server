local invmedium = guiCreateFont(font_medium, 12)

local invguicreated = false 
local vievinventory = false
local iteminfocreated = false
local vievainventory = false

GUIInventory = {
    background,
	abackground,
	help,
	title = {},
	titletext = {},
	iteminfo,
	cell = {},
	items = {},
	acell = {},
	aitems = {}
}

GUIItemInfo = {
    label = {},
    staticimage = {}
}

local function CreateItemInfoGUI()
	if iteminfocreated then return 1 end
	
	GUIItemInfo.staticimage[1] = guiCreateStaticImage(0, 0, 290, 215, ":FiveRP/loginpanel/images/top.png", false)
	guiSetProperty(GUIItemInfo.staticimage[1], "ImageColours", "tl:67000000 tr:67000000 bl:67000000 br:67000000")

	GUIItemInfo.staticimage[2] = guiCreateStaticImage(0, 0, 290, 31, ":FiveRP/loginpanel/images/pane.png", false, GUIItemInfo.staticimage[1])
	guiSetProperty(GUIItemInfo.staticimage[2], "ImageColours", "tl:C8008000 tr:C8008000 bl:C8008000 br:C8008000")

	GUIItemInfo.label[2] = guiCreateLabel(2, 2, 290, 29, "Item Name", false, GUIItemInfo.staticimage[2])
	guiLabelSetHorizontalAlign(GUIItemInfo.label[2], "center", false)
	guiLabelSetVerticalAlign(GUIItemInfo.label[2], "center")
	guiSetVisible(GUIItemInfo.staticimage[1], false)
	guiSetEnabled(GUIItemInfo.staticimage[1], false)
	guiSetFont(GUIItemInfo.label[2], fmedium)
	iteminfocreated = true
end


local function CreateAddInvGUI()
	GUIInventory.abackground = guiCreateStaticImage(0.07, 0.23, 0.25, 0.60, ":FiveRP/loginpanel/images/top.png", true)
	guiSetProperty(GUIInventory.abackground, "ImageColours", "tl:3F000100 tr:3F000100 bl:3F000100 br:3F000100")

	GUIInventory.title[2] = guiCreateStaticImage(0.00, 0.00, 1.00, 0.07, ":FiveRP/loginpanel/images/bottom.png", true, GUIInventory.abackground)
	guiSetProperty(GUIInventory.title[2], "ImageColours", "tl:FF013F00 tr:FF013F00 bl:FF013F00 br:FF013F00")
	
	GUIInventory.titletext[2] = guiCreateLabel(0, 0, 1, 1, "Магазин", true, GUIInventory.title[2])
	guiLabelSetHorizontalAlign(GUIInventory.titletext[2], "center", false)
	guiLabelSetVerticalAlign(GUIInventory.titletext[2], "center")
	guiSetFont(GUIInventory.titletext[2], Five26)

-------------------------------------------------------Ячейки-----------------------------------------------------------------------------
	GUIInventory.acell[1] = guiCreateStaticImage(28, 116, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)
	GUIInventory.acell[2] = guiCreateStaticImage(100, 116, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)	
	GUIInventory.acell[3] = guiCreateStaticImage(172, 116, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)	
	GUIInventory.acell[4] = guiCreateStaticImage(244, 116, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)	
	GUIInventory.acell[5] = guiCreateStaticImage(316, 116, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)	
	GUIInventory.acell[6] = guiCreateStaticImage(388, 116, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)	

	GUIInventory.acell[7] = guiCreateStaticImage(28, 185, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)	
	GUIInventory.acell[8] = guiCreateStaticImage(100, 185, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)	
	GUIInventory.acell[9] = guiCreateStaticImage(172, 185, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)	
	GUIInventory.acell[10] = guiCreateStaticImage(244, 185, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)	
	GUIInventory.acell[11] = guiCreateStaticImage(316, 185, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)	
	GUIInventory.acell[12] = guiCreateStaticImage(388, 185, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)

	GUIInventory.acell[13] = guiCreateStaticImage(28, 255, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)
	GUIInventory.acell[14] = guiCreateStaticImage(100, 255, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)
	GUIInventory.acell[15] = guiCreateStaticImage(172, 255, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)
	GUIInventory.acell[16] = guiCreateStaticImage(244, 255, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)
	GUIInventory.acell[17] = guiCreateStaticImage(316, 255, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)
	GUIInventory.acell[18] = guiCreateStaticImage(388, 255, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)

	GUIInventory.acell[19] = guiCreateStaticImage(28, 324, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)
	GUIInventory.acell[20] = guiCreateStaticImage(100, 324, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)
	GUIInventory.acell[21] = guiCreateStaticImage(172, 324, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)
	GUIInventory.acell[22] = guiCreateStaticImage(244, 324, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)
	GUIInventory.acell[23] = guiCreateStaticImage(316, 324, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)
	GUIInventory.acell[24] = guiCreateStaticImage(388, 324, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)

	GUIInventory.acell[25] = guiCreateStaticImage(28, 394, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)
	GUIInventory.acell[26] = guiCreateStaticImage(100, 394, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)	
	GUIInventory.acell[27] = guiCreateStaticImage(172, 394, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)	
	GUIInventory.acell[28] = guiCreateStaticImage(244, 394, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)
	GUIInventory.acell[29] = guiCreateStaticImage(316, 394, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)
	GUIInventory.acell[30] = guiCreateStaticImage(388, 394, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)

	GUIInventory.acell[31] = guiCreateStaticImage(28, 463, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)	
	GUIInventory.acell[32] = guiCreateStaticImage(100, 463, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)	
	GUIInventory.acell[33] = guiCreateStaticImage(172, 463, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)	
	GUIInventory.acell[34] = guiCreateStaticImage(244, 463, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)	
	GUIInventory.acell[35] = guiCreateStaticImage(316, 463, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)
	GUIInventory.acell[36] = guiCreateStaticImage(388, 463, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.abackground)

	for i in ipairs(GUIInventory.acell) do
		guiSetProperty(GUIInventory.acell[i], "ImageColours", "tl:FF837f7f tr:FF837f7f bl:FF837f7f br:FF837f7f")
	end
	
	for i in ipairs(GUIInventory.acell) do
		GUIInventory.aitems[i] = guiCreateStaticImage(0, 0, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.acell[i])
		guiSetEnabled(GUIInventory.aitems[i], false)
		guiSetAlpha(GUIInventory.aitems[i], 0)
	end
	
	guiSetVisible(GUIInventory.abackground, false)
end

function CreateInventoryGUI()
	if invguicreated then return 1 end
	GUIInventory.background = guiCreateStaticImage(0.67, 0.23, 0.25, 0.60, ":FiveRP/loginpanel/images/top.png", true)
	guiSetProperty(GUIInventory.background, "ImageColours", "tl:3F000100 tr:3F000100 bl:3F000100 br:3F000100")
	
	GUIInventory.title[1] = guiCreateStaticImage(0.00, 0.00, 1.00, 0.07, ":FiveRP/loginpanel/images/bottom.png", true, GUIInventory.background)
	guiSetProperty(GUIInventory.title[1], "ImageColours", "tl:FF013F00 tr:FF013F00 bl:FF013F00 br:FF013F00")
	
	GUIInventory.titletext[1] = guiCreateLabel (0.00, 0.00, 1.00, 1.00, "Инвентарь", true, GUIInventory.title[1])
	
	guiLabelSetHorizontalAlign(GUIInventory.titletext[1], "center", false)
	guiLabelSetVerticalAlign(GUIInventory.titletext[1], "center")
	guiSetFont(GUIInventory.titletext[1], Five26)
	
	GUIInventory.help = guiCreateStaticImage(420, 67, 29, 29, ":FiveRP/loginpanel/images/round.png", false, GUIInventory.background) -- Инфо
	guiSetProperty(GUIInventory.help, "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
	
	GUIInventory.titletext[2] = guiCreateLabel (0.00, 0.00, 1.00, 1.00, "?", true, GUIInventory.help)
	
	guiLabelSetColor(GUIInventory.titletext[2], 22, 26, 31)
	guiLabelSetHorizontalAlign(GUIInventory.titletext[2], "center", false)
	guiLabelSetVerticalAlign(GUIInventory.titletext[2], "center")
	guiSetFont(GUIInventory.titletext[2], fmedium)
	guiSetEnabled(GUIInventory.titletext[2], false)
	
	--------------------------------------------Ячейки инвентаря----------------------------------------------------------------------
	GUIInventory.cell[1] = guiCreateStaticImage(28, 116, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)
	GUIInventory.cell[2] = guiCreateStaticImage(100, 116, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)	
	GUIInventory.cell[3] = guiCreateStaticImage(172, 116, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)	
	GUIInventory.cell[4] = guiCreateStaticImage(244, 116, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)	
	GUIInventory.cell[5] = guiCreateStaticImage(316, 116, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)	
	GUIInventory.cell[6] = guiCreateStaticImage(388, 116, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)	
	
	GUIInventory.cell[7] = guiCreateStaticImage(28, 185, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)	
	GUIInventory.cell[8] = guiCreateStaticImage(100, 185, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)	
	GUIInventory.cell[9] = guiCreateStaticImage(172, 185, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)	
	GUIInventory.cell[10] = guiCreateStaticImage(244, 185, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)	
	GUIInventory.cell[11] = guiCreateStaticImage(316, 185, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)	
	GUIInventory.cell[12] = guiCreateStaticImage(388, 185, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)
	
	GUIInventory.cell[13] = guiCreateStaticImage(28, 255, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)
	GUIInventory.cell[14] = guiCreateStaticImage(100, 255, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)
	GUIInventory.cell[15] = guiCreateStaticImage(172, 255, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)
	GUIInventory.cell[16] = guiCreateStaticImage(244, 255, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)
	GUIInventory.cell[17] = guiCreateStaticImage(316, 255, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)
	GUIInventory.cell[18] = guiCreateStaticImage(388, 255, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)
	
	GUIInventory.cell[19] = guiCreateStaticImage(28, 324, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)
	GUIInventory.cell[20] = guiCreateStaticImage(100, 324, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)
	GUIInventory.cell[21] = guiCreateStaticImage(172, 324, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)
	GUIInventory.cell[22] = guiCreateStaticImage(244, 324, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)
	GUIInventory.cell[23] = guiCreateStaticImage(316, 324, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)
	GUIInventory.cell[24] = guiCreateStaticImage(388, 324, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)
	
	GUIInventory.cell[25] = guiCreateStaticImage(28, 394, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)
	GUIInventory.cell[26] = guiCreateStaticImage(100, 394, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)	
	GUIInventory.cell[27] = guiCreateStaticImage(172, 394, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)	
	GUIInventory.cell[28] = guiCreateStaticImage(244, 394, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)
	GUIInventory.cell[29] = guiCreateStaticImage(316, 394, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)
	GUIInventory.cell[30] = guiCreateStaticImage(388, 394, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)

	GUIInventory.cell[31] = guiCreateStaticImage(28, 463, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)	
	GUIInventory.cell[32] = guiCreateStaticImage(100, 463, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)	
	GUIInventory.cell[33] = guiCreateStaticImage(172, 463, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)	
	GUIInventory.cell[34] = guiCreateStaticImage(244, 463, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)	
	GUIInventory.cell[35] = guiCreateStaticImage(316, 463, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)
	GUIInventory.cell[36] = guiCreateStaticImage(388, 463, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.background)
	
	
	
	for i in ipairs(GUIInventory.cell) do
		guiSetProperty(GUIInventory.cell[i], "ImageColours", "tl:FF837f7f tr:FF837f7f bl:FF837f7f br:FF837f7f")
		
		GUIInventory.items[i] = guiCreateStaticImage(0, 0, 62, 62, ":FiveRP/loginpanel/images/pane.png", false, GUIInventory.cell[i])
		guiSetEnabled(GUIInventory.items[i], false)
		guiSetAlpha(GUIInventory.items[i], 0)
	end
	
	guiSetVisible(GUIInventory.background, false)
	invguicreated = true
	CreateAddInvGUI()
end

function VievaInventory()
	if not invguicreated then CreateInventoryGUI() end
	if not vievainventory then
		guiSetVisible(GUIInventory.abackground, true)
		vievainventory = true
		showCursor(true)
		if vievinventory == false then VievInventory() end
		ViveInvIcons(true)
	else
		guiSetVisible(GUIInventory.abackground, false)
		vievainventory = false
		showCursor(false)
		VievGUIItemInfo(false)
	end
end

function VievInventory()
	if not invguicreated then CreateInventoryGUI() end
	if not vievinventory then
		guiSetVisible(GUIInventory.background, true)
		vievinventory = true
		showCursor(true)
		setPlayerHudComponentVisible ("all", false)
	else
		guiSetVisible(GUIInventory.background, false)
		vievinventory = false
		showCursor(false)
		VievGUIItemInfo(false)
		setPlayerHudComponentVisible ("all", true)
		if vievainventory then VievaInventory() end
	end
end
addEvent("VievInventory", true)
addEventHandler("VievInventory", localPlayer, VievInventory)

function VievGUIItemInfo(bool)
	if not iteminfocreated then CreateItemInfoGUI() end
	guiSetVisible(GUIItemInfo.staticimage[1], bool)
	if bool then addEventHandler ("onClientRender", root, DrawItemInfoText)
	else removeEventHandler ("onClientRender", root, DrawItemInfoText)
	end
end

function cmd_testainv(cmd)
	VievInventory()
	VievaInventory()
end
addCommandHandler("testainv", cmd_testainv)