ItemTable = {}
--ItemTable[1] = {id = 1, name = "nil", imgtype = 0, price = 0, weight = 0, maxstack = 1, desc = "Пустой слот"}
local _items = 0

local function SyncLocalItemData(player)
	triggerClientEvent(player, "LoadItemsData", player, ItemTable)
end
addEvent("SyncLocalItemData", true)
addEventHandler("SyncLocalItemData", root, SyncLocalItemData)

function returnFreeInventorySlot(player)
	local playerid = getPlayerID(player)
	local slot = false
	for i in ipairs(PlayerItems[playerid]) do
		if PlayerItems[playerid][i]["item"] == 0 then 
			slot = i
			break
		end
	end
	return slot
end

function dropItem(player, slot)
	local x,y,z = getElementPosition(player)
	local playerid = getPlayerID(player)
	itemid = PlayerItems[playerid][slot]["item"]
	createObject(ItemTable[itemid]["object"], x, y,z-0.5)
	removeItem(player, slot)
end
addEvent("dropItem", true)
addEventHandler("dropItem", root, dropItem)

function usingItem(player, slot)
	local playerid = getPlayerID(player)
	local itemid = PlayerItems[playerid][slot]["item"]
	if ItemTable[itemid]["type"] == 1 then -- Оружие 
		giveWeaponAC(player, ItemTable[itemid]["custid"], 30, true)
		removeItem(player, slot)
	end
end
addEvent("usingItem", true)
addEventHandler("usingItem", root, usingItem)

function returnFreeAltSlot(altid, type)
	slot = false
	if type == 2 then -- Авто
		for i in ipairs(Vehicles[altid]["trunk"]) do
		if Vehicles[altid]["trunk"][i] == 0 then 
			slot = i
			break
		end
	end
	return slot
	end
end

function buyItem(player, itemid)
	local playerid = getPlayerID(player)
	if Players[playerid]["money"] < ItemTable[itemid]["price"] then return SendPlayerNotice(player, "У вас недостаточно средств!") end
	if not returnFreeInventorySlot(player) then return SendPlayerNotice(player, "В инвентаре нет места!") end
	addItem(player, itemid)
	GivePlayerMoneyAC(player, -ItemTable[itemid]["price"])
end
addEvent("buyItem", true)
addEventHandler("buyItem", root, buyItem)

function addItem(player, itemid, amount, rare)
	local slot = returnFreeInventorySlot(player)
	if not slot then return SendPlayerNotice(player, "В инвентаре нет места!") end
	if amount == nil then amount = 1 end
	if rare == nil then rare = 1 end
	local playerid = getPlayerID(player)
	PlayerItems[playerid][slot]["item"] = itemid 
	PlayerItems[playerid][slot]["amount"] = amount 
	PlayerItems[playerid][slot]["rare"] = rare 
	
	triggerClientEvent(player, "SyncPlayerItemData", player, PlayerItems[playerid])
end
addEvent("addItem", true)
addEventHandler("addItem", root, addItem)

function addAltItem(altid, itemid, type, player)
	local slot = returnFreeAltSlot(altid, type)
	if slot == false then return false end
	if type == 2 then
		Vehicles[altid]["trunk"][slot] = itemid
		if player then
		triggerClientEvent(player, "SyncAltItemData", player, Vehicles[altid]["trunk"], type, altid)
		end
	end
end
addEvent("addAltItem", true)
addEventHandler("addAltItem", resourceRoot, addAltItem)

function removeAltItem(altid, slot, type, player)
	if slot == nil or slot < 1 or slot > 36 then return false end
	if type == 2 then -- Авто
		Vehicles[altid]["trunk"][slot] = 0
		if player then
			triggerClientEvent(player, "SyncAltItemData", player, Vehicles[altid]["trunk"], type, altid)
		end	
	end
end
addEvent("removeAltItem", true)
addEventHandler("removeAltItem", resourceRoot, removeAltItem)

function removeItem(player, slot)
	if slot == nil or slot < 1 or slot > 36 then return false end
	local playerid = getPlayerID(player)
	PlayerItems[playerid][slot]["item"] = 0
	PlayerItems[playerid][slot]["amount"] = 0
	PlayerItems[playerid][slot]["rare"] = 1
	PlayerItems[playerid][slot]["condition"] = 0
	triggerClientEvent(player, "SyncPlayerItemData", player, PlayerItems[playerid])
end
addEvent("removeItem", true)
addEventHandler("removeItem", resourceRoot, removeItem)

function altMoveItem(player, altid, slot, toslot, type)
	if slot == nil or slot < 1 or slot > 36 then return false end
	if toslot == nil or toslot < 1 or toslot > 36 then return false end
	if type == 2 then -- Авто
		Vehicles[altid]["trunk"][toslot] = Vehicles[altid]["trunk"][slot]
		Vehicles[altid]["trunk"][slot] = 0
		triggerClientEvent(player, "SyncAltItemData", player, Vehicles[altid]["trunk"], type, altid)
	end
	
end
addEvent("altMoveItem", true)
addEventHandler("altMoveItem", resourceRoot, altMoveItem)

function moveItem(player, slot, toslot, type)
	local playerid = getPlayerID(player)
	if slot == nil or slot < 1 or slot > 36 then return false end
	if toslot == nil or toslot < 1 or toslot > 36 then return false end
	
	if PlayerItems[playerid][slot]["item"] == 0 then return SendPlayerNotice(player, "Этот слот пустой!") end
	if PlayerItems[playerid][toslot]["item"] ~= 0 then return SendPlayerNotice(player, "Этот слот занят!") end
	
	--------------------------------------------------
	PlayerItems[playerid][toslot]["item"] = PlayerItems[playerid][slot]["item"]
	PlayerItems[playerid][toslot]["amount"] = PlayerItems[playerid][slot]["amount"]
	PlayerItems[playerid][toslot]["rare"] = PlayerItems[playerid][slot]["rare"]
	PlayerItems[playerid][toslot]["condition"] = PlayerItems[playerid][slot]["condition"]
	-------------------------------------------------
	PlayerItems[playerid][slot]["item"] = 0
	PlayerItems[playerid][slot]["amount"] = 0
	PlayerItems[playerid][slot]["rare"] = 1
	PlayerItems[playerid][slot]["condition"] = 0
	
	triggerClientEvent(player, "SyncPlayerItemData", player, PlayerItems[playerid])
end
addEvent("moveItem", true)
addEventHandler("moveItem", resourceRoot, moveItem)

function findItem(player, itemid)
	local slot = false
	for i in ipairs(PlayerItems[playerid]) do
		if PlayerItems[playerid][i]["item"] == itemid then 
			slot = i
			break
		end
	end
	return slot
end

function OpenInventory(player)
	triggerClientEvent(player, "VievInventory", player)
	triggerClientEvent(player, "SyncPlayerItemData", player, PlayerItems[getPlayerID(player)])
end

function OpenAltInventory(player, type, data)
	triggerClientEvent(player, "SyncPlayerItemData", player, PlayerItems[getPlayerID(player)])
	--if type == 1 then
		triggerClientEvent(player, "OpenAltInventory", player, type, data, 1)
	
	--end
end

local function LoadItemData(res)
	if res ~= getThisResource() then return 1 end
	local itemlist = xmlLoadFile ( "inventory/items_list.xml" )
	while xmlFindChild ( itemlist, "item", _items ) ~= false do
		local data = xmlNodeGetAttributes ( xmlFindChild ( itemlist, "item", _items ) )
		ItemTable[_items+1] = data
		ItemTable[_items+1]["desc"] = data["desc"]:gsub("\\n", "\n")
		
		----------------------------------------------------------------
		ItemTable[_items+1]["id"] = tonumber(ItemTable[_items+1]["id"])
		ItemTable[_items+1]["custid"] = tonumber(ItemTable[_items+1]["custid"])
		ItemTable[_items+1]["object"] = tonumber(ItemTable[_items+1]["object"])
		ItemTable[_items+1]["stack"] = tonumber(ItemTable[_items+1]["stack"])
		ItemTable[_items+1]["price"] = tonumber(ItemTable[_items+1]["price"])
		ItemTable[_items+1]["transfer"] = tonumber(ItemTable[_items+1]["transfer"])
		ItemTable[_items+1]["drop"] = tonumber(ItemTable[_items+1]["drop"])
		ItemTable[_items+1]["type"] = tonumber(ItemTable[_items+1]["type"])
		----------------------------------------------------------------
		
		_items = _items +1
	end
	xmlUnloadFile ( itemlist )
	outputDebugString("Загружено ".._items.." предметов инвентаря")

end
addEventHandler ( "onResourceStart", getRootElement(), LoadItemData )
