local jailTimer
JailPlayers = {} -- Таблица с игроками в тюрьме(хранятся ID игроков)

function JailPlayer(player, jailtime)
	if not player or not jailtime then return cancelEvent() end 
	playerid = getElementData(player, "player.id")
	if Players[playerid]["jailtime"] > 0 then
		for i in ipairs(JailPlayers) do
			if i == playerid then  
				table.remove(JailPlayers, i)
				break
			end
		end
	end
	Players[playerid]["jailtime"] = jailtime
	local random = math.random(1,3)
	if Players[playerid]["dragid"] ~= nil then UnDragPlayer(playerid) end
	if random == 1 then 
		setElementPosition(player, 264.0, 77.4, 1001)
	elseif random == 2 then
		setElementPosition(player, 264.0, 81.6, 1001)
	elseif random == 3 then
		setElementPosition(player, 264.0, 86.0, 1001)
	end
	setElementRotation(player,0,0,270,"default",true)
	setPlayerInterior (player, 6)
	setPlayerDimension(player, 2)
	table.insert(JailPlayers, playerid)
	if not jailTimer then
		jailTimer = setTimer(JailTimer, 1000, 0)
	else end
	Players[playerid]["wanted"] = 0
	setPlayerWantedLevel(player, 0)
end

function JailTimer()
	for i in ipairs(JailPlayers) do
		if not iSPlayerOK(JailPlayers[i]) then table.remove(JailPlayers, i)
		else
			Players[JailPlayers[i]]["jailtime"] = Players[JailPlayers[i]]["jailtime"]-1
			if Players[JailPlayers[i]]["jailtime"] <= 0 then
				setElementPosition(Players[JailPlayers[i]]["player"], 246.4423828125, 70.068359375, 1003.640625)
				setElementRotation(Players[JailPlayers[i]]["player"],0,0,180,"default",true)
				-- Освобождаем игрока
				Players[JailPlayers[i]]["jailtime"] = 0
				outputChatBox("Надзиратель: Ты отсидел свой срок и можешь быть свободен!", Players[JailPlayers[i]].player)
				table.remove(JailPlayers, i)
			end
		end	
	end
	if #JailPlayers == 0 then
		killTimer(jailTimer)
		jailTimer = nil
	end
end

function backPlayerToJail(player)
	local random = math.random(1,3)
	if random == 1 then 
		spawnPlayer(player, 264.0, 77.4, 1001, 270, Players[playerid]["skin"])
	elseif random == 2 then
		spawnPlayer(player, 264.0, 81.6, 1001, 270, Players[playerid]["skin"])
	elseif random == 3 then
		spawnPlayer(player, 264.0, 81.6, 1001, 270, Players[playerid]["skin"])
	end
	setPlayerInterior(player, 6)
	setPlayerDimension(player, 2)
end