local sJobPickup = createPickup(2494.8,-1341.8, 1007, 3, 1274, 0)  

local gPlantZoneParent = {}
----------------Маркеры
local gPlantZoneG = {}
local gPlantZone = {}
local gPlantMark = {}

gDelivery = createColCuboid ( 2529.8437, -1338.5947, 1006.0, 6, 2, 2)


----------------------

function LoadJob(res)
	setElementDimension(sJobPickup, 1)
	setElementInterior(sJobPickup, 2)
	if res ~= getThisResource() then return 1 end
	addEventHandler ( "onColShapeHit", gDelivery, onPlayerJoinWorkZone)

	---------------------------Родители Колсфер маркеров------------------------
	gPlantZoneParent[1] = createElement ( "gPlantTable1", "Group gPlantMarkL" )
	gPlantZoneParent[2] = createElement ( "gPlantTable2", "Group gPlantMarkR" )
	---------------------------Стол 1-------------------------------------------
	
	gPlantZoneG[1] = createColCuboid(2509.2536, -1342.897094, 1006.0562, 6, 0.6, 2)
	gPlantZoneG[2] = createColCuboid(2509.2536, -1346.797094, 1006.0562, 6, 0.6, 2)
	addEventHandler ( "onColShapeHit", gPlantZoneG[1], onPlayerJoinWorkZoneG)
	addEventHandler ( "onColShapeHit", gPlantZoneG[2], onPlayerJoinWorkZoneG)
	
	--------------------------------------------------------------------------------------
	gPlantZone[1] = createColCuboid ( 2497.9988, -1338.0382, 1005.9562, 0.6, 0.6, 2)
	gPlantZone[2] = createColCuboid ( 2501.8646, -1338.0382, 1005.9562, 0.6, 0.6, 2)
	gPlantZone[3] = createColCuboid ( 2506.1296, -1338.0382, 1005.9562, 0.6, 0.6, 2)
	
	gPlantZone[4] = createColCuboid ( 2509.9998, -1338.0382, 1005.9562, 0.6, 0.6, 2)
	gPlantZone[5] = createColCuboid ( 2514.1140, -1338.0382, 1005.9562, 0.6, 0.6, 2)
	gPlantZone[6] = createColCuboid ( 2518.0974, -1338.0382, 1005.9562, 0.6, 0.6, 2)
	

	------------------------------------------------------------------------------------
	
	gPlantZone[7] = createColCuboid ( 2498.0988, -1350.5275, 1006.0562, 0.6, 0.6, 2)
	gPlantZone[8] = createColCuboid ( 2502.0974, -1350.5275, 1006.0562, 0.6, 0.6, 2)
	gPlantZone[9] = createColCuboid ( 2506.1296, -1350.5275, 1006.0562, 0.6, 0.6, 2)
	
	gPlantZone[10] = createColCuboid ( 2509.8435, -1350.5275, 1006.0562, 0.6, 0.6, 2)
	gPlantZone[11] = createColCuboid (2513.8669, -1350.5275, 1006.0562, 0.6, 0.6, 2)
	gPlantZone[12] = createColCuboid (2518.0330, -1350.5275, 1006.0562, 0.6, 0.6, 2)		
	
	for i in ipairs(gPlantZone) do
		addEventHandler ( "onColShapeHit", gPlantZone[i], onPlayerJoinWorkZone )
		if i < 7 then 
			setElementParent(gPlantZone[i], gPlantZoneParent[1])
		else
			setElementParent(gPlantZone[i], gPlantZoneParent[2])
		end
	end
end
addEventHandler("onResourceStart", root, LoadJob)

function onPlayerJoinWorkZoneG(player, vw)
	local playerid = getPlayerID(player)
	if Players[playerid]["tparam"]["tempjob"] == 1 and Players[playerid]["tparam"]["sjob"] == 1 then
		Players[playerid]["tparam"]["sjob"] = 2
		Players[playerid]["tparam"]["jobobj"] = createObject(2969, 0,0,0 )
		setElementParent (Players[playerid]["tparam"]["jobobj"], player)  
		setPedAnimation(player, "CARRY", "crry_prtial",0,false)
		setElementInterior(Players[playerid]["tparam"]["jobobj"], 2)
		setElementDimension(Players[playerid]["tparam"]["jobobj"], 1)
		exports.bone_attech:attachElementToBone(Players[playerid]["tparam"]["jobobj"],player,11,-0.15,0.1,0.1, 270.0,-15.0,0.0)
		triggerClientEvent(player, "CreateGunPlantMarker", player, 1, false)
		----------------------------------------------------------------------------
	end	
	
end

function onPlayerJoinWorkZone(player, vw)
	local playerid = getPlayerID(player)
	if source == gDelivery then
		if Players[playerid]["tparam"]["sjob"] ~= 3 then return 0 end
		Players[playerid]["tparam"]["jobitems"] = Players[playerid]["tparam"]["jobitems"]+1
		Players[playerid]["tparam"]["sjob"] = 2
		setPedAnimation(player,"bomber", "bom_plant", -1, false)
		triggerClientEvent(player, "CreateGunPlantMarker", player, 3, false)
		triggerClientEvent(player, "CreateGunPlantMarker", player, 1, true)
		setTimer ( function()
			setPedAnimationProgress(player, "bom_plant", 0.7)
			setTimer(StopAnimation, 800, 1, player) 
			exports.bone_attech:detachElementFromBone(Players[playerid]["tparam"]["jobobj"])
			destroyElement(Players[playerid]["tparam"]["jobobj"])	
		end, 500, 1 )
		Players[playerid]["tparam"]["sjob"] = 1
		--end
		return 1
	end
	if Players[playerid]["tparam"]["sjob"] == 2 then
		setPedAnimation( player, "INT_SHOP", "shop_cashier")
		--if Players[playerid]["tparam"]["sjob"] ~= 3 then return 0 end 
		exports.MiniGames:StartMiniGame(player, 2, 1)
		addEventHandler("MiniGameComplite", player, gPlantMiniGameResult)
		
		exports.bone_attech:detachElementFromBone(Players[playerid]["tparam"]["jobobj"])
		destroyElement(Players[playerid]["tparam"]["jobobj"])
		
		if getElementParent(source) == gPlantZoneParent[1] then
			setElementRotation(player,0,0,0,"default",true)
		elseif getElementParent(source) == gPlantZoneParent[2] then
			setElementRotation(player,0,0,180,"default",true)
		end
	end	
end


local function hitStartJobPickup(player)
	local playerid = getPlayerID(player)
	if getPedOccupiedVehicle(player) then return 1 end
	if not Players[playerid]["tparam"]["sjob"] or Players[playerid]["tparam"]["sjob"] == 0 then
		return ShowMessage(player, 7, "Работа: Сборщик оружия", "Смотрящий: Я вижу ты ищешь работу?\nНу что ж. У меня есть для тебя предложение. Работа очень ответственная.\n\
		Твоя задача брать заготовки и собирать оружие по чертежам.\nИ только попробуй что ни будь стащить! За это в нашем штате серьезный срок!\nЕсли ты согласен на мое предложение, то можешь начинать работать прямо сейчас\
		\nЗарплата: Сдельная(зависит от качества и продукта), расценки можно увидеть на доске", "Отмена", "Работать", "О работе")
	elseif Players[playerid]["tparam"]["sjob"] > 0 and Players[playerid]["tparam"]["tempjob"] == 1 then
		return ShowMessage(player, 7, "Работа: Сборщик оружия", "Смотрящий: Ты что то хотел?", "Ничего", "Уйти с работы", "О работе")
	end
end
addEventHandler ( "onPickupHit", sJobPickup, hitStartJobPickup )


function gPlantMiniGameResult(result)
	setPedAnimation(source,false)
	local playerid = getPlayerID(source)
	-----------------------------------------------------
	-----------------------------------------------------
	if result == true then
		setPedAnimation(source, "CARRY", "crry_prtial",0,false)
		Players[playerid]["tparam"]["jobobj"] = createObject(2969, 0,0,0 )
		setElementParent (Players[playerid]["tparam"]["jobobj"], source)  
		setPedAnimation(source, "CARRY", "crry_prtial",0,false)
		setElementInterior(Players[playerid]["tparam"]["jobobj"], 2)
		setElementDimension(Players[playerid]["tparam"]["jobobj"], 1)
		--attachElements (Players[playerid]["tparam"]["jobobj"], source, 0,0.463495,0.33)  
		--Y - Высота
		exports.bone_attech:attachElementToBone(Players[playerid]["tparam"]["jobobj"],source,11,-0.15,0.1,0.1, 270.0,-15.0,0.0) 
		Players[playerid]["tparam"]["sjob"] = 3
		triggerClientEvent(source, "CreateGunPlantMarker", source, 3, true)
	else
		GivePlayerMoneyAC(source, -25)
		outputChatBox("Вы испортили заготовку", source)
		Players[playerid]["tparam"]["sjob"] = 1
		triggerClientEvent(source, "CreateGunPlantMarker", source, 1, true)
	end
	removeEventHandler("MiniGameComplite", source, gPlantMiniGameResult)
end

