--[[function SawmillGUI()
	dxDrawRectangle(1461, 551, 310, 181, tocolor(0, 123, 0, 255), false)
	dxDrawRectangle(1490, 511, 250, 40, tocolor(0, 123, 0, 255), false)
	dxDrawRectangle(1466, 556, 299, 170, tocolor(0, 35, 0, 255), false)
	dxDrawText("Работа: Лесоруб", 1496, 515, 1735, 546, tocolor(255, 255, 255, 255), 1.00, dxfont0_Roboto-Medium, "center", "center", false, false, false, false, false)
	dxDrawText("Бензопила:", 1469, 562, 1765, 582, tocolor(255, 255, 255, 255), 1.00, dxfont0_Roboto-Medium, "center", "center", false, false, false, false, false)
	dxDrawText("Топливо:", 1469, 592, 1852, 612, tocolor(255, 255, 255, 255), 1.00, userdata: 000202E1, "left", "center", false, false, false, false, false)
	dxDrawText("Состояние цепи:", 1469, 612, 1852, 632, tocolor(255, 255, 255, 255), 1.00, userdata: 000202E1, "left", "center", false, false, false, false, false)
	dxDrawText("Статистика:", 1469, 642, 1765, 662, tocolor(255, 255, 255, 255), 1.00, userdata: 000202E1, "center", "center", false, false, false, false, false)
	dxDrawText("Добыто дерева: 0 КГ", 1469, 672, 1852, 692, tocolor(255, 255, 255, 255), 1.00, userdata: 000202E1, "left", "center", false, false, false, false, false)
	dxDrawText("Заработано: $0", 1469, 692, 1852, 712, tocolor(255, 255, 255, 255), 1.00, userdata: 000202E1, "left", "center", false, false, false, false, false)
end]]

local Markers = {}
------------------Работа: Завод оружия------------------
local function CreateGunPlantMarker(type, bool)
	if bool then	
		if type == 1 then
			Markers[1] = Marker(2512.1318, -1342.4951, 1006.9562, "arrow", 1.0, 255, 0, 0)
			Markers[2] = Marker(2512.0761, -1346.5048, 1006.9562, "arrow", 1.0, 255, 0, 0)
			setElementInterior(Markers[1], 2)
			setElementDimension(Markers[1], 1)
			setElementInterior(Markers[2], 2)
			setElementDimension(Markers[2], 1)
		elseif type == 3 then
			Markers[1] = Marker(2531.5693, -1336.957, 1006.9562, "arrow", 1.0, 255, 0, 0)
			setElementInterior(Markers[1], 2)
			setElementDimension(Markers[1], 1)
		end
	else
		for i in ipairs(Markers) do
			destroyElement(Markers[i])
		end
	end
end
addEvent("CreateGunPlantMarker", true)
addEventHandler("CreateGunPlantMarker", root, CreateGunPlantMarker)

------------------Работа: Лесопилка---------------------
local function CreateMillWareHouseMarker()
	createMarker (-530.2138671875, -61.1328125, 62.0921875, "cylinder", 1.0, 255, 0, 0)
	addEventHandler ( "onClientMarkerHit", root, MillMarkerHit)
end
addEvent("CreateMillWareHouseMarker", true)
addEventHandler("CreateMillWareHouseMarker", root, CreateMillWareHouseMarker)

function MillMarkerHit(player)
	destroyElement(source)
	removeEventHandler("onClientMarkerHit", root, MillMarkerHit)
	triggerServerEvent("SawmilClientMarkerHit", player, player)
end
---------------------------------------------------------