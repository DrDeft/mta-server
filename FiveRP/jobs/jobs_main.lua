JobInfo = {}
JobInfo[1] = {salary = 10}
JobInfo[2] = {salary = 10}

function StartJob(player, jobid)
	local playerid = getPlayerID(player)
	if Players[playerid]["tparam"]["sjob"] then return false end
	Players[playerid]["tparam"]["sjob"] = jobid
	Players[playerid]["tparam"]["tempjob"] = 1
	if jobid == 1 then
		triggerClientEvent(player, "CreateGunPlantMarker", player, 1, true)
	elseif jobid == 2 then
		StartSawmilJob(player)
	end
	return true
end

function StopJob(player, salary)
	local playerid = getPlayerID(player)
	if not Players[playerid]["tparam"]["sjob"] then return false end
	GivePlayerMoneyAC(player, salary)
	if Players[playerid]["tparam"]["tempjob"] == 1 and Players[playerid]["tparam"]["sjob"] ~= 1 then
		exports.bone_attech:detachElementFromBone(Players[playerid]["tparam"]["jobobj"])
		destroyElement(Players[playerid]["tparam"]["jobobj"])	
	end
	if Players[playerid]["tparam"]["sjob"] == 1 then
		triggerClientEvent(player, "CreateGunPlantMarker", player, 3, false)
	end
	Players[playerid]["tparam"]["sjob"] = nil
	Players[playerid]["tparam"]["tempjob"] = nil
	Players[playerid]["tparam"]["jobobj"] = nil
	
	toggleControl(player, "sprint", true)
	toggleControl(player, "jump", true)
	toggleControl(player, "fire", true)
	toggleControl(player, "previous_weapon", true)
	toggleControl(player, "next_weapon", true)
	setPedWalkingStyle(player, Players[playerid]["WalkingStyle"])
	return true
end