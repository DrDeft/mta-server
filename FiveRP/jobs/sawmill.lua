local TreesData = {}
--[[
	Статусы дерева:
	0 - Взрослое дерево
	1 - Процесс вырубки дерева
	2 - Дерево лежит на земле
	3 - Дерево ростет
]]
TreesData[1] = {object, pos = {-492.89999, -48.9, 59.7}, targetpos = 84.6015, status = 0, condition = 100, sphere}

local GrowthTreeTimer

local sJobPickup = createPickup(-524.5166015625, -81.5087890625, 62.457107543945, 3, 1275, 0)  

function LoadJobs()
	for i in ipairs(TreesData) do
		TreesData[i].object = Object(659, TreesData[i].pos[1], TreesData[i].pos[2], TreesData[i].pos[3])
		TreesData[i].sphere = ColShape.Sphere(TreesData[i].pos[1], TreesData[i].pos[2], TreesData[i].pos[3], 1.5)
		addEventHandler( "onColShapeHit", TreesData[i].sphere, EnterTrees)
	end
end

local function getTreesID(pickup)
	for i in ipairs(TreesData) do
		if pickup == TreesData[i].sphere then return i end
	end
	return false
end

local function hitStartJobPickup(player)
	local playerid = getPlayerID(player)
	if getPedOccupiedVehicle(player) then return 1 end
	if not Players[playerid]["tparam"]["sjob"] or Players[playerid]["tparam"]["sjob"] == 0 then
		-- Начало работы
		return ShowMessage(player, 8, "Работа: Лесопилка", "Описание работы", "Отмена", "Работать", "О работе")
	elseif Players[playerid]["tparam"]["sjob"] == 2 then
	-- Завершение работы
		return ShowMessage(player, 8, "Работа: Лесопилка", "Смотрящий: Ты что то хотел?", "Ничего", "Уйти с работы", "О работе")
	end
end
addEventHandler ( "onPickupHit", sJobPickup, hitStartJobPickup )

function EnterTrees(player)
	local playerid = getPlayerID(player)
	local treesid = getTreesID(source)
	Players[playerid]["tparam"]["jobtree"] = treesid
	if Players[playerid]["tparam"]["sjob"] ~= 2 or Players[playerid]["tparam"]["tempjob"] ~= 1 then return true end
	if treesid and TreesData[treesid].status == 0 or TreesData[treesid].status == 2 then
		exports.MiniGames:StartMiniGame(player, 3, 1)
		addEventHandler("MiniGameComplite", player, sawmilMiniGameResult)
		--Players[playerid]["tparam"]["jobobj"] = createObject(341, 0,0,0)
		--exports.bone_attech:attachElementToBone(Players[playerid]["tparam"]["jobobj"],player,12,-0.000000,0.010000,0.004000,17.699996,-90.299964,-1.899997)
		if TreesData[treesid].status == 0 then
			local x1, y1 = getElementPosition(player)
			local rotation = findRotation(x1, y1, TreesData[treesid].pos[1], TreesData[treesid].pos[2]) 
			setElementRotation(player, 0,0,rotation,"default",true)
			setPedAnimation(player, "CHAINSAW", "WEAPON_csaw",-1,true, true, false, true)
			TreesData[treesid].status = 1
			Players[playerid]["tparam"]["tempjob"] = 1
		elseif TreesData[treesid].status == 2 then
			setPedAnimation(player, "CHAINSAW", "CSAW_G",-1,true, true, false, true)
			Players[playerid]["tparam"]["tempjob"] = 2
			
			local x1, y1 = getElementPosition(player)
			local rotation = findRotation(x1, y1, TreesData[treesid].pos[1], y1) 
			setElementRotation(player, 0,0,rotation,"default",true)
			
		end
		toggleControl(player, "fire", false) 
		toggleControl(player, "left", false)  
		toggleControl(player, "right", false) 
		toggleControl(player, "forwards", false)  
		toggleControl(player, "backwards", false)  	
		toggleControl(player, "enter_exit", false)
	end	
end

function sawmilMiniGameResult(result)
	removeEventHandler("MiniGameComplite", source, sawmilMiniGameResult)
	local playerid = getPlayerID(source)
	---------------------Падение дерева--------------------------------------
	if TreesData[Players[playerid]["tparam"]["jobtree"]].status == 1 then
		if result then
			moveObject(TreesData[1].object, 3000, TreesData[1].pos[1], TreesData[1].pos[2], TreesData[1].pos[3], TreesData[1].targetpos, 0, 0, "InQuad")
			setTimer ( function()
				TreesData[Players[playerid]["tparam"]["jobtree"]].status = 2
				destroyElement(TreesData[Players[playerid]["tparam"]["jobtree"]].sphere)
				TreesData[Players[playerid]["tparam"]["jobtree"]].sphere = ColShape.Cuboid(TreesData[Players[playerid]["tparam"]["jobtree"]].pos[1]-1.25, TreesData[Players[playerid]["tparam"]["jobtree"]].pos[2]-9.0, TreesData[Players[playerid]["tparam"]["jobtree"]].pos[3], 2.5, 9.0, 1.0)
				addEventHandler( "onColShapeHit", TreesData[Players[playerid]["tparam"]["jobtree"]].sphere, EnterTrees)
				
			end, 3000, 1)
		else
			TreesData[Players[playerid]["tparam"]["jobtree"]].status = 0
		end	
		setPedAnimation(source,false)
	else
		if result then
			exports.bone_attech:detachElementFromBone(Players[playerid]["tparam"]["jobobj"])
			destroyElement(Players[playerid]["tparam"]["jobobj"])
			Players[playerid]["tparam"]["jobobj"] = createObject(1463, 0,0,0)
			setObjectScale(Players[playerid]["tparam"]["jobobj"], 0.3)
			exports.bone_attech:attachElementToBone(Players[playerid]["tparam"]["jobobj"],source,11,-0.15,0.1,0.1, 270.0,-8.0,0.0)
			setPedWalkingStyle(player, Players[playerid]["WalkingStyle"])
			setPedAnimation(source, "CARRY", "crry_prtial",0,false)
			triggerClientEvent(source, "CreateMillWareHouseMarker", source)
			addEventHandler("SawmilClientMarkerHit", source, SawmilClientMarkerHit)
			TreesData[Players[playerid]["tparam"]["jobtree"]].condition = TreesData[Players[playerid]["tparam"]["jobtree"]].condition-100
			if TreesData[Players[playerid]["tparam"]["jobtree"]].condition <= 0 then
				destroyElement(TreesData[Players[playerid]["tparam"]["jobtree"]].object)
				TreesData[Players[playerid]["tparam"]["jobtree"]].status = 3
				TreesData[Players[playerid]["tparam"]["jobtree"]].object = Object(659, TreesData[Players[playerid]["tparam"]["jobtree"]].pos[1], TreesData[Players[playerid]["tparam"]["jobtree"]].pos[2], TreesData[Players[playerid]["tparam"]["jobtree"]].pos[3])
				if isTimer(GrowthTreeTimer) == false then GrowthTreeTimer = setTimer(GrowthTree, 1000, 0) end
			end
		else
			setPedAnimation(source,false)
		end
	end	
	-------------------------------------------------------------------------
end

function StartCutTree(player, TreeID)
	
end

function CompliteCutTree(player)
	setPedAnimation(source,false)
end

function StartSawmilJob(player)
	local playerid = getPlayerID(player)
	Players[playerid]["tparam"]["jobobj"] = createObject(341, 0,0,0)
	exports.bone_attech:attachElementToBone(Players[playerid]["tparam"]["jobobj"],player,12,-0.000000,0.010000,0.004000,17.699996,-90.299964,-1.899997)
	giveWeapon(player, 0, 0, true)
	toggleControl(player, "sprint", false)
	toggleControl(player, "jump", false)
	toggleControl(player, "fire", false)
	toggleControl(player, "previous_weapon", false)
	toggleControl(player, "next_weapon", false)
	setPedWalkingStyle(player, 66)  
end

function SawmilClientMarkerHit(player)
	local playerid = getPlayerID(player)
	setPedAnimation(player,"bomber", "bom_plant", -1, false)
	Players[playerid]["tparam"]["jobitems"] = Players[playerid]["tparam"]["jobitems"]+1
		setTimer ( function()
			setPedAnimationProgress(player, "bom_plant", 0.7)
			setTimer(StopAnimation, 800, 1, player) 
			exports.bone_attech:detachElementFromBone(Players[playerid]["tparam"]["jobobj"])
			destroyElement(Players[playerid]["tparam"]["jobobj"])	
			Players[playerid]["tparam"]["jobobj"] = createObject(341, 0,0,0)
			exports.bone_attech:attachElementToBone(Players[playerid]["tparam"]["jobobj"],player,12,-0.000000,0.010000,0.004000,17.699996,-90.299964,-1.899997)
			setPedWalkingStyle(player, 341)
		end, 500, 1 )
	removeEventHandler("SawmilClientMarkerHit", player, SawmilClientMarkerHit)
	Players[playerid]["tparam"]["tempjob"] = 1
end
addEvent("SawmilClientMarkerHit", true)

function GrowthTree()
	local z = 0
	for i in ipairs(TreesData) do
		if TreesData[i].status == 3 then
			z = z+1
			TreesData[i].condition = TreesData[i].condition+5
			if TreesData[i].condition >= 20 and TreesData[i].condition <= 50 then
				setObjectScale(TreesData[i].object, 1, 1, 0.2)
			elseif TreesData[i].condition >= 50 and TreesData[i].condition < 100 then
				setObjectScale(TreesData[i].object, 1, 1, 0.5)
			elseif TreesData[i].condition >= 100 then
				TreesData[i].status = 0
				destroyElement(TreesData[i].sphere)
				setObjectScale(TreesData[i].object, 1)
				TreesData[i].sphere = ColShape.Sphere(TreesData[i].pos[1], TreesData[i].pos[2], TreesData[i].pos[3], 1.5)
				addEventHandler( "onColShapeHit", TreesData[i].sphere, EnterTrees)
			end
		end
	end
	if z == 0 then 
		killTimer(GrowthTreeTimer) 
		GrowthTreeTimer = nil 
	end
end
