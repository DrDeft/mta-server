GUINewLogin = {
    checkbox = {},
    staticimage = {},
    loginbox = {},
    passbox = {},
    keylogin = {},
    label = {},
    keyreg = {}
}
setHeatHaze ( 0 )

RegData = {login, mail, pass, invite, sex = 1, resident = 1, skin = 1, name = nil, age = 18}
font_medium = "fonts/Roboto-Medium.ttf"
font_five = "fonts/Five.ttf"
Five = guiCreateFont(font_five, 20)
fmedium = guiCreateFont(font_medium, 10)
local pane = ":FiveRP/loginpanel/images/pane.png"
local loginpanelcreate = false

local LoginPanelShowType = true

function guiCreateBoxEdit(x, y, w1, h1, text, type, ...)
	local Edit = guiCreateEdit(x, y, w1, h1,text, type, ...)
	local Enbl = {}
	--[[if type then
		Enbl[1] = guiCreateStaticImage(0.00, 0.9, 1, 0.15, pane, true, Edit) --Изображение снизу эдит-поля (pane - это переменная изображения)
		Enbl[2] = guiCreateStaticImage(0.00, 0.00, 1, 0.15, pane, true, Edit) --Изображение сверху эдит-поля (pane - это переменная изображения)
		Enbl[3] = guiCreateStaticImage(0.96, 0, 0.04, 1, pane, true, Edit) --Изображение справа эдит-поля (pane - это переменная изображения)
		Enbl[4] = guiCreateStaticImage(0.00, 0.0, 0.04, 1, pane, true, Edit) --Изображение слева эдит-поля (pane - это переменная изображения)
	
	else]]
	local w1, h1 = guiGetSize (Edit, false)
		Enbl[1] = guiCreateStaticImage(0, 0, w1, 5, pane, false, Edit) --Изображение сверху эдит-поля (pane - это переменная изображения)
		Enbl[2] = guiCreateStaticImage(0, 0, 3, h1, pane, false, Edit) --Изображение слева эдит-поля (pane - это переменная изображения)
		Enbl[3] = guiCreateStaticImage(w1-3, 0, 3, h1, pane, false, Edit) --Изображение справа эдит-поля (pane - это переменная изображения)
		Enbl[4] = guiCreateStaticImage(0, h1-3, w1, 3, pane, false, Edit) --Изображение снизу эдит-поля (pane - это переменная изображения)
	--end
	for i = 1, 4 do
		guiSetEnabled(Enbl[i], false)
	end
	--guiBringToFront (Edit)
	return Edit
end

function CreateLoginPanel()
	GUINewLogin.staticimage[1] = guiCreateStaticImage(0.37, 0.283, 0.26, 0.04, ":FiveRP/loginpanel/images/top.png", true)
	guiSetProperty(GUINewLogin.staticimage[1], "ImageColours", "tl:FE008000 tr:FE008000 bl:FE008000 br:FE008000")

	GUINewLogin.label[1] = guiCreateLabel(0.10, 0.00, 0.81, 0.84, "Авторизация", true, GUINewLogin.staticimage[1])
	guiSetFont(GUINewLogin.label[1], Five)
	guiLabelSetHorizontalAlign(GUINewLogin.label[1], "center", false)
	guiLabelSetVerticalAlign(GUINewLogin.label[1], "bottom")


	GUINewLogin.staticimage[2] = guiCreateStaticImage(0.37, 0.32, 0.26, 0.31, ":FiveRP/loginpanel/images/top.png", true)
	guiSetProperty(GUINewLogin.staticimage[2], "ImageColours", "tl:C9000000 tr:C9000000 bl:C9000000 br:C9000000")

	GUINewLogin.loginbox[2] = guiCreateBoxEdit(0.30, 0.18, 0.40, 0.12, "Логин", true, GUINewLogin.staticimage[2])
	GUINewLogin.passbox[2] = guiCreateBoxEdit(0.30, 0.34, 0.40, 0.12, "Пароль", true, GUINewLogin.staticimage[2])
	GUINewLogin.checkbox[1] = guiCreateCheckBox(0.26, 0.49, 0.39, 0.06, "Запомнить пароль?", true, true, GUINewLogin.staticimage[2])
	guiSetProperty(GUINewLogin.checkbox[1], "NormalTextColour", "FF636262")
	GUINewLogin.keylogin[2] = guiCreateStaticImage(0.30, 0.57, 0.40, 0.12, ":FiveRP/loginpanel/images/bottom.png", true, GUINewLogin.staticimage[2])

	GUINewLogin.label[2] = guiCreateLabel(0.02, 0.00, 0.93, 1.00, "Войти", true, GUINewLogin.keylogin[2])
	guiLabelSetColor(GUINewLogin.label[2], 22, 26, 31)
	guiLabelSetHorizontalAlign(GUINewLogin.label[2], "center", false)
	guiLabelSetVerticalAlign(GUINewLogin.label[2], "center")

	GUINewLogin.keyreg[2] = guiCreateStaticImage(0.30, 0.74, 0.40, 0.12, ":FiveRP/loginpanel/images/bottom.png", true, GUINewLogin.staticimage[2])

	GUINewLogin.label[3] = guiCreateLabel(0.02, 0.00, 0.93, 1.00, "Создать аккаунт", true, GUINewLogin.keyreg[2])
	guiLabelSetColor(GUINewLogin.label[3], 22, 26, 31)
	guiLabelSetHorizontalAlign(GUINewLogin.label[3], "center", false)
	guiLabelSetVerticalAlign(GUINewLogin.label[3], "center")

	GUINewLogin.keylogin[1] = guiCreateStaticImage(0.26, 0.57, 0.08, 0.12, ":FiveRP/loginpanel/images/round.png", true, GUINewLogin.staticimage[2])
	GUINewLogin.keylogin[3] = guiCreateStaticImage(0.66, 0.57, 0.08, 0.12, ":FiveRP/loginpanel/images/round.png", true, GUINewLogin.staticimage[2])
	GUINewLogin.keyreg[1] = guiCreateStaticImage(0.26, 0.74, 0.08, 0.12, ":FiveRP/loginpanel/images/round.png", true, GUINewLogin.staticimage[2])
	GUINewLogin.keyreg[3] = guiCreateStaticImage(0.66, 0.74, 0.08, 0.12, ":FiveRP/loginpanel/images/round.png", true, GUINewLogin.staticimage[2])
	GUINewLogin.passbox[1] = guiCreateStaticImage(0.26, 0.34, 0.08, 0.12, ":FiveRP/loginpanel/images/round.png", true, GUINewLogin.staticimage[2])
	GUINewLogin.loginbox[1] = guiCreateStaticImage(0.26, 0.18, 0.08, 0.12, ":FiveRP/loginpanel/images/round.png", true, GUINewLogin.staticimage[2])
	GUINewLogin.loginbox[3] = guiCreateStaticImage(0.66, 0.18, 0.08, 0.12, ":FiveRP/loginpanel/images/round.png", true, GUINewLogin.staticimage[2])
	GUINewLogin.passbox[3] = guiCreateStaticImage(0.66, 0.34, 0.08, 0.12, ":FiveRP/loginpanel/images/round.png", true, GUINewLogin.staticimage[2])
	GUINewLogin.label[4] = guiCreateLabel(0.02, 0.07, 0.96, 0.07, "Ошибка: Неверный логин или пароль!", true, GUINewLogin.staticimage[2])
	guiSetFont(GUINewLogin.label[4], fmedium)
	guiLabelSetColor(GUINewLogin.label[4], 171, 0, 0)
	guiLabelSetHorizontalAlign(GUINewLogin.label[4], "center", false)
	guiLabelSetVerticalAlign(GUINewLogin.label[4], "center")   
	
	-----------------------отключение нажатия на некоторые элементы--------------------------------
	guiSetEnabled ( GUINewLogin.staticimage[1], false)
	guiSetEnabled ( GUINewLogin.loginbox[1], false)
	guiSetEnabled ( GUINewLogin.loginbox[3], false)
	guiSetEnabled ( GUINewLogin.passbox[1], false)
	guiSetEnabled ( GUINewLogin.passbox[3], false)
	
	guiSetEnabled ( GUINewLogin.label[2], false)
	guiSetEnabled ( GUINewLogin.label[3], false)
	
	guiBringToFront ( GUINewLogin.staticimage[1] )
	guiBringToFront ( GUINewLogin.loginbox[2] )
	guiBringToFront ( GUINewLogin.passbox[2] )
	guiBringToFront ( GUINewLogin.keylogin[2] )
	guiBringToFront ( GUINewLogin.keyreg[2] )
	
	guiSetFont(GUINewLogin.loginbox[2], fmedium)
	guiSetFont(GUINewLogin.passbox[2], fmedium)
	guiSetFont(GUINewLogin.label[2], fmedium)
	guiSetFont(GUINewLogin.label[3], fmedium)
	
	guiSetProperty(GUINewLogin.loginbox[2], "NormalTextColour", "FF727272")
	guiSetProperty(GUINewLogin.passbox[2], "NormalTextColour", "FF727272")
	guiSetProperty(GUINewLogin.passbox[2], "ActiveSelectionColour", "FF259925")
	guiSetProperty(GUINewLogin.loginbox[2], "ActiveSelectionColour", "FF259925")
	
	for i in ipairs(GUINewLogin.keylogin) do
		guiSetProperty(GUINewLogin.keyreg[i], "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
		guiSetProperty(GUINewLogin.keylogin[i], "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
	end	
	guiSetVisible(GUINewLogin.label[4], false)
	--VievLoginPanel(false)
	loginpanelcreate = true
	LoadAccountData()
end

function VievLoginPanel(bool)
	showCursor(bool)
	if not loginpanelcreate then return CreateLoginPanel() end
	
	if bool then
		LoginPanelShowType = true
		guiSetPosition ( GUINewLogin.keylogin[1], 0.26, 0.57, true)
		guiSetPosition ( GUINewLogin.keylogin[2], 0.30, 0.57, true)
		guiSetPosition ( GUINewLogin.keylogin[3], 0.66, 0.57, true)
		
		guiSetPosition ( GUINewLogin.keyreg[1], 0.26, 0.74, true)
		guiSetPosition ( GUINewLogin.keyreg[2], 0.30, 0.74, true)
		guiSetPosition ( GUINewLogin.keyreg[3], 0.66, 0.74, true)
		guiSetText ( GUINewLogin.label[1], "Авторизация" )
		guiSetText ( GUINewLogin.label[3], "Создать аккаунт" )
		guiSetText ( GUINewLogin.label[2], "Войти" )
	end	
	
	for i in ipairs(GUINewLogin.staticimage) do
		guiSetVisible (GUINewLogin.staticimage[i], bool)
	end
	for i in ipairs(GUINewLogin.loginbox) do
		guiSetVisible (GUINewLogin.loginbox[i], bool)
		guiSetVisible (GUINewLogin.passbox[i], bool)
		guiSetVisible (GUINewLogin.keylogin[i], bool)
		guiSetVisible (GUINewLogin.keyreg[i], bool)
	end
	for i in ipairs(GUINewLogin.label) do
		guiSetVisible (GUINewLogin.label[i], bool)
		if i == 4 and bool == true then guiSetVisible(GUINewLogin.label[4], false) end
	end
	guiSetVisible (GUINewLogin.checkbox[1], bool)
	-------------Поднятие элементов на верхний уровань--------------
	guiBringToFront ( GUINewLogin.staticimage[1] )
	guiBringToFront ( GUINewLogin.loginbox[2] )
	guiBringToFront ( GUINewLogin.passbox[2] )
	guiBringToFront ( GUINewLogin.keylogin[2] )
	guiBringToFront ( GUINewLogin.keyreg[2] )
	
end

addEvent("hideLoginPanel", true) --Скрытие логин панели
addEventHandler("hideLoginPanel", root, function() VievLoginPanel(false) end) --Функцией скрываем логин панель

addEvent("showLoginPanel", true)
addEventHandler("showLoginPanel", root, function() VievLoginPanel(true) end) --Функцией показываем


addEventHandler("onClientGUIClick", root, function()
	if not loginpanelcreate then return 1 end
	if source == GUINewLogin.keylogin[1] or source == GUINewLogin.keylogin[2] or source == GUINewLogin.keylogin[3] then
		if LoginPanelShowType then
			if guiCheckBoxGetSelected ( GUINewLogin.checkbox[1] ) then
				triggerServerEvent("loginPlayer", localPlayer, localPlayer, guiGetText(GUINewLogin.loginbox[2]), guiGetText(GUINewLogin.passbox[2]), true)
			else
				DeleteAccountData()
				triggerServerEvent("loginPlayer", localPlayer, localPlayer, guiGetText(GUINewLogin.loginbox[2]), guiGetText(GUINewLogin.passbox[2]), false)
			end	
		else
			VievRegPanel(false)
			VievLoginPanel(true)
		end
	end
	if source == GUINewLogin.keyreg[1] or source == GUINewLogin.keyreg[2] or source == GUINewLogin.keyreg[3] then
		if LoginPanelShowType then
			VievRegPanel(true)
		else
			--Следующий шаг регистрации
			ValidateRegDate()
		end
	end
	if not LoginPanelShowType then
		if source == GUIRegPanel.boxemail[2] then --Если мы нажимаем на поле ввода логина
			if guiGetText(GUIRegPanel.boxemail[2]) == "E-Mail" then --То делаем проверку, написано ли в данном поле слово логин
				--Если да, то чистим текст
				guiSetText(GUIRegPanel.boxemail[2], "")
			end
		else
			if guiGetText(GUIRegPanel.boxemail[2]) == "" or --Если поле пустое
			guiGetText(GUIRegPanel.boxemail[2]) == " " --Или в нём стоит пробел
			then
				guiSetText(GUIRegPanel.boxemail[2], "E-Mail") --То мы возвращаем слово Логин обратно
			end
		end
		if source == GUIRegPanel.boxinvite[2] then --Если мы нажимаем на поле ввода логина
			if guiGetText(GUIRegPanel.boxinvite[2]) == "Инвайт-код" then --То делаем проверку, написано ли в данном поле слово логин
				--Если да, то чистим текст
				guiSetText(GUIRegPanel.boxinvite[2], "")
			end	
		else --Если нажимаем не на поле ввода логина
		--То проверяем на символы
			if guiGetText(GUIRegPanel.boxinvite[2]) == "" or --Если поле пустое
			guiGetText(GUIRegPanel.boxinvite[2]) == " " --Или в нём стоит пробел
			then
				guiSetText(GUIRegPanel.boxinvite[2], "Инвайт-код") --То мы возвращаем слово Логин обратно
			end
		end
	end
	if source == GUINewLogin.loginbox[2] then --Если мы нажимаем на поле ввода логина
		if guiGetText(GUINewLogin.loginbox[2]) == "Логин" then --То делаем проверку, написано ли в данном поле слово логин
			--Если да, то чистим текст
			guiSetText(GUINewLogin.loginbox[2], "")
		end
	else --Если нажимаем не на поле ввода логина
		--То проверяем на символы
		if guiGetText(GUINewLogin.loginbox[2]) == "" or --Если поле пустое
			guiGetText(GUINewLogin.loginbox[2]) == " " --Или в нём стоит пробел
			then
				guiSetText(GUINewLogin.loginbox[2], "Логин") --То мы возвращаем слово Логин обратно
		end
	end

	--Тоже самое повторим и с паролем

	if source == GUINewLogin.passbox[2] then --Если мы нажимаем на поле ввода пароля
		if guiGetText(GUINewLogin.passbox[2]) == "Пароль" then --То делаем проверку, написано ли в данном поле слово пароль
			--Если да, то чистим текст, и маскируем поле
			guiSetText(GUINewLogin.passbox[2], "")
			guiEditSetMasked(GUINewLogin.passbox[2], true)
		end
	else --Если нажимаем не на поле ввода логина
		--То проверяем на символы
		if guiGetText(GUINewLogin.passbox[2]) == "" or --Если поле пустое
			guiGetText(GUINewLogin.passbox[2]) == " " --Или в нём стоит пробел
			then
				guiSetText(GUINewLogin.passbox[2], "Пароль") --То мы возвращаем слово Пароль обратно
				guiEditSetMasked(GUINewLogin.passbox[2], false) --И делаем текст видимым
		end
	end

end)

--А для фокусировки на гуи (tab например нажали), чтобы текст исчезал, мы просто отправим триггер на нажатие
addEventHandler("onClientGUIFocus", root, function()
	if source == GUINewLogin.loginbox[2] then triggerEvent("onClientGUIClick", GUINewLogin.loginbox[2]) end --Для логин поля мы отправим триггер на клик
	if source == GUINewLogin.passbox[2] then triggerEvent("onClientGUIClick", GUINewLogin.passbox[2]) end --И для пароля
end)
--А это при расфокусировании с гуи элемента
addEventHandler("onClientGUIBlur", root, function()
	if source == GUINewLogin.loginbox[2] then triggerEvent("onClientGUIClick", GUINewLogin.loginbox[2]) end --Отправляем те же самые триггеры
	if source == GUINewLogin.passbox[2] then triggerEvent("onClientGUIClick", GUINewLogin.passbox[2]) end --И для пароля тоже
end)

addEventHandler("onClientMouseEnter", root, function() --Событие при наведении на GUI-элемент

	if source == GUINewLogin.keylogin[1] or source == GUINewLogin.keylogin[2] or source == GUINewLogin.keylogin[3]  then
		guiSetProperty(GUINewLogin.keylogin[1], "ImageColours", "tl:FF006400 tr:FF006400 bl:FF006400 br:FF006400")
		guiSetProperty(GUINewLogin.keylogin[2], "ImageColours", "tl:FF006400  tr:FF006400 bl:FF006400 br:FF006400")
		guiSetProperty(GUINewLogin.keylogin[3], "ImageColours", "tl:FF006400  tr:FF006400 bl:FF006400 br:FF006400")
	elseif source == GUINewLogin.keyreg[1] or source == GUINewLogin.keyreg[2] or source == GUINewLogin.keyreg[3] then
		guiSetProperty(GUINewLogin.keyreg[1], "ImageColours", "tl:FF006400 tr:FF006400 bl:FF006400 br:FF006400")
		guiSetProperty(GUINewLogin.keyreg[2], "ImageColours", "tl:FF006400  tr:FF006400 bl:FF006400 br:FF006400")
		guiSetProperty(GUINewLogin.keyreg[3], "ImageColours", "tl:FF006400  tr:FF006400 bl:FF006400 br:FF006400")
	end

end)

addEventHandler("onClientMouseLeave", root, function() --Событие при наведении на GUI-элемент

	if source == GUINewLogin.keylogin[1] or source == GUINewLogin.keylogin[2] or source == GUINewLogin.keylogin[3]  then
		guiSetProperty(GUINewLogin.keylogin[1], "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
		guiSetProperty(GUINewLogin.keylogin[2], "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
		guiSetProperty(GUINewLogin.keylogin[3], "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
	elseif source == GUINewLogin.keyreg[1] or source == GUINewLogin.keyreg[2] or source == GUINewLogin.keyreg[3] then
		guiSetProperty(GUINewLogin.keyreg[1], "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
		guiSetProperty(GUINewLogin.keyreg[2], "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
		guiSetProperty(GUINewLogin.keyreg[3], "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
	end

end)

function WrongPassword(type)
	if type == 1 then
		guiSetText(GUINewLogin.label[4], "Ошибка: Неверный логин или пароль!")
	elseif type == 2 then
		guiSetText(GUINewLogin.label[4], "Ошибка: Данный аккаунт заблокирован!")
	elseif type == 3 then
		guiSetText(GUINewLogin.label[4], "Ошибка: Данный аккаунт уже в игре!")
	end
	guiSetVisible(GUINewLogin.label[4], true)
end
addEvent("WrongPassword", true)
addEventHandler("WrongPassword", root, WrongPassword)






-----------------------------Регистрация---------------------------------------------------------------------------------------
GUIRegPanel = {
    staticimage = {},
    boxemail = {},
	boxinvite = {}
}
local RegPanelCreated = false

function CreateRegPanel()
	GUIRegPanel.boxemail[2] = guiCreateBoxEdit(0.30, 0.50, 0.40, 0.12, "E-Mail", true, GUINewLogin.staticimage[2])
	guiSetProperty(GUIRegPanel.boxemail[2], "NormalTextColour", "FF727272")
	GUIRegPanel.boxinvite[2] = guiCreateBoxEdit(0.30, 0.66, 0.40, 0.12, "Инвайт-код", true, GUINewLogin.staticimage[2])
	guiSetProperty(GUIRegPanel.boxinvite[2], "NormalTextColour", "FF727272")
	--Окнугления для полей------------------------------------
	GUIRegPanel.boxemail[1] = guiCreateStaticImage(0.26, 0.50, 0.08, 0.12, ":FiveRP/loginpanel/images/round.png", true, GUINewLogin.staticimage[2])
	GUIRegPanel.boxemail[3] = guiCreateStaticImage(0.66, 0.50, 0.08, 0.12, ":FiveRP/loginpanel/images/round.png", true, GUINewLogin.staticimage[2])
	GUIRegPanel.boxinvite[1] = guiCreateStaticImage(0.26, 0.66, 0.08, 0.12, ":FiveRP/loginpanel/images/round.png", true, GUINewLogin.staticimage[2])
	GUIRegPanel.boxinvite[3] = guiCreateStaticImage(0.66, 0.66, 0.08, 0.12, ":FiveRP/loginpanel/images/round.png", true, GUINewLogin.staticimage[2])
	-----------------------------------------------------
	guiSetEnabled ( GUIRegPanel.boxemail[1], false)
	guiSetEnabled ( GUIRegPanel.boxemail[3], false)
	guiSetEnabled ( GUIRegPanel.boxinvite[1], false)
	guiSetEnabled ( GUIRegPanel.boxinvite[3], false)
	
	guiBringToFront ( GUIRegPanel.boxinvite[2] )
	guiBringToFront ( GUIRegPanel.boxemail[2] )
	
	guiSetProperty(GUIRegPanel.boxemail[2], "NormalTextColour", "FF727272")
	guiSetProperty(GUIRegPanel.boxinvite[2], "NormalTextColour", "FF727272")
	guiSetProperty(GUIRegPanel.boxinvite[2], "ActiveSelectionColour", "FF259925")
	guiSetProperty(GUIRegPanel.boxemail[2], "ActiveSelectionColour", "FF259925")
	guiSetFont(GUIRegPanel.boxemail[2], fmedium)
	guiSetFont(GUIRegPanel.boxinvite[2], fmedium)
	
	RegPanelCreated = true
	VievRegPanel(true)
	CreateRegHelpGUI()

end





function VievRegPanel(bool)
	VievLoginPanel(false) -- скрываем панель авторизации
	showCursor(true)
	if not RegPanelCreated then
		return CreateRegPanel()
	end
	LoginPanelShowType = false
	for i in ipairs(GUINewLogin.staticimage) do
		guiSetVisible (GUINewLogin.staticimage[i], bool)
	end
	if bool then
		guiSetText ( GUINewLogin.label[1], "Регистрация" )
		guiSetText ( GUINewLogin.label[3], "Далее" )
		guiSetText ( GUINewLogin.label[2], "Назад" )
		guiSetPosition ( GUINewLogin.keylogin[1], 0.01, 0.83, true)
		guiSetPosition ( GUINewLogin.keylogin[2], 0.06, 0.83, true)
		guiSetPosition ( GUINewLogin.keylogin[3], 0.42, 0.83, true)
		
		guiSetPosition ( GUINewLogin.keyreg[1], 0.51, 0.83, true)
		guiSetPosition ( GUINewLogin.keyreg[2], 0.55, 0.83, true)
		guiSetPosition ( GUINewLogin.keyreg[3], 0.90, 0.83, true)
	end	
	
	for i in ipairs(GUINewLogin.loginbox) do
		guiSetVisible (GUINewLogin.loginbox[i], bool)
		guiSetVisible (GUINewLogin.passbox[i], bool)
		guiSetVisible (GUINewLogin.keylogin[i], bool)
		guiSetVisible (GUINewLogin.keyreg[i], bool)
		guiSetVisible (GUIRegPanel.boxemail[i], bool)
		guiSetVisible (GUIRegPanel.boxinvite[i], bool)
		
	end
	
	for i in ipairs(GUINewLogin.label) do
		guiSetVisible (GUINewLogin.label[i], bool)
		if i == 4 and bool == true then guiSetVisible(GUINewLogin.label[4], false) end
	end
	
end

function ValidateRegDate()
	-------------------------Валидация поля логин----------------------------------------
	if string.len(guiGetText(GUINewLogin.loginbox[2])) < 5 then -- 
		guiSetText(GUINewLogin.label[4], "Логин не может быть меньше 5 символов!")
		guiSetVisible(GUINewLogin.label[4], true)
		return 1
	end
	if string.find(guiGetText(GUINewLogin.loginbox[2]), "%W") ~= nil then 
		guiSetText(GUINewLogin.label[4], "Логин может состоять только из символов a-Z и 0-9")
		guiSetVisible(GUINewLogin.label[4], true)
		return 1;
	end		
	RegData["login"] = guiGetText(GUINewLogin.loginbox[2])
	--------------------------Валидация пароля---------------------------------------------
	if string.len(guiGetText(GUINewLogin.passbox[2])) < 6 then
		guiSetText(GUINewLogin.label[4], "Пароль не может быть меньше 6 символов!")
		guiSetVisible(GUINewLogin.label[4], true)
		return 1
	end
	if string.find(guiGetText(GUINewLogin.passbox[2]), "%W") ~= nil then
		guiSetText(GUINewLogin.label[4], "Пароль может состоять только из символов a-Z и 0-9")
		guiSetVisible(GUINewLogin.label[4], true)
		return 1;
	end	
	RegData["pass"] = guiGetText(GUINewLogin.passbox[2])
	--------------------------------------Валидация EMail---------------------------------------
	if not string.match(guiGetText(GUIRegPanel.boxemail[2]), "[A-Za-z0-9%.%%%+%-]+@[A-Za-z0-9%.%%%+%-]+%.%w%w%w?%w?") then
		guiSetText(GUINewLogin.label[4], "Введите E-Mail адрес!")
		guiSetVisible(GUINewLogin.label[4], true)
		return 1
	
	end
	RegData["mail"] = guiGetText(GUIRegPanel.boxemail[2])
	--------------------------------Проверка инвайт кода-------------------------------------------
	--if guiGetText(GUIRegPanel.boxinvite[2]) ~= "FiveRPBeta2017" then
		--guiSetText(GUINewLogin.label[4], "Для доступа к альфа тесту введите инвайт-код")
		--guiSetVisible(GUINewLogin.label[4], true)
		--return 1;
	--end		
	guiSetVisible(GUINewLogin.label[4], false)
	RegData["invite"] = guiGetText(GUIRegPanel.boxinvite[2])
	triggerServerEvent("LocalCheckReg", localPlayer, localPlayer, guiGetText(GUINewLogin.loginbox[2]))
end


-------------Надпись загрузка при входе------------------------
addEventHandler("onClientResourceStart", resourceRoot,
    function()
        VievLoginPanel(true) 
		setPlayerHudComponentVisible ("all", false)
		showChat (false)
    end
)

function CheckRegisterAccount(bool)
	if bool then
		guiSetText(GUINewLogin.label[4], "Аккаунт с таким логином уже существует!")
		guiSetVisible(GUINewLogin.label[4], true)
	else 
		VievRegPanel(false)
		VievCharacterGUI(true)
		bindKey( "arrow_d", "down", RegPressKey) 
		bindKey( "arrow_u", "down", RegPressKey) 
		bindKey( "arrow_l", "down", RegPressKey) 
		bindKey( "arrow_r", "down", RegPressKey) 
		bindKey( "enter", "down", RegPressKey) 
		setElementInterior ( localPlayer, 1 )
		setElementPosition ( localPlayer, 199.9423828125, -34.9541015625, 1002.3040161133)
		setCameraMatrix (202.90100097656, -38.728618621826, 1003.8399658203, 144.06074523926, 34.761489868164, 970.1201171875)
	end
end
addEvent("CheckRegisterAccount", true) 
addEventHandler("CheckRegisterAccount", root, CheckRegisterAccount)

function SaveAccountData(login, hash)
	local xmlcookie = xmlLoadFile ( "loginpanel/cookie.xml" )
	if not xmlcookie then --Создаем файл Cookie
		local xmlcookie = xmlCreateFile ("loginpanel/cookie.xml", "cookie")
		local node = xmlCreateChild(xmlcookie, "data")
		xmlNodeSetAttribute(node, "login", login)
		xmlNodeSetAttribute(node, "password", hash) --md5 ( string str )
		xmlSaveFile(xmlcookie)
		xmlUnloadFile (xmlcookie)    
	else
		local node = xmlFindChild (xmlcookie, "data",0)
		if not node then
			--Пустой файл сохранений
			node = xmlCreateChild(xmlcookie, "data")
			xmlNodeSetAttribute(node, "login", login)
			xmlNodeSetAttribute(node, "password", hash)
			xmlSaveFile(xmlcookie)
			xmlUnloadFile (xmlcookie) 
		else
			--Данные в файле есть
			xmlNodeSetAttribute(node, "login", login)
			xmlNodeSetAttribute(node, "password", hash)
			xmlSaveFile(xmlcookie)
			xmlUnloadFile (xmlcookie)    
		end
	end	
end
addEvent("SaveAccountData", true)
addEventHandler("SaveAccountData", root, SaveAccountData)

function DeleteAccountData()
	local xmlcookie = xmlLoadFile ( "loginpanel/cookie.xml" )
	if not xmlcookie then return 1 end
	local node = xmlFindChild(xmlcookie, "data", 0)
	xmlDestroyNode(node)
    xmlSaveFile(xmlcookie)
    xmlUnloadFile(xmlcookie)
end

function LoadAccountData()
	local xmlcookie = xmlLoadFile ( "loginpanel/cookie.xml" )
	if not xmlcookie then return 1 end
	local node = xmlFindChild (xmlcookie, "data",0)
	if not node then return 1 end
	local login = xmlNodeGetAttribute(node, "login")
	local password = xmlNodeGetAttribute(node, "password")
	
	guiSetText(GUINewLogin.loginbox[2], login)
	guiSetText(GUINewLogin.passbox[2], password)
	guiEditSetMasked(GUINewLogin.passbox[2], true)
end