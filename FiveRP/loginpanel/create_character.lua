GUICreateChar = {
    label = {},
    staticimage = {}
}

Five26 = guiCreateFont(font_five, 20)
fmedium15 = guiCreateFont(font_medium, 15)

local sex = {"Мужской", "Женский"}
local resident = {"Местный", "Приезжий"}

local regMaleSkins = {2, 14, 15, 17, 20, 23, 29, 30, 32, 44, 47, 48, 60, 170, 188, 250, 210}
local regFeMaleSkins = {12, 13, 40, 41, 55, 56, 65, 69, 76, 90, 91, 93, 131, 141, 148, 150, 169, 190, 192, 193, 195}

local CreateCharacter = false
local SelectedCLine = 0
local CharacterName = false

GUICharacterName = {
    staticimage = {},
    edit = {},
    label = {}
}

function ChangeCharacterName()
	GUICharacterName.staticimage[1] = guiCreateStaticImage(0.38, 0.37, 0.25, 0.22, ":FiveRP/loginpanel/images/top.png", true)
	guiSetProperty(GUICharacterName.staticimage[1], "ImageColours", "tl:D1010000 tr:D1010000 bl:D1010000 br:D1010000")

	GUICharacterName.staticimage[2] = guiCreateStaticImage(0.00, 0.00, 1.00, 0.12, ":FiveRP/loginpanel/images/pane.png", true, GUICharacterName.staticimage[1])
	guiSetProperty(GUICharacterName.staticimage[2], "ImageColours", "tl:FE0C4B00 tr:FE0C4B00 bl:FE0C4B00 br:FE0C4B00")

	GUICharacterName.label[1] = guiCreateLabel(0.00, 0.03, 0.99, 0.82, "Имя персонажа", true, GUICharacterName.staticimage[2])
	guiSetFont(GUICharacterName.label[1], Five26)
	guiLabelSetHorizontalAlign(GUICharacterName.label[1], "center", false)
	guiLabelSetVerticalAlign(GUICharacterName.label[1], "center")
	
	GUICharacterName.label[2] = guiCreateLabel(0, 39, 480, 32, "Введите Имя и Фамилию вашего персонажа через пробел\nНапример: Ivan Ivanov", false, GUICharacterName.staticimage[1])
	guiLabelSetHorizontalAlign(GUICharacterName.label[2], "center", false)

	GUICharacterName.edit[1] = guiCreateBoxEdit(0.30, 0.34, 0.37, 0.15, "Имя Фамилия", true, GUICharacterName.staticimage[1])
	guiSetFont(GUICharacterName.edit[1], fmedium)
	
	GUICharacterName.staticimage[5] = guiCreateStaticImage(0.26, 0.34, 0.08, 0.15, ":FiveRP/loginpanel/images/round.png", true, GUICharacterName.staticimage[1])
	GUICharacterName.staticimage[6] = guiCreateStaticImage(0.63, 0.34, 0.08, 0.15, ":FiveRP/loginpanel/images/round.png", true, GUICharacterName.staticimage[1])
	guiSetEnabled(GUICharacterName.staticimage[5], false)
	guiSetEnabled(GUICharacterName.staticimage[6], false)
	guiBringToFront ( GUICharacterName.edit[1] )
	guiSetProperty(GUICharacterName.edit[1], "NormalTextColour", "FF727272")
	guiSetProperty(GUICharacterName.edit[1], "ActiveSelectionColour", "FF259925")
	-------------------------------------------------------------------------------------------------------------------------------------------------------------
	GUICharacterName.staticimage[3] = guiCreateStaticImage(0.07, 0.67, 0.37, 0.15, ":FiveRP/loginpanel/images/bottom.png", true, GUICharacterName.staticimage[1])
	GUICharacterName.staticimage[7] = guiCreateStaticImage(0.03, 0.67, 0.08, 0.15, ":FiveRP/loginpanel/images/round.png", true, GUICharacterName.staticimage[1])
	GUICharacterName.staticimage[8] = guiCreateStaticImage(0.405, 0.67, 0.08, 0.15, ":FiveRP/loginpanel/images/round.png", true, GUICharacterName.staticimage[1])
	
	GUICharacterName.label[3] = guiCreateLabel(0.02, 0.00, 0.93, 1.00, "Принять", true, GUICharacterName.staticimage[3])
	guiLabelSetColor(GUICharacterName.label[3], 22, 26, 31)
	guiLabelSetHorizontalAlign(GUICharacterName.label[3], "center", false)
	guiLabelSetVerticalAlign(GUICharacterName.label[3], "center")
	guiSetEnabled(GUICharacterName.label[3], false)
	-----------------------------------------------------------------------------------------------------------------------------------------------------------
	
	GUICharacterName.staticimage[4] = guiCreateStaticImage(0.55, 0.67, 0.37, 0.15, ":FiveRP/loginpanel/images/bottom.png", true, GUICharacterName.staticimage[1])
	GUICharacterName.staticimage[9] = guiCreateStaticImage(0.51, 0.67, 0.08, 0.15, ":FiveRP/loginpanel/images/round.png", true, GUICharacterName.staticimage[1])
	GUICharacterName.staticimage[10] = guiCreateStaticImage(0.88, 0.67, 0.08, 0.15, ":FiveRP/loginpanel/images/round.png", true, GUICharacterName.staticimage[1])
	
	GUICharacterName.label[4] = guiCreateLabel(0.02, 0.00, 0.93, 1.00, "Отмена", true, GUICharacterName.staticimage[4])
	guiLabelSetColor(GUICharacterName.label[4], 22, 26, 31)
	guiLabelSetHorizontalAlign(GUICharacterName.label[4], "center", false)
	guiLabelSetVerticalAlign(GUICharacterName.label[4], "center")
	guiSetEnabled(GUICharacterName.label[4], false)
	-----------------------------------------------------------------------------------------------------------------------------------------------------------
	
	for i = 3, 10 do
		if i ~= 5 and i ~= 6 then
			guiSetProperty(GUICharacterName.staticimage[i], "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
		end	
	end
	for i = 2, 4 do
		guiSetFont(GUICharacterName.label[i], fmedium)
	end
	VievChaneCharacterName(false)
end

function VievChaneCharacterName(bool)
	
	for i in ipairs(GUICharacterName.staticimage) do
		guiSetVisible(GUICharacterName.staticimage[i], bool)
	end
	
	guiSetVisible(GUICharacterName.edit[1], bool)
	
	for i in ipairs(GUICharacterName.label) do
		guiSetVisible(GUICharacterName.label[i], bool)
	end

end

function GUIACCEPT(bool)
	
	for i in ipairs(GUICharacterName.staticimage) do
		guiSetVisible(GUICharacterName.staticimage[i], bool)
	end
	
	--guiSetVisible(GUICharacterName.edit[1], bool)
	
	for i in ipairs(GUICharacterName.label) do
		guiSetVisible(GUICharacterName.label[i], bool)
	end

end


function CreateCharacterGUI()
	
	if CreateCharacter then return VievCharacterGUI(true) end

	GUICreateChar.staticimage[1] = guiCreateStaticImage(0.04, 0.07, 0.18, 0.06, ":FiveRP/loginpanel/images/pane.png", true)
	guiSetProperty(GUICreateChar.staticimage[1], "ImageColours", "tl:FF0A4900 tr:FF0A4900 bl:FF0A4900 br:FF0A4900")

	GUICreateChar.label[1] = guiCreateLabel(0.01, 0.08, 0.97, 0.86, "Редактор персонажей", true, GUICreateChar.staticimage[1])
	guiSetFont(GUICreateChar.label[1], Five26)
	guiLabelSetHorizontalAlign(GUICreateChar.label[1], "center", false)
	guiLabelSetVerticalAlign(GUICreateChar.label[1], "center")


	GUICreateChar.staticimage[2] = guiCreateStaticImage(0.04, 0.13, 0.18, 0.035, ":FiveRP/loginpanel/images/pane.png", true)
	guiSetProperty(GUICreateChar.staticimage[2], "ImageColours", "tl:FF000500 tr:FF000500 bl:FF000500 br:FF000500")

	GUICreateChar.label[2] = guiCreateLabel(0.06, 0.00, 0.91, 1.00, "НОВЫЙ ПЕРСОНАЖ", true, GUICreateChar.staticimage[2])
	guiSetFont(GUICreateChar.label[2], fmedium)
	guiLabelSetColor(GUICreateChar.label[2], 10, 73, 0)
	guiLabelSetVerticalAlign(GUICreateChar.label[2], "center")


	GUICreateChar.staticimage[3] = guiCreateStaticImage(0.04, 0.16, 0.18, 0.03, ":FiveRP/loginpanel/images/pane.png", true)
	--guiSetProperty(GUICreateChar.staticimage[3], "ImageColours", "tl:FEFFFFFF tr:FEFFFFFF bl:FEFFFFFF br:FEFFFFFF")
	guiSetProperty(GUICreateChar.staticimage[3], "ImageColours", "tl:8B000000 tr:8B000000 bl:8B000000 br:8B000000")

	GUICreateChar.label[3] = guiCreateLabel(0.02, 0.00, 0.19, 1.00, "Имя", true, GUICreateChar.staticimage[3])
	guiSetFont(GUICreateChar.label[3], fmedium)
	guiSetEnabled(GUICreateChar.label[3], false)
	guiLabelSetVerticalAlign(GUICreateChar.label[3], "center")
	GUICreateChar.label[4] = guiCreateLabel(0.27, 0.00, 0.70, 1.00, "Имя Фамилия", true, GUICreateChar.staticimage[3])
	guiSetFont(GUICreateChar.label[4], fmedium)
	guiLabelSetHorizontalAlign(GUICreateChar.label[4], "right", false)
	guiLabelSetVerticalAlign(GUICreateChar.label[4], "center")


	GUICreateChar.staticimage[4] = guiCreateStaticImage(0.04, 0.19, 0.18, 0.0305, ":FiveRP/loginpanel/images/pane.png", true)
	guiSetProperty(GUICreateChar.staticimage[4], "ImageColours", "tl:8B000000 tr:8B000000 bl:8B000000 br:8B000000")

	GUICreateChar.label[5] = guiCreateLabel(0.02, 0.00, 0.14, 1.00, "Пол", true, GUICreateChar.staticimage[4])
	guiSetFont(GUICreateChar.label[5], fmedium)
	guiSetEnabled(GUICreateChar.label[5], false)
	guiLabelSetVerticalAlign(GUICreateChar.label[5], "center")
	GUICreateChar.label[6] = guiCreateLabel(0.27, 0.00, 0.70, 1.00, sex[1], true, GUICreateChar.staticimage[4])
	guiSetFont(GUICreateChar.label[6], fmedium)
	guiLabelSetVerticalAlign(GUICreateChar.label[6], "center")
	guiLabelSetHorizontalAlign(GUICreateChar.label[6], "right", false)
	--GUICreateChar.label[7] = guiCreateLabel(0.68, 0.00, 0.06, 1.00, "<", true, GUICreateChar.staticimage[4])
	--guiSetFont(GUICreateChar.label[7], fmedium15)
	--guiLabelSetVerticalAlign(GUICreateChar.label[7], "center")
	--GUICreateChar.label[8] = guiCreateLabel(0.93, 0.00, 0.06, 1.00, ">", true, GUICreateChar.staticimage[4])
	--guiSetFont(GUICreateChar.label[8], fmedium15)
	--guiLabelSetVerticalAlign(GUICreateChar.label[8], "center")


	GUICreateChar.staticimage[5] = guiCreateStaticImage(0.04, 0.22, 0.18, 0.03, ":FiveRP/loginpanel/images/pane.png", true)
	guiSetProperty(GUICreateChar.staticimage[5], "ImageColours", "tl:8B000000 tr:8B000000 bl:8B000000 br:8B000000")

	GUICreateChar.label[7] = guiCreateLabel(0.02, 0.00, 0.44, 1.00, "Место жительства", true, GUICreateChar.staticimage[5])
	guiSetFont(GUICreateChar.label[7], fmedium)
	guiLabelSetVerticalAlign(GUICreateChar.label[7], "center")
	GUICreateChar.label[8] = guiCreateLabel(0.27, 0.00, 0.70, 1.00, resident[1], true, GUICreateChar.staticimage[5])
	guiSetFont(GUICreateChar.label[8], fmedium)
	guiLabelSetVerticalAlign(GUICreateChar.label[8], "center")
	guiLabelSetHorizontalAlign(GUICreateChar.label[8], "right", false)
	--GUICreateChar.label[9] = guiCreateLabel(0.66, 0.00, 0.06, 1.00, "<", true, GUICreateChar.staticimage[5])
	--guiSetFont(GUICreateChar.label[9], fmedium15)
	--guiLabelSetVerticalAlign(GUICreateChar.label[9], "center")
	--GUICreateChar.label[12] = guiCreateLabel(0.94, 0.00, 0.06, 1.00, ">", true, GUICreateChar.staticimage[5])
	--guiSetFont(GUICreateChar.label[12], fmedium15)
	--guiLabelSetVerticalAlign(GUICreateChar.label[12], "center")


	GUICreateChar.staticimage[6] = guiCreateStaticImage(0.04, 0.25, 0.18, 0.03, ":FiveRP/loginpanel/images/pane.png", true)
	guiSetProperty(GUICreateChar.staticimage[6], "ImageColours", "tl:8B000000 tr:8B000000 bl:8B000000 br:8B000000")

	GUICreateChar.label[9] = guiCreateLabel(0.02, 0.00, 0.44, 1.00, "Дата рождения", true, GUICreateChar.staticimage[6])
	guiSetFont(GUICreateChar.label[9], fmedium)
	guiLabelSetVerticalAlign(GUICreateChar.label[9], "center")
	GUICreateChar.label[10] = guiCreateLabel(0.27, 0.00, 0.70, 1.00, "01.01.1990", true, GUICreateChar.staticimage[6])
	guiSetFont(GUICreateChar.label[10], fmedium)
	guiLabelSetVerticalAlign(GUICreateChar.label[10], "center")
	guiLabelSetHorizontalAlign(GUICreateChar.label[10], "right", false)


	GUICreateChar.staticimage[7] = guiCreateStaticImage(0.04, 0.28, 0.18, 0.0305, ":FiveRP/loginpanel/images/pane.png", true)
	guiSetProperty(GUICreateChar.staticimage[7], "ImageColours", "tl:8B000000 tr:8B000000 bl:8B000000 br:8B000000")

	GUICreateChar.label[11] = guiCreateLabel(0.02, 0.00, 0.44, 1.00, "Внешность", true, GUICreateChar.staticimage[7])
	guiSetFont(GUICreateChar.label[11], fmedium)
	guiLabelSetVerticalAlign(GUICreateChar.label[11], "center")
	GUICreateChar.label[12] = guiCreateLabel(0.27, 0.00, 0.70, 1.00, "ID: 2", true, GUICreateChar.staticimage[7])
	guiSetFont(GUICreateChar.label[12], fmedium)
	guiLabelSetVerticalAlign(GUICreateChar.label[12], "center")
	guiLabelSetHorizontalAlign(GUICreateChar.label[12], "right", false)
	--GUICreateChar.label[17] = guiCreateLabel(0.67, 0.00, 0.06, 1.00, "<", true, GUICreateChar.staticimage[7])
	--guiSetFont(GUICreateChar.label[17], fmedium15)
	--guiLabelSetVerticalAlign(GUICreateChar.label[17], "center")
	--GUICreateChar.label[18] = guiCreateLabel(0.91, 0.00, 0.06, 1.00, ">", true, GUICreateChar.staticimage[7])
	--guiSetFont(GUICreateChar.label[18], fmedium15)
	--guiLabelSetVerticalAlign(GUICreateChar.label[18], "center")


	GUICreateChar.staticimage[8] = guiCreateStaticImage(0.04, 0.31, 0.18, 0.03, ":FiveRP/loginpanel/images/pane.png", true)
	guiSetProperty(GUICreateChar.staticimage[8], "ImageColours", "tl:FF006400 tr:FF006400 bl:FF006400 br:FF006400")

	GUICreateChar.label[13] = guiCreateLabel(0.02, 0.00, 0.91, 1.00, "Сохранить и продолжить", true, GUICreateChar.staticimage[8])
	guiSetFont(GUICreateChar.label[13], fmedium)
	guiLabelSetVerticalAlign(GUICreateChar.label[13], "center")  

	--Отключаем нажатия на некликабельные GUI
	guiSetEnabled ( GUICreateChar.label[3], false)
	guiSetEnabled ( GUICreateChar.label[5], false)
	guiSetEnabled ( GUICreateChar.label[7], false)
	guiSetEnabled ( GUICreateChar.label[9], false)
	guiSetEnabled ( GUICreateChar.label[11], false)
	guiSetEnabled ( GUICreateChar.label[13], false)
	guiSetEnabled ( GUICreateChar.label[2], false)
	
	CreateCharacter = true
	ChangeCharacterName()
end

function VievCharacterGUI(bool)
--fadeCamera(true)
	if bool then
		if not CreateCharacter then 
			SendRegHelp("Используйте клавиши 'Вверх', 'Вниз' для переключения между строками. А кнопки 'Влево' и 'Вправо' для изменения значений")
			return CreateCharacterGUI()
		end
	end	
	
	for i in ipairs(GUICreateChar.staticimage) do
		guiSetVisible (GUICreateChar.staticimage[i], bool)
	end
	
	for i in ipairs(GUICreateChar.label) do
		guiSetVisible (GUICreateChar.label[i], bool)
	end
	
end

function MoveSelectGUICharter()
	if source == GUICharacterName.staticimage[3] or source == GUICharacterName.staticimage[7] or source == GUICharacterName.staticimage[8] then
		guiSetProperty(GUICharacterName.staticimage[3], "ImageColours", "tl:FF006400 tr:FF006400 bl:FF006400 br:FF006400")
		guiSetProperty(GUICharacterName.staticimage[7], "ImageColours", "tl:FF006400  tr:FF006400 bl:FF006400 br:FF006400")
		guiSetProperty(GUICharacterName.staticimage[8], "ImageColours", "tl:FF006400  tr:FF006400 bl:FF006400 br:FF006400")
	elseif source == GUICharacterName.staticimage[4] or source == GUICharacterName.staticimage[9] or source == GUICharacterName.staticimage[10] then
		guiSetProperty(GUICharacterName.staticimage[4], "ImageColours", "tl:FF006400 tr:FF006400 bl:FF006400 br:FF006400")
		guiSetProperty(GUICharacterName.staticimage[9], "ImageColours", "tl:FF006400  tr:FF006400 bl:FF006400 br:FF006400")
		guiSetProperty(GUICharacterName.staticimage[10], "ImageColours", "tl:FF006400  tr:FF006400 bl:FF006400 br:FF006400")
	end
	
end
addEventHandler("onClientMouseEnter", root, MoveSelectGUICharter)

function MoveLeaveGUICharter()
	if source == GUICharacterName.staticimage[3] or source == GUICharacterName.staticimage[7] or source == GUICharacterName.staticimage[8] then
		guiSetProperty(GUICharacterName.staticimage[3], "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
		guiSetProperty(GUICharacterName.staticimage[7], "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
		guiSetProperty(GUICharacterName.staticimage[8], "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
	elseif source == GUICharacterName.staticimage[4] or source == GUICharacterName.staticimage[9] or source == GUICharacterName.staticimage[10] then
		guiSetProperty(GUICharacterName.staticimage[4], "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
		guiSetProperty(GUICharacterName.staticimage[9], "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
		guiSetProperty(GUICharacterName.staticimage[10], "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
	end
end
addEventHandler("onClientMouseLeave", root, MoveLeaveGUICharter)


function PaintLine(id, bool)
	if bool == nil then bool = true end
	if bool == true then 
		if id == 1 then
			guiSetProperty(GUICreateChar.staticimage[3], "ImageColours", "tl:FEFFFFFF tr:FEFFFFFF bl:FEFFFFFF br:FEFFFFFF")
			guiLabelSetColor(GUICreateChar.label[3], 0, 0, 0)
			guiLabelSetColor(GUICreateChar.label[4], 0, 0, 0)
			SendRegHelp("Нажмите клавишу 'Enter' для изменения имени и фамилии вашего персонажа")
			
		elseif id == 2 then
			guiSetProperty(GUICreateChar.staticimage[4], "ImageColours", "tl:FEFFFFFF tr:FEFFFFFF bl:FEFFFFFF br:FEFFFFFF")
			guiLabelSetColor(GUICreateChar.label[5], 0, 0, 0)
			guiLabelSetColor(GUICreateChar.label[6], 0, 0, 0)
			--guiLabelSetColor(GUICreateChar.label[7], 0, 0, 0)
			--guiLabelSetColor(GUICreateChar.label[8], 0, 0, 0)
			SendRegHelp("Используйте клавиши 'Влево' и 'Вправо' для изменения пола вашего персонажа")
			
		elseif id == 3 then
			guiSetProperty(GUICreateChar.staticimage[5], "ImageColours", "tl:FEFFFFFF tr:FEFFFFFF bl:FEFFFFFF br:FEFFFFFF")
			guiLabelSetColor(GUICreateChar.label[7], 0, 0, 0)
			guiLabelSetColor(GUICreateChar.label[8], 0, 0, 0)
			--guiLabelSetColor(GUICreateChar.label[9], 0, 0, 0)
			--guiLabelSetColor(GUICreateChar.label[12], 0, 0, 0)
			SendRegHelp("Используйте кливаши 'Влево' и 'Вправо' для изменения места жительства вашего персонажа. От этого параметра зависят начальные задания!")
			
		elseif id == 4 then
			guiSetProperty(GUICreateChar.staticimage[6], "ImageColours", "tl:FEFFFFFF tr:FEFFFFFF bl:FEFFFFFF br:FEFFFFFF")
			guiLabelSetColor(GUICreateChar.label[9], 0, 0, 0)
			guiLabelSetColor(GUICreateChar.label[10], 0, 0, 0)
			SendRegHelp("Нажмите клавишу 'Enter' для изменения даты рождения вашего ИГРОВОГО персонажа")
			
		elseif id == 5 then
			guiSetProperty(GUICreateChar.staticimage[7], "ImageColours", "tl:FEFFFFFF tr:FEFFFFFF bl:FEFFFFFF br:FEFFFFFF")
			guiLabelSetColor(GUICreateChar.label[11], 0, 0, 0)
			guiLabelSetColor(GUICreateChar.label[12], 0, 0, 0)
			--guiLabelSetColor(GUICreateChar.label[17], 0, 0, 0)
			--guiLabelSetColor(GUICreateChar.label[18], 0, 0, 0)
			SendRegHelp("Используйте клавиши 'Влево' и 'Вправо' для изменения внешности вашего персонажа")

		elseif id == 6 then
			guiSetProperty(GUICreateChar.staticimage[8], "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
			SendRegHelp("Нажмите клавишу 'Enter' для закрытия редактора создания персонажа")
		end	
	else
		VievRegHelp(false)
		if id == 1 then
			guiSetProperty(GUICreateChar.staticimage[3], "ImageColours", "tl:8B000000 tr:8B000000 bl:8B000000 br:8B000000")
			guiLabelSetColor(GUICreateChar.label[3], 255, 255, 255)
			guiLabelSetColor(GUICreateChar.label[4], 255, 255, 255)
		elseif id == 2 then
			guiSetProperty(GUICreateChar.staticimage[4], "ImageColours", "tl:8B000000 tr:8B000000 bl:8B000000 br:8B000000")
			guiLabelSetColor(GUICreateChar.label[5], 255, 255, 255)
			guiLabelSetColor(GUICreateChar.label[6], 255, 255, 255)
			--guiLabelSetColor(GUICreateChar.label[7], 255, 255, 255)
			--guiLabelSetColor(GUICreateChar.label[8], 255, 255, 255)
		elseif id == 3 then
			guiSetProperty(GUICreateChar.staticimage[5], "ImageColours", "tl:8B000000 tr:8B000000 bl:8B000000 br:8B000000")
			guiLabelSetColor(GUICreateChar.label[7], 255, 255, 255)
			guiLabelSetColor(GUICreateChar.label[8], 255, 255, 255)
			--guiLabelSetColor(GUICreateChar.label[9], 255, 255, 255)
			--guiLabelSetColor(GUICreateChar.label[12], 255, 255, 255)
		elseif id == 4 then
			guiSetProperty(GUICreateChar.staticimage[6], "ImageColours", "tl:8B000000 tr:8B000000 bl:8B000000 br:8B000000")
			guiLabelSetColor(GUICreateChar.label[9], 255, 255, 255)
			guiLabelSetColor(GUICreateChar.label[10], 255, 255, 255)
		elseif id == 5 then
			guiSetProperty(GUICreateChar.staticimage[7], "ImageColours", "tl:8B000000 tr:8B000000 bl:8B000000 br:8B000000")
			guiLabelSetColor(GUICreateChar.label[11], 255, 255, 255)
			guiLabelSetColor(GUICreateChar.label[12], 255, 255, 255)
			--guiLabelSetColor(GUICreateChar.label[17], 255, 255, 255)
			--guiLabelSetColor(GUICreateChar.label[18], 255, 255, 255)
		elseif id == 6 then
			guiSetProperty(GUICreateChar.staticimage[8], "ImageColours", "tl:FF006400 tr:FF006400 bl:FF006400 br:FF006400")
		end
		
	end
end

local GUIRegHelp = {
    label = {},
    staticimage = {}
}

function CreateRegHelpGUI()
	GUIRegHelp.staticimage[1] = guiCreateStaticImage(0.73, 0.07, 0.16, 0.03, ":FiveRP/loginpanel/images/pane.png", true)
	guiSetProperty(GUIRegHelp.staticimage[1], "ImageColours", "tl:ED010000 tr:ED010000 bl:ED010000 br:ED010000")

	GUIRegHelp.label[1] = guiCreateLabel(0.01, 0.00, 0.97, 1.00, "Подсказка при регистрации 123321 символов", true, GUIRegHelp.staticimage[1])
	guiSetFont(GUIRegHelp.label[1], fmedium)
	guiLabelSetHorizontalAlign(GUIRegHelp.label[1], "left", true)
	VievRegHelp(false)
end

function SendRegHelp(text)
	local number = string.len(text)/2
	local str = number/41
	local str = math.floor(str)
	text = tostring(text)
	guiSetText ( GUIRegHelp.label[1], text)
	guiSetSize ( GUIRegHelp.staticimage[1], 0.16, str*0.03, true)
	VievRegHelp(true)
	playSFX("genrl", 53, 6, false)
end	
function VievRegHelp(bool)
	if bool then 
		VievRegHelp(false)
	end
	guiSetVisible (GUIRegHelp.label[1], bool)
	guiSetVisible (GUIRegHelp.staticimage[1], bool)
end

function OnClickCreateButtom()
	-----------------------------------------Изменение имени персонажа--------------------------------------------------------------------------------
	if source == GUICreateChar.staticimage[3] or source == GUICreateChar.label[3] or source == GUICreateChar.label[4] then
		VievChaneCharacterName(true);
		unbindKey( "arrow_d", "down", RegPressKey) 
		unbindKey( "arrow_u", "down", RegPressKey) 
		unbindKey( "arrow_l", "down", RegPressKey) 
		unbindKey( "arrow_r", "down", RegPressKey) 
		unbindKey( "enter", "down", RegPressKey) 
	-------------------------------------------------Принять------------------------------------------------------------------------------------------
	elseif source == GUICharacterName.staticimage[3] or source == GUICharacterName.staticimage[7] or source == GUICharacterName.staticimage[8] then
		guiLabelSetColor( GUICharacterName.label[2], 255, 255, 255 )
		if not string.match(guiGetText(GUICharacterName.edit[1]), "[A-Z]+[a-z]+ [A-Z]+[a-z]") then 
			SendRegHelp("Введите корректное РП имя персонажа! Например: Ivan Ivanov")
			return guiLabelSetColor( GUICharacterName.label[2], 255, 0, 0 )
			
		end
		guiSetText(GUICreateChar.label[4], guiGetText(GUICharacterName.edit[1]))
		VievChaneCharacterName(false);
		RegData["name"] = guiGetText(GUICharacterName.edit[1])
		bindKey( "arrow_d", "down", RegPressKey) 
		bindKey( "arrow_u", "down", RegPressKey) 
		bindKey( "arrow_l", "down", RegPressKey) 
		bindKey( "arrow_r", "down", RegPressKey) 
		bindKey( "enter", "down", RegPressKey) 
	-----------------------------------------------Отмена---------------------------------------------------------------------------------------------
	elseif source == GUICharacterName.staticimage[4] or source == GUICharacterName.staticimage[9] or source == GUICharacterName.staticimage[10] then
		VievChaneCharacterName(false);
		bindKey( "arrow_d", "down", RegPressKey) 
		bindKey( "arrow_u", "down", RegPressKey) 
		bindKey( "arrow_l", "down", RegPressKey) 
		bindKey( "arrow_r", "down", RegPressKey) 
		bindKey( "enter", "down", RegPressKey) 
		guiLabelSetColor( GUICharacterName.label[2], 255, 255, 255 )
	---------------------------------------------------------------Изменение пола----------------------------------------------------------------------
	elseif source == GUICreateChar.label[6] or source == GUICreateChar.staticimage[4] then
		if RegData["sex"] == 1 then
			RegData["sex"] = 2;
		else
			RegData["sex"] = 1;
		end	
		guiSetText(GUICreateChar.label[6], sex[RegData["sex"]])
	elseif source == GUICreateChar.label[8] or source == GUICreateChar.staticimage[5] then
		if RegData["resident"] == 1 then
			RegData["resident"] = 2;
		else
			RegData["resident"] = 1;
		end	
		guiSetText(GUICreateChar.label[8], resident[RegData["resident"]])
	end
	
end
addEventHandler("onClientGUIClick", root, OnClickCreateButtom)
function RegPressKey(key, state)
	if key == "arrow_d" then
		PaintLine(SelectedCLine, false)
		if SelectedCLine == 6 then SelectedCLine = 1
		else
			SelectedCLine = SelectedCLine+1
		end	
		PaintLine(SelectedCLine)
	elseif key == "arrow_u" then
		PaintLine(SelectedCLine, false)
		if SelectedCLine == 1 then SelectedCLine = 6
		else
			SelectedCLine = SelectedCLine-1
		end	
		PaintLine(SelectedCLine)
	elseif key == "arrow_l" or key == "enter" then
		playSFX("genrl", 53, 6, false)
		if SelectedCLine == 1 then
			VievChaneCharacterName(true);
			unbindKey("arrow_d", "down", RegPressKey) 
			unbindKey("arrow_u", "down", RegPressKey) 
			unbindKey("arrow_l", "down", RegPressKey) 
			unbindKey("arrow_r", "down", RegPressKey)
			unbindKey("enter", "down", RegPressKey) 
		elseif SelectedCLine == 2 then
			RegData["skin"] = 1
			if RegData["sex"] == 1 then
				RegData["sex"] = 2;
				guiSetText(GUICreateChar.label[12], "ID:"..regFeMaleSkins[RegData["skin"]].."")	
				setElementModel (localPlayer, regFeMaleSkins[RegData["skin"]])
			else
				RegData["sex"] = 1;
				guiSetText(GUICreateChar.label[12], "ID:"..regMaleSkins[RegData["skin"]].."")
				setElementModel (localPlayer, regMaleSkins[RegData["skin"]])
			end	
			guiSetText(GUICreateChar.label[6], sex[RegData["sex"]])
		elseif SelectedCLine == 3 then
			if RegData["resident"] == 1 then
				RegData["resident"] = 2;
			else
				RegData["resident"] = 1;
			end	
			guiSetText(GUICreateChar.label[8], resident[RegData["resident"]])
			
		elseif SelectedCLine == 5 then
			if RegData["sex"] == 1 then 
				if RegData["skin"] == 1 then 
					RegData["skin"] = #regMaleSkins
				else 
					RegData["skin"] = RegData["skin"]-1 
				end
				guiSetText(GUICreateChar.label[12], "ID:"..regMaleSkins[RegData["skin"]].."")
				setElementModel (localPlayer, regMaleSkins[RegData["skin"]])
			elseif RegData["sex"] == 2 then
				if RegData["skin"] == 1 then 
					RegData["skin"] = #regFeMaleSkins
				else 
					RegData["skin"] = RegData["skin"]-1 
				end
				guiSetText(GUICreateChar.label[12], "ID:"..regFeMaleSkins[RegData["skin"]].."")
				setElementModel (localPlayer, regFeMaleSkins[RegData["skin"]])
			end
			
		elseif SelectedCLine == 6 then
			if RegData["name"] ~= nil then
				triggerServerEvent("LocalCheckName", localPlayer, localPlayer, RegData["name"])
			end	
		end		
	--Влево
	elseif key == "arrow_r" then
		playSFX("genrl", 53, 6, false)
		if SelectedCLine == 1 then
			VievChaneCharacterName(true);
			unbindKey( "arrow_d", "down", RegPressKey) 
			unbindKey( "arrow_u", "down", RegPressKey) 
			unbindKey( "arrow_l", "down", RegPressKey) 
			unbindKey( "arrow_r", "down", RegPressKey)
			unbindKey( "enter", "down", RegPressKey) 
		elseif SelectedCLine == 2 then
			RegData["skin"] = 1
			if RegData["sex"] == 1 then
				RegData["sex"] = 2;
				guiSetText(GUICreateChar.label[12], "ID:"..regFeMaleSkins[RegData["skin"]].."")	
				setElementModel (localPlayer, regFeMaleSkins[RegData["skin"]])
			else
				RegData["sex"] = 1;
				guiSetText(GUICreateChar.label[12], "ID:"..regMaleSkins[RegData["skin"]].."")
				setElementModel (localPlayer, regMaleSkins[RegData["skin"]])
			end	
			guiSetText(GUICreateChar.label[6], sex[RegData["sex"]])
		elseif SelectedCLine == 3 then
			if RegData["resident"] == 1 then
				RegData["resident"] = 2;
			else
				RegData["resident"] = 1;
			end	
			guiSetText(GUICreateChar.label[8], resident[RegData["resident"]])
		elseif SelectedCLine == 5 then
			if RegData["sex"] == 1 then 
				if RegData["skin"] ==  #regMaleSkins then 
					RegData["skin"] = 1
				else 
					RegData["skin"] = RegData["skin"]+1 
				end
				guiSetText(GUICreateChar.label[12], "ID:"..regMaleSkins[RegData["skin"]].."")
				setElementModel (localPlayer, regMaleSkins[RegData["skin"]])
			elseif RegData["sex"] == 2 then 
				if RegData["skin"] ==  #regFeMaleSkins then 
					RegData["skin"] = 1
				else 
					RegData["skin"] = RegData["skin"]+1 
				end
				guiSetText(GUICreateChar.label[12], "ID:"..regFeMaleSkins[RegData["skin"]].."")	
				setElementModel (localPlayer, regFeMaleSkins[RegData["skin"]])
			end
		end		
	--Вправо
	end
end
--[[
function CreateAcceptCharacterGUI()
	GUIAcceptCharacter.staticimage[1] = guiCreateStaticImage(0.38, 0.37, 0.25, 0.22, ":FiveRP/loginpanel/images/top.png", true)
	guiSetProperty(GUIAcceptCharacter.staticimage[1], "ImageColours", "tl:D1010000 tr:D1010000 bl:D1010000 br:D1010000")

	GUIAcceptCharacter.staticimage[2] = guiCreateStaticImage(0.00, 0.00, 1.00, 0.12, ":FiveRP/loginpanel/images/pane.png", true, GUIAcceptCharacter.staticimage[1])
	guiSetProperty(GUIAcceptCharacter.staticimage[2], "ImageColours", "tl:FE0C4B00 tr:FE0C4B00 bl:FE0C4B00 br:FE0C4B00")

	GUIAcceptCharacter.label[1] = guiCreateLabel(0.00, 0.03, 0.99, 0.82, "Готово?", true, GUIAcceptCharacter.staticimage[2])
	guiSetFont(GUIAcceptCharacter.label[1], Five26)
	guiLabelSetHorizontalAlign(GUIAcceptCharacter.label[1], "center", false)
	guiLabelSetVerticalAlign(GUIAcceptCharacter.label[1], "center")
	
	GUIAcceptCharacter.label[2] = guiCreateLabel(0.00, 0.16, 1.00, 0.57, "Имя персонажа: Wen Fox\nПол: Мужской\nМесто жительства: Местный\nДата рождения: 01.01.1990\nВнешность: 152\n\nПерсонаж будет привязан к аккаунту: DrDeft", true, GUIAcceptCharacter.staticimage[1])
	guiLabelSetHorizontalAlign(GUIAcceptCharacter.label[2], "center", false)

	-------------------------------------------------------------------------------------------------------------------------------------------------------------
	GUIAcceptCharacter.staticimage[3] = guiCreateStaticImage(0.07, 0.76, 0.37, 0.15, ":FiveRP/loginpanel/images/bottom.png", true, GUIAcceptCharacter.staticimage[1])
	GUIAcceptCharacter.staticimage[5] = guiCreateStaticImage(0.03, 0.76, 0.08, 0.15, ":FiveRP/loginpanel/images/round.png", true, GUIAcceptCharacter.staticimage[1])
	GUIAcceptCharacter.staticimage[6] = guiCreateStaticImage(0.405, 0.76, 0.08, 0.15, ":FiveRP/loginpanel/images/round.png", true, GUIAcceptCharacter.staticimage[1])
	
	GUIAcceptCharacter.label[3] = guiCreateLabel(0.02, 0.00, 0.93, 1.00, "Принять", true, GUIAcceptCharacter.staticimage[3])
	guiLabelSetColor(GUIAcceptCharacter.label[3], 22, 26, 31)
	guiLabelSetHorizontalAlign(GUIAcceptCharacter.label[3], "center", false)
	guiLabelSetVerticalAlign(GUIAcceptCharacter.label[3], "center")
	guiSetEnabled(GUIAcceptCharacter.label[3], false)
	-----------------------------------------------------------------------------------------------------------------------------------------------------------
	
	GUIAcceptCharacter.staticimage[4] = guiCreateStaticImage(0.55, 0.76, 0.37, 0.15, ":FiveRP/loginpanel/images/bottom.png", true, GUIAcceptCharacter.staticimage[1])
	GUIAcceptCharacter.staticimage[7] = guiCreateStaticImage(0.51, 0.76, 0.08, 0.15, ":FiveRP/loginpanel/images/round.png", true, GUIAcceptCharacter.staticimage[1])
	GUIAcceptCharacter.staticimage[8] = guiCreateStaticImage(0.88, 0.76, 0.08, 0.15, ":FiveRP/loginpanel/images/round.png", true, GUIAcceptCharacter.staticimage[1])
	
	GUIAcceptCharacter.label[4] = guiCreateLabel(0.02, 0.00, 0.93, 1.00, "Отмена", true, GUIAcceptCharacter.staticimage[4])
	guiLabelSetColor(GUIAcceptCharacter.label[4], 22, 26, 31)
	guiLabelSetHorizontalAlign(GUIAcceptCharacter.label[4], "center", false)
	guiLabelSetVerticalAlign(GUIAcceptCharacter.label[4], "center")
	guiSetEnabled(GUIAcceptCharacter.label[4], false)
	-----------------------------------------------------------------------------------------------------------------------------------------------------------
	
	for i = 3, 8 do
			guiSetProperty(GUIAcceptCharacter.staticimage[i], "ImageColours", "tl:FF008000 tr:FF008000 bl:FF008000 br:FF008000")
		end	
	end
	for i = 2, 4 do
		guiSetFont(GUIAcceptCharacter.label[i], fmedium)
	end
end]]

function AcceptName(bool)
	if bool then
		if RegData["sex"] == 1 then
			RegData["skin"] = regMaleSkins[RegData["skin"]]
		else
			RegData["skin"] = regFeMaleSkins[RegData["skin"]]
		end
		triggerServerEvent("CreateAccount", localPlayer, localPlayer, RegData["login"], RegData["pass"], RegData["mail"], RegData["invite"], RegData["name"], RegData["sex"], RegData["resident"], RegData["age"], RegData["skin"])
		VievCharacterGUI(false);
		unbindKey( "arrow_d", "down", RegPressKey) 
		unbindKey( "arrow_u", "down", RegPressKey) 
		unbindKey( "arrow_l", "down", RegPressKey) 
		unbindKey( "arrow_r", "down", RegPressKey)
		unbindKey( "enter", "down", RegPressKey) 
		showCursor(false)
		removeEventHandler("onClientMouseEnter", root, MoveSelectGUICharter) 
		removeEventHandler("onClientMouseLeave", root, MoveLeaveGUICharter) 
		removeEventHandler("onClientGUIClick", root, OnClickCreateButtom)  
		SendRegHelp(" ", false)
	else
		VievChaneCharacterName(true)
		SendRegHelp("Данное имя уже занято!")
	end
end
addEvent("AcceptName", true)
addEventHandler("AcceptName", root, AcceptName)