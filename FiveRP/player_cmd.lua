function cmd_pay(player, cmd, pid, amount)
	local playerid = getPlayerID(player)
	amount = tonumber(amount)
	pid = tonumber(pid)
	if amount == nil or amount < 1 or pid == nil then return outputChatBox("#AAAAAAПиши: /pay [ID][Сумма]", player, 0, 191, 255) end
	if(iSPlayerOK(pid)) then
		if(pid == playerid) then return SendPlayerNotice(player, "Вы указали свой ID!")	end
		if(Players[playerid]["money"] < amount) then return SendPlayerNotice(player, "У вас нет столько средств!")
		else
			GivePlayerMoneyAC(player, -amount)
			GivePlayerMoneyAC(Players[pid]["player"], amount)
			if amount > 15000 then SendToAdmin(player, "#FF644B[ADMWARN]Игрок "..Players[playerid]["name"].." передал игроку "..Players[pid]["name"].." $"..amount.."") end
			OnPlayerRPChat(player, "достает пачку денег и передает ее "..Players[pid]["name"].."", 0)
			outputChatBox("#AAAAAAВы передали $"..amount.." игроку "..Players[pid]["name"].."", player, 0, 191, 255)
			outputChatBox("#AAAAAAИгрок "..Players[playerid]["name"].." передал вам $"..amount.."", Players[pid]["player"], 0, 191, 255)
		end	
	else SendPlayerNotice(player, "Игрок с данным ID не найден или не авторизовался!")	
	end
end
addCommandHandler("pay", cmd_pay)   

--[[
function cmd_me(player, cmd, text)
	if text == nil then  return outputChatBox("Пиши: /me [действие]", player) end
	OnPlayerRPChat(player, ""..text.."", 2)
end
addCommandHandler("me", cmd_me)]]
function cmd_do(player, cmd, ...)
	local text = table.concat({...}, " ")
	text = string.gsub((""..text..""), '#%x%x%x%x%x%x', '')
	if ... == nil then  return outputChatBox("#AAAAAAПиши: /do [действие]", player) end
	OnPlayerRPChat(player, ""..text.."", 1)
end
addCommandHandler("do", cmd_do)   

function cmd_try(player, cmd, ...)
	local text = table.concat({...}, " ")
	text = string.gsub((""..text..""), '#%x%x%x%x%x%x', '')
	if ... == nil then  return outputChatBox("#AAAAAAПиши: /try [действие]", player) end
	OnPlayerRPChat(player, ""..text.."", 2)
end
addCommandHandler("try", cmd_try)   

function cmd_b(player, cmd, ...)
	local text = table.concat({...}, " ")
	text = string.gsub((""..text..""), '#%x%x%x%x%x%x', '')
	if ... == nil then  return outputChatBox("#AAAAAAПиши: /b [текст]", player) end
	OnPlayerRPChat(player, ""..text.."", 3)
end
addCommandHandler("b", cmd_b) 

function cmd_en(player, cmd)
	if not getPedOccupiedVehicle(player) then return SendPlayerNotice(player,"Вы должны быть в автомобиле!") end
	if getPedOccupiedVehicleSeat(player) ~= 0 then return SendPlayerNotice(player,"Вы должны быть за рулем!") end
	local playerid = getPlayerID(player)
	local vid = getElementData(getPedOccupiedVehicle(player), "vehicle.id")
	if Vehicles[vid]["type"] == 0 and Vehicles[vid]["owner"] ~= Players[playerid]["mysqlid"] then return SendPlayerNotice(player, "У вас нет ключей от этого авто!")
	elseif Vehicles[vid]["type"] == 2 and Vehicles[vid]["owner"] ~= Players[playerid]["frac"] then
		if Vehicles[vid]["owner"] == 8 and isTimer(Players[playerid]["tparam"]["DrivingSchoolExamTimer"]) == false then
			return SendPlayerNotice(player, "У вас нет ключей от этого авто!") 
		end
	end	
	if Vehicles[vid]["engine"] == 0 then
		--StartEngine(getPedOccupiedVehicle(player), true)
		OnPlayerRPChat(player, "поворачивает ключ зажигания и пытается запустить двигатель.", 0)
		exports.speedometer:updateCarEngine(player, 4)
		setTimer(TimerStartEngine, 3000, 1, player, getPedOccupiedVehicle(player))
	else
		OnPlayerRPChat(player, "повернул(а) ключ зажигания и загрушил(а) двигатель.", 0)
		StartEngine(getPedOccupiedVehicle(player), false)
	
	end
end
addCommandHandler("en", cmd_en) 

function cmd_belt(player, cmd)
	if not getPedOccupiedVehicle(player) then return SendPlayerNotice(player,"Вы должны быть в автомобиле!") end
	local playerid = getPlayerID(player)
	if Players[playerid]["tparam"]["belt"] == nil or Players[playerid]["tparam"]["belt"] == 0 then
		Players[playerid]["tparam"]["belt"] = 1
		OnPlayerRPChat(player, "пристегивает ремень безопасности.", 0)
	else
		Players[playerid]["tparam"]["belt"] = 0
		OnPlayerRPChat(player, "отстегивает ремень безопасности.", 0)
	end
	exports.speedometer:updateCarBelt(player, Players[playerid]["tparam"]["belt"])
end
addCommandHandler("belt", cmd_belt) 

function cmd_carlight(player, cmd)
	if not getPedOccupiedVehicle(player) then return SendPlayerNotice(player,"Вы должны быть в автомобиле!") end
	if getPedOccupiedVehicleSeat(player) ~= 0 then return SendPlayerNotice(player,"Вы должны быть за рулем!") end
	local playerid = getPlayerID(player)
	local vid = getElementData(getPedOccupiedVehicle(player), "vehicle.id")
	if Vehicles[vid]["light"] ~= 2 then
		LightVehicle(Vehicles[vid]["vehicle"], 2)
	else
		LightVehicle(Vehicles[vid]["vehicle"], 1)
	end
end
addCommandHandler("carlight", cmd_carlight) 

function cmd_lock(player, cmd)
	local vid
	local playerid = getPlayerID(player)
	if not getPedOccupiedVehicle(player) then 
		vehicle = GetNearestVehicle(player)
		if not vehicle then return SendPlayerNotice(player, "Вы должны быть в автомобиле или рядом с ним!") end
		vid = getElementData(vehicle, "vehicle.id")
	else
		vid = getElementData(getPedOccupiedVehicle(player), "vehicle.id")
		vehicle = getPedOccupiedVehicle(player)
	end
	if Vehicles[vid]["type"] == 0 and Vehicles[vid]["owner"] ~= Players[playerid]["mysqlid"] then return SendPlayerNotice(player, "У вас нет ключей от этого авто!")
	elseif Vehicles[vid]["type"] == 2 and Vehicles[vid]["owner"] ~= Players[playerid]["frac"] then return SendPlayerNotice(player, "У вас нет ключей от этого авто!") end
	if Vehicles[vid]["lock"] == 1 then
		LockVehicle(vehicle, false)
		OnPlayerRPChat(player, "достает ключи и открывает двери авто.", 0)
	else 
		LockVehicle(vehicle, true) 
		OnPlayerRPChat(player, "достает ключи и закрывает двери авто.", 0)
	end	
end
addCommandHandler("lock", cmd_lock) 

function cmd_eject(player, cmd, id)
	local playerid = getPlayerID(player)
	local id = tonumber(id)
	if not id or id < 1 then return outputChatBox("#AAAAAAПиши: /eject [id]") end
	if not iSPlayerOK(id) then return SendPlayerNotice(player, "Игрок не авторизован!") end
	if getPedOccupiedVehicleSeat(player) ~= 0 or getPedOccupiedVehicle (player) ~= getPedOccupiedVehicle(Players[id]["player"]) then return SendPlayerNotice(player, "Вы должны быть за рулем автомобиля в котором находиться игрок!") end
	--OnPlayerRPChat(player, "выталкивает "..Players[id]["name"].." из автомобиля.", 0)
	if math.random (0 ,1) == 0 then
		local x,y,z = getElementPosition(Players[id]["player"])
		OnPlayerRPChat(player, "выталкивает "..Players[id]["name"].." из автомобиля#00FF00 (Успешно).", 0)
		removePedFromVehicle(Players[id]["player"])
		setElementPosition(Players[id]["player"], x,y,z+3)
	else
		OnPlayerRPChat(player, "выталкивает "..Players[id]["name"].." из автомобиля#FF0000 (Неудачно).", 0)
	end
end
addCommandHandler("eject", cmd_eject) 

function cmd_hidehud(player, cmd, visible)
	visible = tonumber(visible)
	if visible == 1 then setPlayerHudComponentVisible ( player, "all", true) ShowChat ( player, true )
	else
		setPlayerHudComponentVisible ( player, "all", false)
		ShowChat ( player, false )
	end	
end
addCommandHandler("hidehud", cmd_hidehud)

function cmd_mm(player, cmd)
	VievServerMenu(player)
end
addCommandHandler("mm", cmd_mm)

function cmd_im(player, cmd, id)
	local playerid = getPlayerID(player)
	if id ~= nil then
		id = tonumber(id)
		if not iSPlayerOK(id) then return SendPlayerNotice(player, "Игрок не авторизован!") end
		Players[playerid]["tparam"]["targetpid"] = id
	end
	if Players[playerid]["tparam"]["targetpid"] == nil or iSPlayerOK(Players[playerid]["tparam"]["targetpid"]) == false then
		return outputChatBox("Для использования выберите игрока ПКМ или пиши: /im [id]")
	end	
	OpenInteractionMenu(player)
end
addCommandHandler("im", cmd_im)

function cmd_sit(player,cmd, anim)
	anim = tonumber(anim)
	if not anim or anim < 1 or anim > 4 then return outputChatBox("Пиши: /sit [1-4]", player) end
	if anim == 1 then setPedAnimation (player,  "BEACH", "ParkSit_M_loop", -1, false)
	elseif anim == 2 then 
		local ped = player
		local desiredRelativePosition = Vector3(0, -0.06, -0.0)
		local matrix = player.matrix
		local newPosition = matrix:transformPosition(desiredRelativePosition)
		setElementPosition(ped, newPosition, false)
		setPedAnimation (player,  "JST_BUISNESS","girl_02", -1, false, false)
	elseif anim == 3 then setPedAnimation (player,  "INT_HOUSE","LOU_In", -1, false)
	elseif anim == 4 then
		local desiredRelativePosition = Vector3(0, -0.015, -0.0)
		local matrix = player.matrix
		local newPosition = matrix:transformPosition(desiredRelativePosition)
		setElementPosition(player, newPosition, false)
		setPedAnimation (player,  "PED","SEAT_down", -1, false, false, false)
	end
end
addCommandHandler("sit", cmd_sit)


function cmd_sa(player, cmd)
	setPedAnimation(player, false)
end
addCommandHandler("sa", cmd_sa)

function cmd_myid(player, cmd)
	local playerid = getPlayerID(player)
	outputChatBox("Ваш ID: "..playerid.."", player)
end
addCommandHandler ("myid", cmd_myid)


function cmd_showlic(player, cmd, pid)
	local playerid = getPlayerID(player)
	local lic = Players[playerid]["license"]
	local lictext = {"#ff0000Отсутсвует#ffffff", "#008000Имеется#ffffff"}
	if pid then
		pid = tonumber(pid)
		if not iSPlayerOK(pid) then return SendPlayerNotice(player, "Игрок не авторизован!") end
		if pid == playerid then 
			return outputChatBox("Пиши: /showlic, что бы посмотреть на свои лицензии", player)
		end
		ShowMessage(Players[pid]["player"], 0, "Лицензии "..Players[playerid]["name"].."", "Лицензия категории А: "..lictext[lic[1]+1].."\nЛицензия категории B: "..lictext[lic[2]+1].."\nЛицензия категории C: "..lictext[lic[3]+1].."\nЛицензия на авиатранспорт: "..lictext[lic[4]+1].."\nЛицензия на водный транспорт: "..lictext[lic[5]+1].."\nЛицензия на оружие: "..lictext[lic[6]+1].."", "Закрыть")
		OnPlayerRPChat(player, "показывает свои лицензии "..Players[pid]["name"].."", 0)
		
	else
		outputChatBox("Пиши: /showlic [ID], что бы показать свой список лицензий", player)
		ShowMessage(player, 0, "Лицензии "..Players[playerid]["name"].."", "Лицензия категории А: "..lictext[lic[1]+1].."\nЛицензия категории B: "..lictext[lic[2]+1].."\nЛицензия категории C: "..lictext[lic[3]+1].."\nЛицензия на авиатранспорт: "..lictext[lic[4]+1].."\nЛицензия на водный транспорт: "..lictext[lic[5]+1].."\nЛицензия на оружие: "..lictext[lic[6]+1].."", "Закрыть")
		
		OnPlayerRPChat(player, "смотрит на свои лицензии", 0)
	end	
end
addCommandHandler("showlic", cmd_showlic)

function cmd_stats(player, cmd)
	local pid = getPlayerID(player)
	local FracName = "Нет"
	local FracRank = "Нет"
	local FracPost = "Нет"
	if Players[pid].frac ~= 0 then
		FracName = Fractions[Players[pid].frac]["name"]
		FracRank = Fractions[Players[pid].frac]["ranks"][Players[pid].rank]
		FracPost = Fractions[Players[pid].frac]["posts"][Players[pid].post]
	end	
	ShowMessage(player, 0, "Статистика персонажа", "ID аккаунта: "..Players[pid].mysqlid.."\nИмя персонажа: "..Players[pid].name.."\nДата регистрации: 06.09.2017\nСтатус: Игрок\nУровень: "..Players[pid].level.."\nОпыт: "..Players[pid].exp.."\nНаличные: $"..Players[pid].money.."\nУровень розыска: "..Players[pid].wanted.."\nНомер телефона: 777\n\nФракция: "..FracName.."\nРанг: "..FracRank.."\nПост: "..FracPost.."\n\nДом: Нет\nТранспорт: Нет", "Закрыть")
	
end
addCommandHandler("stats", cmd_stats)

function cmd_crack(player, cmd, anim)
	--setPedAnimation(ped "crack", "crckdeth1")
	setPedAnimation (player, "CRACK", anim, -1, false)
end
addCommandHandler("crack", cmd_crack)