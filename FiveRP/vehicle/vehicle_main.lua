--[[
	Типы авто:
	0 - Игрока
	1 - Продажа
	2 - Гос
	3 - Рабочие
	4 - Аренда
]]

local StartEngineCar = {}
local UpdateVehicleTimer 

function CreateVehicleTable(id)
	Vehicles[id] = {id, vehicle, mid, model, color1 = {}, color2 = {}, pos = {}, int, world, fuel, owner, lock, engine, health, light, paintjob, price, type, trunk = {}, panels = {}, tune = {}}
end
function LoadVehicle(query, playerid)
	
	local resultTable, num, err = dbPoll (query, 0) 
	local fraccars = 0
	if resultTable then 
		for i, row in ipairs(resultTable) do 
			local vid = ReturnFreeVehID()
			Vehicles[vid]["id"] = vid
			Vehicles[vid]["mid"] = row['id']
			Vehicles[vid]["model"] = row['model']
			--Vehicles[vid]["color"][1] = row['color_one']
			Vehicles[vid]["pos"][1] = row['pos_x']
			Vehicles[vid]["pos"][2] = row['pos_y']
			Vehicles[vid]["pos"][3] = row['pos_z']
			Vehicles[vid]["pos"][4] = row['angle']
			Vehicles[vid]["vehicle"] = createVehicle(Vehicles[vid]["model"], Vehicles[vid]["pos"][1], Vehicles[vid]["pos"][2], Vehicles[vid]["pos"][3])
			setElementData(Vehicles[vid]["vehicle"], "vehicle.id", vid)
			setElementRotation (Vehicles[vid]["vehicle"], 0, 0, Vehicles[vid]["pos"][4], "default") 
			fraccars = fraccars+1
			Vehicles[vid]["fuel"] = row['fuel']
			Vehicles[vid]["owner"] = tonumber(row['owner'])
			Vehicles[vid]["paintjob"] = row['paintjob']
			Vehicles[vid]["type"] = tonumber(row['type'])
			Vehicles[vid]["price"] = row['price']
			Vehicles[vid]["health"] = tonumber(row['health'])
			setElementHealth(Vehicles[vid]["vehicle"], Vehicles[vid]["health"])
			-----------------------------------
			local titem = row['item']
			local tamount = row['amount']
			local trare = row['rare']
			local item = explode("|", titem)
			local amount = explode("|", tamount)
			local rare = explode("|", trare)
			for z in ipairs(item) do
				Vehicles[vid]["trunk"][z] = {}
				Vehicles[vid]["trunk"][z]["item"] = item
				Vehicles[vid]["trunk"][z]["amount"] = amount
				Vehicles[vid]["trunk"][z]["rare"] = rare
			end
			-----------------------------------
			Vehicles[vid]["lock"] = tonumber(row['lock'])
			Vehicles[vid]["engine"] = row['engine']	
			Vehicles[vid]["light"] = tonumber(row['light'])		
			local color1 = tostring(row['color_one'])
			local color2 = tostring(row['color_two'])
			local panel = row['panels']
			local tune = row['tune']
			local tunes = explode("|", tune)
			local panels = explode("|", panel)
			local colors1 = explode("|", color1)
			local colors2 = explode("|", color2)
			for c in ipairs(colors1) do
				Vehicles[vid]["color1"][c] = colors1[c]
				Vehicles[vid]["color2"][c] = colors2[c]
			end
			---------------------------Состояние авто---------------------------------------------
			for c = 1, 7 do
				Vehicles[vid]["panels"][c] = tonumber(panels[c])
				setVehiclePanelState(Vehicles[vid]["vehicle"], c-1, Vehicles[vid]["panels"][c])
			end
			
			for c = 8, 13 do
				Vehicles[vid]["panels"][c] = tonumber(panels[c])
				setVehicleDoorState(Vehicles[vid]["vehicle"], c-8, Vehicles[vid]["panels"][c])
			end
			------------------------------Тюнинг из GTA SA------------------------------------------
			for c = 1, 17 do
				Vehicles[vid]["tune"][c] = tonumber(tunes[c])
				if tunes[c] ~= 0 then
					addVehicleUpgrade(Vehicles[vid]["vehicle"], Vehicles[vid]["tune"][c])
				end	
			end
			----------------------------------------------------------------------------------------
			setVehicleColor ( Vehicles[vid]["vehicle"], Vehicles[vid]["color1"][1], Vehicles[vid]["color1"][2], Vehicles[vid]["color1"][3], Vehicles[vid]["color2"][1], Vehicles[vid]["color2"][2], Vehicles[vid]["color2"][3] )          
			if Vehicles[vid]["lock"] == 1 then setVehicleLocked (Vehicles[vid]["vehicle"], true)
			elseif Vehicles[vid]["lock"] == 0 then setVehicleLocked (Vehicles[vid]["vehicle"], false ) end
			if Vehicles[vid]["engine"] == 1 then setVehicleEngineState (Vehicles[vid]["vehicle"], true)
			elseif Vehicles[vid]["engine"] then setVehicleEngineState (Vehicles[vid]["vehicle"], false ) end
			setVehicleOverrideLights ( Vehicles[vid]["vehicle"], Vehicles[vid]["light"]) 
			
			setVehiclePlateText(Vehicles[vid]["vehicle"], "SA"..vid.."")
			
			----------------------------------Устанавливаем коорды респавна для машин автошколы----------------------------
			if Vehicles[vid]["type"] == 2 and Vehicles[vid]["owner"] == 8 then
				setVehicleRespawnPosition(Vehicles[vid]["vehicle"], Vehicles[vid]["pos"][1], Vehicles[vid]["pos"][2], Vehicles[vid]["pos"][3], 0, 0, Vehicles[vid]["pos"][4])
			end
			----------------------------------------------------------------------------------------------------------------
			LoadedCars = LoadedCars+1   
			
			if playerid ~= nil then
				table.insert(Players[playerid]["vehicles"], vid)
			end
			
		end
	end
	if playerid ~= nil then
		return true 
	end
	outputDebugString("Загружено "..fraccars.." авто!")
	dbFree(query)
end



function SaveFractionsVehicle()
	for i in ipairs(Vehicles) do
		if Vehicles[i]["type"] == 2 then
				SaveVehicle(i)
		end	
	end
end

function SaveVehicle(i)
	local r1,r2,r3, r4, r5, r6 = getVehicleColor (Vehicles[i]["vehicle"], true)
	if Vehicles[i]["type"] == 0 then
		Vehicles[i]["pos"][1], Vehicles[i]["pos"][2], Vehicles[i]["pos"][3] = getElementPosition(Vehicles[i]["vehicle"])
		_, _, Vehicles[i]["pos"][4] = getElementRotation(Vehicles[i]["vehicle"], "default")  
	end	
	
	Vehicles[i]["health"] = getElementHealth(Vehicles[i]["vehicle"])
	
	local panels = ""
	local tune = ""
	------------------------------------------------------------------------------------------
	for c = 1, 7 do
		Vehicles[i]["panels"][c] = getVehiclePanelState(Vehicles[i]["vehicle"], c-1)
		if c == 1 then
			panels = ""..Vehicles[i]["panels"][c]..""
		else
			panels = ""..panels.."|"..Vehicles[i]["panels"][c]..""
		end	
	end
	------------------------------------------------------------------------------------------
	for c = 8, 13 do
		Vehicles[i]["panels"][c] = getVehicleDoorState(Vehicles[i]["vehicle"], c-8)
		panels = ""..panels.."|"..Vehicles[i]["panels"][c]..""
	end
	------------------------------------------------------------------------------------------
	for c = 1, 17 do
		if c == 1 then
			tune = ""..Vehicles[i]["tune"][c]..""
		else
			tune = ""..tune.."|"..Vehicles[i]["tune"][c]..""
		end	
	end
	------------------------------------------------------------------------------------------
	dbExec(SQL, "UPDATE `vehicle` SET \
	`owner` = '"..Vehicles[i]["owner"].."', \
	`model` = '"..Vehicles[i]["model"].."', \
	`pos_x` = '"..Vehicles[i]["pos"][1].."', \
	`pos_y` = '"..Vehicles[i]["pos"][2].."', \
	`pos_z` = '"..Vehicles[i]["pos"][3].."', \
	`angle` = '"..Vehicles[i]["pos"][4].."', \
	`color_one` = '"..Vehicles[i]["color1"][1].."|"..Vehicles[i]["color1"][2].."|"..Vehicles[i]["color1"][3].."', \
	`color_two` = '"..Vehicles[i]["color2"][1].."|"..Vehicles[i]["color2"][2].."|"..Vehicles[i]["color2"][3].."', \
	`fuel` = '"..Vehicles[i]["fuel"].."', \
	`paintjob` = '"..Vehicles[i]["paintjob"].."', \
	`type` = '"..Vehicles[i]["type"].."', \
	`price` = '"..Vehicles[i]["price"].."', \
	`lock` = '"..Vehicles[i]["lock"].."', \
	`engine` = '"..Vehicles[i]["engine"].."', \
	`light` = '"..Vehicles[i]["light"].."', \
	`health` = '"..Vehicles[i]["health"].."', \
	`panels` = '"..panels.."', \
	`tune` = '"..tune.."' \
	WHERE `id` = '"..Vehicles[i]["mid"].."' LIMIT 1")
end

function ReturnFreeVehID()
	for i = 1, LoadedCars do
		if Vehicles[i] == nil then CreateVehicleTable(i) end
		if Vehicles[i]["vehicle"] == nil then return i end
	end	
end

function LockVehicle(vehicle, bool)
	vid = getElementData(vehicle, "vehicle.id")
	setVehicleLocked (vehicle, bool)
	if bool then 
		Vehicles[vid]["lock"] = 1 
	else 
		Vehicles[vid]["lock"] = 0 
	end
end

function LightVehicle(vehicle, light)
	vid = getElementData(vehicle, "vehicle.id")
	Vehicles[vid]["light"] = light
	setVehicleOverrideLights ( vehicle, Vehicles[vid]["light"])
	local driver = getVehicleOccupant(vehicle)
	if driver then
		if light ~= 2 then
			exports.speedometer:updateCarLight(driver, 2)
		elseif light == 2 then
			exports.speedometer:updateCarLight(driver, 1)
		end
	end	
end

function StartEngine(vehicle, bool)
	vid = getElementData(vehicle, "vehicle.id") 
	setVehicleEngineState (vehicle, bool)
	if bool then
		local cintable = false
		for i in ipairs(StartEngineCar) do
			if vid == StartEngineCar[i] then
				cintable = true
				break
			end	
		end	
		if cintable == false then
			table.insert(StartEngineCar, vid)
		end
		Vehicles[vid]["engine"] = 1	
	else 
		Vehicles[vid]["engine"] = 0
		for i in ipairs(StartEngineCar) do
			if vid == StartEngineCar[i] then
				table.remove(StartEngineCar, i)
				break
			end
		end
	end
	local driver = getVehicleOccupant(vehicle)
	if driver then
		exports.speedometer:updateCarEngine(driver, Vehicles[vid]["engine"])
	end
end

function TimerStartEngine(player, vehicle)
	vid = getElementData(vehicle, "vehicle.id") 
	if getElementHealth (vehicle) < 301 or Vehicles[vid]["fuel"] <= 0 then
		exports.speedometer:updateCarEngine(player, 3)
		return OnPlayerRPChat(player, "Двигатель заглох.", 1)
	end
	OnPlayerRPChat(player, "Двигатель запущен.", 1)
	StartEngine(vehicle, true)
end

function GetNearestVehicle(player, size)
	if size == nil then size = 5 end
	local px,py,pz = getElementPosition(player)
	local Sphere = createColSphere(px,py,pz, size)
	local _Vehicles1 = getElementsWithinColShape(Sphere, "vehicle")
	destroyElement(Sphere)
	local distance1, distance2 = 0, 0
	local count = #_Vehicles1
	local veh
	if count == nil or count == 0 then return false end
	for index,Vehicle in ipairs( _Vehicles1 ) do
		if count == 1 then 
			return Vehicle
		elseif count > 1 then
			distance2 = getDistanceBetweenPoints3D ( px, py, pz, getElementPosition(Vehicle))
			if distance1 == 0 or distance1 > distance2 then distance1 = distance2 veh = Vehicle end
		end
	end
	return veh
end

function OnPlayerEnterVehicle(vehicle, seat, jacked)
	local vid = getElementData(vehicle, "vehicle.id")
	if not vid then return 1 end
	local playerid = getPlayerID(source)
	Players[playerid]["tparam"]["belt"] = 0
	if seat == 0 then -- место водителя
		setVehicleOverrideLights ( vehicle, Vehicles[vid]["light"]) 
		local light
		if Vehicles[vid]["light"] == 2 then light = 1
		elseif Vehicles[vid]["light"] == 1 then light = 2 end
		if Vehicles[vid]["engine"] == 0 then
			StartEngine(vehicle, false)
		end
		exports.speedometer:vievSpeedometer(source, true, Vehicles[vid]["fuel"], Vehicles[vid]["engine"], 3, light)
		bindKey(source, "lctrl", "down", "en")
		bindKey(source, "2", "down", "carlight")
		bindKey(source, "b", "down", "belt")
		outputChatBox("Запуск двигателя - #0dc426\"L.CTRL\" #ffffff/ Габаритные огни -  #0dc426\"2\"#ffffff / Пристегнуть ремень - #0dc426\"B\"", source)
	end
	----------------Отстегиваем игрока от /drag, если он сел в авто---------------
	if Players[playerid]["drag"] ~= nil then
		UnDragPlayer(playerid)
	end 
end
addEventHandler ( "onPlayerVehicleEnter", getRootElement(), OnPlayerEnterVehicle )

function OnPlayerExitVehicle (vehicle, seat, jacked)
	local playerid = getPlayerID(source)
	if seat == 0 then
		exports.speedometer:vievSpeedometer(source, false)
		unbindKey(source, "lctrl", "down", "en")
		unbindKey(source, "2", "down", "carlight")
		unbindKey(source, "b", "down", "belt")
	end
	if Players[playerid]["tparam"]["belt"] == 1 then
		OnPlayerRPChat(source, "отстегивает ремень безопасности и выходит из автомобиля.", 0)
	end	
end
addEventHandler ( "onPlayerVehicleExit", getRootElement(), OnPlayerExitVehicle)

function getElementSpeed(theElement, unit)
	if unit == nil then unit = "km/h" end
    -- Check arguments for errors
    assert(isElement(theElement), "Bad argument 1 @ getElementSpeed (element expected, got " .. type(theElement) .. ")")
    assert(getElementType(theElement) == "player" or getElementType(theElement) == "ped" or getElementType(theElement) == "object" or getElementType(theElement) == "vehicle", "Invalid element type @ getElementSpeed (player/ped/object/vehicle expected, got " .. getElementType(theElement) .. ")")
    assert((unit == nil or type(unit) == "string" or type(unit) == "number") and (unit == nil or (tonumber(unit) and (tonumber(unit) == 0 or tonumber(unit) == 1 or tonumber(unit) == 2)) or unit == "m/s" or unit == "km/h" or unit == "mph"), "Bad argument 2 @ getElementSpeed (invalid speed unit)")
    -- Default to m/s if no unit specified and 'ignore' argument type if the string contains a number
    unit = unit == nil and 0 or ((not tonumber(unit)) and unit or tonumber(unit))
    -- Setup our multiplier to convert the velocity to the specified unit
    local mult = (unit == 0 or unit == "m/s") and 50 or ((unit == 1 or unit == "km/h") and 210 or 111.84681456)
    -- Return the speed by calculating the length of the velocity vector, after converting the velocity to the specified unit
    return (Vector3(getElementVelocity(theElement)) * mult).length
end

function UpdateVehicleFuel()
	for i in ipairs(StartEngineCar) do
		vid = StartEngineCar[i]
		local driver = getVehicleOccupant(Vehicles[vid]["vehicle"])
		if Vehicles[vid]["fuel"] > 0 then
			Vehicles[vid]["fuel"] = Vehicles[vid]["fuel"] - 0.02
		else
			--Бензина нет!
			StartEngine(Vehicles[vid]["vehicle"], false)
			if driver then exports.speedometer:updateCarEngine(driver, 3) end
		end
		if driver then
			exports.speedometer:updateCarFuel(driver, Vehicles[vid]["fuel"])
		end
	end
end
UpdateVehicleTimer = setTimer(UpdateVehicleFuel, 1000, 0)

function openTrunk(player, vehicleid)
	if Vehicles[vehicleid]["lock"] == 1 then return SendPlayerNotice(player, "Багажник автомобиля закрыт!") end
	OpenAltInventory(player, 2, Vehicles[vid]["trunk"])
end

function fixCar(vehicle)
	if getElementType(vehicle) ~= "vehicle" then return false end
	local vid = getElementData(vehicle, "vehicle.id")
	fixVehicle(vehicle)
	if vid then
		Vehicles[vid]["health"] = 1000
		for c = 1, 7 do
			Vehicles[vid]["panels"][c] = 0
			setVehiclePanelState(Vehicles[vid]["vehicle"], c-1, Vehicles[vid]["panels"][c])
		end
		for c = 8, 13 do
			Vehicles[vid]["panels"][c] = 0
			setVehicleDoorState(Vehicles[vid]["vehicle"], c-8, Vehicles[vid]["panels"][c])
		end
	end	
end

function unLoadPlayerVehicle(player)
	SavePlayerVehicles(player)
	
	
	
end

function SavePlayerVehicles(player)
	local playerid = getPlayerID(player)
	
	for i in ipairs(Players[playerid]["vehicles"]) do
		SaveVehicle(Players[playerid]["vehicles"][i])
	end
end

function reSpawnDrivingSchoolCar(vehicle)
	local vehid = getElementData(vehicle, "vehicle.id")
	StartEngine(vehicle, false)
	fixCar(vehicle)
	Vehicles[vehid]["fuel"] = 60
	respawnVehicle(vehicle)
	setVehicleLocked (vehicle, false)
end