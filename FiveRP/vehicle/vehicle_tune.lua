function StartTune(player)
	local playerid = getPlayerID(player)
	local text = ""
	local vehicle = getPedOccupiedVehicle(player)
	-------------------------------------------------------------
	Players[playerid]["tparam"]["tunecat"] = nil
	triggerClientEvent("startCarTune", player, false)
	-------------------------------------------------------------
	for i = 1, 17 do
		local utable = getVehicleCompatibleUpgrades (vehicle, i-1)
		if #utable > 0 then
			local stext = getVehicleUpgradeSlotRusName (i-1)
			if #text < 1 then
				text = ""..text..""..stext..""
			else
				text = ""..text.."\n"..stext..""
			end
		end
	end
	ShowSelectMenu(player, 110, "Los Santos Custom", text)
end

function OpenTuneCategory(player, category)
	local playerid = getPlayerID(player)
	if category == 0 then
		Players[playerid]["tparam"]["tunecat"] = nil
		return triggerClientEvent("startCarTune", player, false)
	end
	local vehicle = getPedOccupiedVehicle(player)
	local vehicleid = getElementData(vehicle, "vehicle.id")
	local slots = {}
	for i = 1, 17 do
		local utable = getVehicleCompatibleUpgrades(vehicle, i-1)
		if #utable > 0 then
			table.insert(slots, i-1)
		end
	end
	Players[playerid]["tparam"]["tunecat"] = slots[category]
	local utable = getVehicleCompatibleUpgrades(vehicle, Players[playerid]["tparam"]["tunecat"])
	local text = ""
	for i in ipairs(utable) do
		if #text < 1 then
			text = ""..text..""..tostring(Tuning[utable[i]-999]["name"])..""
		else
			text = ""..text.."\n"..tostring(Tuning[utable[i]-999]["name"])..""
		end
	end
	text = ""..text.."\nНазад"
	triggerClientEvent("startCarTune", player, true, utable, Vehicles[vehicleid]["tune"][slots[category]])
	ShowSelectMenu(player, 111, "Los Santos Custom", text)
	slots = nil
end

function applyTune(player, keyid, price)
	local playerid = getPlayerID(player)
	if keyid == 0 then
		Players[playerid]["tparam"]["tunecat"] = nil
		return triggerClientEvent("startCarTune", player, false)
	end
	local playerid = getPlayerID(player)
	local vehicle = getPedOccupiedVehicle(player)
	local utable = getVehicleCompatibleUpgrades(vehicle, Players[playerid]["tparam"]["tunecat"])
	addVehicleUpgrade (vehicle, utable[keyid])
end

function getVehicleUpgradeSlotRusName(slot)
	local name = ""
	if slot == 0 then
		name = "Капот"
	elseif slot == 1 then
		name = "Разное"
	elseif slot == 2 then
		name = "Спойлер"
	elseif slot == 3 then
		name = "Боковые пороги"
	elseif slot == 4 then
		name = "Передний бампер"
	elseif slot == 5 then
		name = "Задний бампер"
	elseif slot == 6 then
		name = "Фары"
	elseif slot == 7 then
		name = "Крыша"
	elseif slot == 8 then
		name = "Нитро"
	elseif slot == 9 then
		name = "Гидравлика"
	elseif slot == 10 then
		name = "Стерео"
	elseif slot == 11 then
		name = "Неизвестно"
	elseif slot == 12 then
		name = "Колеса"
	elseif slot == 13 then
		name = "Выхлоп"
	elseif slot == 14 then
		name = "Передний бампер"
	elseif slot == 15 then
		name = "Задний бампер"
	elseif slot == 16 then
		name = "Разное"
	end
	return name
end