local TuneElements
local vehicle
local LastTuneID = 1
local CurrentTune = nil
function switchTune(elementID)
	if elementID <= #TuneElements then
		LastTuneID = elementID
		addVehicleUpgrade(vehicle, TuneElements[elementID])
	else
		removeVehicleUpgrade(vehicle, TuneElements[LastTuneID])
		if CurrentTune ~= 0 then
			addVehicleUpgrade(vehicle, CurrentTune)
		end
	end
end
addEvent("MSelectPressKey", true)

function startCarTune(bool, elements, tune)
	if bool then 
		addEventHandler("MSelectPressKey", localPlayer, switchTune)
		TuneElements = elements
		vehicle = getPedOccupiedVehicle(localPlayer)
		LastTuneID = 1
		CurrentTune = tune
	else
		removeEventHandler("MSelectPressKey", localPlayer, switchTune)
		TuneElements = nil
		vehicle = nil
		CurrentTune = nil
	end
end
addEvent("startCarTune", true)
addEventHandler("startCarTune", localPlayer, startCarTune)