Tuning = {}

Tuning[1] = {name = "Pro", category = "Spoiler", price = 10}
Tuning[2] = {name = "Win", category = "Spoiler", price = 10}
Tuning[3] = {name = "Drag", category = "Spoiler", price = 10}
Tuning[4] = {name = "Alpha", category = "Spoiler", price = 10}
Tuning[5] = {name = "Champ Scoop", category = "Hood", price = 10}
Tuning[6] = {name = "Fury Scoop", category = "Hood", price = 10}
Tuning[7] = {name = "Roof Scoop", category = "Roof", price = 10}
Tuning[8] = {name = "Right Sideskirt", category = "Sideskirt", price = 10}
Tuning[9] = {name = "5 times", category = "Nitro", price = 10}
Tuning[10] = {name = "2 times", category = "Nitro", price = 10}
Tuning[11] = {name = "10 times", category = "Nitro", price = 10}
Tuning[12] = {name = "Race Scoop", category = "Hood", price = 10}
Tuning[13] = {name = "Worx Scoop", category = "Hood", price = 10}
Tuning[14] = {name = "Round Fog", category = "Lamps", price = 10}
Tuning[15] = {name = "Champ", category = "Spoiler", price = 10}
Tuning[16] = {name = "Race", category = "Spoiler", price = 10}
Tuning[17] = {name = "Worx", category = "Spoiler", price = 10}
Tuning[18] = {name = "Left Sideskirt", category = "Sideskirt", price = 10}
Tuning[19] = {name = "Upswept", category = "Exhaust", price = 10}
Tuning[20] = {name = "Twin", category = "Exhaust", price = 10}
Tuning[21] = {name = "Large", category = "Exhaust", price = 10}
Tuning[22] = {name = "Medium", category = "Exhaust", price = 10}
Tuning[23] = {name = "Small", category = "Exhaust", price = 10}
Tuning[24] = {name = "Fury", category = "Spoiler", price = 10}
Tuning[25] = {name = "Square Fog", category = "Lamps", price = 10}
Tuning[26] = {name = "Offroad", category = "Wheels", price = 10}
Tuning[27] = {name = "Right Alien Sideskirt", category = "Sideskirt", price = 10}
Tuning[28] = {name = "Left Alien Sideskirt", category = "Sideskirt", price = 10}
Tuning[29] = {name = "Alien", category = "Exhaust", price = 10}
Tuning[30] = {name = "X-Flow", category = "Exhaust", price = 10}
Tuning[31] = {name = "Left X-Flow Sideskirt", category = "", price = 10}
Tuning[32] = {name = "Right X-Flow Sideskirt", category = "", price = 10}
Tuning[33] = {name = "Alien Roof Vent", category = "Sideskirt", price = 10}
Tuning[34] = {name = "X-Flow Roof Vent", category = "Sideskirt", price = 10}
Tuning[35] = {name = "Alien", category = "Exhaust", price = 10}
Tuning[36] = {name = "X-Flow Roof Vent", category = "Roof", price = 10}
Tuning[37] = {name = "Right Alien Sideskirt", category = "SideSkirt", price = 10}
Tuning[38] = {name = "X-Flow", category = "Exhaust", price = 10}
Tuning[39] = {name = "Alien Roof Vent", category = "Roof", price = 10}
Tuning[40] = {name = "Left X-Flow Sideskirt", category = "SideSkirt", price = 10}
Tuning[41] = {name = "Left Alien Sideskirt", category = "SideSkirt", price = 10}
Tuning[42] = {name = "Right X-Flow Sideskirt", category = "SideSkirt", price = 10}
Tuning[43] = {name = "Right Chrome Sideskirt", category = "SideSkirt", price = 10}
Tuning[45] = {name = "Slamin", category = "Exhaust", price = 10}
Tuning[46] = {name = "Chrome", category = "Exhaust", price = 10}
Tuning[47] = {name = "X-Flow", category = "Exhaust", price = 10}
Tuning[48] = {name = "Alien", category = "Exhaust", price = 10}
Tuning[49] = {name = "Right Alien Sideskirt", category = "SideSkirt", price = 10}
Tuning[50] = {name = "Right X-Flow Sideskirt", category = "SideSkirt", price = 10}
Tuning[51] = {name = "Alien", category = "Spoiler", price = 10}
Tuning[52] = {name = "X-Flow", category = "Spoiler", price = 10}
Tuning[53] = {name = "Left Alien Sideskirt", category = "SideSkirt", price = 10}
Tuning[54] = {name = "Left X-Flow Sideskirt", category = "SideSkirt", price = 10}
Tuning[55] = {name = "X-Flow", category = "Roof", price = 10}
Tuning[56] = {name = "Alien", category = "Roof", price = 10}
Tuning[57] = {name = "Alien", category = "Roof", price = 10}
Tuning[58] = {name = "Right Alien Sideskirt", category = "SideSkirt", price = 10}
Tuning[59] = {name = "Right X-Flow Sideskirt", category = "SideSkirt", price = 10}
Tuning[60] = {name = "Alien", category = "Spoiler", price = 10}
Tuning[61] = {name = "X-Flow", category = "Exhaust", price = 10}
Tuning[62] = {name = "X-Flow", category = "Spoiler", price = 10}
Tuning[63] = {name = "X-Flow", category = "Roof", price = 10}
Tuning[64] = {name = "Left Alien Sideskirt", category = "Sideskirt", price = 10}
Tuning[65] = {name = "Left X-Flow Sideskirt", category = "Sideskirt", price = 10}
Tuning[66] = {name = "Alien", category = "Exhaust", price = 10}
Tuning[67] = {name = "Alien", category = "Exhaust", price = 10}
Tuning[68] = {name = "X-Flow", category = "Exhaust", price = 10}
Tuning[69] = {name = "Alien", category = "Roof", price = 10}
Tuning[70] = {name = "X-Flow", category = "Roof", price = 10}
Tuning[71] = {name = "Right Alien Sideskirt", category = "Sideskirt", price = 10}
Tuning[72] = {name = "Right X-Flow Sideskirt", category = "Sideskirt", price = 10}
Tuning[73] = {name = "Left X-Flow Sideskirt", category = "Sideskirt", price = 10}
Tuning[74] = {name = "Left X-Flow Left X-Flow Sideskirt", category = "Sideskirt", price = 10}
Tuning[75] = {name = "Shadow", category = "Wheels", price = 10}
Tuning[76] = {name = "Mega", category = "Wheels", price = 10}
Tuning[77] = {name = "Rimshine", category = "Wheels", price = 10}
Tuning[78] = {name = "Wires", category = "Wheels", price = 10}
Tuning[79] = {name = "Classic", category = "Wheels", price = 10}
Tuning[80] = {name = "Twist", category = "Wheels", price = 10}
Tuning[81] = {name = "Cutter", category = "Wheels", price = 10}
Tuning[82] = {name = "Switch", category = "Wheels", price = 10}
Tuning[83] = {name = "Grove", category = "Wheels", price = 10}
Tuning[84] = {name = "Import", category = "Wheels", price = 10}
Tuning[85] = {name = "Dollar", category = "Wheels", price = 10}
Tuning[86] = {name = "Trance", category = "Wheels", price = 10}
Tuning[87] = {name = "Atomic", category = "Wheels", price = 10}
Tuning[88] = {name = "Stereo", category = "Stereo", price = 10}
Tuning[89] = {name = "Hydraulics", category = "Hydraulics", price = 10}
Tuning[90] = {name = "Alien", category = "Roof", price = 10}
Tuning[91] = {name = "X-Flow", category = "Exhaust", price = 10}
Tuning[92] = {name = "Right Alien Sideskirt", category = "Sideskirt", price = 10}
Tuning[93] = {name = "X-Flow", category = "Roof", price = 10}
Tuning[94] = {name = "Alien", category = "Exhaust", price = 10}
Tuning[95] = {name = "Right X-Flow Sideskirt", category = "Sideskirt", price = 10}
Tuning[96] = {name = "Left Alien Sideskirt", category = "Sideskirt", price = 10}
Tuning[97] = {name = "Right X-Flow Sideskirt", category = "Sideskirt", price = 10}
Tuning[98] = {name = "Ahab", category = "Wheels", price = 10}
Tuning[99] = {name = "Virtual", category = "Wheels", price = 10}
Tuning[100] = {name = "Access", category = "Wheels", price = 10}
Tuning[101] = {name = "Left Chrome Sideskirt", category = "Sideskirt", price = 10}
Tuning[102] = {name = "Chrome Grill", category = "Bullbar", price = 10}
Tuning[103] = {name = "Left `Chrome Flames` Sideskirt", category = "", price = 10}
Tuning[104] = {name = "Left `Chrome Strip` Sideskirt", category = "", price = 10}
Tuning[105] = {name = "Covertible", category = "Roof", price = 10}
Tuning[106] = {name = "Chrome", category = "Exhaust", price = 10}
Tuning[107] = {name = "Slamin", category = "Exhaust", price = 10}
Tuning[108] = {name = "Right `Chrome Arches`", category = "	Sideskirt", price = 10}
Tuning[109] = {name = "Left `Chrome Strip` Sideskirt", category = "Sideskirt", price = 10}
Tuning[110] = {name = "Right `Chrome Strip` Sideskirt", category = "Sideskirt", price = 10}
Tuning[111] = {name = "Chrome", category = "Rear Bullbars", price = 10}
Tuning[112] = {name = "Slamin", category = "Rear Bullbars", price = 10}
Tuning[113] = {name = "Little Sign?", category = "Front Sign?", price = 10}
Tuning[114] = {name = "Little Sign?", category = "Front Sign?", price = 10}
Tuning[115] = {name = "Chrome", category = "Exhaust", price = 10}
Tuning[116] = {name = "Slamin", category = "Exhaust", price = 10}
Tuning[117] = {name = "Chrome", category = "Front Bullbars", price = 10}
Tuning[118] = {name = "Slamin", category = "Front Bullbars", price = 10}
Tuning[119] = {name = "Chrome", category = "Front Bullbars", price = 10}
Tuning[120] = {name = "Right `Chrome Trim` Sideskirt", category = "Front Bumper", price = 10}
Tuning[121] = {name = "Right `Wheelcovers` Sideskirt", category = "Sideskirt", price = 10}
Tuning[122] = {name = "Left `Chrome Trim` Sideskirt", category = "Sideskirt", price = 10}
Tuning[123] = {name = "Left `Wheelcovers` Sideskirt", category = "Sideskirt", price = 10}
Tuning[124] = {name = "Right `Chrome Flames` Sideskirt", category = "Sideskirt", price = 10}
Tuning[125] = {name = "Bullbar Chrome Bars", category = "Sideskirt", price = 10}
Tuning[126] = {name = "Left `Chrome Arches` Sideskirt", category = "Bullbars", price = 10}
Tuning[127] = {name = "Bullbar Chrome Lights", category = "Sideskirt", price = 10}
Tuning[128] = {name = "Chrome Exhaust", category = "Exhaust", price = 10}
Tuning[129] = {name = "Slamin Exhaust", category = "Exhaust", price = 10}
Tuning[130] = {name = "Vinyl Hardtop", category = "Roof", price = 10}
Tuning[131] = {name = "Chrome", category = "Exhaust", price = 10}
Tuning[132] = {name = "Hardtop", category = "Roof", price = 10}
Tuning[133] = {name = "Softtop", category = "Roof", price = 10}
Tuning[133] = {name = "Slamin", category = "Exhaust", price = 10}
Tuning[134] = {name = "Right `Chrome Strip` Sideskirt", category = "Sideskirt", price = 10}
Tuning[135] = {name = "Right `Chrome Strip` Sideskirt", category = "SideSkirt", price = 10}
Tuning[136] = {name = "Slamin", category = "Exhaust", price = 10}
Tuning[137] = {name = "Chrome", category = "Exhaust", price = 10}
Tuning[138] = {name = "Left `Chrome Strip` Sideskirt", category = "Sideskirt", price = 10}
Tuning[139] = {name = "Alien", category = "Spoiler", price = 10}
Tuning[140] = {name = "X-Flow", category = "Spoiler", price = 10}
Tuning[141] = {name = "X-Flow", category = "Rear Bumper", price = 10}
Tuning[142] = {name = "Alien", category = "Rear Bumper", price = 10}
Tuning[143] = {name = "Left Oval Vents", category = "Vents", price = 10}
Tuning[144] = {name = "Right Oval Vents", category = "Vents", price = 10}
Tuning[145] = {name = "Left Square Vents", category = "Vents", price = 10}
Tuning[146] = {name = "Right Square Vents", category = "Vents", price = 10}
Tuning[147] = {name = "X-Flow", category = "Spoiler", price = 10}
Tuning[148] = {name = "Alien", category = "Spoiler", price = 10}
Tuning[149] = {name = "X-Flow", category = "Rear Bumper", price = 10}
Tuning[150] = {name = "Alien", category = "Rear Bumper", price = 10}
Tuning[151] = {name = "Alien", category = "Rear Bumper", price = 10}
Tuning[152] = {name = "X-Flow", category = "Rear Bumper", price = 10}
Tuning[153] = {name = "X-Flow", category = "Front Bumper", price = 10}
Tuning[154] = {name = "Alien", category = "Front Bumper", price = 10}
Tuning[155] = {name = "Alien", category = "Rear Bumper", price = 10}
Tuning[156] = {name = "Alien", category = "Front Bumper", price = 10}
Tuning[157] = {name = "X-Flow", category = "Rear Bumper", price = 10}
Tuning[158] = {name = "X-Flow", category = "Front Bumper", price = 10}
Tuning[159] = {name = "X-Flow", category = "Spoiler", price = 10}
Tuning[160] = {name = "Alien", category = "Rear Bumper", price = 10}
Tuning[161] = {name = "Alien", category = "Front Bumper", price = 10}
Tuning[162] = {name = "X-Flow", category = "Rear Bumper", price = 10}
Tuning[163] = {name = "Alien", category = "Spoiler", price = 10}
Tuning[164] = {name = "X-Flow", category = "Spoiler", price = 10}
Tuning[165] = {name = "Alien", category = "Spoiler", price = 10}
Tuning[166] = {name = "X-Flow", category = "Front Bumper", price = 10}
Tuning[167] = {name = "Alien", category = "Front Bumper", price = 10}
Tuning[168] = {name = "X-Flow", category = "Rear Bumper", price = 10}
Tuning[169] = {name = "Alien", category = "Rear Bumper", price = 10}
Tuning[170] = {name = "Alien", category = "Front Bumper", price = 10}
Tuning[171] = {name = "X-Flow", category = "Front Bumper", price = 10}
Tuning[172] = {name = "Alien", category = "Front Bumper", price = 10}
Tuning[173] = {name = "X-Flow", category = "Front Bumper", price = 10}
Tuning[174] = {name = "X-Flow", category = "Front Bumper", price = 10}
Tuning[175] = {name = "Chrome", category = "Front Bumper", price = 10}
Tuning[176] = {name = "Slamin", category = "Rear Bumper", price = 10}
Tuning[177] = {name = "Chrome", category = "Front Bumper", price = 10}
Tuning[178] = {name = "Slamin", category = "Rear Bumper", price = 10}
Tuning[179] = {name = "Slamin", category = "Rear Bumper", price = 10}
Tuning[180] = {name = "Chrome", category = "Front Bumper", price = 10}
Tuning[181] = {name = "Chrome", category = "Rear Bumper", price = 10}
Tuning[182] = {name = "Slamin", category = "Front Bumper", price = 10}
Tuning[183] = {name = "Chrome", category = "Front Bumper", price = 10}
Tuning[184] = {name = "Slamin", category = "Rear Bumper", price = 10}
Tuning[185] = {name = "Chrome", category = "Rear Bumper", price = 10}
Tuning[186] = {name = "Slamin", category = "Front Bumper", price = 10}
Tuning[187] = {name = "Slamin", category = "Rear Bumper", price = 10}
Tuning[188] = {name = "Chrome", category = "Rear Bumper", price = 10}
Tuning[189] = {name = "Slamin", category = "Front Bumper", price = 10}
Tuning[190] = {name = "Chrome", category = "Front Bumper", price = 10}
Tuning[191] = {name = "Slamin", category = "Front Bumper", price = 10}
Tuning[192] = {name = "Chrome", category = "Front Bumper", price = 10}
Tuning[193] = {name = "Chrome", category = "Rear Bumper", price = 10}
Tuning[194] = {name = "Slamin", category = "Rear Bumper", price = 10} 	



