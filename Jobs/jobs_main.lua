--JOBSlist = xmlLoadFile("JOBS_list.xml")
local JOBSlist = xmlLoadFile("jobs_list.xml")
local _jobs = 0


JOBS = {}



--[[===============Работы==================
JOBS[1] = {name = "Грузчик", icon = 1239, mapicon = 0, pos = {x = 4, y = 5, z = 3.4, inv, vw = 0}, employment = false}
JOBS[2] = {name = "Дальнобойщик", icon = 1239, mapicon = 0, pos = {x = 8, y = 9, z = 3.4, inv, vw = 0}, employment = true}
--=======================================]]

function importFunctions (resourceName)  
    local resource = getResourceFromName (resourceName); 
    if not resource then 
        outputDebugString ("Couldnt import functions from \"" .. resourceName .. "\" resource because" .. "it doesnt exist!", 2); 
        return; 
    end; 
    local exportedFuncNames = getResourceExportedFunctions (resource); 
    if not exportedFuncNames then return end; 
    for i,j in ipairs (exportedFuncNames) do 
        _G [j] = function (...) call (resource, j, unpack ({...})); end; 
    end; 
end; 
importFunctions("Text-Label");

function LoadJOBSData(res)
	if res ~= getThisResource() then return 1 end
	while xmlFindChild ( JOBSlist, "job", _jobs ) ~= false do
		local data = xmlNodeGetAttributes ( xmlFindChild ( JOBSlist, "job", _jobs ) )
		_jobs = _jobs+1
		JOBS[_jobs] = {pos = {}}
		JOBS[_jobs]["name"] = data["name"]
		JOBS[_jobs]["icon"] = data["icon"]
		JOBS[_jobs]["mapicon"] = data["mapicon"]
		local pos = exports.FiveRP:explode ( "|", data["pos"] )
		for i in ipairs(pos) do
			JOBS[_jobs]["pos"][i] = pos[i]--pos
		end	
		JOBS[_jobs]["employment"] = data["employment"]
	end
	outputDebugString("Загружено ".._jobs.." работ")
	LoadJOBS()
end

function LoadJOBS()
	for i in ipairs(JOBS) do
		JOBS[i]["pickup"] = createPickup ( JOBS[i]["pos"][1], JOBS[i]["pos"][2], JOBS[i]["pos"][3], 3, JOBS[i]["icon"])
		JOBS[i]["Text3D"] = create3DTextLabel("Работа "..JOBS[i]["name"].."", 0xD2BE3CFF, JOBS[i]["pos"][1], JOBS[i]["pos"][2], JOBS[i]["pos"][3]+0.5, 15, JOBS[i]["pos"]["vw"], _, JOBS[i]["pickup"])
	end
end

addEventHandler ( "onResourceStart", getResourceRootElement(), LoadJOBSData)

function GetJOBSInfo(id, virable, p1, p2, p3)
	
	if p1 == nil then
		return JOBS[id][virable]
	else
		return JOBS[id][virable][p1] end
end

function SetJOBSInfo(param, id, virable, p1, p2, p3)
	if p1 == nil then
		JOBS[id][virable] = param
	else
		JOBS[id][virable][p1] = param end
end

function RestartResource(resource)
	if getResourceName(resource) == "JOBS" then
		for i in ipairs(JOBS) do
			delete3DTextLabel(i)
		end
	end
end
addEventHandler ( "onResourceStop", root, RestartResource)