--[[
	Все гос кастомные инты в 200 инте
]]

local Objects = {}
local Textures = {} -- Shader, TextureName, Texture

local LoadedMap = false

function loadMap(filename)
	local mapfile = xmlLoadFile ( "maps/"..filename..".xml" )
	local z = 0
	local t = 0
	-----------------------Парсинг объектов маппинга-----------------------------
	while xmlFindChild ( mapfile, "object", z ) ~= false do
		local data = xmlNodeGetAttributes ( xmlFindChild ( mapfile, "object", z ) )
		Objects[z+1] = exports.CustomOBJ:CreateObject(tonumber(data["model"]), tonumber(data.posX), tonumber(data.posY), tonumber(data.posZ), tonumber(data.rotX), tonumber(data.rotY), tonumber(data.rotZ))
		setElementData(Objects[z+1], "objID", z+1)
		if Objects[z+1] then
			if data["doublesided"] then
				setElementDoubleSided (Objects[z+1], true)
			end
			setElementDimension(Objects[z+1], tonumber(data.dimension))
			setElementInterior (Objects[z+1], 200)
		else
			outputDebugString("Объект не создан. ID: "..(z+1).." Model: "..data["model"].."")
		end	
		--setElementInterior (Objects[z+1], tonumber(data.interior))
		--local node = xmlFindChild ( mapfile, "object", z)
		--xmlNodeSetAttribute(node, "id", z)
		z = z+1	
	end
	------------------------Парсинг текстур маппинга----------------------------
	while xmlFindChild ( mapfile, "texture", t ) ~= false do
		local data = xmlNodeGetAttributes ( xmlFindChild ( mapfile, "texture", t ) )
		data["objectid"] = tonumber(data["objectid"])
		local retextured = false
		for i in ipairs(Textures) do
			if Textures[i]["TextureName"] == data["texturename"] then
				engineApplyShaderToWorldTexture(Textures[i]["Shader"], "*", Objects[data["objectid"]])
				retextured = true
				break
			end
		end
		if retextured ~= true then
			local _Texture = dxCreateTexture("textures/"..data["texturename"]..".png")
			local _Shader = dxCreateShader ("textureshader.fx", 0, 0, false,  "object")
			dxSetShaderValue(_Shader, "tex", _Texture);
			engineApplyShaderToWorldTexture(_Shader, "*", Objects[data["objectid"]])
			table.insert (Textures, {Shader = _Shader, TextureName = data["texturename"], Texture = _Texture})
		end
		retextured = false
		t = t+1
	end
	-------------------------------------
	--xmlSaveFile(mapfile)
	xmlUnloadFile(mapfile)  
	LoadedMap = true
	--removeEventHandler("onClientRender", root, DrawingLoadUI)
	--UIAlpha = 0
	triggerServerEvent("mapLoaded", resourceRoot, true)
	return true
end


function unLoadMap()
	for i in ipairs(Objects) do
		destroyElement(Objects[i])
	end
	for i in ipairs(Textures) do
		engineRemoveShaderFromWorldTexture(Textures[i].Shader, "*")
		destroyElement(Textures[i].Shader)
		destroyElement(Textures[i].Texture)
	end
	Textures = nil
	Objects = nil
	Textures = {}
	Objects = {}
	exports.CustomOBJ:UnloadCustomObjects()
	LoadedMap = false
end
addEvent("unLoadMap", true)
addEventHandler("unLoadMap", localPlayer, unLoadMap)

function enterInterior(MapName)
	--setElementInterior(localPlayer, 200)
	--addEventHandler("onClientRender", root, DrawingLoadUI)
	if LoadedMap then unLoadMap() end
	loadMap(tostring(MapName)) 
end
addEvent("enterInterior", true)
addEventHandler("enterInterior", localPlayer, enterInterior)

local FontFive = dxCreateFont(":Fonts/Five.ttf", 32)
local screenW, screenH = guiGetScreenSize()
local UIAlpha = 0

function DrawingLoadUI()
	if UIAlpha < 255 then
		UIAlpha = UIAlpha+15
	end	
	dxDrawRectangle(screenW * 0.0000, screenH * 0.0000, screenW * 1.0000, screenH * 1.0000, tocolor(0, 0, 0, UIAlpha), false)
	dxDrawText("Загрузка..", 1661, 993, 1910, 1070, tocolor(255, 255, 255, 255), 1.00, FontFive, "left", "top", false, false, false, false, false)
end

function cmd_debugmap(cmd, bool)
	if ( isDebugViewActive() ) then
		bool = tonumber(bool)
		if bool == 1 then
			addEventHandler ( "onClientClick", getRootElement(), getElementClickedID)
		elseif bool == 0 then
			removeEventHandler ( "onClientClick", getRootElement(), getElementClickedID)
		end
	end	
end
addCommandHandler("debugmap", cmd_debugmap)

function getElementClickedID(button, state, absoluteX, absoluteY, worldX, worldY, worldZ, Object)
	if Object then
		local id = getElementData(Object, "objID")
		outputDebugString(tostring(id))
	end
end
