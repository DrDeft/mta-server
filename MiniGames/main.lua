local PlayerMiniGame = {}
MiniGames = {}
--[[
	ID игр:
	1 - Нажатие рандомных цифр по времени(Как JC2)
	2 - Игра Y - N
	3 - Зажать клавишу в чек-зоне(для дровосека)
]]

function StartMiniGame(player, gameid, hard)
	if not hard then hard = 1 end
	--------------------------------------------------------------
	local playerid = getElementData(player, "player.id")
	if PlayerMiniGame[playerid] ~= nil then return false end
	PlayerMiniGame[playerid] = nil
	PlayerMiniGame[playerid] = {}
	PlayerMiniGame[playerid]["gameid"] = gameid
	PlayerMiniGame[playerid]["hard"] = hard
	--------------------------------------------------------------
	if gameid == 1 then
		local data = {}
		local text = " "
		for i = 1, 3+3*hard do
			data[i] = tostring(math.random (0, 9))
			text = ""..text.." "..data[i]..""
		end
		triggerClientEvent(player, "StartTNGame", player, data)
		data = nil
	elseif gameid == 2 then
		PlayerMiniGame[playerid]["progressbar"] = exports.ProgressBar:createProgressBar(player, 0.45, 0.88, 0.10, 0.02, true)
		triggerClientEvent(player, "StartYNMiniGame", player, hard, PlayerMiniGame[playerid]["progressbar"])
	elseif gameid == 3 then
		triggerClientEvent(player, "StartCheckPressGame", player, PlayerMiniGame[playerid]["progressbar"])
	end
	return true
end

function StopMiniGame(player)
	local playerid = getElementData(player, "player.id")
	exports.ProgressBar:deleteProgressBar(player, PlayerMiniGame[playerid]["progressbar"])
	triggerEvent("MiniGameComplite", player, result)   
	PlayerMiniGame[playerid] = nil
end

function SetMiniGameHard(player)
end

function EndMiniGame(player, result) -- Типы: nil - нормально, 1 - Смерть, 2 - Выход из игры
	local playerid = getElementData(player, "player.id")
	if PlayerMiniGame[playerid] ~= nil and PlayerMiniGame[playerid]["progressbar"] ~= nil then
		exports.ProgressBar:deleteProgressBar(player, PlayerMiniGame[playerid]["progressbar"])
	end	
	triggerEvent("MiniGameComplite", player, result)    
	PlayerMiniGame[playerid] = nil
end
addEvent("EndMiniGame", true)
addEventHandler("EndMiniGame", root, EndMiniGame)

addEvent("MiniGameComplite", true)

function cmd_testprees(player, cmd)
	StartMiniGame(player, 3)
end
addCommandHandler("testpress", cmd_testprees)

function OnPlayerQuit()
	EndMiniGame(source, false)
end
addEventHandler("onPlayerQuit", getRootElement(), OnPlayerQuit)

function OnPlayerWasted()
	local playerid = getElementData(source, "player.id")
	if PlayerMiniGame[playerid] ~= nil then
		triggerClientEvent(source, "StopMiniGame", source, false)
	end
end
addEventHandler("onPlayerWasted", root, OnPlayerWasted)
