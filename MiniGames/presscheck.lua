local power
local CheckPos
local CPGameTimer
local PressStatus = nil
local Progress = nil
local screenW, screenH = guiGetScreenSize()

local Font = dxCreateFont(":Fonts/RMedium.ttf", 10)

local xcentre = ((screenW - 329)/2) / 1920

local CPGUI = nil

local function StartCheckPressGame()
	power = 0
	CheckPos = math.random (5, 280)
	PressStatus = nil
	addEventHandler("onClientPreRender", root, CPGameDrawImage)
	addEventHandler("onClientKey", root, CPPressKey)
	Progress = 0
	CPGameTimer = setTimer(CPGameTimerP, 200, 0)
	--CreateCPGameGUI()
	addEventHandler("StopMiniGame", localPlayer, EndCPGame)
end
addEvent("StartCheckPressGame", true)
addEventHandler("StartCheckPressGame", localPlayer, StartCheckPressGame)

function CPPressKey(button, bool)
	if bool and button == "mouse1" then
		PressStatus = true
	elseif bool == false and button == "mouse1" then 
		PressStatus = false
	end
end

function EndCPGame(result)
	removeEventHandler("onClientPreRender", root, CPGameDrawImage)
	removeEventHandler("onClientKey", root, CPPressKey)
	removeEventHandler("StopMiniGame", localPlayer, EndCPGame)
	power = nil
	CheckPos = nil
	killTimer(CPGameTimer)
	CPGameTimer = nil
	PressStatus = nil
	Progress = nil
	triggerServerEvent("EndMiniGame", localPlayer, localPlayer, result)
	setTimer (function() 
		toggleControl( "left", true)  
		toggleControl( "right", true) 
		toggleControl( "forwards", true)  
		toggleControl( "backwards", true)  
		toggleControl( "enter_exit", true) 	
	end, 200, 1 )
end
addEvent("StopMiniGame", true)

function CPGameTimerP()
	if power*3 > CheckPos and power*3 <= CheckPos+20 then
		Progress = Progress+5
		if Progress >= 100 then EndCPGame(true) end	
	end
end

function CPGameDrawImage(deltatime)
	if PressStatus then
		if power < 100 then
			power = power+1
		end	
	elseif PressStatus == false	then
		if power > 0 then
			power = power-1
		end	
	end

	dxDrawRectangle(screenW*xcentre, screenH*0.78981, 327, screenH*0.10277, tocolor(5, 64, 3, 255), true) -- 844, 853, 329, 111
	dxDrawRectangle(screenW*xcentre+5, screenH*0.79537, 317, screenH*0.09074, tocolor(2, 36, 1, 255), true) -- 850, 859, 317, 98
	dxDrawText("Зажмите ЛКМ и удерживайте триггер в белой зоне", screenW*xcentre+5, screenH*0.799, screenW*xcentre+2+322, screenH*0.8231, tocolor(255, 255, 255, 255), 1.00, Font, "center", "top", false, false, true, false, false) -- 851, 863, 1165, 889
    dxDrawText("Процесс "..Progress.."/100", screenW*xcentre+5, screenH*0.861, screenW*xcentre+2+322, screenH*0.8851, tocolor(255, 255, 255, 255), 1.00, Font, "center", "top", false, false, true, false, false) -- 851, 930, 1165, 956
	-------------------------------------------------------------------------------------------
	dxDrawRectangle(screenW*xcentre+13, screenH*0.8287, 300, screenH*0.0277, tocolor(78, 78, 78, 255), true) -- 858, 895, 300, 30
    dxDrawRectangle(screenW*xcentre+13+CheckPos, screenH*0.8287, 20, screenH*0.0277, tocolor(255, 255, 255, 255), true) -- 858+CheckPos, 895, 20, 30
    dxDrawRectangle((screenW*xcentre+13)+power*3, screenH*0.8287, 5, screenH*0.0277, tocolor(230, 142, 16, 255), true) -- 858+power*3, 895, 5, 30 screenW*0.00462
end
