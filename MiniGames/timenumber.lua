local KeyProg = 1
local Keys
local function StartTNGame(data, hard)
	KeyProg = 1
	Keys = data
	addEventHandler("onClientKey", root, TNGameKeyPress)
end
addEvent("StartTNGame", true)
addEventHandler("StartTNGame", localPlayer, StartTNGame)

function TNGameKeyPress(button, bool)
	if bool then
		if button == Keys[KeyProg] then
			outputDebugString("Верно! Кнопка: "..button.."")
			KeyProg = KeyProg+1
		else
			outputDebugString("Ошибка!")
			removeEventHandler("onClientKey", localPlayer, TNGameKeyPress)
		end
	end
end