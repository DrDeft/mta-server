local progress
local key = "y"
local timer
local progbarid
screenW, screenH = guiGetScreenSize()

local function DrawGameUI()
	dxDrawText("#ffff00Press: #ff0000"..key.."",  screenW * 0.4500, screenH * 0.8981, screenW * 0.5500, screenH * 0.9435, tocolor(255, 255, 255, 255), 2.00, "pricedown", "center", "center", false, false, false, true, false)
end

local function GameTimer()
	exports.ProgressBar:setProgressBarProgress(progbarid, progress)
	progress = progress - 2;
	if progress <= 0 then return MiniGameYNOver(false) end
end

local function StartGame()
	exports.ProgressBar:setProgressBarProgress(progbarid, progress)
	progress = 5
	timer = setTimer(GameTimer, 1000, 0)
end

function StartYNMiniGame(hard, pb)
	progbarid = pb
	progress = 0
	exports.ProgressBar:setProgressBarProgress(progbarid, progress)
	local k = math.random(0,1)
	if k == 1 then key = "y" else key = "n" end
	addEventHandler("onClientKey", root, YNKeyPress)
	addEventHandler("onClientRender", root, DrawGameUI)
	addEventHandler("StopMiniGame", localPlayer, MiniGameYNOver)
	
end
addEvent("StartYNMiniGame", true)
addEventHandler("StartYNMiniGame", localPlayer, StartYNMiniGame)


function YNKeyPress(button, bool)
	if progress == 0 then StartGame() end
	if bool then
		if button == key then
			--Ок
			local k = math.random(0,1)
			if k == 1 then key = "y" else key = "n" end
			progress = progress+5
			if progress >= 100 then return MiniGameYNOver(true) end
		else
			--Ошибка
			progress = progress - 15;
			if progress <= 0 then return MiniGameYNOver(false) end
		end
		exports.ProgressBar:setProgressBarProgress(progbarid, progress)
	end
end

function MiniGameYNOver(bool)
	removeEventHandler("onClientRender", root, DrawGameUI)
	removeEventHandler("onClientKey", root, YNKeyPress)
	removeEventHandler("StopMiniGame", localPlayer, MiniGameYNOver)
	killTimer(timer)
	triggerServerEvent("EndMiniGame", localPlayer, localPlayer, bool)
end
addEvent("StopMiniGame", true)