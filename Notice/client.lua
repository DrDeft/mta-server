local dxMedium = false
local screenW, screenH = guiGetScreenSize()
local DownNoticeText = ""
function vievDownNotice(text)
	if text then 
		DownNoticeText = text
		if dxMedium == false then dxMedium = dxCreateFont(":FiveRP/fonts/Roboto-Medium.ttf", 10) end
		addEventHandler("onClientRender", root, RenderDownNotice)
	else
		DownNoticeText = ""
		removeEventHandler("onClientRender", root, RenderDownNotice)
	end
end
addEvent("vievDownNotice", true)
addEventHandler("vievDownNotice", localPlayer, vievDownNotice)

function RenderDownNotice()
	dxDrawImage(screenW * 0.0000, screenH * 0.9731, screenW * 1.0000, screenH * 1.0000, ":FiveRP/loginpanel/images/pane.png", 0, 0, 0, tocolor(1, 0, 0, 149), false)
    dxDrawText(DownNoticeText, screenW * 0.0000, screenH * 0.9731, screenW * 1.0000, screenH * 1.0000, tocolor(255, 255, 255, 255), 1.00, dxMedium, "center", "center", false, false, false, true, false)
end
