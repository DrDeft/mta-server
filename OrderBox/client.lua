local screenW, screenH = guiGetScreenSize()
local RMedium = dxCreateFont(":Fonts/RMedium.ttf", 10)
local BoxSize = {x = (screenW*0.260), y =  screenH * 0.0806}
local BoxPosition = {x = (screenW/2-BoxSize.x/2), y = (screenH * 0.8620)}
local LostTime = BoxSize.x
local LostSecond = 25
local LostTimeColor = tocolor(6, 109, 6, 255)
local NeedPxPerSecond = BoxSize.x/25; -- На сколько px должна уменьшится полоска за 1 сек
local BoxText = nil;
local Viev = false
local OrderBoxTimer = nil


----------------------------------------Дизайн-----------------------------------------------------------------
local removePxPerFrame = 0; -- Кол-во пикселей на которые должна уменьшится полоска за 1 фрейм
function RenderBox()
	--------------------------------------------------------------------------------------------
	dxDrawRectangle(BoxPosition.x, BoxPosition.y, BoxSize.x, BoxSize.y, tocolor(0, 0, 0, 147), false)
	dxDrawRectangle(BoxPosition.x, BoxPosition.y+BoxSize.y, BoxSize.x, screenH*0.0074, tocolor(120, 120, 120, 255), false)
	--------------------------------------------------------------------------------------------
	dxDrawRectangle(BoxPosition.x, BoxPosition.y+BoxSize.y, LostTime, screenH*0.0074, LostTimeColor, false)
	dxDrawText(BoxText, BoxPosition.x, BoxPosition.y, BoxPosition.x+BoxSize.x, 1109, tocolor(255, 255, 255, 255), 1.00, RMedium, "center", "top", false, false, false, true, false)
	dxDrawText("N - Отмена ("..LostSecond.." сек)", BoxPosition.x, BoxPosition.y, BoxPosition.x+BoxSize.x-(screenW*0.044), BoxPosition.y+BoxSize.y, tocolor(255, 255, 255, 255), 1.00, RMedium, "right", "bottom", false, false, false, false, false)
	dxDrawText("Y - Принять", BoxPosition.x+(screenW*0.052), BoxPosition.y, BoxPosition.x+BoxSize.x, BoxPosition.y+BoxSize.y, tocolor(255, 255, 255, 255), 1.00, RMedium, "left", "bottom", false, false, false, false, false)
	LostTime = LostTime - removePxPerFrame
	if LostTime < 0 then LostTime = 0 end
end

function OrderTimer()
	LostSecond = LostSecond-1
	----------Расчет кол-во px для уменшения------------------
	removePxPerFrame = NeedPxPerSecond/getCurrentFPS();
	if LostSecond == 12 then
		LostTimeColor = tocolor(255, 165, 0, 255)
	elseif LostSecond == 5 then
		LostTimeColor = tocolor(255, 0, 0, 255)
	end
	----------------------------------------------------------
	if LostSecond == 0 then
		VievBox(false)
		triggerServerEvent("PlayerOrderBoxResponse", resourceRoot, false)
	end
end
--------------------------------Функционал----------------------------------------------------
function VievBox(bool, text)
	if bool then
		if Viev then return false end
		removePxPerFrame = NeedPxPerSecond/getCurrentFPS();
		OrderBoxTimer = setTimer (OrderTimer, 1000, 25)
		addEventHandler("onClientRender", root, RenderBox)
		BoxText = text
		Viev = true
		bindKey("y", "both", OrderBoxPreesKey)
		bindKey("n", "both", OrderBoxPreesKey)
	else
		if not Viev then return false end
		removeEventHandler("onClientRender", root, RenderBox)
		LostTime = BoxSize.x
		LostSecond = 25
		LostTimeColor = tocolor(6, 109, 6, 255)
		unbindKey("y", "both", OrderBoxPreesKey)
		unbindKey("n", "both", OrderBoxPreesKey)
		if isTimer(OrderBoxTimer) then killTimer(OrderBoxTimer) end
		Viev = false
	end
end

function OrderBoxPreesKey(key)
	if key == "y" then
		triggerServerEvent("PlayerOrderBoxResponse", resourceRoot, true)
	elseif key == "n" then
		triggerServerEvent("PlayerOrderBoxResponse", resourceRoot, false)
	end
	VievBox(false)
end

function ShowOrderBox(text)
	VievBox(true, text)
end
addEvent("ShowOrderBox", true)
addEventHandler("ShowOrderBox", localPlayer, ShowOrderBox)


--[[ Задача:------------------------------------------------------------------------
Раcсчитать насколько px должна уменьшится полоска за 1 frame в течении 1 секунды
Полоска = 500px
500px/25сек = 20px
При 60 FPS полоска должна двигаться на 20/60 = 0.33px
------------------------------------------------------------------------------------




]]
------------------------------------------------------------------------------------------------------------------------------------------------------------
local fps = false
function getCurrentFPS() -- Setup the useful function
    return fps
end

local function updateFPS(msSinceLastFrame)
    fps = (1 / msSinceLastFrame) * 1000
	--dxDrawText(""..getCurrentFPS().." FPS", 8, 18, 104, 60, tocolor(255, 255, 255, 255), 1.00, "default", "left", "top", false, false, false, false, false)
end
addEventHandler("onClientPreRender", root, updateFPS)