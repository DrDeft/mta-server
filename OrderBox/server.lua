function ShowPlayerOrderBox(player, boxID, text)
	triggerClientEvent(player, "ShowOrderBox", player, text)
	return boxID;
end

--[[
	PlayerOrderBoxResponse(bool)
	Принимает нажатую кнопку со стороны клиента для OrderBox
	Y - true
	N - false
]]
function PlayerOrderBoxResponse(bool)
	triggerEvent("OrderBoxResponse", client, bool)	
end
addEvent("PlayerOrderBoxResponse", true)
addEventHandler("PlayerOrderBoxResponse", root, PlayerOrderBoxResponse)