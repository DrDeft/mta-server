local ProgressBars = {}

function createProgressBar(barid, x,y,sx,sy, relative)
	ProgressBars[barid] = {}
	ProgressBars[barid].GUI = {
		staticimage = {}
	}
	CreateGUI(barid, x,y,sx,sy, relative)
	VievProgressBar(barid, true)
	return barid
end
addEvent("createProgressBar", true)
addEventHandler("createProgressBar", localPlayer, createProgressBar)

function CreateGUI(barid, x,y,sx,sy, relative)
	ProgressBars[barid].GUI.staticimage[1] = guiCreateStaticImage(x, y, sx, sy, ":FiveRP/loginpanel/images/bottom.png", relative)
	guiSetProperty(ProgressBars[barid].GUI.staticimage[1], "ImageColours", "tl:FF010000 tr:FF010000 bl:FF010000 br:FF010000")
	ProgressBars[barid].GUI.staticimage[2] = guiCreateStaticImage(0.03, 0.27, 0.94, 0.45, ":FiveRP/loginpanel/images/pane.png", true, ProgressBars[barid].GUI.staticimage[1])
	ProgressBars[barid].GUI.staticimage[3] = guiCreateStaticImage(0.00, 0.00, 1.00, 1.00, ":FiveRP/loginpanel/images/pane.png", true, ProgressBars[barid].GUI.staticimage[2])
	guiSetProperty(ProgressBars[barid].GUI.staticimage[2], "ImageColours", "tl:FFc0c0c0 tr:FFc0c0c0 bl:FFc0c0c0 br:FFc0c0c0")
	for i in ipairs(ProgressBars[barid].GUI.staticimage) do
		guiSetVisible(ProgressBars[barid].GUI.staticimage[i], false)
	end
end

function VievProgressBar(barid, bool)
	for i in ipairs(ProgressBars[barid].GUI.staticimage) do
		guiSetVisible(ProgressBars[barid].GUI.staticimage[i], bool)
	end
end
addEvent("VievProgressBar", true)
addEventHandler("VievProgressBar", localPlayer, VievProgressBar)

function setProgressBarSize(barid, width, height, relative)
	guiSetSize(ProgressBars[barid].GUI.staticimage[1], width, height, relative)
	guiSetSize(ProgressBars[barid].GUI.staticimage[2], 1.00, 1.00, true)
	guiSetSize(ProgressBars[barid].GUI.staticimage[3], 1.00, 1.00, true)
end
addEvent("setProgressBarSize", true)
addEventHandler("setProgressBarSize", localPlayer, setProgressBarSize)

function setProgressBarPosition(barid, x, y, relative)
	guiSetPosition (ProgressBars[barid].GUI.staticimage[1], x, y,relative)
end
addEvent("setProgressBarPosition", true)
addEventHandler("setProgressBarPosition", localPlayer, setProgressBarPosition)

function setProgressBarColor(barid, color, background)
	guiSetProperty(ProgressBars[barid].GUI.staticimage[2], "ImageColours", "tl:"..background.." tr:"..background.." bl:"..background.." br:"..background.."")
	guiSetProperty(ProgressBars[barid].GUI.staticimage[3], "ImageColours", "tl:"..color.." tr:"..color.." bl:"..color.." br:"..color.."")
end
addEvent("setProgressBarColor", true)
addEventHandler("setProgressBarColor", localPlayer, setProgressBarColor)

function setProgressBarProgress(barid, Progress)
	if Progress < 0 then Progress = 0 end
	if Progress > 100 then Progress = 100 end
	local width = Progress * 0.01
	guiSetSize(ProgressBars[barid].GUI.staticimage[3], width, 1.00, true)
end
addEvent("setProgressBarProgress", true)
addEventHandler("setProgressBarProgress", localPlayer, setProgressBarProgress)

function deleteProgressBar(barid)
	barid = tonumber(barid)
	destroyElement(ProgressBars[barid].GUI.staticimage[1])
	ProgressBars[barid] = nil
end
addEvent("deleteProgressBar", true)
addEventHandler("deleteProgressBar", localPlayer, deleteProgressBar)