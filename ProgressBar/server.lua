local ProgressBars = {}


function createProgressBar(player, x, y, sx, sy, relative, color1, color2)
	local playerid = getElementData(player, "player.id")
	local barid = getFreeProgressBarID(player)
	ProgressBars[playerid][barid] = 0
	triggerClientEvent(player, "createProgressBar", player, barid, x, y, sx, sy, relative, color1, color2)
	return barid
end

function getFreeProgressBarID(player)
	local playerid = getElementData(player, "player.id")
	if ProgressBars[playerid] == nil then
		ProgressBars[playerid] = {}
		return 1;
	else
		local t = false
		for i in ipairs(ProgressBars[playerid]) do
			if ProgressBars[playerid][i] == nil then
				t = true
				return i 
			end
			break
		end
		if t == false then
			return #ProgressBars[playerid]+1
		end	
	end
end

function deleteProgressBar(player, barid)
	local playerid = getElementData(player, "player.id")
	if ProgressBars[playerid] ~= nil then
		ProgressBars[playerid][barid] = nil
	end
	triggerClientEvent(player, "deleteProgressBar", player, barid)
end

function setProgressBarPosition(player, barid, x, y, relative)
	triggerClientEvent(player, "setProgressBarPosition", player, barid, x, y, relative)
end

function setProgressBarColor(player, barid,color, background)
	triggerClientEvent(player, "setProgressBarColor", player, barid, color, background)
end

function setProgressBarProgress(player, barid,progress)
	triggerClientEvent(player, "setProgressBarProgress", player, barid, progress)
end

function quitPlayer()
	local playerid = getElementData(source, "player.id")
	if ProgressBars[playerid] ~= nil then
		ProgressBars[playerid] = nil
	end
end
addEventHandler ( "onPlayerQuit", getRootElement(), quitPlayer )