function guiCreateBoxEditOld(x, y, w1, h1, text, type, ...)
	local Edit = guiCreateEdit(x, y, w1, h1,text, type, ...)
	local Enbl = {}
	if type then
		Enbl[1] = guiCreateStaticImage(0.00, 0.9, 1, 0.15, pane, true, Edit) --Изображение снизу эдит-поля (pane - это переменная изображения)
		Enbl[2] = guiCreateStaticImage(0.00, 0.00, 1, 0.15, pane, true, Edit) --Изображение сверху эдит-поля (pane - это переменная изображения)
		Enbl[3] = guiCreateStaticImage(0.96, 0, 0.04, 1, pane, true, Edit) --Изображение справа эдит-поля (pane - это переменная изображения)
		Enbl[4] = guiCreateStaticImage(0.00, 0.0, 0.04, 1, pane, true, Edit) --Изображение слева эдит-поля (pane - это переменная изображения)
	
	else
		Enbl[1] = guiCreateStaticImage(0, 0, w1, 5, pane, false, Edit) --Изображение сверху эдит-поля (pane - это переменная изображения)
		Enbl[2] = guiCreateStaticImage(0, 0, 3, h1, pane, false, Edit) --Изображение слева эдит-поля (pane - это переменная изображения)
		Enbl[3] = guiCreateStaticImage(w1-3, 0, 3, h1, pane, false, Edit) --Изображение справа эдит-поля (pane - это переменная изображения)
		Enbl[4] = guiCreateStaticImage(0, h1-3, w1, 3, pane, false, Edit) --Изображение снизу эдит-поля (pane - это переменная изображения)
	end
	for i = 1, 4 do
		guiSetEnabled(Enbl[i], false)
	end
	guiBringToFront (Edit)
	return Edit
end

function guiCreateBoxEditOld(x, y, w1, h1, text, type, ...)