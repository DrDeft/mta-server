local pickuplist = xmlLoadFile ( "pickup_list.xml" )
local _pickups = 0
local PICKUPS = {}


function LoadPickupData(res)
	if res ~= getThisResource() then return 1 end
	while xmlFindChild ( pickuplist, "pickup", _pickups ) ~= false do
		local data = xmlNodeGetAttributes ( xmlFindChild ( pickuplist, "pickup", _pickups ) )
		PICKUPS[_pickups+1] = {id, epickup, icon = data["icon"], lock = tonumber(data["lock"]), text = data["text"], fraction = data["fraction"], lrank = data["lrank"], outworld = data["outworld"], outint= data["outint"], outpos = {}, inworld = data["inworld"], inint = data["inint"], inpos = {}}
		local inpos = exports.FiveRP:explode ( "|", data["inpos"] )
		local outpos = exports.FiveRP:explode ( "|", data["outpos"] )
		for i = 1, 4 do
			PICKUPS[_pickups+1]["outpos"][i] = tonumber(outpos[i])
			PICKUPS[_pickups+1]["inpos"][i] = tonumber(inpos[i])
		end
		PICKUPS[_pickups+1]["inworld"] = tonumber(PICKUPS[_pickups+1]["inworld"])
		PICKUPS[_pickups+1]["inint"] = tonumber(PICKUPS[_pickups+1]["inint"])
		PICKUPS[_pickups+1]["outworld"] = tonumber(PICKUPS[_pickups+1]["outworld"])
		PICKUPS[_pickups+1]["outint"] = tonumber(PICKUPS[_pickups+1]["outint"])
		
		_pickups = _pickups +1
	end
	xmlUnloadFile ( pickuplist )   
	CreatePickups()

end
addEventHandler ( "onResourceStart", getRootElement(), LoadPickupData )

function SavePickupData(pickupid)
	pickuplist = xmlLoadFile ( "pickup_list.xml" )  
	--if pickupid == nil then
		for i in ipairs(PICKUPS) do
			local node = xmlFindChild ( pickuplist, "pickup", i-1 )
			xmlNodeSetAttribute(node, "icon", PICKUPS[i]["icon"])
			xmlNodeSetAttribute(node, "lock", PICKUPS[i]["lock"])
			xmlNodeSetAttribute(node, "text", PICKUPS[i]["text"])
			xmlNodeSetAttribute(node, "fraction", PICKUPS[i]["fraction"])
			xmlNodeSetAttribute(node, "lrank", PICKUPS[i]["lrank"])
			xmlNodeSetAttribute(node, "outworld", PICKUPS[i]["outworld"])
			xmlNodeSetAttribute(node, "outint", PICKUPS[i]["outint"])
			xmlNodeSetAttribute(node, "outpos", PICKUPS[i]["outpos"][1] .. "|" .. PICKUPS[i]["outpos"][2] .. "|" .. PICKUPS[i]["outpos"][3].."|".. PICKUPS[i]["outpos"][4])
			xmlNodeSetAttribute(node, "inworld", PICKUPS[i]["inworld"])
			xmlNodeSetAttribute(node, "inint", PICKUPS[i]["inint"])
			xmlNodeSetAttribute(node, "inpos", PICKUPS[i]["inpos"][1] .. "|" .. PICKUPS[i]["inpos"][2] .. "|" .. PICKUPS[i]["inpos"][3].."|"..PICKUPS[i]["inpos"][4])
		end
		outputDebugString("Пикапы успешно сохранены!")
	--end	
	xmlSaveFile(pickuplist)
	xmlUnloadFile(pickuplist)  
end
function StopPickupResource(resource)
	if resource == getThisResource ( ) then SavePickupData() end
end

addEventHandler ( "onResourceStop", getRootElement(), StopPickupResource )

function CreatePickups()
	for i in ipairs(PICKUPS) do
		--------------Вход в здание----------------------------
		PICKUPS[i]["id"] = createPickup(PICKUPS[i]["inpos"][1], PICKUPS[i]["inpos"][2], PICKUPS[i]["inpos"][3], 3, PICKUPS[i]["icon"], 0)
		setElementDimension(PICKUPS[i]["id"], PICKUPS[i]["inworld"])
		if PICKUPS[i]["inint"] > 20000 then -- Кастомные интерьеры в 200 инте
			setElementInterior(PICKUPS[i]["id"], 200)
		else
			setElementInterior(PICKUPS[i]["id"], PICKUPS[i]["inint"])
		end
		setElementData(PICKUPS[i]["id"], "pickup.id", i+10000)
		
		-------------Выход из здания---------------------------
		PICKUPS[i]["epickup"] = createPickup(PICKUPS[i]["outpos"][1], PICKUPS[i]["outpos"][2], PICKUPS[i]["outpos"][3], 3, PICKUPS[i]["icon"], 0)
		setElementDimension(PICKUPS[i]["epickup"], PICKUPS[i]["outworld"])
		if PICKUPS[i]["outint"] > 20000 then
			setElementInterior(PICKUPS[i]["epickup"], 200)
		else	
			setElementInterior(PICKUPS[i]["epickup"], PICKUPS[i]["outint"])
		end	
		setElementData(PICKUPS[i]["epickup"], "pickup.id", i+10000)
	
	end
	outputDebugString("Загружено: "..#PICKUPS.." пикапов")
end

function OnPlayerEnterPickup(pickup)
	if getPedOccupiedVehicle(source) then return 1 end
	if getElementData(pickup, "pickup.id") == false or getElementData(pickup, "pickup.id") < 10000 or getElementData(pickup, "pickup.id") > 10000+#PICKUPS then return 1 end
	pickupid = getElementData(pickup, "pickup.id") - 10000
	if PICKUPS[pickupid]["lock"] == 1 then return 1 end -- Добавить нажпись (Закрыто)
	if PICKUPS[pickupid]["id"] == pickup then
		setElementPosition (source, PICKUPS[pickupid]["outpos"][1], PICKUPS[pickupid]["outpos"][2], PICKUPS[pickupid]["outpos"][3] )
		setElementRotation (source, 0, 0, PICKUPS[pickupid]["outpos"][4], "default",true)  		
		exports.FiveRP:setPlayerInterior(source, PICKUPS[pickupid]["outint"])
		exports.FiveRP:setPlayerDimension(source, PICKUPS[pickupid]["outworld"])
	elseif pickup == PICKUPS[pickupid]["epickup"] then
		setElementPosition (source, PICKUPS[pickupid]["inpos"][1], PICKUPS[pickupid]["inpos"][2], PICKUPS[pickupid]["inpos"][3] ) 
		setElementRotation (source, 0, 0, PICKUPS[pickupid]["inpos"][4], "default",true)  
		exports.FiveRP:setPlayerInterior(source, PICKUPS[pickupid]["inint"])
		exports.FiveRP:setPlayerDimension(source, PICKUPS[pickupid]["inworld"])
		--Это выход из здания. Все ОК
	end
	setCameraTarget(source)
end
addEventHandler("onPlayerPickupHit",getRootElement(),OnPlayerEnterPickup)
