local FontSpeed = dxCreateFont(":speedometer/fonts/speed.ttf", 65)
local FontFuel = dxCreateFont("/fonts/speed.ttf", 30)
local CarParams = {fuel = 25, engine = 1, belt = 2, headlight = 2}
local vehicle
local speed = 0
local speedTimer
local distance = 0
local screenW, screenH = guiGetScreenSize()

local Colors = {tocolor(255, 255, 255, 255), tocolor(240, 238, 238, 33), tocolor(255, 0, 0, 255), tocolor(255, 165, 0, 255)}


function DrawingSpeed()
		dxDrawText("000", screenW-436, screenH-139, screenW-268, _, tocolor(240, 238, 238, 33), 1.00, FontSpeed, "right", "top", false, false, false, false, false)
		dxDrawText(speed, screenW-436, screenH-139, screenW-268, _, tocolor(255, 255, 255, 255), 1.00, FontSpeed, "right", "top", false, false, false, false, false)
        dxDrawImage(screenW-208, screenH-121, 32, 32, "/icons/engine.png", 0, 0, 0, Colors[CarParams["engine"]], false)
        dxDrawImage(screenW-240, screenH-74, 32, 32, "/icons/fuel.png", 0, 0, 0, tocolor(255, 255, 255, 255), false)
        dxDrawImage(screenW-250, screenH-121, 32, 32, "/icons/light.png", 0, 0, 0, Colors[CarParams["headlight"]], false)
        dxDrawImage(screenW-166, screenH-121, 32, 32, "/icons/belt.png", 0, 0, 0, Colors[CarParams["belt"]], false)
        dxDrawText(CarParams["fuel"], screenW-198, screenH-85, 1802, 1028, tocolor(255, 255, 255, 255), 1.00, FontFuel, "left", "top", false, false, false, false, false)
end

function updateCarFuel(fuel)
	CarParams["fuel"] = math.ceil(fuel)
	if fuel < 0 then
		CarParams["fuel"] = 0
	end
	--CarParams["fuel"] = math.ceil(CarParams["fuel"])
end
addEvent("updateCarFuel", true)
addEventHandler("updateCarFuel", localPlayer, updateCarFuel)

function updateCarLight(headlight)
	CarParams["headlight"] = headlight
end
addEvent("updateCarLight", true)
addEventHandler("updateCarLight", localPlayer, updateCarLight)

function updateCarEngine(engine)
	CarParams["engine"] = engine
end
addEvent("updateCarEngine", true)
addEventHandler("updateCarEngine", localPlayer, updateCarEngine)

function updateCarBelt(belt)
	if belt == 0 then
		CarParams["belt"] = 3
	else
		CarParams["belt"] = 2
	end
end
addEvent("updateCarBelt", true)
addEventHandler("updateCarBelt", localPlayer, updateCarBelt)

function vievSpeedometer(bool, fuel, engine, belt, headlight)
	if bool then
		CarParams["fuel"] = math.ceil(fuel)
		CarParams["engine"] = engine
		CarParams["belt"] = belt
		CarParams["headlight"] = headlight
		vehicle = getPedOccupiedVehicle(localPlayer)
		speedTimer = setTimer (SpeedTimer, 150, 0)
		addEventHandler ( "onClientRender", root, DrawingSpeed)
	else
		removeEventHandler("onClientRender", root, DrawingSpeed)
		if isTimer(speedTimer) then 
			killTimer(speedTimer)
		end	
		speed = 0
	end
end
addEvent("vievSpeedometer", true)
addEventHandler("vievSpeedometer", localPlayer, vievSpeedometer)

--[[addEventHandler("onClientVehicleEnter", root,
	function(thePlayer)
		if thePlayer == localPlayer then
			vievSpeedometer(true, 38, 1, 2, 2)
			speedTimer = setTimer (SpeedTimer, 150, 0)
		end
	end
)]]

function SpeedTimer()
	speed = math.floor((Vector3(getElementVelocity(vehicle)) * 195).length) --118.84681456
	--if ts < 10 then speed = "00"..ts..""
	--elseif ts < 100 then speed = "0"..ts..""
	--else speed = ts
	--end
end