function vievSpeedometer(player, bool, fuel, engine, belt, headlight)
	if engine == 0 then engine = 2 end
	if belt == 0 then belt = 2 end
	if headlight == 0 then headlight = 2 end
	triggerClientEvent(player, "vievSpeedometer", player, bool, fuel, engine, belt, headlight)
end

function updateCarFuel(player, fuel)
	triggerClientEvent(player, "updateCarFuel", player, fuel)
end

function updateCarEngine(player, engine)
	if engine == 0 then engine = 2 end
	triggerClientEvent(player, "updateCarEngine", player, engine)
end

function updateCarBelt(player, belt)
	triggerClientEvent(player, "updateCarBelt", player, belt)
end

function updateCarLight(player, light)
	triggerClientEvent(player, "updateCarLight", player, light)
end	